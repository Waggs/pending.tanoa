//--- SKL_DebugSetup.sqf 
//--- add to init.sqf:  [] execVM "special\SKL_DebugSetup.sqf" 

_enabled   = true;						// Enable DebugMenu - Allow ParamsArray to handle this or set this to false to override 
SKL_DEBUG_ADMINS = ["NoAdminAllowed"];	// Admin names - Allow ParamsArray to handle this or add player names here to override 
_admins    = SKL_DEBUG_ADMINS; 
_author    = false;						// Author only - Allow ParamsArray to handle this or set this to true to always allow author override
_allowAll  = false;						// Allow everyone to use DebugMenu - Allow ParamsArray to handle this or set to true to override 

//--- Below settings will be adjustable by ParamsArray
if (["SklDebug"] call BIS_fnc_getParamValue == 0) then { _enabled = false }; 

if (["SklDebug"] call BIS_fnc_getParamValue == 1) then { 
    //--- Add player names below here to allow admin access
    SKL_DEBUG_ADMINS = ["DTRAX","PapaReap","Waggs"]; 
    _admins = SKL_DEBUG_ADMINS; 
    _author = true; 
}; 

if (["SklDebug"] call BIS_fnc_getParamValue == 2) then { _author = true }; 

if (["SklDebug",0] call BIS_fnc_getParamValue == 3) then { _allowAll = true }; 

SKL_DebugArray = [_enabled, _admins, _author, _allowAll]; 

//--- Run Skull's Debug Menu 
[] execVM "scripts\SKULL\DebugMenu\SKL_DebugMenu.sqf"; 