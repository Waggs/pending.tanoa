/* fn_moveInVehTimer.sqf 
*  Author: PapaReap 

*  usage: [] call compile preprocessFileLineNumbers "special\fn_moveInVehTimer.sqf"; 
*  this, c130_1] call fnc_moveInVehicle; 
*  [this, c130_1, 1] call fnc_moveInVehicle; 
*/


if (isServer) then { if (isNil "isServerTimerComplete") then { isServerTimerComplete = "false"; publicVariable "isServerTimerComplete"; }; }; 
if !(isNil "playerTimesUp") exitWith {}; 

fnc_moveInCargo = { 
    if !(pr_deBug == 0) then { diag_log format ["*PR* fnc_moveInCargo start"]; }; 
    _unit = _this select 0; 
    _pos = _this select 1; 
    _veh = _this select 2; 

    if (count _this > 3) then { 
        _seat = _this select 3; 
        if (_pos == "cargo") then { 
            _unit moveInCargo [_veh, _seat]; 
        } else { 
            if (_pos == "Turret") then { 
                _unit moveInTurret [_veh, _seat]; 
            }; 
        }; 
    } else { 
        if (_pos == "cargo") then { 
            _unit moveInCargo _veh; 
        } else { 
            if (_pos == "Driver") then { 
                _unit moveInDriver _veh; 
            } else { 
                if (_pos == "Gunner") then { 
                    _unit moveInGunner _veh; 
                } else { 
                    if (_pos == "Commander") then { 
                        _unit moveInCommander _veh; 
                    }; 
                }; 
            }; 
        }; 
    }; 
}; 

fnc_moveInCargoInit = { 
    if !(pr_deBug == 0) then { diag_log format ["*PR* fnc_moveInCargoInit start"]; }; 
    if (!isDedicated) then { 
        waitUntil { !isNull Player }; 
        if (local player) then { 
            waitUntil { time > 5 }; 
            //waitUntil { !(isNil "c130_1") }; 
            if (isServerTimerComplete == "true") exitWith {}; 
            switch (format ["%1", player]) do { 
                case "p1":{ [player, "cargo", c130_1, 1] call fnc_moveInCargo }; 
                case "p2":{ [player, "cargo", c130_1, 2] call fnc_moveInCargo }; 
                case "p3":{ [player, "cargo", c130_1, 3] call fnc_moveInCargo }; 
                case "p4":{ [player, "cargo", c130_1, 4] call fnc_moveInCargo }; 
                case "p5":{ [player, "cargo", c130_1, 5] call fnc_moveInCargo }; 
                case "LeadMedic":{ [player, "cargo", c130_1, 6] call fnc_moveInCargo }; 

                case "p7":{ [player, "cargo", c130_1, 7] call fnc_moveInCargo }; 
                case "p8":{ [player, "cargo", c130_1, 8] call fnc_moveInCargo }; 
                case "p9":{ [player, "cargo", c130_1, 9] call fnc_moveInCargo }; 
                case "p10":{ [player, "cargo", c130_1, 10] call fnc_moveInCargo }; 
                case "p11":{ [player, "cargo", c130_1, 11] call fnc_moveInCargo }; 
                case "p12":{ [player, "cargo", c130_1, 12] call fnc_moveInCargo }; 

                case "p13":{ [player, "cargo", c130_1, 13] call fnc_moveInCargo }; 
                case "p14":{ [player, "cargo", c130_1, 14] call fnc_moveInCargo }; 
                case "p15":{ [player, "cargo", c130_1, 15] call fnc_moveInCargo }; 
                case "p16":{ [player, "cargo", c130_1, 16] call fnc_moveInCargo }; 

                //case "p17":{ [player, "cargo", c130_1, 17] call fnc_moveInCargo }; 
                //case "p18":{ [player, "cargo", c130_1, 18] call fnc_moveInCargo }; 
            }; 
            if !(pr_deBug == 0) then { diag_log format ["*PR* fnc_moveInCargoInit finish"]; }; 
        }; 
    }; 
}; 

[] spawn fnc_moveInCargoInit; 

if !(isServer) exitWith {}; 

aliveServerTimer = { 
/* usage in trigger to set alive persistance 
   cond:  !(isServerTimerComplete == "true")
   onAct: if (isServer && aliveOn) then { ["serverTimerComplete", "true"] call ALiVE_fnc_setData; }; 
*/ 
    //if !(isServer) exitWith {}; 
    if (isNil "isServerTimerComplete") then { isServerTimerComplete = "false"; }; 

    if ((isDedicated) && (aliveOn) && (PersistTasks)) then { 
        waitUntil { (!isnil "alive_sys_player") }; 

        if (!isNil {["serverTimerComplete"] call ALiVE_fnc_getData}) then { isServerTimerComplete = ["serverTimerComplete"] call ALiVE_fnc_getData; }; 
        publicVariable "isServerTimerComplete"; diag_log format ["*PR* isServerTimerComplete, %1", isServerTimerComplete]; 
    } else { 
        publicVariable "isServerTimerComplete"; diag_log format ["*PR* isServerTimerComplete, %1", isServerTimerComplete]; 
    }; 

    if (isNil "moveHold") then { moveHold = false; publicVariable "moveHold"; }; 
}; 

_this spawn aliveServerTimer; 

serverTimer = { 
    //if !(isServer) exitWith {}; 
    _time = 120; 
    _placeTime = floor (serverTime); 
    _timeToComplete = _placeTime + _time; server_timeToComplete = _timeToComplete; 

    while { (floor (serverTime) < _timeToComplete) } do { sleep 1; }; 
    playerTimesUp = true; publicVariable "playerTimesUp"; 
}; 

if (isNil "serverTimesUp") then { 
    serverTimesUp = true; publicVariable "serverTimesUp"; 
    _this spawn serverTimer; 
}; 