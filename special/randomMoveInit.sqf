/* randomMoveInit.sqf
*  Author: PapaReap
*
*  RANDOM MOVER
*  Random Movers: Can be any object, marker, logic, trigger. Can use as many as you like.
*  Trigger Usage: [crash, [crash_1,crash_2,crash_3,crash_4], [hWreck,pilot1,pilot2,task1succeeded,task1Damage,"area2"]] remoteExec ["pr_fnc_randomMove", 0, false]; // testing required
*  Usage: Separate by commas i.e. - randomMover = [<REFERENCE POINT>, [<RANDOM POSITIONS TO MOVE TO>], [<OBJECTS TO BE MOVED>]];
*  Arguments:
*  0: <REFERENCE POINT>              - Name of object or position to get reference to movable objects, use only one. Can be any object, marker, logic, trigger, positions, etc...   - (variable name, [position])
*  1: <RANDOM POSITIONS TO MOVE TO>  - Object names to use for random placement, any amount can be used. Can be any object, marker, logic, trigger, positions, etc...   - (variable name, [position])
*  2: <OBJECTS TO BE MOVED>          - Objects to be moved, any amount can be used.  - (variable name)  note: not position
*  ver 1.0 2016-05-25
*/


randomMover = [
    [
/*  <0> */  crash,
/*  <1> */  [crash_1, crash_2, crash_3, crash_4],
/*  <2> */  [hWreck, pilot1, pilot2, task1succeeded, task1Damage, "area2"]
    ]
/* un-comment lines below to add more random movers, can copy paste format above, replace names */
//   ,[
//          <enter more random movers here>
//    ]
];




/*************************************** DO NOT EDIT BELOW ***************************************/

if !(isServer) exitWith {};

waitUntil { time > 0 };

if (isNil "randomMove") then { randomMove = true; };
for [{ _loop = 0 }, { _loop < count randomMover }, { _loop = _loop + 1 }] do {
    waitUntil { randomMove };
    randomMove = false;
    _reference = (randomMover select _loop) select 0;
    _movePos = (randomMover select _loop) select 1;
    _moveable = (randomMover select _loop) select 2;
    [_reference, _movePos, _moveable] remoteExec ["pr_fnc_randomMove", 0, false]; // check local server moving instead
};

//[crash, [crash_1,crash_2,crash_3,crash_4], [hWreck,pilot1,pilot2,task1succeeded,task1Damage,"area2"]] remoteExec ["pr_fnc_randomMove", 0, false];
//[[crash, [crash_1,crash_2,crash_3,crash_4], [hWreck,pilot1,pilot2,task1succeeded,task1Damage,"area2"]], "pr_fnc_randomMove", true, false] call BIS_fnc_MP;