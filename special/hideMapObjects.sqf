/* hideMapObjects.sqf
*  Author: PapaReap
*
*  Trigger Usage: [_position, _radius, _percent, _blacklist] remoteExec ["pr_fnc_hideMapObjects", 0, false];
*  Usage: Separate by commas i.e. - clearMapObject = [POSITION, RADIUS, PERCENTAGE TO REMOVE, [BLACKLIST]];
*  Arguments:
*  0: <POSITION>              - Name of object or position to get reference to movable objects, use only one. Can be any object, marker, logic, trigger, positions, etc...   - (variable name, [position])
*  1: <RADIUS>                - Radius to remove objects
*  2: <PERCENTAGE TO REMOVE>  - Percentage to remove, between 0-100
*  3: <BLACKLIST>
*
*  Ver 1.0 2016-08-09
*/

clearMapObjects = [
    [
/*  <0> */  clear, // position
/*  <1> */  200, // radius
/*  <2> */  100, // random percentage
/*  <3> */  [] // blacklist
    ]
/* un-comment lines below to add more random movers, can copy paste format above, replace names */
   ,[ clear_1, 200, 100, [] ]
   ,[ clear_2, 200, 100, [] ]
   ,[ clear_3, 200, 100, [] ]
   ,[ clear_4, 200, 100, [] ]
   ,[ clear_5, 200, 100, [] ]
   ,[ clear_6, 200, 100, [] ]
];




/*************************************** DO NOT EDIT BELOW ***************************************/

if !(isServer) exitWith {};

if (isNil "clearMapObject") then { clearMapObject = true; };
for [{ _loop = 0 }, { _loop < count clearMapObjects }, { _loop = _loop + 1 }] do {
    waitUntil { clearMapObject };
    clearMapObject = false;
    _position = (clearMapObjects select _loop) select 0;
    _radius = (clearMapObjects select _loop) select 1;
    _percent = (clearMapObjects select _loop) select 2;
    _blacklist = (clearMapObjects select _loop) select 3;
    [_position, _radius, _percent, _blacklist] remoteExec ["pr_fnc_hideMapObjects", 0, false];
};
