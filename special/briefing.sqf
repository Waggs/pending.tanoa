
//<font color='#F8C701'><marker name='gpg'>Gulf Playa Gyron.</marker></font><br/><br/> 

[
    west,
        ["Situation",
            "Kalochori is being over ran by Russian Forces.<br/><br/>While their time there has been short, they are starting to get a foot hold on the area rather quickly"],
        ["Mission",
            "Break the stronghold the Russians have on the south eastern side of Kalochori, take the town back.<br/><br/>Destroy any static weapon sites you come across."],
        ["Execution",
            "Alpha and Bravo team shall approach from the south east by <font color='#F8C701'><marker name='behere'>sea.</marker></font><br/>You will be dropped off on the <font color='#F8C701'><marker name='here'>shoreline,</marker></font> where you shall start your ingress to Kalochori.<br/><br/>Expect some resistance.<br/><br/>Charlie and Delta shall provide ground and air support from this <font color='#F8C701'><marker name='cd'>location.</marker></font><br/>"],
        ["Exfiltration",
            "There will be no exfiltration, once you have control of Kalochori, you will wait and hold your position at the main church."]
] call FHQ_fnc_ttAddBriefing;



