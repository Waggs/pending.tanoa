

if !(isServer) exitWith {};

timerData = [
    60,   // Desired time: Desired mission run time with the below mission player base
    90,   // Max Time: maximum mission time, in minutes
    45,   // Min Time: minimum mission run time, in minutes

    true,  // Allow scaling: allow time scaling for playable units
    6,     // Player baseLine: baseline for maximum time based on playable units
    1     // Time adjustment: time to add or subtract per player from maximum time, in minutes
];



/*************************************** DO NOT EDIT BELOW ***************************************/

waitUntil { time > 0 };

missionTimer = {
    _time = timerData select 0;
    _maxTime = timerData select 1;
    _minTime = timerData select 2;
    _allowScaling = timerData select 3;
    _baseLine = timerData select 4;
    _timeAdjust = timerData select 5;
    _multiplier = 0;

    _unitCount = count playableUnits;
    _time = _time * 60;
    _maxTime = _maxTime * 60;
    _minTime = _minTime * 60;
    _timeAdjust = _timeAdjust * 60;

    if (_allowScaling) then {
        if (_unitCount < _baseLine) then {
            _multiplier = _baseLine - _unitCount;
            _maxTime = _maxTime + (_timeAdjust * _multiplier);
        } else {
            if (_unitCount > _baseLine) then {
                _multiplier = _unitCount - _baseLine;
                _maxTime = _maxTime - (_timeAdjust * _multiplier);
            };
        };
    };

    _maxTime = (_maxTime) max _minTime;
    pr_maxTime = _maxTime;
    _placeTime = floor (serverTime);
    _timeToComplete = _placeTime + _maxTime; server_missionTimer = _timeToComplete;

    while { (floor (serverTime) < _timeToComplete) } do { sleep 1; };
    mission_Over = true; publicVariable "mission_Over";
};

if (isNil "missionTimerInit") then {
    missionTimerInit = true;
    _this spawn missionTimer;
};