if (isServer) then
{
//	Hint "Wait until the Vehicle Ammo Box will be loaded on HEMTT";
	waitUntil {!(isNil "IL_Do_Load") && !(isNil "IL_Init_Veh")}; //Wait for IgiLoad
//	sleep 3; //For hint only
	
	
	[mh, true] call IL_Init_Veh; //Init vehicle
	0 = [mh, [typeOf offroad], "B", true, offroad] spawn IL_Do_Load; //Do load on HEMTT
	sleep .5;	
//	[mh, true] call IL_Init_Veh; //Init vehicle	
//	0 = [mh, [typeOf staticBox], "B", true, staticBox] spawn IL_Do_Load; //Do load on Mohawk
//	sleep 5;
};
