/*
*  Author: PapaReap
*  Change the loadout as required by your mission
*  Ver 1.0 2014-02-13 initial release
*  Ver 1.1 2015-01-15 allow for agm
*  Ver 1.2 2015-04-28 allow for ace
*  Ver 1.3 2015-11-26 restructure loadout calls
*  Ver 1.4 2016-02-18 add diver to the mix, code optimization
*
*  Available loadouts
*  [Squad_leader, Team_leader, MG_Light, MG_Heavy, Grenadier_light, Grenadier_Heavy, AT, AT_specialist, AA_light, AA_heavy, Engineer, Medic, Jtac, AmmoBearerMag, Marksman, Sniper, LadderCarrier]
*  EXAMPLE: case "p1":{ [player] call Squad_leader; };
*
*  To assign a Ace Vehicle key to player the following is needed
*  Arguments:
*  0: <PLAYER>        e.g. - (REQUIRED) player
*  1: <NAME VEHICLE>  e.g. - (REQUIRED) name of vehicle to assign key to
*  2: <ALLOW>         e.g. - (REQUIRED) bool, true or false
*  3: <LOCK VEHICLE>  e.g. - (REQUIRED) bool, true or false
*  EXAMPLE: case "p1":{ [player] call Squad_leader; if (aceOn) then { [player, sl_car, true, true] call fncAceKey; }; };
*
*  To equip player with diver gear
*  Available optional loadouts: "SL", "TL", "MGL", "MGH", "GLL", "GLH", "AT", "ATS", "AAL", "AAH", "ENG", "MED", "JTAC", "ABM", "MRK", "SNIP", "LAD"
*  Arguments:
*  0: <PLAYER>        e.g. - (REQUIRED) player
*  1: <WEAPON CLASS>  e.g. - (OPTIONAL) string, enter one of the strings above to give diver extended loadouts: ("SL" would be simular to using (call Squad_leader))
*  EXAMPLES:
*  Default diver:              case "p1":{ [player] call fn_diver; };
*  Diver with extended gear:   case "p1":{ [player,"SL"] call fn_diver;
*  Diver with Ace vehicle key: case "p1":{ [player] call fn_diver; if (aceOn) then { [player, sl_boat, true, true] call fncAceKey; }; };
*/

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_Client start"]; };

if (aceOn) then {
    fncAceKey = {
        /*   Usage:
            [player, name of car, add key to player, lock car on mission start] call fncAceKey;
            [player,     repair1,              true,                      true] call fncAceKey;
        */
        private ["_unit","_car","_allow","_lockState"];
        _unit      = _this select 0;
        _car       = _this select 1;
        _allow     = _this select 2;
        _lockState = _this select 3;
        [_car, _lockState] call ACE_VehicleLock_fnc_setVehicleLockEH;
        [_unit, _car, _allow] call ACE_VehicleLock_fnc_addKeyForVehicle;
    };
};

/*
DESCRIPTION             - FUNCTION CALL                      DESCRIPTION                      - FUNCTION CALL
STANDARD SOLDIER                                             DIVERS
                                                             [Diver]                          - [player] call fn_diver
[Squad leader]          - [player] call Squad_leader         [Diver - Squad leader]           - [player,"SL"] call fn_diver
[Team leader]           - [player] call Team_leader          [Diver - Team leader]            - [player,"TL"] call fn_diver
[MachineGunner - Light] - [player] call MG_Light             [Diver - MachineGunner - Light]  - [player,"MGL"] call fn_diver
[MachineGunner - Heavy] - [player] call MG_Heavy             [Diver - MachineGunner - Heavy]  - [player,"MGH"] call fn_diver
[Grenadier - Light]     - [player] call Grenadier_light      [Diver - Grenadier - Light]      - [player,"GLL"] call fn_diver
[Grenadier - Heavy]     - [player] call Grenadier_Heavy      [Diver - Grenadier - Heavy]      - [player,"GLH"] call fn_diver
[AT]                    - [player] call AT                   [Diver - AT]                     - [player,"AT"] call fn_diver
[AT - Specialist]       - [player] call AT_specialist        [Diver - AT - Specialist]        - [player,"ATS"] call fn_diver
[AA - Light]            - [player] call AA_light             [Diver - AA - Light]             - [player,"AAL"] call fn_diver
[AA - Heavy]            - [player] call AA_Heavy             [Diver - AA - Heavy]             - [player,"AAH"] call fn_diver
[Repair - Demo]         - [player] call Engineer             [Diver - Repair - Demo]          - [player,"ENG"] call fn_diver
[Medic]                 - [player] call Medic                [Diver - Medic]                  - [player,"MED"] call fn_diver
[JTAC]                  - [player] call Jtac                 [Diver - JTAC]                   - [player,"JTAC"] call fn_diver
[Ammo Bearer Mags]      - [player] call AmmoBearerMag        [Diver - Ammo Bearer Mags]       - [player,"ABM"] call fn_diver
[Marksman]              - [player] call Marksman             [Diver - Marksman]               - [player,"MRK"] call fn_diver
[Sniper]                - [player] call Sniper               [Diver - Sniper]                 - [player,"SNIP"] call fn_diver
[Ladder Carrier]        - [player] call LadderCarrier        [Diver - Ladder Carrier]         - [player,"LAD"] call fn_diver
*/

if (!isDedicated) then {
    waitUntil { ((!isNull player) && (gearCompiled)) };
    if (local player) then {

        waitUntil { time > 0 };
        [] call fnc_gear;
        /*switch (faction player) do {
            //case "BLU_G_F":{ [] call compile preprocessFile "scripts\CUSTOM\loadouts\PR_Fia.sqf"; };
            //case "IND_F":{ [] call compile preprocessFile "scripts\CUSTOM\loadouts\PR_Ind.sqf"; };
            case "CIV_F":{ [] call compile preProcessFile "scripts\PR\loadouts\units\civ\fn_civ.sqf"; };  //--- "Civialian Loadout"
        };*/
        switch (format ["%1",player]) do {
            case "p1":{ [player] call Squad_leader; /* if (aceOn) then { [player, sl_car, true, true] call fncAceKey; }; */};               // Alpha [Squad Leader]
            //case "p1":{ [player,"SL",true] call fn_diver; /* if (aceOn) then { [player, sl_car, true, true] call fncAceKey; }; */};               // Alpha [Squad Leader]
            //case "p1":{ [player,"SL"] call fn_diver; /* if (aceOn) then { [player, sl_car, true, true] call fncAceKey; }; */};               // Alpha [Squad Leader]
            //case "p1":{ [player] call fn_diver; /* if (aceOn) then { [player, sl_car, true, true] call fncAceKey; }; */};               // Alpha [Squad Leader]
            case "p2":{ [player] call MG_Light; };                        // Alpha [MachineGunner - Light]
            case "p3":{ [player] call Grenadier_light; };                 // Alpha [Grenadier - Light]
            case "p4":{ [player] call AT; };                              // Alpha [AT]
            //case "p4":{ [player,"AT"] call fn_diver; };                              // Alpha [AT]
            case "p5":{ [player] call Engineer; };                        // Alpha [Repair - Demo]
            case "LeadMedic":{ [player] call Medic; };                    // Alpha [Lead Medic]

            case "p7":{ [player] call Team_leader; };                     // Bravo [Team leader]
            case "p8":{ [player] call MG_Heavy; };                        // Bravo [MachineGunner - Heavy]
            case "p9":{ [player] call Grenadier_Heavy; };                 // Bravo [Grenadier Heavy]
            case "p10":{ [player] call AA_Heavy; };                       // Bravo [AA - Heavy]
            case "p11":{ [player] call AT_specialist; };                  // Bravo [AT - Specialist]
            case "p12":{ [player] call Medic; };                          // Bravo [Medic]

            case "p13":{ [player] call Team_leader; };                    // Charlie [Team Leader]
            case "p14":{ [player] call AmmoBearerMag; };                  // Charlie [Ammo Bearer]
            case "p15":{ [player] call Jtac; };                           // Charlie [JTAC]
            case "p16":{ [player] call Engineer; };                       // Charlie [Repair - Demo]

            case "p17":{ [player] call LeadPilot; };                      // Delta [Lead Pilot]
            case "p18":{ [player] call CoPilot; };                        // Delta [Pilot Medic]

            // Russian
            case "p19":{ [player] call Team_leader; };                    // Alpha [Team Leader]
            case "p20":{ [player] call MG_Light; };                       // Alpha [MachineGunner - Light]
            case "p21":{ [player] call Grenadier_light; };                // Alpha [Grenadier - Light]
            case "p22":{ [player] call AT; };                             // Alpha [AT]
            case "p23":{ [player] call Engineer; };                       // Alpha [Repair - Demo]
            case "p24":{ [player] call Medic; };                          // Alpha [Lead Medic]

            //case "p17":{ [player] call Pilot_Team_Leader; };              // Charlie [Lead Pilot]
            //case "p18":{ [player] call Pilot; };                          // Charlie [Pilot]
            //case "p19":{ [player] call Heli_Crew; };                      // Charlie [Crew]
            //case "p20":{ [player] call Heli_Crew; };                      // Charlie [Crew]

            case "civ1":{ [player] call War_Reporter; };                  // War Reporter
        };
        myCustomLoadout = true;
    };
};

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_Client complete"]; };

