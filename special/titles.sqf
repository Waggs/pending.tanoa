if (!isNil "letsStart") exitWith {};
titleCut ["", "BLACK FADED", 999];
sleep 5;
_date0 = (date select 0);
_date1 = (date select 1);
_date2 = (date select 2);
_date3 = (date select 3);
_date4 = (date select 4);

[ 
	[ 
		["South of Vostok Airbase"], 
		format ["%1.%2.%3",_date1,_date2,_date0], 
		format ["%1:%2hrs",_date3,_date4]
	] , .6, 0.75, "<t align = 'right' shadow = '1' size = '1.0'>%1</t>" 
] spawn BIS_fnc_typeText;
sleep 15;

[ 
	[ 
		["O P E R A T I O N"],
		["SNOW DOVE"],
		["Mission 3"]
	] , .6, 0.75, "<t align = 'right' shadow = '1' size = '1.0'>%1</t>" 
] spawn BIS_fnc_typeText;
sleep 10;
titleCut ["", "BLACK IN", 0];
letsStart = true; publicVariable "letsStart";