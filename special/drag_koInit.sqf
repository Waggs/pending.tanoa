/* drag_koInit.sqf
*  Author: PapaReap
*
*  RANDOM MOVER
*  Random Movers: Can be any object, marker, logic, trigger. Can use as many as you like.
*  Trigger Usage: [crash, [crash_1,crash_2,crash_3,crash_4], [hWreck,pilot1,pilot2,task1succeeded,task1Damage,"area2"]] remoteExec ["pr_fnc_randomMove", 0, false]; // testing required
*  Usage: Separate by commas i.e. - randomMover = [<REFERENCE POINT>, [<RANDOM POSITIONS TO MOVE TO>], [<OBJECTS TO BE MOVED>]];
*  Arguments:
*  0: <REFERENCE POINT>              - Name of object or position to get reference to movable objects, use only one. Can be any object, marker, logic, trigger, positions, etc...   - (variable name, [position])
*  1: <RANDOM POSITIONS TO MOVE TO>  - Object names to use for random placement, any amount can be used. Can be any object, marker, logic, trigger, positions, etc...   - (variable name, [position])
*  2: <OBJECTS TO BE MOVED>          - Objects to be moved, any amount can be used.  - (variable name)  note: not position
*  ver 1.0 2016-05-25
*/


_addDragToAll = true; //--- true to add drag to all units on the map, false to add units in array below
_noDragArray = [] + (units pg1) + (units pg2) + (units pg3) + (units pg4);  //--- Add to this array to remove drag from unit

//--- If ( _addDragToAll ) above is set to true, the array's below will be ignored

//--- Can use multibles of east, west, resistance, civilian.  If left blank no sides will be used
_dragSide = [east, civilian];

//--- add drag only to units in this array,
_dragArray = [joe, joe_1, joe_2, mark];







/*************************************** DO NOT EDIT BELOW ***************************************/

//[] call compile preprocessFileLineNumbers (prDRAGf + "fn_dragBody.sqf");  //--- Can drag dead units
[] execVM (prDRAGf + "fn_dragBody.sqf");

if !(isServer) exitWith {};
waitUntil { time > 5 };

_noDragArray = _noDragArray + [logic_HQ, logic_leader, logic_artillery, logic_firefly, logic_pigeon, pg1_ai, pg2_ai, pg3_ai, pg4_ai];

if (_addDragToAll) then {
    _dragArray = [];
    _dragArray = allUnits;
} else {
    if (count _dragSide > 0) then {
        for [{ _loop = 0 }, { _loop < count _dragSide }, { _loop = _loop + 1 }] do {
            _side = _dragSide select _loop;
            {
                if ((side _x) == _side) then {
                    if !(_x in _dragArray) then {
                        _dragArray = _dragArray + [_x];
                    };
                }; 
            } forEach allUnits;
        };
    };
};

_dragArray = _dragArray - _noDragArray;
pr_dragArray = _dragArray; publicVariable "pr_dragArray";

//[] call compile preprocessFileLineNumbers (prDRAGf + "fn_dragBody.sqf");  //--- Can drag dead units
dragInitized = true; publicVariable "dragInitized";
