
//--- Adds Reaper screen at mission start
//cutRsc ["ReapPause", "PLAIN"];

#include "scripts\PR\missionInit.sqf"

if !(aceOn) then {
    MISSION_ROOT = call {
        private "_arr";
        _arr = toArray __FILE__;
        _arr resize (count _arr - 8);
        toString _arr
    };

    [] execVM "scripts\ais_injury\aisEndMission.sqf";
    TCB_AIS_PATH = "scripts\ais_injury\";
    [] call compile preprocessFileLineNumbers (TCB_AIS_PATH+"init_ais.sqf");
    [] call blueHud_fnc_initHud;

    [player] call compile preprocessFileLineNumbers "scripts\ais_injury\fn_aisRandomWake.sqf";  //--- Player health & crawling
};

//--- Adds draggable bodies to killed units
//null = allUnits execVM "scripts\dragBody\H8_dragBody.sqf";
