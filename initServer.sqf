
execVM "scripts\PR\scripts\init\passToHCs.sqf"; //--- Pass Ownership to Headless Clients 

//--- cleanup player bodies on disconnect 
addMissionEventHandler ['HandleDisconnect',{ 
    [(_this select 0)] spawn { 
        _oldBody = (_this select 0); 
        sleep 4; 
        _oldBody = nil; 
        deleteVehicle (_this select 0); 
    }; 
}]; 

pr_fnc_hideMe = { 
    _oldUnit = _this select 0; 
    if (isServer) then { 
        _oldUnit hideObjectGlobal true; 
    }; 
}; 

if !(aceOn) then {
    if (["PersistantTasks",0] call BIS_fnc_getParamValue == 1) then {
        if ((isDedicated) && (isClass (configfile >> "CfgPatches" >> "SRU_PDB"))) then {
            call compile preProcessFile "\sru_pdb\initServer_pdb.sqf";
        };
    };
};
