// Get mortar to fire - V1.0
//
// in mortar init field: _nil = this execVM "custom_scripts\SKL_Mortar.sqf";
//
// Then you can command it (ArtyName) to fire with a trigger on detected for instance:
//   {gunner ArtyName commandTarget _x} forEach thisList;

//		{gunner arty1 commandTarget _x} forEach thisList;
//	after calling upsmon _nil = (vehicle (units arty1 select 0)) execVM "custom_scripts\SKL_Mortar.sqf";

sleep 3;

_veh = _this; //while {_veh == objNil};
if !(local _veh) exitWith {};

sleep 2;

while {(canMove _veh) && (gunner _veh != objNULL)} do
{
    _veh setVehicleAmmo 0.2;
    
    waitUntil {((!isNull assignedTarget (gunner _veh)) && !(isPlayer gunner _veh))};
    
    _gunner = gunner _veh;

    _gunner setUnitAbility 0.5;
    _gunner setSkill 0.5;
    _gunner setSkill ["spotDistance", 2];
    _gunner setSkill ["spotTime", 1];
    //_gunner setSkill ["aimingAccuracy", 0.2];
    //_gunner setSkill ["reloadSpeed",0.2];

    _gunner setCombatMode "RED";
    _gunner setBehaviour "COMBAT";

    _target = assignedTarget (_gunner);

    _veh setVehicleAmmo 1;
    sleep 1;
    _delay = 2;
    
    if ((alive _target) && (_target isKindOf "LAND")) then {_veh fireAtTarget [_target];
    //hint format ["Morar %1 firing at %2",_veh,_target];
    _delay = 7;};
    
    sleep _delay;

};
