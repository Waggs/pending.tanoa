// Debug Dialog
// To use, set a radio trigger to activate: _handle = CreateDialog "SKL_DEBUG_DIALOG";
// In description.ext add:  
//         #include "scripts\SKULL\DebugMenu.hpp"

#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

#include "constants.hpp"
#include "dialog_housepos.hpp"
#include "dialog_vehiclespawn.hpp"
#include "dialog_weaponspawn.hpp"
#include "dialog_ammospawn.hpp"
#include "dialog_music.hpp"
#include "dialog_objectEd.hpp"
#include "ids.h"
#include "SKL_ZeusMenu.hpp"

class SKLD_BOX
{
	type = 0;
	idc = -1;
	style = ST_CENTER;
	shadow = 2;
	colorBackground[] = {
		0.2,
		0.3,
		0.2,
		0.7
	};
    colorText[] = {1,1,1,1};
	font = "PuristaMedium";
	sizeEx = 0.02;
	text = "";
};
class SKL_CLIPBOARD_DIALOG
{
    idd = 1802;
    movingenabled = true;
    onLoad = "((_this select 0) displayCtrl  1690) ctrlSetText (uiNamespace getVariable 'SKL_ClipBoard');";
    class Controls
    {
        class SKLD_ClipBox: SKLD_BOX
        {
            idc = -1;
            text = ""; //--- ToDo: Localize;
            x = 3 * GUI_GRID_W + GUI_GRID_X;
            y = 2 * GUI_GRID_H + GUI_GRID_Y;
            w = 34 * GUI_GRID_W;
            h = 6 * GUI_GRID_H;
        };
        class SKLD_ClipFrame: FHQRscFrame
        {
            idc = 1800;
            text = "Clipboard Cutter"; //--- ToDo: Localize;
            x = 3 * GUI_GRID_W + GUI_GRID_X;
            y = 2 * GUI_GRID_H + GUI_GRID_Y;
            w = 34 * GUI_GRID_W;
            h = 6 * GUI_GRID_H;
            moving = true;
            sizeEx = 0.04;
        };
        class SKLD_Command: FHQRscEdit
        {
            idc = 1690;
            text = "..."; //--- ToDo: Localize;
            x = 4 * GUI_GRID_W + GUI_GRID_X;
            y = 4 * GUI_GRID_H + GUI_GRID_Y;
            w = 30 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.035;
        };
        class SKLD_ButtonOK: FHQRscButton
        {
            idc = 1691;
            text = "Done"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            action = "closeDialog 0;";
        };
    };
};
class SKL_DEBUG_DIALOG
{
    idd = 1801;
    movingenabled = true;
    onLoad = "((_this select 0) displayCtrl  1636) ctrlSetText (uiNamespace getVariable 'SKL_PrevCommand'); ((_this select 0) displayCtrl  1641) ctrlSetText (format ['OPFOR: %1  Units: %2',{(side _x) == EAST} count allUnits, count allUnits]);   ((_this select 0) displayCtrl  1657) ctrlSetText (format ['EAST: %1  WEST: %2',{(side _x) == EAST} count allGroups, {(side _x) == WEST} count allGroups]);                         ((_this select 0) displayCtrl  1652) ctrlSetText format ['%1',(date select 0)];((_this select 0) displayCtrl  1653) ctrlSetText format ['%1',(date select 1)]; ((_this select 0) displayCtrl  1654) ctrlSetText format ['%1',(date select 2)]; ((_this select 0) displayCtrl  1649) ctrlSetText format ['%1',(date select 3)]; ((_this select 0) displayCtrl  1650) ctrlSetText format ['%1',(date select 4)];              ((_this select 0) displayCtrl  1640) sliderSetPosition (fogParams select 0)*10; ((_this select 0) displayCtrl  1646) sliderSetPosition (fogParams select 1)*20; ((_this select 0) displayCtrl  1648) sliderSetPosition (fogParams select 2)/10; ((_this select 0) displayCtrl  1643) sliderSetPosition overcast*10; ((_this select 0) displayCtrl  1644) ctrlSetText (format ['Position: %1',getPosATL player]);";
    
    class Controls
    {
        class SKLD_Box: SKLD_BOX
        {
            idc = -1;
            text = ""; //--- ToDo: Localize;
            x = 3 * GUI_GRID_W + GUI_GRID_X;
            y = 2 * GUI_GRID_H + GUI_GRID_Y;
            w = 34 * GUI_GRID_W;
            h = 24 * GUI_GRID_H;
        };
        ////////////////////////////////////////////////////////
        // GUI EDITOR OUTPUT START (by SkullTT, v1.063, #Vememo)
        ////////////////////////////////////////////////////////

        class SKLD_Frame: FHQRscFrame
        {
            idc = 1800;
            text = "Skull's Debug Menu - v2.1"; //--- ToDo: Localize;
            x = 3 * GUI_GRID_W + GUI_GRID_X;
            y = 2 * GUI_GRID_H + GUI_GRID_Y;
            w = 34 * GUI_GRID_W;
            h = 24 * GUI_GRID_H;
            moving = false;
            sizeEx = 0.04;
        };
        class SKLD_Button1a: FHQRscButton
        {
            idc = 1600;
            text = "Invisible"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 4 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "player setCaptive true; hint 'You are invisible';";
        };
        class SKLD_Button1b: FHQRscButton
        {
            idc = 1601;
            text = "Visible"; //--- ToDo: Localize;
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 4 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "player setCaptive false; hint 'You are visible';";
        };
        class SKLD_Button2a: FHQRscButton
        {
            idc = 1602;
            text = "Invulnerable"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "player allowDamage false; player setDamage 0; if (vehicle player != player) then {vehicle player allowDamage false;}; hint 'You are god like';";
        };
        class SKLD_Button2b: FHQRscButton
        {
            idc = 1603;
            text = "Vulnerable"; //--- ToDo: Localize;
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "player allowDamage true; if (vehicle player != player) then {vehicle player allowDamage true;}; hint 'You are human';";
        };
        class SKLD_Button3a: FHQRscButton
        {
            idc = 1604;
            text = "Spawn Helo"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 7 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "['B_Heli_Light_01_armed_F' createVehicle (position player)] call z_AddObjects; nil";
        };
        class SKLD_Button3b: FHQRscButton
        {
            idc = 1605;
            text = "Spawn Quadbike"; //--- ToDo: Localize;
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 7 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "['B_Quadbike_01_F' createVehicle (position player)] call z_AddObjects;";
        };
        class SKLD_Button4a: FHQRscButton
        {
            idc = 1606;
            text = "Map Teleport"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "onMapSingleClick {vehicle player setPos _pos; hintSilent ''; openMap false; onMapSingleClick { }; }; closeDialog 0; hint 'Click on the map to teleport'; showMap true; openMap true;";
        };
        class SKLD_Button4b: FHQRscButton
        {
            idc = 1608;
            text = "Map Tele Group"; //--- ToDo: Localize;
            x = 12  * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "onMapSingleClick { {_x setPos _pos} count units group player; hintSilent ''; openMap false; onMapSingleClick { }; }; closeDialog 0; hint 'Click on the map to teleport the group'; showMap true; openMap true;";
        };
        class SKLD_Button5a: FHQRscButton
        {
            idc = 1607;
            text = "Look Teleport"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 10  * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "vehicle player setpos screenToWorld [0.5,0.5];";
        };
        class SKLD_Button5b: FHQRscButton
        {
            idc = 1609;
            text = "Look Tele Grp"; //--- ToDo: Localize;
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 10 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "{_x setpos screenToWorld [0.5,0.5];} count units group player;";
        };
        class SKLD_Button6a: FHQRscButton
        {
            idc = 1610;
            text = "--"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button6b: FHQRscButton
        {
            idc = 1611;
            text = "--"; //--- ToDo: Localize;
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button7a: FHQRscButton
        {
            idc = 1624;
            text = "Music"; //--- ToDo: Localize;
            action = "closeDialog 0; [] spawn FHQ_DebugConsole_Music;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 13 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button7b: FHQRscButton
        {
            idc = 1626;
            text = "Object Editor"; //--- ToDo: Localize;
            action = "closeDialog 0; [] spawn FHQ_DebugConsole_OPS;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 13 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button8a: FHQRscButton
        {
            idc = 1625;
            text = "Weapon Crates";
            action = "closeDialog 0; [] spawn FHQ_DebugConsole_SpawnAmmo;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button8b: FHQRscButton
        {
            idc = 1627;
            text = "House Positions";
            action = "closeDialog 0; [] spawn FHQ_DebugConsole_HousePos;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button9a: FHQRscButton
        {
            idc = 1633;
            text = "Unit Customization";
            action = "closeDialog 0; [] spawn FHQ_DebugConsole_SpawnWeapon;";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 16 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button9b: FHQRscButton
        {
            idc = 1634;
            text = "Save Loadout";
            action = "closeDialog 0; [] call FHQ_DebugConsole_SaveLoadout;";
            x = 12 * GUI_GRID_W + GUI_GRID_X;
            y = 16 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };

        //////////////////////////////////////////////////////////////////

        class SKLD_Button1c: FHQRscButton
        {
            idc = 1612;
            text = "Test1 True"; //--- ToDo: Localize;
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 4 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "TEST1 = true; publicVariable 'TEST1'; hint 'Test1 is true'";
        };
        class SKLD_Button1d: FHQRscButton
        {
            idc = 1613;
            text = "Test1 False"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 4 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "TEST1 = false; publicVariable 'TEST1'; hint 'Test1 is false'";
        }; 
        class SKLD_Button2c: FHQRscButton
        {
            idc = 1614;
            text = "Spawn Vehicle"; //--- ToDo: Localize;
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "closeDialog 0; [] spawn FHQ_DebugConsole_SpawnVehicle;";
        };
        class SKLD_Button2d: FHQRscButton
        {
            idc = 1615;
            text = "Restore Vehicle"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "if (vehicle player != player) then {(vehicle player) setDamage 0; (vehicle player) setVehicleAmmo 1; (vehicle player) setFuel 1;};";
        };

        class SKLD_Button3c: FHQRscButton
        {
            idc = 1616;
            text = "BLUFOR man"; //--- ToDo: Localize;
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 7 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
			action = "'B_Soldier_F' createUnit [ screenToWorld [0.5,0.5], createGroup WEST, 'this call z_addObjects'];";
        };
        class SKLD_Button3d: FHQRscButton
        {
            idc = 1617;
            text = "Kill Nearby BLUFOR"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 7 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "{if (((side _x) == WEST) && (_x distance player < 300) && (_x != player)) then { _x setDamage 1; }; } count allUnits;";
        };
        class SKLD_Button4c: FHQRscButton
        {
            idc = 1618;
            text = "OPFOR man"; //--- ToDo: Localize;
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "'O_Soldier_F' createUnit [ screenToWorld [0.5,0.5], createGroup EAST, 'this call z_addObjects'];";
        };
        class SKLD_Button4d: FHQRscButton
        {
            idc = 1621;
            text = "Kill Nearby OPFOR"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "{if (((side _x) == EAST) && (_x distance player < 300) && (_x != player)) then { _x setDamage 1; }; } count allUnits;";
        };
        class SKLD_Button5c: FHQRscButton
        {
            idc = 1620;
            text = "GUER man"; //--- ToDo: Localize;
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 10 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "'I_Soldier_F' createUnit [ screenToWorld [0.5,0.5], createGroup RESISTANCE, 'this call z_addObjects'];";
        };
        class SKLD_Button5d: FHQRscButton
        {
            idc = 1621;
            text = "Kill Nearby GUER"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 10 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "{if (((side _x) == RESISTANCE) && (_x distance player < 300) && (_x != player)) then { _x setDamage 1; }; } count allUnits;";
        };
        class SKLD_Button6c: FHQRscButton
        {
            idc = 1622;
            text = "Reveal Units"; //--- ToDo: Localize;
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action ="closeDialog 0; openMap true; { group player reveal [_x, 0]; group player reveal [_x, 4]; } count allUnits; ";
        };
        class SKLD_Button6d: FHQRscButton
        {
            idc = 1623;
            text = "Toggle Groups"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "[] spawn FHQ_DebugConsole_ToggleGroup;";
        };
        class SKLD_Button7c: FHQRscButton
        {
            idc = 1628;
            text = "Get Object IDs"; //--- ToDo: Localize;
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 13 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            action = "ctrlSetText [1637, format ['%1',call compile ('nearestObjects [player, [], 10]')] ];";
            sizeEx = 0.03; 
        };
        class SKLD_Button7d: FHQRscButton
        {
            idc = 1630;
            text = "Set SKLO Object"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 13 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            action = "SKLO = cursorTarget; ctrlSetText [1637, format ['SKLO=%1',SKLO] ];";
            sizeEx = 0.03; 
        };
        class SKLD_Button8c: FHQRscButton
        {
            idc = 1629;
            text = "Reset Rating"; //--- ToDo: Localize;
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            action = "_ct = cursorTarget; if (isNull _ct) then {_ct = player}; [format ['%1 addRating -(rating %1)', _ct], 'SKL_DebugCmd', true] call BIS_fnc_MP;";
            sizeEx = 0.03; 
        };
        class SKLD_Button8d: FHQRscButton
        {
            idc = 1631;
            text = "Reset Animation";//"Nimitz Hanger"; //--- ToDo: Localize;
            action = "player switchMove ''; cursorTarget switchMove '';";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button9c: FHQRscButton
        {
            idc = 1629;
            text = "Zeus Editable";//"Nimitz Hanger"; //--- ToDo: Localize;
            action = "[[cursorTarget],'z_AddObjects',false] call BIS_fnc_MP;";
            //action = "(player) setPos [(getMarkerPos ""hangar"") select 0, (getMarkerPos ""hangar"") select 1, 6.5]; hint ""you have been teleported to the hangar"";";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 16 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button9d: FHQRscButton
        {
            idc = 1631;
            text = "Zeus Menu";//"Nimitz FlightDeck"; //--- ToDo: Localize;
            action = "if (player in ([] call BIS_fnc_listCuratorPlayers)) then {closeDialog 0; cursorTarget spawn zmenu;};";
            //action = "(player) setPos [(getMarkerPos ""flightdeck"") select 0, (getMarkerPos ""flightdeck"") select 1, 10]; hint ""you have been teleported to the flightdeck"";  ";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 16 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button10c: FHQRscButton
        {
            idc = 1655;
            text = "Camera On"; //--- ToDo: Localize;
            action = "closeDialog 0; 1 spawn SKL_DebugConsole_Camera;";
            x = 21.5 * GUI_GRID_W + GUI_GRID_X;
            y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };
        class SKLD_Button10d: FHQRscButton
        {
            idc = 1656;
            text = "Camera Off"; //--- ToDo: Localize;
            action = "closeDialog 0; 0 spawn SKL_DebugConsole_Camera;";
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
        };

        ////////////////////////////////////////////////////////////////
        ////////////////////////  F O G  ///////////////////////////////
        class SKLD_Slider1Text: FHQRscText
        {
            idc = 1639;
            text = "Fog"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.025;
        };
        class SKLD_Slider1 : FHQRscSlider
        { 
            idc = 1640; 
            style = SL_HORZ; 
            x = 7.0 * GUI_GRID_W + GUI_GRID_X;
            y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 0.3; 
            h = 0.025; 
            onSliderPosChanged = "ctrlSetText [1637,  format ['0 setFog [%1, %2, %3]',(sliderPosition 1640)/10,(sliderPosition 1646)/20,(sliderPosition 1648)*10]]; [ [0, (sliderPosition 1640)/10, (sliderPosition 1646)/20, (sliderPosition 1648)*10], 'SKL_DebugSetFog', true] call BIS_fnc_MP"; 
        };

        class SKLD_Slider2Text: FHQRscText
        {
            idc = 1645;
            text = "Fog Decay"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 18.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.025;
        };
        class SKLD_Slider2 : FHQRscSlider
        { 
            idc = 1646; 
            style = SL_HORZ; 
            x = 7.0 * GUI_GRID_W + GUI_GRID_X;
            y = 18.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 0.3; 
            h = 0.025; 
            onSliderPosChanged = "ctrlSetText [1637,  format ['0 setFog [%1, %2, %3]',(sliderPosition 1640)/10,(sliderPosition 1646)/20,(sliderPosition 1648)*10]]; [ [0, (sliderPosition 1640)/10, (sliderPosition 1646)/20, (sliderPosition 1648)*10], 'SKL_DebugSetFog', true] call BIS_fnc_MP"; 
        };
        class SKLD_Slider3Text: FHQRscText
        {
            idc = 1647;
            text = "Fog Top"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.025;
        };
        class SKLD_Slider3 : FHQRscSlider
        { 
            idc = 1648; 
            style = SL_HORZ; 
            x = 7.0 * GUI_GRID_W + GUI_GRID_X;
            y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 0.3; 
            h = 0.025; 
            onSliderPosChanged = "ctrlSetText [1637,  format ['0 setFog [%1, %2, %3]',(sliderPosition 1640)/10,(sliderPosition 1646)/20,(sliderPosition 1648)*10]]; [ [0, (sliderPosition 1640)/10, (sliderPosition 1646)/20, (sliderPosition 1648)*10], 'SKL_DebugSetFog', true] call BIS_fnc_MP";  
        };
        class SKLD_Slider4Text: FHQRscText
        {
            idc = 1642;
            text = "Weather"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.025;
        };
        class SKLD_Slider4 : FHQRscSlider
        { 
            idc = 1643; 
            style = SL_HORZ; 
            x = 7.0 * GUI_GRID_W + GUI_GRID_X;
            y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 0.3; 
            h = 0.025; 
            action = "[ -0.2, 'SKL_DebugWeather', true] call BIS_fnc_MP;";
            onSliderPosChanged = "ctrlSetText [1637,  format ['setovercast %1',(sliderPosition 1643)/10]]; [(sliderPosition 1643)/10, 'SKL_DebugWeather', true] call BIS_fnc_MP;"; 
        };

        ////////////////////////////////////////////////////////////////////////
        /////////////////////////  T I M E  ////////////////////////////////////
        class SKLD_Year: FHQRscEdit
        {
            idc = 1652;
            text = "Y"; //--- ToDo: Localize;
            x = 27 * GUI_GRID_W + GUI_GRID_X;
            y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 2 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.03;
        };
        class SKLD_Month: FHQRscEdit
        {
            idc = 1653;
            text = "M"; //--- ToDo: Localize;
            x = 29.5 * GUI_GRID_W + GUI_GRID_X;
            y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 1.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.03;
        };
        class SKLD_Day: FHQRscEdit
        {
            idc = 1654;
            text = "D"; //--- ToDo: Localize;
            x = 31.5 * GUI_GRID_W + GUI_GRID_X;
            y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 1.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.03;
        };
        class SKLD_Hours: FHQRscEdit
        {
            idc = 1649;
            text = "H"; //--- ToDo: Localize;
            x = 27 * GUI_GRID_W + GUI_GRID_X;
            y = 21 * GUI_GRID_H + GUI_GRID_Y;
            w = 1.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.03;
        };
        class SKLD_Minutes: FHQRscEdit
        {
            idc = 1650;
            text = "M"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 21 * GUI_GRID_H + GUI_GRID_Y;
            w = 1.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.03;
        };
        class SKLD_SetTime: FHQRscButton
        {
            idc = 1651;
            text = "Set Time"; //--- ToDo: Localize;
            x = 31 * GUI_GRID_W + GUI_GRID_X;
            y = 21 * GUI_GRID_H + GUI_GRID_Y;
            w = 4 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.03; 
            action = "_d = [parseNumber (ctrlText 1652),parseNumber (ctrlText 1653),parseNumber (ctrlText 1654),parseNumber (ctrlText 1649),parseNumber (ctrlText 1650)]; [_d, 'SKL_DebugDate', true] call BIS_fnc_MP; ctrlSetText [1637,  format ['%1 date',_d]];";
        };

        ////////////////////////////////////////////////////////////////////////////
        /////////////////////////  C O M M A N D  //////////////////////////////////
        class SKLD_Aim: FHQRscText
        {
            idc = 1635;
            text = "+"; //--- ToDo: Localize;
            x = 19.5 * GUI_GRID_W + GUI_GRID_X;
            y = 12 * GUI_GRID_H + GUI_GRID_Y;
            w = 1.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.05;
        };
        class SKLD_PosText: FHQRscActiveText
        {
            type = CT_ACTIVETEXT;
            idc = 1644;
            text = "Position: ..."; //--- ToDo: Localize;
            action = "_s = (format ['%1',getPosATL player]); if (!isServer) then {uiNamespace setVariable ['SKL_ClipBoard',_s]; _handle = CreateDialog 'SKL_CLIPBOARD_DIALOG';} else {copyToClipboard _s; hint 'Player position copied to clipboard';};";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 21.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 20.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.03;
        }; 
        class SKLD_Command: FHQRscEdit
        {
            idc = SKL_ID_COMMAND;
            text = "getPos player"; //--- ToDo: Localize;
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 22.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 20.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.04;
        }
        class SKLD_CommandResponse: FHQRscActiveText
        {
            type = CT_ACTIVETEXT;
            idc = 1637;
            text = "..."; //--- ToDo: Localize;
            action = "_s = (ctrlText 1637); if (!isServer) then {uiNamespace setVariable ['SKL_ClipBoard',_s]; _handle = CreateDialog 'SKL_CLIPBOARD_DIALOG';} else {copyToClipboard _s; hint 'Output copied to clipboard';};";
            x = 4.5 * GUI_GRID_W + GUI_GRID_X;
            y = 24 * GUI_GRID_H + GUI_GRID_Y;
            w = 23 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.04;
            canModify = 0;
        }; 
        class SKLD_CommandButton: FHQRscButton
        {
            idc = 1638;
            text = "Go"; //--- ToDo: Localize;
            x = 25.5 * GUI_GRID_W + GUI_GRID_X;
            y = 22.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 1.5 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
            action = "ctrlSetText [1637, format ['%1',call compile (ctrlText 1636)] ]; uiNamespace setVariable ['SKL_PrevCommand', (ctrlText 1636)]; ";
        };
        
        class SKLD_SCommandButton: FHQRscButton
        {
            idc = 1659;
            text = "S"; //--- ToDo: Localize;
            x = 25.5 * GUI_GRID_W + GUI_GRID_X;
            y = 23.75 * GUI_GRID_H + GUI_GRID_Y;
            w = 0.75 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
            action = "publicVariable 'SKLO';[ctrlText 1636, 'SKL_DebugCmd', false] call BIS_fnc_MP; uiNamespace setVariable ['SKL_PrevCommand', (ctrlText 1636)]; ";
        };
        
        class SKLD_GCommandButton: FHQRscButton
        {
            idc = 1660;
            text = "G"; //--- ToDo: Localize;
            x = 26.35 * GUI_GRID_W + GUI_GRID_X;
            y = 23.75 * GUI_GRID_H + GUI_GRID_Y;
            w = 0.75 * GUI_GRID_W;
            h = 1 * GUI_GRID_H;
            action = "publicVariable 'SKLO';[ctrlText 1636, 'SKL_DebugCmd', true] call BIS_fnc_MP; uiNamespace setVariable ['SKL_PrevCommand', (ctrlText 1636)]; ";
        };

        ///////////////////////////////////////////////////////////////
        /////////////////////////  O  K  //////////////////////////////
        class SKLD_OPFORText: FHQRscText
        {
            idc = SKL_ID_UNIT_COUNT;
            text = "OPFOR: ...   Units: ..."; //--- ToDo: Localize;
            x = 27.5 * GUI_GRID_W + GUI_GRID_X;
            y = 22 * GUI_GRID_H + GUI_GRID_Y;
            w = 8 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.03;
        };

        class SKLD_GROUPText: FHQRscText
        {
            idc = 1657;
            text = "EAST: ...   WEST: ..."; //--- ToDo: Localize;
            x = 27.5 * GUI_GRID_W + GUI_GRID_X;
            y = 22.8 * GUI_GRID_H + GUI_GRID_Y;
            w = 8 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEX = 0.03;
        };
        
        class SKLD_ButtonOK: FHQRscButton
        {
            idc = 1632;
            text = "OK"; //--- ToDo: Localize;
            x = 29 * GUI_GRID_W + GUI_GRID_X;
            y = 24 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            action = "closeDialog 0;";
        };
        class SKLD_UpdateButton: FHQRscButton
        {
            idc = 1658;
            text = "*"; //--- ToDo: Localize;
            x = 35.5 * GUI_GRID_W + GUI_GRID_X;
            y = 22.75 * GUI_GRID_H + GUI_GRID_Y;
            w = 0.75 * GUI_GRID_W;
            h = 0.75 * GUI_GRID_H;
            action = "ctrlSetText [1641,format ['OPFOR: %1  Units: %2',{(side _x) == EAST} count allUnits, count allUnits]];   ctrlSetText [1657,format ['EAST: %1  WEST: %2',{(side _x) == EAST} count allGroups, {(side _x) == WEST} count allGroups]]; ";
        };
        class SKLD_Text: FHQRscText
        {
            text = "(leveraged from FHQ_DebugConsole by Varanon)";
            x = 22.5 * GUI_GRID_W + GUI_GRID_X;
            y = 25.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 20 * GUI_GRID_W;
            h = 0.5 * GUI_GRID_H;
            sizeEx = 0.02;
        }
        ////////////////////////////////////////////////////////
        // GUI EDITOR OUTPUT END
        ////////////////////////////////////////////////////////

    };
};    
      