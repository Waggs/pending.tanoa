#include "ids.h"

if (isNil "z_AddObjects") then {
    0 execVM "scripts\SKULL\DebugMenu\SKL_ZeusObjects.sqf";
};

FHQ_DebugConsole_SelectedAmmoCrate = -1;
FHQ_DebugConsole_AmmoBoxFilterLast = 0;

FHQ_DebugConsole_SelectAmmoBox = {
    _index = _this select 0;
       
    FHQ_DebugConsole_SelectedAmmoCrate = lbValue [AMMOSPAWN_DIALOG_LIST, _index];
};

FHQ_DebugConsole_SpawnAmmoBox = {
    if (FHQ_DebugConsole_SelectedAmmoCrate == -1) exitWith {/* Nothing selected */};
    _box = FHQ_DebugConsole_AmmoCrateList select FHQ_DebugConsole_SelectedAmmoCrate;
    
    _dir = getDir player;
    _pos = [player, random(3)+1, (_dir - 15) + random(30)] call BIS_fnc_relPos;
   	_ammo = createVehicle [_box select 0, _pos, [], 0, "NONE"];
	[_ammo] call z_AddObjects; 
};


FHQ_DebugConsole_AmmoBoxFilterList = {
	_filter = lbData [AMMOSPAWN_DIALOG_FILTER, FHQ_DebugConsole_AmmoBoxFilterLast];

    switch (_filter) do {
	    case "Ammo Boxes": {
        	["Ammo"] call FHQ_DebugConsole_AmmoBoxFilterListBuild;
            FHQ_DebugConsole_SelectedAmmoCrate = -1;
    	};
    	case "Backpacks": {
			["Backpacks"] call FHQ_DebugConsole_AmmoBoxFilterListBuild;
            FHQ_DebugConsole_SelectedAmmoCrate = -1;
	    };
	};        
};

FHQ_DebugConsole_AmmoBoxFilterListBuild = {
	FHQ_DebugConsole_AmmoCrateList = [];

	_type = _this select 0;
	_cnt = 0;
	_cfgVehicles = configFile >> "cfgVehicles";
	_display = findDisplay AMMOSPAWN_DIALOG;
	lbClear AMMOSPAWN_DIALOG_LIST;

	for "_i" from 0 to (count _cfgVehicles) - 1 do {
		_current = _cfgVehicles select _i;
		if (isClass _current) then {
			_configName = configName(_current);
			_scope = getNumber(configFile >> "cfgVehicles" >> _configName >> "scope");
			_class =  getText(configFile >> "cfgVehicles" >> _configName >> "vehicleclass");
			_displayName =  getText(configFile >> "cfgVehicles" >> _configName >> "displayName");
            
			if (_scope == 2 && _class == _type) then {
				FHQ_DebugConsole_AmmoCrateList = FHQ_DebugConsole_AmmoCrateList + [[_configName, _displayName]];
			
	            lbAdd [AMMOSPAWN_DIALOG_LIST, _displayName]; 
				lbSetValue [AMMOSPAWN_DIALOG_LIST, _cnt, _cnt];
            	_cnt = _cnt + 1;
			};
		};
	};

            
	lbSort (_display displayCtrl AMMOSPAWN_DIALOG_LIST);
};


disableSerialization;

_ok = createDialog "FHQ_DebugDialog_AmmoSpawn";


{
	private "_index";
        
	_index = lbAdd [AMMOSPAWN_DIALOG_FILTER, _x];
	lbSetData [AMMOSPAWN_DIALOG_FILTER, _index, _x];
} forEach ["Ammo Boxes", "Backpacks"];
 lbSetCurSel [AMMOSPAWN_DIALOG_FILTER, 0];
 
["Ammo"] call FHQ_DebugConsole_AmmoBoxFilterListBuild;