Version 3.0  4-17-14
2.0: Added Zeus menu for curators.  Fixed bug in camera.
1.15: Add reset anim button and use getPosATL for player pos display
1.14: Fix names of sounds that have changed (button clicks and such)
1.12: Current state of menu when release notes started being kept