////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Varanon, v1.062, #Xogamo)
////////////////////////////////////////////////////////
class FHQ_DebugDialog_Music
{
	idd = 140874;
	movingEnable = true;
	enableSimulation = true;
	class Objects
	{

	};

	class ControlsBackground
	{
		class FHQ_DebugMusic_Backdrop: FHQRscText
			{
				idc = 1001;
				x = 0.29374 * safezoneW + safezoneX;
				y = 0.252426 * safezoneH + safezoneY;
				w = 0.41252 * safezoneW;
				h = 0.522655 * safezoneH;
				colorBackground[] = {0,0,0,0.4};
			};
	};

	class Controls
	{
		class FHQ_DebugMusic_Header: FHQRscText
		{
			idc = 1000;
			text = "Music Player"; //--- ToDo: Localize;
			x = 0.29374 * safezoneW + safezoneX;
			y = 0.224918 * safezoneH + safezoneY;
			w = 0.41252 * safezoneW;
			h = 0.0275082 * safezoneH;
			colorBackground[] = {0,0,0,1};
		};
		class FHQ_DebugMusic_Close: FHQRscButton
		{
			action = "closeDialog 0";

			idc = 1600;
			text = "Close"; //--- ToDo: Localize;
			x = 0.29374 * safezoneW + safezoneX;
			y = 0.747574 * safezoneH + safezoneY;
			w = 0.0902388 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugMusic_Back: FHQRscButton
		{
			action = "closeDialog 0; _handle = CreateDialog 'SKL_DEBUG_DIALOG';";

			idc = 1601;
			text = "Back"; //--- ToDo: Localize;
			x = 0.390424 * safezoneW + safezoneX;
			y = 0.747574 * safezoneH + safezoneY;
			w = 0.0902388 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugWeapon_Selection: FHQRscListbox
		{
			onLBSelChanged = "[_this select 1] call FHQ_DebugConsole_SelectMusicTrack;";

			idc = 1500;
			x = 0.300195 * safezoneW + safezoneX;
			y = 0.26625 * safezoneH + safezoneY;
			w = 0.399629 * safezoneW;
			h = 0.426377 * safezoneH;
		};
		class FHQ_DebugConsole_PlayMusicTrack: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_PlayMusicTrack;";

			idc = 1602;
			text = "Play"; //--- ToDo: Localize;
			x = 0.616016 * safezoneW + safezoneX;
			y = 0.7475 * safezoneH + safezoneY;
			w = 0.0902344 * safezoneW;
			h = 0.0275 * safezoneH;
		};
		class FHQ_DebugConsole_StopMusicTrack: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_StopMusicTrack;";

			idc = 1603;
			text = "Stop"; //--- ToDo: Localize;
			x = 0.519336 * safezoneW + safezoneX;
			y = 0.7475 * safezoneH + safezoneY;
			w = 0.0902344 * safezoneW;
			h = 0.0275 * safezoneH;
		};
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT END
		////////////////////////////////////////////////////////
	};
};
