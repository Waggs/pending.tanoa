#include "ids.h"

FHQ_DebugConsole_TrackList = [];
FHQ_DebugConsole_SelectedTrack = -1;

FHQ_DebugConsole_MusicPlayerListBuild = {
    FHQ_DebugConsole_TrackList = [];
    
    _cnt = 0;
    _cfgMusic = configFile >> "cfgMusic";
    _display = findDisplay MUSIC_DIALOG;
	lbClear MUSIC_DIALOG_LIST;
    
    for "_i" from 0 to (count _cfgMusic) - 1 do {
        _current = _cfgMusic select _i;
        
        if (isClass _current) then {
            _configName = configName(_current);
            _name = getText(configFile >> "cfgMusic" >> _configName >> "name");

           if (_name != "") then {
                 FHQ_DebugConsole_TrackList = FHQ_DebugConsole_TrackList +
                 	[[_name, _configName]];
                    
                 lbAdd [MUSIC_DIALOG_LIST, _name];
                 lbSetValue [MUSIC_DIALOG_LIST, _cnt, _cnt];
                 
                 _cnt = _cnt + 1;
              };
         };
	};
    
    lbSort (_display displayCtrl MUSIC_DIALOG_LIST);
};

FHQ_DebugConsole_SelectMusicTrack = {
    _index = _this select 0;
       
    FHQ_DebugConsole_SelectedTrack = lbValue [MUSIC_DIALOG_LIST, _index];
};

FHQ_DebugConsole_PlayMusicTrack = {
    if (FHQ_DebugConsole_SelectedTrack == -1) exitWith {};
    
    _track = FHQ_DebugConsole_TrackList select FHQ_DebugConsole_SelectedTrack;
    playMusic (_track select 1);
    
    hint format["Playing track %1 (config name %2)", _track select 0, _track select 1]; 
	_string = format ['playMusic "%1";', _track select 1];
	copyToClipboard _string;
    uiNamespace setVariable ['SKL_PrevCommand', _string];
};

FHQ_DebugConsole_StopMusicTrack = {
    playMusic "";
};
    
disableSerialization;

_ok = createDialog "FHQ_DebugDialog_Music";


call FHQ_DebugConsole_MusicPlayerListBuild;
