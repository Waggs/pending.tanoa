// Zeus costumes

// [ "name", [uniforms], [vests], [backpacks], [hats], [glasses], weapon, [ammo], [items]]
// use [] for empty arrays 

_standardItems = ["SmokeShell","SmokeShell","HandGrenade","HandGrenade","Chemlight_red","Chemlight_green","Chemlight_yellow"];
_standardGlasses = ["","","G_Sport_Greenblack","G_Shades_Black","G_Shades_Blue","G_Shades_Green","G_Shades_Red","G_Sport_Blackred","G_Tactical_Clear","G_Sport_Red"];
_militaryGlasses = ["","G_Tactical_Clear","G_Combat"];
z_ZeusCostumes =
[
////////////////////////////////////////
[ "Civilian",
["U_C_Poloshirt_blue","U_C_Poloshirt_burgundy","U_C_Poloshirt_stripped","U_C_Poloshirt_tricolour","U_Rangemaster","U_NikosBody","U_C_Poor_1","U_C_Poor_2"],// uniform
[],// vest
[],// backpack
["H_Cap_red","H_Booniehat_tan","H_BandMask_blk"],// hat
_standardGlasses, // glasses
[], //weapons
[],//ammo
_standardItems],
////////////////////////////////////////
[ "BLUFOR Officer",
["U_B_CombatUniform_mcam"],// uniform
["V_BandollierB_rgr"],// vest
["tf_rt1523g"],// backpack
["H_MilCap_mcamo"],// hat
_militaryGlasses + _standardGlasses, // glasses
["arifle_MXC_ACO_F","hgun_Pistol_heavy_02_F"],//weapon
["30Rnd_65x39_caseless_mag","6Rnd_45ACP_Cylinder"],//ammo
_standardItems],
////////////////////////////////////////
[ "BLUFOR Pilot",
["U_B_HeliPilotCoveralls"],// uniform
["V_TacVest_blk"],// vest
[],// backpack
["H_PilotHelmetHeli_B"],// hat
[], // glasses
["SMG_01_Holo_F"], //weapons
["30Rnd_45ACP_Mag_SMG_01"],//ammo
_standardItems],
////////////////////////////////////////
[ "BLUFOR Crewman",
["U_B_CombatUniform_mcam_vest"],// uniform
["V_BandollierB_rgr"],// vest
[],// backpack
["H_HelmetCrew_B"],// hat
_militaryGlasses + _standardGlasses, // glasses
["arifle_MXC_F","hgun_P07_F"], //weapons
["30Rnd_65x39_caseless_mag","16Rnd_9x21_Mag"],//ammo
_standardItems],
////////////////////////////////////////
[ "BLUFOR Diver",
["U_B_Wetsuit"],// uniform
["V_RebreatherB"],// vest
[],// backpack
[],// hat
["G_B_Diving"], // glasses
["arifle_SDAR_F"], //weapons
["30Rnd_556x45_Stanag","20Rnd_556x45_UW_mag"],//ammo
_standardItems + ["Chemlight_blue"]],
////////////////////////////////////////
[ "BLUFOR Guerilla",
["U_BG_Guerilla1_1","U_BG_Guerilla2_1","U_BG_Guerilla2_2","U_BG_Guerilla2_3","U_BG_Guerilla3_1","U_BG_Guerilla3_2"],// uniform
["V_Chestrig_oli","V_Chestrig_blk","V_TacVest_blk","V_BandollierB_khk"],// vest
[],// backpack
["H_Watchcap_blk","H_Bandanna_khk","H_Cap_oli","H_Watchcap_camo","H_Booniehat_khk"],// hat
_standardGlasses, // glasses
["arifle_TRG21_MRCO_F","hgun_ACPC2_F"], //weapons
["30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_Tracer_Yellow","9Rnd_45ACP_Mag"],//ammo
_standardItems],
////////////////////////////////////////
[ "OPFOR Officer",
["U_O_OfficerUniform_ocamo"],// uniform
["V_BandollierB_khk"],// vest
[],// backpack
["H_Beret_ocamo"],// hat
[], // glasses
["arifle_Katiba_C_ACO_F","hgun_Pistol_heavy_02_Yorris_F"],//weapon
["30Rnd_65x39_caseless_green","6Rnd_45ACP_Cylinder"],//ammo
_standardItems],
////////////////////////////////////////
[ "OPFOR Soldier",
["U_O_CombatUniform_ocamo"],// uniform
["V_HarnessO_brn"],// vest
[],// backpack
["H_HelmetO_ocamo"],// hat
_militaryGlasses + _standardGlasses, // glasses
["arifle_Katiba_C_ACO_pointer_F","hgun_Rook40_F"],//weapon
["30Rnd_65x39_caseless_green","16Rnd_9x21_Mag"],//ammo
_standardItems],
////////////////////////////////////////
[ "OPFOR Guerilla",
["U_OG_Guerilla2_1","U_OG_Guerilla2_2""U_OG_Guerilla2_3","U_OG_Guerilla3_1","U_OG_Guerilla3_2"],// uniform
["V_Chestrig_oli"],// vest
[],// backpack
["H_Watchcap_blk","H_Booniehat_khk"],// hat
_standardGlasses, // glasses
["arifle_TRG21_MRCO_F","hgun_ACPC2_F"], //weapons
["30Rnd_556x45_Stanag","9Rnd_45ACP_Mag"],//ammo
_standardItems],
////////////////////////////////////////
[ "Independent Officer",
["U_I_OfficerUniform"],// uniform
["V_BandollierB_oli"],// vest
[],// backpack
["H_MilCap_dgtl"],// hat
[], // glasses
["arifle_Mk20C_ACO_F","hgun_ACPC2_F"], //weapons
["30Rnd_556x45_Stanag","9Rnd_45ACP_Mag"],//ammo
_standardItems],
////////////////////////////////////////
[ "Independent Infantryman",
["U_I_CombatUniform"],// uniform
["V_PlateCarrierIA1_dgtl"],// vest
[],// backpack
["H_HelmetIA"],// hat
_militaryGlasses, // glasses
["arifle_Mk20_ACO_pointer_F","hgun_ACPC2_F"], //weapons
["30Rnd_556x45_Stanag","9Rnd_45ACP_Mag"],//ammo
_standardItems]
]
