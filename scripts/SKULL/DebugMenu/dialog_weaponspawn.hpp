
class FHQ_DebugDialog_WeaponSpawn
{
	idd = 140877;
	movingEnable = true;
	enableSimulation = true;
	class Objects
	{

	};

	class ControlsBackground
	{
		class FHQ_DebugMore_Backdrop: FHQRscText
		{
			idc = 1001;
			x = 0.106836 * safezoneW + safezoneX;
			y = 0.2525 * safezoneH + safezoneY;
			w = 0.406055 * safezoneW;
			h = 0.5225 * safezoneH;
			colorBackground[] = {0,0,0,0.4};
		};
	};

	class Controls
	{
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT START (by Varanon, v1.063, #Zixise)
		////////////////////////////////////////////////////////
		class FHQ_DebugWeapon_Header: FHQRscText
		{
			idc = 1000;
			text = "Character Customization"; //--- ToDo: Localize;
			x = 0.106836 * safezoneW + safezoneX;
			y = 0.225 * safezoneH + safezoneY;
			w = 0.41252 * safezoneW;
			h = 0.0275082 * safezoneH;
			colorBackground[] = {0,0,0,1};
		};
		class FHQ_DebugWeapon_Close: FHQRscButton
		{
			action = "closeDialog 0";

			idc = 1600;
			text = "Close"; //--- ToDo: Localize;
			x = 0.113281 * safezoneW + safezoneX;
			y = 0.7475 * safezoneH + safezoneY;
			w = 0.0515625 * safezoneW;
			h = 0.0275 * safezoneH;
		};
		class FHQ_DebugWeapon_Back: FHQRscButton
		{
			action = "closeDialog 0; _handle = CreateDialog 'SKL_DEBUG_DIALOG';";

			idc = 1601;
			text = "Back"; //--- ToDo: Localize;
			x = 0.171289 * safezoneW + safezoneX;
			y = 0.7475 * safezoneH + safezoneY;
			w = 0.0515625 * safezoneW;
			h = 0.0275 * safezoneH;
		};
		class FHQ_DebugWeapon_Selection: FHQRscListbox
		{
			onLBSelChanged = "[_this select 1] call FHQ_DebugConsole_SelectWeapon;";
			onLBDblClick = "[_this select 1] call FHQ_DebugConsole_SelectWeapon; [] call FHQ_DebugConsole_SpawnWeaponNow;";
			idc = 1500;
			x = 0.113281 * safezoneW + safezoneX;
			y = 0.3075 * safezoneH + safezoneY;
			w = 0.180469 * safezoneW;
			h = 0.42625 * safezoneH;
		};
		class FHQ_DebugWeapon_SpawnNow: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_SpawnWeaponNow;";

			idc = 1602;
			text = "Add"; //--- ToDo: Localize;
			x = 0.461333 * safezoneW + safezoneX;
			y = 0.7475 * safezoneH + safezoneY;
			w = 0.0515625 * safezoneW;
			h = 0.0275 * safezoneH;
		};
		class FHQ_DebugWeapon_Filter: FHQRscCombo
		{
			onLBSelChanged	= "FHQ_DebugConsole_WeaponFilterLast = _this select 1; call FHQ_DebugConsole_WeaponFilterList;";

			idc = 2100;
			x = 0.113281 * safezoneW + safezoneX;
			y = 0.26625 * safezoneH + safezoneY;
			w = 0.399629 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugWeapon_Image: FHQRscPicture
		{
			style = 48 + ST_KEEP_ASPECT_RATIO;

			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,0)";
			x = 0.300195 * safezoneW + safezoneX;
			y = 0.3075 * safezoneH + safezoneY;
			w = 0.212695 * safezoneW;
			h = 0.23375 * safezoneH;
		};
		class FHQ_DebugWeapon_Text: FHQRscStructuredText
		{
			idc = 1100;
			x = 0.300195 * safezoneW + safezoneX;
			y = 0.54125 * safezoneH + safezoneY;
			w = 0.212695 * safezoneW;
			h = 0.1925 * safezoneH;
		};
		class FHQ_DebugCononsole_WeaponsClear: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_ClearInventory;";

			idc = 1603;
			text = "Clear All"; //--- ToDo: Localize;
			x = 0.300195 * safezoneW + safezoneX;
			y = 0.7475 * safezoneH + safezoneY;
			w = 0.0451172 * safezoneW;
			h = 0.0275 * safezoneH;
		};
		class FHQ_DebugWeapon_SpawnNowA: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_SpawnWeaponNowAdd;";

			idc = 1604;
			text = "To Weapon"; //--- ToDo: Localize;
			x = 0.40332 * safezoneW + safezoneX;
			y = 0.7475 * safezoneH + safezoneY;
			w = 0.0515625 * safezoneW;
			h = 0.0275 * safezoneH;
		};
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT END
		////////////////////////////////////////////////////////

	};
};


