_cr = toString [13, 10];
_loadoutString = 
  "private [""_unit"", ""_backpack""];" 
+ _cr 
+ _cr 
+ "/* Replace _this below if you want to call this script differently */" + _cr
+ "_unit = _this;" + _cr
+ _cr
+ "if (!isNull _unit) then {" + _cr
+ "    removeAllWeapons _unit;" + _cr
+ "    removeAllItems _unit;" + _cr
+ "    removeBackpack _unit;" + _cr;

_loadoutString = _loadoutString
    + "    removeAllAssignedItems _unit;" + _cr
    + "    removeUniform _unit;" + _cr
    + "    removeVest _unit;" + _cr
    + "    removeHeadgear _unit;" + _cr
    + "    removeGoggles _unit;" + _cr;

    



_loadoutString = _loadoutString 
    + _cr 
    + "    /* Other gear, goggles, vest, uniform */" + _cr;
        
if (uniform player != "") then {
    	_loadoutString = _loadoutString + format["    _unit addUniform ""%1"";", uniform player] + _cr;
};
    
if (vest player != "") then {
     	_loadoutString = _loadoutString + format["    _unit addVest ""%1"";", vest player] + _cr;
};
    
if (headgear player != "") then {
        _loadoutString = _loadoutString + format["    _unit addHeadgear ""%1"";", headgear player] + _cr;
};
    
if (goggles player != "") then {
        _loadoutString = _loadoutString + format["    _unit addGoggles ""%1"";", goggles player] + _cr;
};    
    
_loadoutString = _loadoutString 
	+ _cr
	+ "    /* Magazines and weapons in main inventory */" + _cr;

       
_weapons = [primaryWeapon player] + [secondaryWeapon player] + [handgunWeapon player] ;

if (primaryWeapon player != "") then {
    {
        _loadoutString = _loadoutString + format ["    _unit addMagazine ""%1"";", _x] + _cr;
	} foreach primaryWeaponMagazine player;
};

if (secondaryWeapon player != "") then {
    {
        _loadoutString = _loadoutString + format ["    _unit addMagazine ""%1"";", _x] + _cr;
	} foreach secondaryWeaponMagazine player;
};

if (handgunWeapon player != "") then {
    {
        _loadoutString = _loadoutString + format ["    _unit addMagazine ""%1"";", _x] + _cr;
	} foreach handgunMagazine player;
};

{
	if (_x != "") then {
		_loadoutString = _loadoutString + format["    _unit addWeapon ""%1"";", _x] + _cr;
	};            
} forEach _weapons;   


_loadoutString = _loadoutString 
	+ _cr
	+ "    /* Weapons attachments and magazines */" + _cr;
    
{
    if (_x != "") then {
		_loadoutString = _loadoutString + format["    _unit addPrimaryWeaponItem ""%1"";", _x] + _cr;
    };
} forEach primaryWeaponItems player;

//	_muzzles = getArray (configFile / "CfgWeapons" / _weapon / "muzzles");
//	_muzzle = if (_muzzles select 0 == "this") then {_weapon;} else {_muzzles select 0;};
//#error primaryWeaponMagazine, secondaryWeaponMagazine, handgunMagazine
//#error addPrimaryWeaponitem magazine ? Does that work ?

    
{
    if (_x != "") then {
		_loadoutString = _loadoutString + format["    _unit addSecondaryWeaponItem ""%1"";", _x] + _cr;
    };
} forEach secondaryWeaponItems player;
    
{
    if (_x != "") then {
		_loadoutString = _loadoutString + format["    _unit addHandgunItem ""%1"";", _x] + _cr;
	};            
} forEach handGunItems player;

if (vest player != "") then {
    _vest = vestContainer player;
    _loadoutString = _loadoutString 
	+ _cr
	+ "    /* Magazines and weapons in vest */" + _cr;
	_loadoutString = _loadoutString + format ["    _vest = vestContainer _unit;"]  + _cr;
    _cargo = getMagazineCargo _vest;
    _i = 0;
  	while {_i < count (_cargo select 0)} do {
		_loadoutString = _loadoutString + format["    _vest addMagazineCargoGlobal [""%1"", %2];", (_cargo select 0) select _i, (_cargo select 1) select _i] + _cr;
		_i = _i + 1;
	};
    
    _cargo = getWeaponCargo _vest;
    _i = 0;
  	while {_i < count (_cargo select 0)} do {
		_loadoutString = _loadoutString + format["    _vest addWeaponCargoGlobal [""%1"", %2];", (_cargo select 0) select _i, (_cargo select 1) select _i] + _cr;
		_i = _i + 1;
    }; 
    
   	_cargo = getitemCargo _vest;
    _i = 0;
  	while {_i < count (_cargo select 0)} do {
		_loadoutString = _loadoutString + format["    _vest addItemCargoGlobal [""%1"", %2];", (_cargo select 0) select _i, (_cargo select 1) select _i] + _cr;
		_i = _i + 1;
    }; 
};

if (uniform player != "") then {
    _uniform = uniformContainer player;
	_loadoutString = _loadoutString 
	+ _cr
	+ "    /* Magazines and weapons in uniform */" + _cr;
	_loadoutString = _loadoutString + format ["    _uniform = uniformContainer _unit;"]  + _cr;
    _cargo = getMagazineCargo _uniform;
    _i = 0;
  	while {_i < count (_cargo select 0)} do {
		_loadoutString = _loadoutString + format["    _uniform addMagazineCargoGlobal [""%1"", %2];", (_cargo select 0) select _i, (_cargo select 1) select _i] + _cr;
		_i = _i + 1;
	};
    
    _cargo = getWeaponCargo _uniform;
    _i = 0;
  	while {_i < count (_cargo select 0)} do {
		_loadoutString = _loadoutString + format["    _uniform addWeaponCargoGlobal [""%1"", %2];", (_cargo select 0) select _i, (_cargo select 1) select _i] + _cr;
		_i = _i + 1;
    }; 
    
   	_cargo = getitemCargo _uniform;
    _i = 0;
  	while {_i < count (_cargo select 0)} do {
		_loadoutString = _loadoutString + format["    _uniform addItemCargoGlobal [""%1"", %2];", (_cargo select 0) select _i, (_cargo select 1) select _i] + _cr;
		_i = _i + 1;
    }; 
};


#ifdef UNOBTANIUM    
{
    if (_x == "Binocular") then {
        _loadoutString = _loadoutString 
        + format["    _unit addWeapon ""%1"";", _x] + _cr;
    } else {
      	if (_x != "" && _x != "Binocular" && _x != (headgear player) && _x != (goggles player)) then {
            _loadoutString = _loadoutString 
           	+ format["    _unit addItem ""%1""; _unit assignItem ""%1"";", _x] + _cr;
       	};
    };
} foreach assignedItems player;
#endif
{
  	if (_x != "" && _x != (headgear player) && _x != (goggles player)) then {
            _loadoutString = _loadoutString 
           	+ format["    _unit addItem ""%1""; _unit assignItem ""%1"";", _x] + _cr;
    };
} foreach assignedItems player;
        
 
 

_weaponMuzzles = getArray(configFile >> "cfgWeapons" >> primaryWeapon player >> "muzzles");
_loadoutString = _loadoutString + _cr + format["    _unit selectWeapon ""%1"";", _weaponMuzzles select 0] + _cr;
_loadoutString = _loadoutString + "    reload _unit;" + _cr;
    
      
_backpack = unitBackpack player;
if (!isNull _backpack) then {
    _loadoutString = 
    	  _loadoutString + _cr 
        + "    /* Backpack */" + _cr
        + "    _unit addBackpack """ + typeOf _backpack + """;" + _cr 
        + "    _backpack = unitBackPack _unit;" + _cr
        + "    clearMagazineCargoGlobal _backpack;" + _cr
        + "    clearWeaponCargoGlobal _backpack;" + _cr + _cr;
    
    _cargo = getMagazineCargo _backpack;
    _i = 0;
  	while {_i < count (_cargo select 0)} do {
		_loadoutString = _loadoutString + format["    _backpack addMagazineCargoGlobal [""%1"", %2];", (_cargo select 0) select _i, (_cargo select 1) select _i] + _cr;
		_i = _i + 1;
	};
    
    _cargo = getWeaponCargo _backpack;
    _i = 0;
  	while {_i < count (_cargo select 0)} do {
		_loadoutString = _loadoutString + format["    _backpack addWeaponCargoGlobal [""%1"", %2];", (_cargo select 0) select _i, (_cargo select 1) select _i] + _cr;
		_i = _i + 1;
    }; 
    
   	_cargo = getitemCargo _backpack;
    _i = 0;
  	while {_i < count (_cargo select 0)} do {
		_loadoutString = _loadoutString + format["    _backpack addItemCargoGlobal [""%1"", %2];", (_cargo select 0) select _i, (_cargo select 1) select _i] + _cr;
		_i = _i + 1;
    }; 
    	
};

_loadoutString = _loadoutString + "};" + _cr;

if (!isServer) then {
    uiNamespace setVariable ['SKL_ClipBoard',_loadoutString]; 
    _handle = CreateDialog 'SKL_CLIPBOARD_DIALOG';
} 
else 
{
    copyToClipboard _loadoutString;
    hint "Saved loadout to clipboard";
};
