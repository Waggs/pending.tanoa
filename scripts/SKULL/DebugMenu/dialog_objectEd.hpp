////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Varanon, v1.062, #Xogamo)
////////////////////////////////////////////////////////


class FHQ_DebugDialog_objectEd
{
	idd = 140878;
	movingEnable = false;
	enableSimulation = true;
	class Objects
	{

	};

	class ControlsBackground
	{
		class FHQ_DebugConsole_OPSMap: FHQRscMapControl
		{
			idc = 2300;
			//x = -10;
			//y = -10;
			x = 0.773281 * safezoneW + safezoneX;
			y = 0.016 * safezoneH + safezoneY;
			w = 0.216563 * safezoneW;
			h = 0.374 * safezoneH;
			colorBackground[] = {0,0,0,0.2};
		};

		class MouseFrame: FHQRscListBox
		{
			onMouseMoving = "_this call FHQ_DebugConsole_OPSMouseHandler; call FHQ_DebugConsole_OPSUpdateCamera;";
			onMouseHolding = "call FHQ_DebugConsole_OPSMouseHolding; call FHQ_DebugConsole_OPSUpdateCamera;";
			onMouseButtonDown = "_this call FHQ_DebugConsole_OPSMouseDownHandler";
			onMouseButtonUp = "_this call FHQ_DebugConsole_OPSMouseUpHandler";
			onKeyDown = "_this call FHQ_DebugConsole_OPSKeyDownHandler";
			onKeyUp = "_this call FHQ_DebugConsole_OPSKeyUpHandler";
			onMouseZChanged  = "_this call FHQ_DebugConsole_OPSZChangeHandler";

			idc = 1801;
			x = safeZoneX;
			y = safeZoneY;
			w = safeZoneW;
			h = safeZoneH;
			colorBackground[] = {0,0.3,0,0};
		};
	};

	class Controls
	{
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT START (by Varanon, v1.063, #Vaxoto)
		////////////////////////////////////////////////////////

		class FHQ_DebugConsole_OPSLabelX: FHQRscText
		{
			idc = 1000;
			text = "Pos"; //--- ToDo: Localize;
			x = 0.154531 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.0257812 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSRho: FHQRscText
		{
			idc = 1003;
			text = "R"; //--- ToDo: Localize;
			x = 0.164844 * safezoneW + safezoneX;
			y = 0.929 * safezoneH + safezoneY;
			w = 0.0103125 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSPosX: FHQRscEdit
		{
			onChar = "call FHQ_DebugConsole_OPSSetX";

			idc = 1400;
			x = 0.185469 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.108281 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSPosY: FHQRscEdit
		{
			onChar = "call FHQ_DebugConsole_OPSSetY";

			idc = 1401;
			x = 0.185469 * safezoneW + safezoneX;
			y = 0.841 * safezoneH + safezoneY;
			w = 0.108281 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSPosZ: FHQRscEdit
		{
			onChar = "call FHQ_DebugConsole_OPSSetZ";

			idc = 1402;
			x = 0.185469 * safezoneW + safezoneX;
			y = 0.885 * safezoneH + safezoneY;
			w = 0.108281 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSAngle: FHQRscEdit
		{
			onChar = "call FHQ_DebugConsole_OPSSetRotationText";

			idc = 1403;
			x = 0.185469 * safezoneW + safezoneX;
			y = 0.929 * safezoneH + safezoneY;
			w = 0.108281 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_ObjectList: FHQRscListbox
		{
			onLBSelChanged = "[_this select 1] call FHQ_DebugConsole_OPSSelectObject;";

			idc = 1500;
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.187 * safezoneH;
		};
		class FHQ_DebugConsole_OPSAdd: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_Create;";

			idc = 1606;
			text = "Add"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.0773437 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSDel: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_Delete";

			idc = 1607;
			text = "Del"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.841 * safezoneH + safezoneY;
			w = 0.0773437 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSClasses: FHQRscListbox
		{
			onLBSelChanged = "[_this select 1] call FHQ_DebugConsole_OPSSelectClass;";

			idc = 1501;
			x = 0.613437 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.165 * safezoneH;
		};
		class FHQ_DebugConsole_OPSSelCat: FHQRscCombo
		{
			onLBSelChanged	= "FHQ_DebugConsole_ClassCatLast = _this select 1; call FHQ_DebugConsole_ClassFilterList;";

			idc = 2100;
			x = 0.613437 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class FHQ_DebugConsole_OPSDrop: FHQRscButton
		{
			action = "true call FHQ_DebugConsole_OPS_EnableSim;";

			idc = 1600;
			text = "Drop"; //--- ToDo: Localize;
			x = 0.00499997 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
			tooltip = "Enable simulation to let the object drop"; //--- ToDo: Localize;
		};
		class FHQ_DebugConsole_OPSHover: FHQRscButton
		{
			action = "false call FHQ_DebugConsole_OPS_EnableSim;";

			idc = 1601;
			text = "Float"; //--- ToDo: Localize;
			x = 0.00499997 * safezoneW + safezoneX;
			y = 0.841 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
			tooltip = "DIsable simulation on the object, making it float"; //--- ToDo: Localize;
		};
		class FHQ_DebugConsole_OPSHoverASL: FHQRscButton
		{
			action = "false call FHQ_DebugConsole_OPS_EnableSimASL;";

			idc = 1609;
			text = "Float Horiz"; //--- ToDo: Localize;
			x = 0.00499997 * safezoneW + safezoneX;
			y = 0.890 * safezoneH + safezoneY;
			w = 0.050 * safezoneW;
			h = 0.033 * safezoneH;
			tooltip = "DIsable simulation on the object, making it float, keep it horizontal"; //--- ToDo: Localize;
		};
		class FHQ_DebugConsole_OPSComposition: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_SaveComp";

			idc = 1602;
			text = "Save Composition"; //--- ToDo: Localize;
			x = 0.881562 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.113437 * safezoneW;
			h = 0.033 * safezoneH;
			tooltip = "Save all objects as composition"; //--- ToDo: Localize;
		};
		class FHQ_DebugConsole_OPSSaveCompAbs: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_SaveCompAbs";

			idc = 1603;
			text = "Save Composition/Abs"; //--- ToDo: Localize;
			x = 0.881562 * safezoneW + safezoneX;
			y = 0.841 * safezoneH + safezoneY;
			w = 0.113437 * safezoneW;
			h = 0.033 * safezoneH;
			tooltip = "Save all objects as composition with absolute coordinates"; //--- ToDo: Localize;
		};
		class FHQ_DebugConsole_OPSSave: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_Save";

			idc = 1604;
			text = "Save mergable mission"; //--- ToDo: Localize;
			x = 0.881562 * safezoneW + safezoneX;
			y = 0.885 * safezoneH + safezoneY;
			w = 0.113437 * safezoneW;
			h = 0.033 * safezoneH;
			tooltip = "Save all objects as mergable mission"; //--- ToDo: Localize;
		};
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT END
		////////////////////////////////////////////////////////

	};
};

#ifndef UNOBTANIUM
/*
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT START (by Varanon, v1.063, #Naguho)
		////////////////////////////////////////////////////////

		class FHQ_DebugConsole_OPSOrientation: FHQRscSlider
		{
			onSliderPosChanged = "[_this select 1] call FHQ_DebugConsole_OPSSetRotation;";

			idc = 1900;
			x = 0.00499997 * safezoneW + safezoneX;
			y = 0.973 * safezoneH + safezoneY;
			w = 0.28875 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class FHQ_DebugConsole_OPSUp: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_moveUp;";

			idc = 1600;
			text = "Up"; //--- ToDo: Localize;
			x = 0.00499997 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSDown: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_moveDown;";

			idc = 1601;
			text = "Down"; //--- ToDo: Localize;
			x = 0.00499997 * safezoneW + safezoneX;
			y = 0.841 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSForw: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_moveForward;";

			idc = 1602;
			text = "Front"; //--- ToDo: Localize;
			x = 0.0565625 * safezoneW + safezoneX;
			y = 0.852 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSBack: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_moveBackward;";

			idc = 1603;
			text = "Back"; //--- ToDo: Localize;
			x = 0.0565625 * safezoneW + safezoneX;
			y = 0.896 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSLeft: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_moveLeft;";

			idc = 1604;
			text = "Left"; //--- ToDo: Localize;
			x = 0.0204687 * safezoneW + safezoneX;
			y = 0.896 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSRight: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_moveRight;";

			idc = 1605;
			text = "Right"; //--- ToDo: Localize;
			x = 0.0926562 * safezoneW + safezoneX;
			y = 0.896 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSLabelX: FHQRscText
		{
			idc = 1000;
			text = "Pos"; //--- ToDo: Localize;
			x = 0.154531 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.0257812 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSRho: FHQRscText
		{
			idc = 1003;
			text = "R"; //--- ToDo: Localize;
			x = 0.164844 * safezoneW + safezoneX;
			y = 0.929 * safezoneH + safezoneY;
			w = 0.0103125 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSPosX: FHQRscEdit
		{
			onKillFocus = "call FHQ_DebugConsole_OPSSetX";

			idc = 1400;
			x = 0.185469 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.108281 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSPosY: FHQRscEdit
		{
			onKillFocus = "call FHQ_DebugConsole_OPSSetY";

			idc = 1401;
			x = 0.185469 * safezoneW + safezoneX;
			y = 0.841 * safezoneH + safezoneY;
			w = 0.108281 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSPosZ: FHQRscEdit
		{
			onKillFocus = "call FHQ_DebugConsole_OPSSetZ";

			idc = 1402;
			x = 0.185469 * safezoneW + safezoneX;
			y = 0.885 * safezoneH + safezoneY;
			w = 0.108281 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSAngle: FHQRscEdit
		{
			onKillFocus = "call FHQ_DebugConsole_OPSSetRotationText";

			idc = 1403;
			x = 0.185469 * safezoneW + safezoneX;
			y = 0.929 * safezoneH + safezoneY;
			w = 0.108281 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_ObjectList: FHQRscListbox
		{
			onLBSelChanged = "[_this select 1] call FHQ_DebugConsole_OPSSelectObject;";

			idc = 1500;
			x = 0.304062 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.187 * safezoneH;
		};
		class FHQ_DebugConsole_OPSAdd: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_Create;";

			idc = 1606;
			text = "Add"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSDel: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_Delete";

			idc = 1607;
			text = "Del"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.841 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSSave: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_OPS_Save";

			idc = 1608;
			text = "Save"; //--- ToDo: Localize;
			x = 0.5 * safezoneW + safezoneX;
			y = 0.951 * safezoneH + safezoneY;
			w = 0.0309375 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class FHQ_DebugConsole_OPSClasses: FHQRscListbox
		{
			onLBSelChanged = "[_this select 1] call FHQ_DebugConsole_OPSSelectClass;";

			idc = 1501;
			x = 0.613437 * safezoneW + safezoneX;
			y = 0.819 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.165 * safezoneH;
		};
		class FHQ_DebugConsole_OPSSelCat: FHQRscCombo
		{
			onLBSelChanged	= "FHQ_DebugConsole_ClassCatLast = _this select 1; call FHQ_DebugConsole_ClassFilterList;";

			idc = 2100;
			x = 0.613437 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.185625 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class FHQ_DebugConsole_OPSStepLabel: FHQRscText
		{
			idc = 1002;
			text = "Step"; //--- ToDo: Localize;
			x = 0.0488541 * safezoneW + safezoneX;
			y = 0.800704 * safezoneH + safezoneY;
			w = 0.0257812 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class FHQ_DebugConsole_OPSStep: FHQRscEdit
		{
			onKillFocus = "call FHQ_DebugConsole_OPSSetStep";

			idc = 1404;
			text = "1.0"; //--- ToDo: Localize;
			x = 0.0771875 * safezoneW + safezoneX;
			y = 0.797 * safezoneH + safezoneY;
			w = 0.0464063 * safezoneW;
			h = 0.033 * safezoneH;
		};
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT END
		////////////////////////////////////////////////////////

*/
#endif
