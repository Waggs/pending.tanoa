//--- SKL_DebugMenu.sqf
//--- Requires "special\SKL_DebugSetup.sqf"
//--- ver 2.0 2015-05-24

#include "ids.h"

_enabled  = SKL_DebugArray select 0;
_admins   = SKL_DebugArray select 1;
_author   = SKL_DebugArray select 2;
_allowAll = SKL_DebugArray select 3;

if (!isDedicated) then { waitUntil { local player; player == player; }; };

_allowed = false;

// Allow admins
if (_enabled) then {
    _allowed = _allowAll;
    { if ((name player) == _x) then { _allowed = true; } } count _admins;
};

// Allow author
if (_author) then {
    if (((name player) == getText(missionConfigFile >> "author")) || (!isNull (getAssignedCuratorLogic player))) then { _allowed = true; };
};

// Allow Zeus
if (!isNull (getAssignedCuratorLogic player)) then {_allowed = true; };

SKL_DebugRespawn = compileFinal "_this addAction [""<t color='#ffcc00'>Debug Menu</t>"", '_handle = CreateDialog ""SKL_DEBUG_DIALOG"";', 0, -98, false];";
SKL_DebugSetFog = compileFinal "(_this select 0) setFog [(_this select 1),(_this select 2),(_this select 3)];";
SKL_DebugDate = compileFinal "setDate _this;";
SKL_DebugWeather = compileFinal "skipTime -24; 86400 setOvercast (((_this) MIN 1) MAX 0); skipTime 24; 0 = [] spawn { sleep 0.1; simulWeatherSync; };";
SKL_DebugCmd = compileFinal "call compile _this";
    
if (_allowed) then {
    SKL_DebugConsole_Camera = compile preprocessFileLineNumbers (PATH + "debugConsole_camera.sqf");
    FHQ_DebugConsole_HousePos = compile preprocessFileLineNumbers (PATH + "debugConsole_housepos.sqf");
    FHQ_DebugConsole_SpawnVehicle = compile preprocessFileLineNumbers (PATH + "debugConsole_spawnVehicle.sqf");
    FHQ_DebugConsole_SpawnWeapon = compile preprocessFileLineNumbers (PATH + "debugConsole_spawnWeapon.sqf");
    FHQ_DebugConsole_SpawnAmmo = compile preprocessFileLineNumbers (PATH + "debugConsole_spawnAmmo.sqf");
    FHQ_DebugConsole_SaveLoadout = compile preprocessFileLineNumbers (PATH + "debugConsole_saveLoadout.sqf");
    FHQ_DebugConsole_Music = compile preprocessFileLineNumbers (PATH + "debugConsole_music.sqf");
    FHQ_DebugConsole_ToggleGroup = compile preprocessFileLineNumbers (PATH + "debugConsole_groupicons.sqf");
    FHQ_DebugConsole_OPS = compile preprocessFileLineNumbers (PATH + "debugConsole_objectEd.sqf");
    //FHQ_DebugConsole_Record = compile preprocessFileLineNumbers (PATH + "debugConsole_record.sqf");
    //FHQ_DebugConsole_ToggleOutputWindow = compile preprocessFileLineNumbers (PATH + "debugConsole_outputconsole.sqf");
    //FHQ_DebugConsole_ShowPositionInit = compile preprocessFileLineNumbers (PATH + "debugConsole_ShowPositionInit.sqf");

    SKL_KeyHandler = { hint 'empty SKL_KeyHandler'; };
    SKL_KeyEventId = -1;
    
    SKL_KeyEventHandler = {
        private "_res";
        _res = false;
        if ((_this select 1) == 28) then { // ENTER key
            [] call SKL_KeyHandler;
            _res = true;
            call SKL_DebugUnWatchKeys; // disable trapping the ENTER key
        };
        _res;
    };
    SKL_DebugWatchKeys = {
        [] spawn {
            if (!isDedicated) then {
                waitUntil {!isNull (findDisplay 46)};
                SKL_KeyEventId = (findDisplay 46) displayAddEventHandler ["KeyDown", "_this call SKL_KeyEventHandler;"];
            };
        };
    };
    SKL_DebugUnWatchKeys = {
        if (SKL_KeyEventId != -1) then { (findDisplay 46) displayRemoveEventHandler ["KeyDown",SKL_KeyEventId]; };
        SKL_KeyEventId = -1;
        SKL_KeyHandler = {};
    };
    
    player addEventHandler ["respawn",{_unit = _this select 0; _unit call SKL_DebugRespawn}];
    player call SKL_DebugRespawn;
};
