// SKL_FhMz.sqf   
// ******** NOTE THE USE OF "T" IS NO LONGER NEEDED, NOW ACCOMPLISHED IN PARAMSARRAY *************
// [type, size, numUnits, trigger, side(optional), skill(optional)] call SKL_FH_MZ
//
// in init.sqf:       SKL_FH_MZ = compile preprocessFileLineNumbers "scripts\SKL_FhMz.sqf";
// in trigger on act: ["MZ", 300, count playableUnits, thisTrigger] call SKL_FH_MZ
// in trigger on act: ["MZ", [100,300], count playableUnits, thisTrigger] call SKL_FH_MZ
// *** NO LONGER USE "T" ****  in trigger on act: [["MI","T"], 300, count playableUnits, thisTrigger] call SKL_FH_MZ
// *** NO LONGER USE "T" **** in trigger on act: [["MZ","T","UR","Marker"], 300, count playableUnits, thisTrigger] call SKL_FH_MZ
// in trigger on act: ["FHG", 300, count playableUnits, thisTrigger] call SKL_FH_MZ
//
// type: either a single item (string) or an array containing items.  
// The array must have one main item, can have one customization, and can have one upsmon. 
// If the upsmon is there, it can be followed by a marker. 
// So the following are valid arrays: ["FH","U"]  ["FH,"T"]  ["FH","T","U"]  ["FH","U","marker"]  ["FH","T","U","marker"] 
//
//          "FH"     - fillHouse (patrol)
//          "FHI"    - fillHouse patrol inside
//          "FHG"    - fillHouse garrison (uses UPSMON script, same as ["FHI","UF"])
//          "MZ"     - militarize with infantry and vehicles 
//          "MZI"    - militarize with infantry 
//          "MZV"    - militarize with vehicles
//          "MZB"    - militarize with boats
//          "MZD"    - militarize with divers
//          "MZO"    - militarize Ocean - divers and boats
//          "MZVS"   - militarize with vehicles stationary (don't patrol)
//          "MZBS"   - militarize with boats stationary (don't patrol)
//          "FH_MZI" - both fillHouse and Militarize with infantry
//          "FH_MZV" - both fillHouse and militarize with vehicles
//          "FH_MZ"  - both fillHouse and militarize with infantry and vehicles 
//
//      							Customization *** THIS IS NO LONGER USED ***
//          						"T"      - dress the units in taliban type clothing
//
//      UPSMON: to use UPSMON, type should be an array of type and upsmon options
//          "U"      - upsmon safe
//			"UA"	 - upsmon aware
//          "US"     - upsmon stealth
//          "UF"     - upsmon fortify
//          "UR"     - upsmon on roads safe
//          "URA"     - upsmon on roads aware
//      If UPSMON is specified using an array, you can add an optional marker to the array for the UPSMON patrol area
//
// size:  The spawn radius in meters. 
//          Can be an single size or array of two sizes.  First is spawn size, the second is patrol size.
//
// count: Number of player units to scale for.  
//          For all players: count playableUnits
//          For the group entering the trigger: count units group (thisList select 0)
//          If count is negative, it indicates to use that exact count.  So a MZV of -2 means spawn 2 vehicles exactly.
//
// trigger: the trigger which indicates radius and center point (usually  thisTrigger)
//          can also be an array [ center, size ] ie.  [ [125,552,1],200]
//
// side:    Optional argument EAST, WEST, RESISTANCE, CIVILIAN (default will be EAST (_SKL_FhMz_enemySide below))
//          can also be string of the above (ie. "East", ...)
//
// skill:   number from 0 to 1 (or "default")
//
// 2.3: 2015-05-23 - Changed usage on Taliban Clothing, now called and changeable from ParamsArray.ext
// Version 2.2  4-21-14
// 2.2: Added Skill optional parameter
// 2.1: Made to work without trigger, fixed bug if only vehicles in MZ, set UPSMON to primarily safe mode
// 2.0: Updated to work with upsmon as garrison
// 1.9: add Garrison scripts & civilian side option
// 1.8: add marker option for UPSMON and UR-road option 
// 1.7: fix _initScript = nil instead of null causing units not to cache if not using upsmon
// 1.6: fix upsmon caching correctly - upsmon controlled units DO cache now
// 1.5: fix bug that broke caching - upsmon controlled units do not cache
// 1.4: add upsmon integration
// 1.3: add FHG, MZVS, MZBS options
// 1.2: fix usage example (remove _nil=)
// 1.1: add MZ diver, boat, ocean
// 1.0: initial release

if (!isServer && hasInterface) exitWith {};
if ((isNull aiSpawnOwner) && !(isServer)) exitWith {};
if (!(isNull aiSpawnOwner) && !(aiSpawnOwner == player)) exitWith {};

_this spawn {
    if (isNil "red_unitsRead") then { waitUntil { !(isNil "red_unitsRead") }; };

    // the density of units spawned and is scaled by count argument (enter number for 10 players over 200 m radius)
    _SKL_FhMz_scaleUnitsPer4SqKM = 30;
    // the density of units spawned and is scaled by count argument
    _SKL_FhMz_scaleVehPer4SqKM   =  1;
    // the random percentage.  Spawn will be the number plus up to this percentage of the total
    _SKL_FhMz_randomPercent      = (["RandomPercentage",0] call BIS_fnc_getParamValue);
    // the enemy side to spawn in (west=1, east=2, independent=3)
    _SKL_FhMz_enemySide          = (["EnemySide",2] call BIS_fnc_getParamValue);
    // split into squads of this size or smaller
    _SKL_FhMz_maxSquadSize       = 8;
    // the default enemy skill level 0 - 1 or "default"
    _SKL_FhMz_defaultSkillLevel  = 0.6;

    _SKL_FhMz_scaleUnitsPer4SqKM = ["FHMZ_PARAM_UNITS_P4K", _SKL_FhMz_scaleUnitsPer4SqKM] call BIS_fnc_getParamValue;
    _SKL_FhMz_scaleVehPer4SqKM = ["FHMZ_PARAM_VEHCILES_P4K", _SKL_FhMz_scaleVehPer4SqKM] call BIS_fnc_getParamValue;
    _SKL_FhMz_randomPercent = ["FHMZ_PARAM_RANDOM_PERCENTAGE", _SKL_FhMz_randomPercent] call BIS_fnc_getParamValue;
    _SKL_FhMz_defaultSkillLevel = ["FHMZ_PARAM_AI_SKILL", _SKL_FhMz_defaultSkillLevel] call BIS_fnc_getParamValue;

    if (isNil "SKL_FH_MZ_CNTR") then { SKL_FH_MZ_CNTR = 0 };
    _fh_mz_cntr = SKL_FH_MZ_CNTR;
    SKL_FH_MZ_CNTR = SKL_FH_MZ_CNTR + 100;

    //diag_log format ["SKL_FH_MZ %1 (%2,%3,%4,%4)",_this,_SKL_FhMz_scaleUnitsPer4SqKM,_SKL_FhMz_scaleVehPer4SqKM,_SKL_FhMz_randomPercent,_SKL_FhMz_defaultSkillLevel];

    _type = _this select 0;
    _upsmonType = "";
    _upsmonMarker = "";
   _option = "";
    if (typeName (_type) == "ARRAY") then {
        private "_index";
        _type = (_this select 0) select 0;
        _index = 1;
        _upsmonType = (_this select 0) select _index;
        if (count (_this select 0) > _index + 1) then {
            _upsmonMarker = (_this select 0) select (_index + 1);
            _upsmonMarker setMarkerAlpha 0;
        };
    };

    _sizeSpawn = _this select 1;
    _sizePatrol = _sizeSpawn;
    if (typeName (_sizeSpawn) == "ARRAY") then {
        _sizeSpawn = (_this select 1) select 0;
        _sizePatrol = (_this select 1) select 1;
    };

    _countPlayers = _this select 2;
    _trig = _this select 3;
    _trigPos = [0,0,0];
    _trigSize = 500;
    if (typeName _trig == "ARRAY") then {
        _trigPos = _trig select 0;
        _trigSize = _trig select 1;
    } else {
        _trigPos = getPos _trig;
        _trigSize = (((triggerArea _trig) select 0) + ((triggerArea _trig) select 1))/2;
    };

    if (count _this > 4) then {
        _side = _this select 4;
        if (typeName _side == "STRING") then {
            _side = toUpper _side;
            switch (_side) do {
                case "EAST":       {_side = EAST};
                case "WEST":       {_side = WEST};
                case "RESISTANCE": {_side = RESISTANCE};
                case "CIVILIAN":   {_side = CIVILIAN};
                default            {_side = EAST};
            };
        };
        switch (_side) do {
            case EAST:       {_SKL_FhMz_enemySide = 2};
            case WEST:       {_SKL_FhMz_enemySide = 1};
            case RESISTANCE: {_SKL_FhMz_enemySide = 3};
            case CIVILIAN:   {_SKL_FhMz_enemySide = 0};
            default          {_SKL_FhMz_enemySide = 2};
        };
    };

    if (count _this > 5) then { _SKL_FhMz_defaultSkillLevel = _this select 5; };
    if (_SKL_FhMz_defaultSkillLevel == 0) then { _SKL_FhMz_defaultSkillLevel = 0.6; };

    _mkr = format ["SKL_FHMZ_%1", _fh_mz_cntr];
     createMarker [_mkr, _trigPos];
    _mkr setMarkerShape "ELLIPSE";
    _mkr setMarkerAlpha 0;
    _mkr setMarkerSize [_sizePatrol, _sizePatrol];

    _units_F  = abs _countPlayers;
    _units_MI = abs _countPlayers;
    _units_MV = abs _countPlayers;
    if (_countPlayers >= 0) then {
        _units_F  = ceil (_countPlayers * (2 * _sizeSpawn / 100) ^ 1.8 * _SKL_FhMz_scaleUnitsPer4SqKM / 160);
        _units_MI = ceil (_countPlayers * (2 * _sizeSpawn / 100) ^ 1.8 * _SKL_FhMz_scaleUnitsPer4SqKM / 160);
        _units_MV = ceil (_countPlayers * (2 * _sizeSpawn / 100) ^ 1.8 * _SKL_FhMz_scaleVehPer4SqKM  / 160);
    };

    if (_sizeSpawn > 0 && _units_F == 0)  then { _units_F  = 1; };
    if (_sizeSpawn > 0 && _units_MI == 0) then { _units_MI = 1; };
    if (_sizeSpawn > 0 && _units_MV == 0) then { _units_MV = 1; };

    _mzInfantry = 0;
    _mzVehicle = 0;
    _mzBoats = 0;
    _fhInfantry = 0;
    _inside = 2;
    _ocean = false;
    _patrol = true;
    _initScript = "";
    _spawnOnly = false;
    switch (toUpper (_type)) do {
        case "FH":     { _fhInfantry = _units_F; };
        case "FHI":    { _fhInfantry = _units_F; _inside = 1 };
        case "MZI":    { _mzInfantry = _units_MI; };
        case "MZV":    { _mzVehicle  = _units_MV; };
        case "MZVS":   { _mzVehicle  = _units_MV; _patrol = false; };
        case "MZ":     { _mzInfantry = _units_MI; _mzVehicle = _units_MV; };
        case "MZB":    { _mzVehicle  = _units_MV; _ocean = true };
        case "MZBS":   { _mzVehicle  = _units_MV; _ocean = true; _patrol = false; };
        case "MZD":    { _mzInfantry = _units_MV; _ocean = true };
        case "MZO":    { _mzInfantry = _units_MI; _mzVehicle = _units_MV; _ocean = true };
        case "FH_MZI": { _mzInfantry = _units_MI; _fhInfantry = _units_F; };
        case "FH_MZV": { _mzVehicle  = _units_MV; _fhInfantry = _units_F; };
        case "FH_MZ":  { _mzInfantry = _units_MI; _mzVehicle = _units_MV;  _fhInfantry = _units_F; };
        case "FHG":    { _fhInfantry = _units_F; _inside = 1; _patrol = false; _spawnOnly = true;_upsmonType = "UF"; };
        default        { _s = format ["Error in SKL_FhMz: invalid type %1 at %2", _type, _trigPos]; hint _s; diag_log _s; };
    };

    if (_upsmonType != "") then {
        _spawnOnly = true;
        _patrol = false;
        _showmarker = "HIDEMARKER"; if (pr_deBug == 2) then { _showmarker = "SHOWMARKER"; };
        _upsmonOptions = format ["'SPAWNED', '%1', 'EXTERNAL_CACHE'", _showmarker];
        switch (toUpper (_upsmonType)) do {
            case "U":   { _upsmonOptions = _upsmonOptions + ", 'NOWAIT', 'SAFE'"; };
            case "UA":  { _upsmonOptions = _upsmonOptions + ", 'NOWAIT', 'AWARE'"; };
            case "US":  { _upsmonOptions = _upsmonOptions + ", 'NOWAIT', 'STEALTH'"; };
            case "UF":  { _upsmonOptions = _upsmonOptions + ", 'SAFE', 'FORTIFY', 'NOFOLLOW', 'RANDOMA', 'NOWP2', 'NOSMOKE'";};
            case "UR":  { _upsmonOptions = _upsmonOptions + ", 'NOWAIT', 'SAFE', 'COLUMN', 'ONROAD'"; };
            case "URA":  { _upsmonOptions = _upsmonOptions + ", 'NOWAIT', 'AWARE', 'ONROAD'"; };
            default     { _s = format ["Error in SKL_FhMz: invalid upsmon type %1 at %2", _upsmonType, _trigPos];hint _s; diag_log _s; };
        };
        if (_upsmonMarker == "") then { _upsmonMarker = _mkr; };
        if (isNil "FHMZ_UPSMON") then { FHMZ_UPSMON = compile preprocessFileLineNumbers 'scripts\UPSMON\UPSMON.sqf'; };
        _initScript = format ["if (_this == (leader (group _this))) then { [ _this, '%1', %2] call FHMZ_UPSMON; };", _upsmonMarker, _upsmonOptions];
    };
    if ((["EnemyUnits", 1] call BIS_fnc_getParamValue) == 4) then {
        if (isNil "FHMZ_OPTION_T") then {
            [] spawn {FHMZ_OPTION_T = compile preprocessFileLineNumbers "scripts\PR\scripts\ai\clothing\fn_r_TalibanClothing.sqf"};
            _timeout = time + 3;
            while {(isNil "FHMZ_OPTION_T") && (time < _timeout)};
            if (isNil "FHMZ_OPTION_T") then { FHMZ_OPTION_T = "" };
        };
        _initScript = _initScript + "[_this] call FHMZ_OPTION_T";
    };

    //diag_log format ["SKL_FH_MZ mzI %1, mzV %2, mzB %3, fhI %4", _mzInfantry, _mzVehicle, _mzBoats, _fhInfantry];

    if (isNil "FHMZ_SIMPLECACHE") then { FHMZ_SIMPLECACHE = compile preProcessFileLineNumbers "scripts\LV\LV_functions\LV_fnc_simpleCache.sqf"; };

    _loop = 1;

    if (_mzInfantry > 0 || _mzVehicle > 0 ) then {
        while { (_mzInfantry > 0) || (_mzVehicle > 0) } do {
            if (isNil "FHMZ_MILITARIZE") then { FHMZ_MILITARIZE = compile preProcessFileLineNumbers "scripts\LV\militarize.sqf"; };
            _squad = _mzInfantry Min _SKL_FhMz_maxSquadSize;
            [_mkr, _SKL_FhMz_enemySide, _sizePatrol, [_squad>0 && !_ocean, _squad>0 && _ocean], [_mzVehicle>0 && !_ocean, _mzVehicle>0 && _ocean, false], !_patrol, [_squad,round (_squad * _SKL_FhMz_randomPercent/100)], [_mzVehicle, round (_mzVehicle * _SKL_FhMz_randomPercent/100)], _SKL_FhMz_defaultSkillLevel, nil, _initScript, _fh_mz_cntr, _spawnOnly] call FHMZ_MILITARIZE;

            if (true) then { //_upsmonType == "") then
                [[_fh_mz_cntr], playableUnits, _trigSize, true, true] spawn FHMZ_SIMPLECACHE;
            };

            _fh_mz_cntr = _fh_mz_cntr + 1;

            _mzVehicle = 0;
            _mzInfantry = _mzInfantry - _SKL_FhMz_maxSquadSize;

            sleep 1;
        };
    };

    while { _fhInfantry > 0 } do {
        if (isNil "FHMZ_FILLHOUSE") then { FHMZ_FILLHOUSE = compile preProcessFileLineNumbers "scripts\LV\fillHouse.sqf"; };
        _squad = _fhInfantry Min _SKL_FhMz_maxSquadSize;
        [_mkr, _SKL_FhMz_enemySide, _patrol, _inside, [_squad, round (_squad * _SKL_FhMz_randomPercent/100)], _sizePatrol, _SKL_FhMz_defaultSkillLevel, nil, _initScript, _fh_mz_cntr, false] call FHMZ_FILLHOUSE;

        if (true) then { //_upsmonType == "") then
            [[_fh_mz_cntr], playableUnits, _trigSize, true, true] spawn FHMZ_SIMPLECACHE;
        };

        _fh_mz_cntr = _fh_mz_cntr + 1;

        _fhInfantry = _fhInfantry - _SKL_FhMz_maxSquadSize;

        sleep 1;
    };
};
