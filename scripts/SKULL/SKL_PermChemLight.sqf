// Perpetual Chem Light
//
// _nil = [type, pos] execVM "scripts\SKL_PermChemLight.sqf"
// ie. _nil = ["ChemLight_Red", getMarkerPos "mLED1"] execVM "scripts\SKL_PermChemLight.sqf"
//
//     This method doesn't work at present until BIS fixes allowing init field in a chem light
//     _nil = this execVM "scripts\SKL_PermChemLight.sqf"
//     where 'this' is a chemlight 
//
// Version 1.1  2-13-15
// 1.1: fix usage notes
// 1.0: initial release

private ["_light","_pos","_type","_oldLight"];

_light = objNull;
_type = "ChemLight_Red";
_pos = [0,0,0];

if (typeName _this == "ARRAY") then 
{
    if (isServer) then
    {
        _type = _this select 0;
        _pos = _this select 1;
        _light = _type createVehicle _pos;
    };
}
else
{
    _light = _this;
    _type = typeOf _light;
    _pos = getPos _light;
    if (!local _light) exitWith{};
};

if (isNull _light) exitWith{};

while {true} do
{
    sleep (60*10);
    _oldLight = _light;
    _light = _type createVehicle _pos;
    sleep 2;
    deleteVehicle _oldLight;
};
