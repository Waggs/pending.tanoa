// Call Pilot: use in teleport board to allow JIP'ers to catch up with their group
//
// usage - in init field: _nil = [this,'Pickup needed at Zvezda Base'] execVM "scripts\SKULL\SKL_CallPilot.sqf";
//

if (isDedicated) exitWith {};

waitUntil {player == player};

_allowed_pilots = ["B_Helipilot_F","B_helicrew_F","B_Pilot","O_helipilot_F","O_helicrew_F","O_Pilot_F","I_helipilot_F","I_helicrew_F","I_Pilot_F","C_man_pilot_F"];

SKL_CALL_PILOT = {};
SKL_CALL_PILOT_TIME = 0;

if ((typeOf player) in _allowed_pilots) then
{
    SKL_CALL_PILOT = compile format [
        "if (time - SKL_CALL_PILOT_TIME > 5) then
         {
             titleText ['%1 : ' + name _this, 'PLAIN'];
         };
        ",(_this select 1)];
};

(_this select 0) addAction ["Call for pilot", 
    { [_this select 1,"SKL_CALL_PILOT"] spawn BIS_fnc_MP;
     hint 'Pilot called';
    }];
	
