// SKL_PlayerMarkers.sqf
//
// used by SKL_PlayerMenu.sqf


if (isNil "SKL_PM_ALLOW_ENEMY") then { SKL_PM_ALLOW_ENEMY = true; };
if (isNil "SKL_MARKERS_TYPE") then { SKL_MARKERS_TYPE = "SELF"; };
if (isNil "SKL_PM_FORCE_MARKER") then { SKL_PM_FORCE_MARKER = ""; };

SKL_MARKER_ENEMIES = false;
SKL_MARKER_COUNTER = 0;

SKL_SetMarkerType = {
    SKL_MARKERS_TYPE = _this;
};

SKL_ShowEnemyMarkers = {
    SKL_MARKER_ENEMIES = (SKL_PM_ALLOW_ENEMY) && (_this > 0);
};

if (!isDedicated && !(difficultyEnabled "Map")) then {
    [] spawn {
        private ["_showType","_showEnemy", "_allowEnemy"];
        waitUntil {!(isNull player) && isPlayer player};

        nc_safeName = {
            private ["_name","_cnt"];
            _name = _this getVariable ["SKL_PM_MARKER_NAME",""];
            if (_name == "") then
            {   
                SKL_MARKER_COUNTER = SKL_MARKER_COUNTER + 1;
                _name = format ["SKL_PM_MARKER_%1",SKL_MARKER_COUNTER];
                _this setVariable ["SKL_PM_MARKER_NAME",_name]; 
            };
            _name
        };

        nc_pinMarkerF = {
            private["_unit","_unitList","_unitName","_color","_fMarkerArray","_prevFMarkerArray","_searchList","_list","_marker","_eMarkerBase","_eMarkerCount","_ecnt","_counter","_currentType","_team"];
            _eMarkerBase = "skl_enemy_marker_";
            _eMarkerCount = 0;
            _counter = 0;;
            _prevFMarkerArray = [];
            while {true} do {
                waitUntil {!(isNull player) && isPlayer player};
                _unit = player;
                _fMarkerArray = [];
                _unitList = [];
                _searchList = [];
                _list = [];
                _currentType = if (SKL_PM_FORCE_MARKER == "") then {SKL_MARKERS_TYPE} else {SKL_PM_FORCE_MARKER};
                _named = false;
                _namedLeaders = false;
                _unitSide = side _unit;
                switch (_currentType) do
                {
                    case "NONE": {   
                        _unitList = [];};
                    case "SELF": {
                        _unitList = [_unit];};
                    case "TEAM": {  
                        _unitList = units _unit; };
                    case "TEAM_N": {  
                        _named = true;
                        _unitList = units _unit; };
                    case "SQUAD": { 
                        {if (side (group _x) == _unitSide) then {_unitList set [count _unitList,_x]} } forEach playableUnits; };
                    case "SQUAD_T": {
                        _named = true;
                        _unitList = units _unit;
                        {if (side (group _x) == _unitSide) then {_unitList set [count _unitList,_x]} } forEach playableUnits; };
                    case "SQUAD_N": {
                        _namedLeaders = true;
                        {if (side (group _x) == _unitSide) then {_unitList set [count _unitList,_x]} } forEach playableUnits; };
                    case "SIDE": {  
                        {if (side (group _x) == _unitSide && _unit distance _x < 70) then {_unitList set [count _unitList,_x]} } forEach allUnits; };
                    case "FRIEND": { 
                        {_xSide = (side (group _x)); if ((_xSide != civilian) && (_xSide getFriend _unitSide > 0.5) && (_unit distance _x) < 70) then {_unitList set [count _unitList,_x]} } forEach allUnits;};
                    case "PLAYERS": {
                        {if ((side (group _x)) getFriend _unitSide > 0.5) then {_unitList set [count _unitList,_x]} } forEach playableUnits;};
                };
                                
                {
                    _marker = (_x) call nc_safeName;
                    _fMarkerArray = _fMarkerArray + [_marker];
                    _color = "ColorBlack";
                    _team = assignedTeam _x; // sets _team to nil if not grouped with player
                    if (isNil "_team") then {_team = ""};
                    if (isMultiplayer) then {_team = _x getVariable ["SKL_PM_TeamColor","MAIN"]};
                    if (_namedLeaders) then {_named = leader _x == _x};
                    switch (_team) do
                    {
                        case "RED":    {_color = "ColorRed"};
                        case "GREEN":  {_color = "ColorGreen"};
                        case "BLUE":   {_color = "ColorBlue"};
                        case "YELLOW": {_color = "ColorYellow"};
                        default        
                        { 
                            switch (side (group _x)) do
                            {
                                case west:       {_color = "ColorWEST"};
                                case east:       {_color = "ColorEAST"};
                                case resistance: {_color = "ColorGUER"};
                                case civilian:   {_color = "ColorCIV"};
                                default          {_color = "ColorBlack"};
                            };
                        };
                    };
                    //if !(alive _x) then { _color = "ColorOrange" }; //FAR
                    if (!(alive _x) || !(isNil "FAR_ReviveMode")) then { 
                        if (_x getVariable "FAR_isUnconscious" == 1) then { 
                            _color = "ColorOrange"
                        }; 
                    }; 
                        
                    if (getMarkerColor _marker == "") then {
                        _marker = createMarkerLocal [_marker,[0,0]];
                        _marker setMarkerShapeLocal "ICON";
                        if (_x == _unit) then {
                            _marker setMarkerTypeLocal "mil_triangle";
                            _marker setMarkerSizeLocal [0.75,0.95];
                            _marker setMarkerAlphaLocal 1.0;
                        } else {
                            _marker setMarkerTypeLocal "mil_triangle";
                            _marker setMarkerSizeLocal [0.6,0.8];
                            _marker setMarkerAlphaLocal (if (_team == "NONE") then {0.4} else {0.6});
                        }
                    };
                    _marker setMarkerColorLocal _color;                
                    _marker setMarkerDirLocal (getDir _x);
                    _marker setMarkerPosLocal (getPos _x);

                    if (!(alive _x) || !(isNil "FAR_ReviveMode")) then { 
                        if (_x getVariable "FAR_isUnconscious" == 1) then { 
                            _marker setMarkerTextLocal format ["%1 %2", name _x, "is down"]; 
                        } else { 
                            _marker setMarkerTextLocal (if (_named) then {name _x} else {""} ); 
                        }; 
                    } else {
                        _marker setMarkerTextLocal (if (_named) then {name _x} else {""} ); 
                    }; 
                    sleep 0.1;
                } forEach _unitList;
                    
                {
                    if (!(_x in _fMarkerArray)) then {deleteMarkerLocal _x};
                } forEach _prevFMarkerArray;
                _prevFMarkerArray = _fMarkerArray;
                
                // enemy markers
                if (_counter > 1) then
                {       
                    _ecnt = _eMarkerCount;
                    _eMarkerCount = 0;
                    if (SKL_MARKER_ENEMIES) then
                    {
                        {
                            _side = (_x select 2);
                            if ((_side != civilian) && ( _unitSide getFriend _side < 0.5) && (count crew (_x select 4) > 0)) then 
                            {
                                _eMarkerCount = _eMarkerCount + 1;
                                _marker = format ["%1%2",_eMarkerBase,_eMarkerCount];
                                if (_eMarkerCount >= _ecnt) then
                                {                           
                                    _marker = createMarkerLocal [_marker,[0,0]];
                                    _marker setMarkerShapeLocal "ICON";
                                    _marker setMarkerTypeLocal "mil_box";
                                    _marker setMarkerAlphaLocal 1.0;
                                    _marker setMarkerColorLocal "ColorPink"; 
                                }; 
                                _msize = if ((_x select 1) isKindOf "Man") then {0.6} else {0.8};
                                _marker setMarkerSizeLocal [_msize,_msize];
                                _marker setMarkerPosLocal (_x select 0);
                            };
                        } forEach (_unit nearTargets 800);
                    };
                    for "_i" from _eMarkerCount + 1 to _ecnt do
                    {
                        _marker = format ["%1%2",_eMarkerBase,_i];
                        deleteMarkerLocal _marker;
                    };
                    _counter = 0;
                };
                _counter = _counter + 1;
                
                sleep 1; 
            };
        };
        
        [] spawn nc_pinMarkerF;
    };
    
    // need to capture when units change assigned teams as it isn't broadcast to all the systems
    [] spawn {
        while {true} do
        {
            // Wait for player to be group leader
            waitUntil {sleep 5; leader(player) == player};
            {   
                private ["_color"];
                _color = assignedTeam _x;
                if (isNil "_color") then {_color = ""};
                if (_color != (_x getVariable ["SKL_PM_TeamColor","--"])) then
                {
                    _x setVariable ["SKL_PM_TeamColor",_color, true];
                };
            } forEach(units player);
        };
    };
};