// SKL_HideAway.sqf - V1.0
// Hide AI until needed
//
// _nil = [[units],hide] execVM "scripts\SKL_HideAway.sqf"
//
// units = units to hide
// hide = true to hide, false to restore
//
// in init.sqf: [[],false] execVM "scripts\SKL_HideAway.sqf"
// then in trigger or whatever _nil = [[o1,o2,o3],true] execVM "scripts\SKL_HideAway.sqf"

if (isNil "SKL_HIDEAWAY") then 
{
    SKL_HIDEAWAY = compile '
        _units =_this select 0;
        _hide = _this select 1; 

        { _x hideObject _hide} foreach _units;

        if (_hide) then
        {
           { {_x disableAI "MOVE";} forEach (crew _x); _x disableAI "MOVE"; _x setBehaviour "Stealth"; _x engineOn false; _x setCombatMode "Blue"} forEach _units;
           sleep 10;
           {_x engineOn false; } forEach _units;
           
        }
        else
        {
           { {_x enableAI "MOVE";} forEach (crew _x); _x enableAI "MOVE"; _x setBehaviour "Careless"; _x engineOn true; _x setCombatMode "Red"} forEach _units;
        };
    ';
};

if (count (_this select 0) > 0) then
{
    [_this,"SKL_HIDEAWAY"] spawn BIS_fnc_MP;
}