

_end_mission = ["EndMission", 1] call BIS_fnc_getParamValue;  //mash_Array 0  - ends mission when all players are unconscious

fnc_missionEnd = { 
    if !(isServer) exitWith {}; 
    waitUntil { time > 0 }; 
    private ["_unconscious","_sleep"]; 
    _unconscious  = 0; 
    _sleep  = ["Unconscious", 0] call BIS_fnc_getParamValue; 
    sleep 20; 

    while { true } do { 
        _unconscious = 0; 
        if ({ alive _x } count playableUnits != { _x getVariable ["tcb_ais_agony", false] } count playableUnits) then { _unconscious = 1 }; 
        if (_unconscious == 0) then { 
            sleep _sleep; 
            if ({ alive _x } count playableUnits != { _x getVariable ["tcb_ais_agony", false] } count playableUnits) then { _unconscious = 1 }; 
            if (_unconscious == 0) exitWith { mission_Over = true; publicVariable "mission_Over" }; 
            sleep 5; 
        }; 
    }; 
}; 

if (isServer) then { 
    mission_Over = false; publicVariable "mission_Over"; 
    if (_end_mission == 1) then { [] spawn fnc_missionEnd }; 
}; 
