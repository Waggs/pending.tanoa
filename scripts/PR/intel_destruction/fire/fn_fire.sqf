// by ALIAS 
// nul = [this,_day_time,_life_time,_radius,_damage_inflicted_surround,_kill_vehicle_in_fire,_human] spawn compile preprocessFile "Scripts\fire.sqf"; 

// [carhigh, true, 300, 1, 0.01, true, false] spawn pr_fnc_fire

/* 
   * Script MP and SP compatible. 
   ** Script runs on client side only so the server is not loaded with unnecessary tasks. 
   *** However mind the number of fires they can still cause frames drop. Test and see what it works for you. 

   // not needed, adjusted in script - _day_time                   - boolean, true for day, false for night 
                                 * to keep particles to the minimum i use 2 versions for fire, one for night and one for day, use whatever fits in your mission better 
   _life_time                  - seconds, fire will be put off after the time you set for life time 
   _radius                     - meters, you want to be covered by fire, note that at certain values the fire doesn't look good, so use it wisely 
   _damage_inflicted_surround  - 0..1, amount of damage you to be inflicted upon objects close to fire, use smaller values and test damage is in loop 
   _kill_vehicle_in_fire       - boolean, true if you want the vehicle blowing up when fire is gone, false if you want just to delete the vehicle 
      // not needed, adjusted in script - _human                      - boolean, true if the object set on fire is a footmobile, false if is an object (buildings, wrecks, vehicles etc) 
*/ 

if (!hasInterface) exitWith {}; 

_obj = _this select 0; 
_life_time = _this select 1; 
_radius = _this select 2; 
_damage_inflicted_surround = _this select 3; 
_kill_vehicle_in_fire = _this select 4; 

//  duration - fire lifetime 
[_life_time, _obj, _kill_vehicle_in_fire] spawn { 
    _lft = _this select 0; 
    _obje = _this select 1; 
    _killv = _this select 2; 
    sleep _lft; 

    if (_killv) then { 
        _obje setDammage 1; 
        sleep 30+ random 60; 
    }; 

    deletevehicle _obje; 
}; 

if (_damage_inflicted_surround > 0) then { 
    [_obj, _radius, _damage_inflicted_surround] spawn { 
        _obje = _this select 0; 
        _radiux = _this select 1; 
        _dam = _this select 2; x_dam = _dam; 
        _damNew = 0; 
        while { !isNull _obje } do { 
            _burnable = (nearestObjects [_obje, ["Air", "Car", "House", "Building", "Man", "Ship", "Tank", "TrackedAPC", "WheeledAPC"], _radiux + 3]) - [_obje]; pr_burnable = _burnable; 

            { 
                switch true do { 
                    case (_x isKindOf "Man"):        { _damNew = _dam * 3; }; 
                    case (_x isKindOf "Car"):        { _damNew = _dam * 1.5; }; 
                    case (_x isKindOf "Ship"):       { _damNew = _dam * 1.5; }; 
                    case (_x isKindOf "Air"):        { _damNew = _dam * 1.25; }; 
                    case (_x isKindOf "WheeledAPC"): { _damNew = _dam / 1.5; }; 
                    case (_x isKindOf "TrackedAPC"): { _damNew = _dam / 2.5; }; 
                    case (_x isKindOf "Tank"):       { _damNew = _dam / 3.9; }; 
                    case (_x isKindOf "House"):      { _damNew = _dam / (7 + random 2) * (_radiux * 0.75); }; 
                    case (_x isKindOf "Building"):   { _damNew = _dam / (7 + random 2) * (_radiux * 0.75); }; 
                    default {}; 
                }; 

                _d = damage _x; 
                _d = _d + _damNew; 

                if (_x == player) then {
                    if ((player distance _obje) < _radiux + 3) then { 
                        enableCamShake true; 
                        addCamShake [5, 1, 17]; 
                        playsound "burned"; 
                        player setdammage _d; 
                        sleep 2 + random 1; 
                    }; 
                } else { 
                    if ((_x distance _obje) < _radiux + 3) then { 
                        _x setDamage _d; 
                        sleep 2 + random 1; 
                    }; 
                }; 
                sleep 0.1; 
            } forEach _burnable; 
        }; 
    }; 
}; 

_smokeArr = { 
    _obj = _this select 0; 
    _radius = _this select 1; 

    _smoke = "#particlesource" createVehicleLocal (getPosATL _obj); 
    _smoke setParticleCircle [_radius + 1, [0, 0, 0]]; 

    //particleSource setParticleRandom [lifeTime,        position,      moveVelocity, rotationVelocity, size,          color, randomDirectionPeriod, randomDirectionIntensity, {angle}, bounceOnSurface]
    _smoke setParticleRandom             [      30, [0.25, 0.25, 0], [0.175, 0.175, 0],                0, 0.25, [0, 0, 0, 0.1],                      0,                        0]; 
    _smoke setParticleParams [ ["\A3\data_f\cl_basic", 1, 0, 1], "", "Billboard", 
    /*TimmerPer*/       1, 
    /*Lifetime*/        3 + random 25, 
    /*Position*/        [0, 0, 0], 
    /*MoveVelocity*/    [0, 0, 0.7 + random 0.05], 
    /*Simulation*/
      /*rotationVel,*/  49.5 + random 0.5, 
      /*weight,*/       9.98 + random 0.02, 
      /*volume,*/       7.85 + random 0.05, 
      /*rubbing*/       0.09 + random 0.01, 
    /*Scale*/           [_radius/2+1.5, _radius/2+2.5, _radius/2+4, _radius/2+7, _radius/2+9, _radius/2+15], 
    /*Color*/           [[0, 0, 0, 0.2], [0, 0, 0, 0.2], [0.01, 0.01, 0.01, 0.3], [0, 0, 0, 0.2], [0.01, 0.01, 0.01, 0.2], [0, 0, 0, 0.1]], 
    /*AnimSpeed*/       [0.07 + random 0.01], 
    /*randDirPeriod*/   1, 
    /*randDirIntesity*/ 0, 
    /*onTimerScript*/   "", 
    /*DestroyScript*/   "", 
    /*Follow*/         _obj 
    ]; 
    _smoke setDropInterval 0.1; 
}; 

_heatWaveArr = { 
    _obj = _this select 0; 
    _radius = _this select 1; 

    _heatWaves = "#particlesource" createVehicleLocal (getPosATL _obj); 
    _heatWaves setParticleCircle [_radius + 1, [0, 0, 0]]; 
    _heatWaves setParticleRandom [0, [0.25, 0.25, 0], [0.175, 0.175, 0], 0, 0.25, [0, 0, 0, 0.1], 0, 0]; 
    _heatWaves setParticleParams [["\A3\data_f\ParticleEffects\Universal\Refract.p3d", 1, 0, 1], "", "Billboard", 
    /*TimmerPer*/       1, 
    /*Lifetime*/        7, 
    /*Position*/        [0, 0, 0], 
    /*MoveVelocity*/    [0, 0, 0.75], 
    /*Simulation*/
      /*rotationVel,*/  30 + random 10, 
      /*weight,*/       10.5, 
      /*volume,*/       7.9, 
      /*rubbing*/       random 0.2, 
    /*Scale*/           [_radius/2+3, _radius/2+2, _radius/2+1], 
    /*Color*/           [[0.1, 0.1, 0.1, 0.1], [0.25, 0.25, 0.25, 0.5], [0.5, 0.5, 0.5, 0]], 
    /*AnimSpeed*/       [0.08], 
    /*randDirPeriod*/   1, 
    /*randDirIntesity*/ 0, 
    /*onTimerScript*/   "", 
    /*DestroyScript*/   "", 
    /*Follow*/          _obj, 
    /*angle*/           1, 
    /*onSurface*/       true, 
    /*bounce factor*/   1, 
    /*emissiveColor*/   [[0,0,0,0]] 
    ]; 
    _heatWaves setDropInterval 0.1; 
}; 

_fireArr = { 
    _obj = _this select 0; 
    _radius = _this select 1; 
    _flames = "#particlesource" createVehicleLocal (getPosATL _obj); 
    _flames setParticleCircle [_radius - _radius/9, [0, 0, 0]]; 
    _flames setParticleRandom [1, [0.25, 0.25, 0], [0.175, 0.175, 0.1], 5, 0.25, [0, 0, 0, 0.5], 0.5, 0]; 
    _flames setParticleParams [["\A3\data_f\cl_exp", 1, 0, 1], "", "Billboard", 1, 2.5, [0, 0, 0], [0, 0, 2], 50, 10, 7.9, 0.1, [_radius/2+2,_radius/2+1,_radius/2+0.5], [[1, 1, 1, 1], [0.3, 0.3, 0.3, 0.5], [0, 0, 0, 0]], [0.08], 1, 0, "", "", _obj]; 
    _flames setDropInterval 0.1; 
}; 

_fireGlowArr = { 
    _obj = _this select 0; 
    _radius = _this select 1; 
    _fireGlow = "#lightpoint" createVehicleLocal ([1,1,1]); 
    _fireGlow lightAttachObject [_obj, [0,0,-1]]; 
    _fireGlow setLightAmbient [0.7,0.2,0]; // r,g,b 
    _fireGlow setLightColor [0.7,0.2,0]; // r,g,b 
    _fireGlow setLightUseFlare true; 
    _fireGlow setLightDayLight false; 

    while { !isNull _obj } do { 
        _fireGlow setLightBrightness 2+ random 2; 
        _fireGlow setLightAttenuation [ 
        /*start*/          _radius + random 0.5, 
        /*constant*/       70 + random 30, 
        /*linear*/         200 + random 50, 
        /*quadratic*/      1, 
        /*hardlimitstart*/ 1 + random 0.5, 
        /* hardlimitend*/  300 + random 200 
        ]; 
        sleep 0.1; 
    }; 
    deletevehicle _fireGlow; 
}; 

_nil = [_obj, _radius] spawn _fireArr; 
_nil = [_obj, _radius] spawn _heatWaveArr; 
_nil = [_obj, _radius] spawn _smokeArr; 
_nil = [_obj, _radius] spawn _fireGlowArr; 

while { !isNull _obj } do { if (_obj isKindOf "Man") then { _obj say3D "tipat"; /* scream */ } else { _obj say3D "flames"; }; sleep 3.5; }; 
