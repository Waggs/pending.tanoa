

//sleep (random 15) max 5; 
//[sklo, true, 300, 0.25, 0.01, true, false] spawn pr_fnc_fire

_fireArr                   = _this; 
_obj                       = _this select 0; 
_life_time                 = [_fireArr, 1, 300] call BIS_fnc_paramIn; 
_radius                    = [_fireArr, 2, 1] call BIS_fnc_paramIn; 
_damage_inflicted_surround = [_fireArr, 3, 0.01] call BIS_fnc_paramIn; 
_kill_vehicle_in_fire      = [_fireArr, 4, true] call BIS_fnc_paramIn; 
_random = false; 
_burn = ""; 

if (_life_time == 0) then { _random = true; }; 

if ((_obj isKindOf "Air") || (_obj isKindOf "Car") || (_obj isKindOf "House") || (_obj isKindOf "Man") || (_obj isKindOf "Ship") || (_obj isKindOf "Tank") || (_obj isKindOf "TrackedAPC") || (_obj isKindOf "TrackedAPC")) then { 
    _pos = getpos _obj; 
    _burn = "Land_HelipadEmpty_F" createVehicle _pos; 
    _burn attachTo [_obj, [0,0,-1]]; 
}; 

if (_random) then { 
    _life_time = 180 + (random 1200) max 120;           // _life_time = _this select 2; 
    _radius = (random 2.5) max 1;                       // _radius = _this select 3; 
    //_damage_inflicted_surround = 0.05 + (random 0.05); // _damage_inflicted_surround = _this select 4; 
    _damage_inflicted_surround = 0.05 * _radius;         // _damage_inflicted_surround = _this select 4; 
}; 
//_life_time = 300; 
[[_burn, _life_time, _radius, _damage_inflicted_surround, _kill_vehicle_in_fire], "pr_fnc_fire", true, false] call bis_fnc_MP; 

//sklo setdamage 0; [sklo, 300, 2,0.2,true] spawn pr_fnc_fireRandomizer
