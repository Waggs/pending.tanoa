private ["_bomb", "_time"]; 
_bomb = [_this, 0, objNull, [objNull]] call BIS_fnc_param; 
_time = [_this, 1, 0, [0]] call BIS_fnc_param; 

//Validate parameters 
if (isNull _bomb) exitWith {"Object parameter must not be objNull. Accepted: OBJECT" call BIS_fnc_error}; 

_warn1800 = false; if (_time > 1800) then { _warn1800 = true; }; 
_warn600 = false; if (_time > 600) then { _warn600 = true; }; 
_warn300 = false; if (_time > 300) then { _warn300 = true; }; 
_warn60 = false; if (_time > 60) then { _warn60 = true; }; 
_warn10 = false; if (_time > 10) then { _warn10 = true; }; 
_lastWarn = true; 
_c = 0; 

_text = format ["Detonation: %1", [((_time)/60)+.01,"HH:MM"] call BIS_fnc_timetostring]; 
["<t size='0.6' color='#ff8c00'>" + _text + "</t>" ,0.35,1.185,8,0] spawn BIS_fnc_dynamicText; 

while { _time > 0 && !DEFUSED } do { 
    _time = _time - 1; 
    _c = _c + 1; 
    //hintSilent format ["Bomb Detonation: \n %1", [((_time)/60)+.01,"HH:MM"] call BIS_fnc_timetostring]; 
    if ((_time > 1800) && (_c > 59)) then { 
        _text = format ["Detonation: %1", [((_time)/60)+.01,"HH:MM"] call BIS_fnc_timetostring]; 
        ["<t size='0.6' color='#ff8c00'>" + _text + "</t>" ,0.35,1.185,8,0] spawn BIS_fnc_dynamicText; 
        _c = 0; 
    } else { 
        if ((_time < 1800) && (_time > 600)) then { 
            if (_warn1800) then { 
                _warn1800 = false; 
                ["<t size='0.6' color='#a1ff00'>" + "30 MINUTES UNTIL DETONATION" + "</t>" ,0.35,1.185,8,1] spawn BIS_fnc_dynamicText; 
            }; 
            if ((_time < 1795) && (_c > 44)) then { 
                _text = format ["Detonation: %1", [((_time)/60)+.01,"HH:MM"] call BIS_fnc_timetostring]; 
                ["<t size='0.6' color='#a1ff00'>" + _text + "</t>" ,0.35,1.185,8,0] spawn BIS_fnc_dynamicText; 
                _c = 0; 
            }; 
        } else { 
            if ((_time < 600) && (_time > 300)) then { 
                if (_warn600) then { 
                    _warn600 = false; 
                    ["<t size='0.6' color='#ffff00'>" + "10 MINUTES UNTIL DETONATION" + "</t>" ,0.35,1.185,8,1] spawn BIS_fnc_dynamicText; 
                }; 
                if ((_time < 595) && (_c > 29)) then { 
                    _text = format ["Detonation: %1", [((_time)/60)+.01,"HH:MM"] call BIS_fnc_timetostring]; 
                    ["<t size='0.6' color='#ffff00'>" + _text + "</t>" ,0.35,1.185,8,0] spawn BIS_fnc_dynamicText; 
                    _c = 0; 
                }; 
            } else { 
                if ((_time < 300) && (_time > 60)) then { 
                    if (_warn300) then { 
                        _warn300 = false; 
                        ["<t size='0.6' color='#ffaa00'>" + "5 MINUTES UNTIL DETONATION" + "</t>" ,0.35,1.185,8,1] spawn BIS_fnc_dynamicText; 
                    }; 
                    if ((_time < 295) && (_c > 14)) then { 
                        _text = format ["Detonation: %1", [((_time)/60)+.01,"HH:MM"] call BIS_fnc_timetostring]; 
                        ["<t size='0.6' color='#ffaa00'>" + _text + "</t>" ,0.35,1.185,8,0] spawn BIS_fnc_dynamicText; 
                        _c = 0; 
                    }; 
                } else {  
                    if ((_time < 60) && (_time > 55)) then { 
                        if (_warn60) then { 
                            _warn60 = false; 
                            ["<t size='0.6' color='#ff5400'>" + "1 MINUTE UNTIL DETONATION" + "</t>" ,0.35,1.185,8,1] spawn BIS_fnc_dynamicText; 
                        }; 
                    }; 
                }; 
            }; 
        }; 
    }; 
    if (_time < 55) then { 
        _timeNow = diag_tickTime; 
        _timeFuture = _timeNow + 49; 
        while { ((diag_tickTime < _timeFuture) && !(DEFUSED) && !(ARMED)) } do { 
            _text = format ["Detonation: %1", [(((_timeFuture - diag_tickTime))/60)+.01,"HH:MM:SS:MM"] call BIS_fnc_timetostring]; 
            ["<t size='0.6' color='#ff0000'>" + _text + "</t>" ,0.35,1.185,8,0] spawn BIS_fnc_dynamicText; 
        }; 
        if (DEFUSED) exitWith {}; 
        sleep 2; 
        if (ARMED) then { 
            ["<t size='1' color='#ff0000'>" + "BOMB IS TRIGGERED, TAKE COVER" + "</t>" ,0,0.8,8,1] spawn BIS_fnc_dynamicText; 
        } else { 
            ["<t size='1' color='#ff0000'>" + "TIME'S UP, TAKE COVER" + "</t>" ,0,0.8,8,1] spawn BIS_fnc_dynamicText; 
        }; 
        sleep 3; 
        if (useNuclear) then { 
            [[_bomb], "pr_fnc_nuke", true, false] call BIS_fnc_MP; 
        } else { 
            _blast = createVehicle ["HelicopterExploSmall", position _bomb, [], 0, "NONE"]; 
            { 
                if (_x distance _bomb <= 15) then { _x setDamage 1 }; 
            } forEach allUnits; 
            deleteVehicle _bomb; 
        }; 
        _time = 0; 
    }; 
    if (ARMED) then { _time = 10; ARMED = false; publicVariable "ARMED"; }; 
    sleep 1; 
}; 
