//Parameters
private ["_wire","_cutWire", "_compare"];
wireCut = true;
pr_wrongWire = false;

pr_wireArr  = _this select 0;
pr_wireCount = count pr_wireArr;
_cutWire = [_this,1,"",[""]] call BIS_fnc_param; z_cutWire = _cutWire;

if (isNil "pr_cutWireCount") then { pr_cutWireCount = 0; };
if (isNil "pr_defusedWire") then { pr_defusedWire = []; };
_wiresLeft = 0;

if ((count pr_wireArr > 0) && (pr_cutWireCount == 0) && !(pr_wrongWire)) then {
    waitUntil { wireCut };
    _wire1 = pr_wireArr select 0;
    _compare = [_wire1, _cutWire] call BIS_fnc_areEqual;
    pr_cutWireCount = pr_cutWireCount + 1;
    if (_compare) then {
        pr_defusedWire = pr_defusedWire + [_cutWire];
        _wiresLeft = pr_wireCount - pr_cutWireCount;
        _text = format ["%1 is the correct wire,<br/>only %2 wires left", _cutWire, _wiresLeft];
        ["<t size='0.7' color='#4cff00'>" + _text + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
    } else {
        pr_defusedWire = pr_defusedWire + [_cutWire];
        pr_wrongWire = true;
        _text = format ["%1 is the Wrong wire", _cutWire];
        ["<t size='0.7' color='#ff5000'>" + _text + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
    };
    wireCut = false;
};

if ((count pr_wireArr > 1) && (pr_cutWireCount == 1) && !(pr_wrongWire)) then {
    waitUntil { wireCut };
    _wire2 = pr_wireArr select 1;
    _compare = [_wire2, _cutWire] call BIS_fnc_areEqual;
    pr_cutWireCount = pr_cutWireCount + 1;
    if (_compare) then {
        pr_defusedWire = pr_defusedWire + [_cutWire];
        _wiresLeft = pr_wireCount - pr_cutWireCount;
        _text = format ["%1 is the correct wire,<br/>only %2 wires left", _cutWire, _wiresLeft];
        ["<t size='0.7' color='#4cff00'>" + _text + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
    } else {
        pr_defusedWire = pr_defusedWire + [_cutWire];
        pr_wrongWire = true;
        _text = format ["%1 is the Wrong wire", _cutWire];
        ["<t size='0.7' color='#ff5000'>" + _text + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
    };
    wireCut = false;
};

if ((count pr_wireArr > 2) && (pr_cutWireCount == 2) && !(pr_wrongWire)) then {
    waitUntil { wireCut };
    _wire3 = pr_wireArr select 2;
    _compare = [_wire3, _cutWire] call BIS_fnc_areEqual;
    pr_cutWireCount = pr_cutWireCount + 1;
    if (_compare) then {
        pr_defusedWire = pr_defusedWire + [_cutWire];
        _wiresLeft = pr_wireCount - pr_cutWireCount;
        _text = format ["%1 is the correct wire,<br/>only %2 wires left", _cutWire, _wiresLeft];
        ["<t size='0.7' color='#4cff00'>" + _text + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
    } else {
        pr_defusedWire = pr_defusedWire + [_cutWire];
        pr_wrongWire = true;
        _text = format ["%1 is the Wrong wire", _cutWire];
        ["<t size='0.7' color='#ff5000'>" + _text + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
    };
    wireCut = false;
};

if ((count pr_wireArr > 3) && (pr_cutWireCount == 3) && !(pr_wrongWire)) then {
    waitUntil { wireCut };
    _wire4 = pr_wireArr select 3;
    _compare = [_wire4, _cutWire] call BIS_fnc_areEqual;
    pr_cutWireCount = pr_cutWireCount + 1;
    if (_compare) then {
        pr_defusedWire = pr_defusedWire + [_cutWire];
        _wiresLeft = pr_wireCount - pr_cutWireCount;
        _text = format ["%1 is the correct wire,<br/>only %2 wires left", _cutWire, _wiresLeft];
        ["<t size='0.7' color='#4cff00'>" + _text + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
    } else {
        pr_defusedWire = pr_defusedWire + [_cutWire];
        pr_wrongWire = true;
        _text = format ["%1 is the Wrong wire", _cutWire];
        ["<t size='0.7' color='#ff5000'>" + _text + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
    };
    wireCut = false;
};

waitUntil { ((pr_wireCount == pr_cutWireCount) || pr_wrongWire) };

//compare wires
_compare = [pr_defusedWire, pr_wireArr] call BIS_fnc_areEqual;
sleep 3;
if (_compare) then {
    ["<t size='1' color='#57e241'>" + "BOMB DEFUSED" + "</t>" ,0,0.7,10,0] spawn BIS_fnc_dynamicText;
    DEFUSED = true; publicVariable "DEFUSED";
    playSound "button_close";
} else {
    //["<t size='1' color='#ff5b5b'>" + "BOMB ARMED" + "</t>" ,0,0.7,10,0] spawn BIS_fnc_dynamicText;
    ARMED = true; publicVariable "ARMED";
    playSound "button_wrong";
};

closeDialog 0;
//Return Value
pr_defusedWire
