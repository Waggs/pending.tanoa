//[[_object,_start_time,_total_time], "pr_fnc_nuke_electroPulse", true, false] call bis_fnc_MP; 
private ["_object", "_start_time", "_total_time", "_fuel", "_sleep"]; 

_object = _this select 0; 
_start_time = _this select 1; 
_total_time = _this select 2; 
enableCamShake true; 
addCamShake [5, 20, 10]; 

_sleep = 0.5; 

while { ((alive _object) && (time - _start_time < _total_time)) } do { 
    if (!(vehicle player == player) && (driver vehicle player == player)) then { 
        driver _object action ["lightoff", _object]; 
        _fuel = fuel _object; 
        _object setfuel 0; 
        sleep (random 2 + random 2); 
        _object setfuel _fuel; 
        sleep random _sleep; 
        _sleep = _sleep + 0.5; 
    }; 
}; 
