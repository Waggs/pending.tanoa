//main nuclear explosion script

if !(isServer) exitWith {};
sleep 0.5;
//if !(isNil "nukeUsed") exitWith {};
//nukeUsed = true;
//if (isNil "NuclearWarhead") then { NuclearWarhead = true; publicVariable "NuclearWarhead"; };

_bomb = _this select 0;
_nukeUsed = _bomb getVariable "nukeUsed";
if (_nukeUsed == "true") exitWith {};
_bomb setVariable ["nukeUsed", "true", true];

_bArr = _bomb getVariable "nukeArray"; zz_bArr=_bArr;
_yield     = _bArr select 0;
_radius    = _bArr select 1;
_doDamage  = _bArr select 2;
_fire      = _bArr select 3;
_fireScale = _bArr select 4;
//_irradTime = _bArr select 5;
//_reqGPS    = _bArr select 6;
//_blowSpeed = _bArr select 7;
//_radDamage = _bArr select 8;
//_carArmor  = _bArr select 9;
_blacklist = _bArr select 10;
//_radFree   = _bArr select 11;

_pos = getpos _bomb;
_posx = _pos select 0;
_posy = _pos select 1;
_posz = _pos select 2;

missionNamespace setVariable ["NuclearWarhead", [_bomb, _yield, _radius]];

if (isNil { _radius }) then { _radius = 20 * _yield ^ 0.4; }; //1050
//_destroy = (_radius/1.9) max 450;
_destroy = (_yield / 50) max 30;

_vehicles = [];
_vehicles = ((nearestObjects [_bomb, ["Air", "Car", "Ship", "Tank", "TrackedAPC", "WheeledAPC" ], _radius * 2.9]) - (_bomb nearObjects _destroy) - _blacklist);

_airs = [];
_airs = ((nearestObjects [_bomb, ["Air"], _radius]) - (_bomb nearObjects _destroy) - _blacklist);

_land = [];
_land = ((nearestObjects [_bomb, ["Car", "Man", "Ship", "Tank", "TrackedAPC", "WheeledAPC" ], _radius]) - (_bomb nearObjects _destroy) - _blacklist);

_house = [];
_house = ((nearestObjects [_bomb, ["House","Building"], _radius]) - (_bomb nearObjects _destroy) - _blacklist);

_lights = [];
_lights = ((nearestObjects [_bomb, ["Lamps_Base_F","PowerLines_base_F"], _radius * 2]) - (_bomb nearObjects _destroy) - _blacklist);

_objectsSkip = [];
_objectsSkip = ((nearestObjects [_bomb, ["Ruins","Thing","Small_items","LaserTarget","FireSectorTarget"], _radius]) - (_bomb nearObjects _destroy) - _blacklist);

_objects = [];
_objects = ((nearestObjects [_bomb, [], _radius]) - (_bomb nearObjects _destroy)) - _airs - _vehicles - _house - _objectsSkip - _blacklist;

_exp = "HelicopterExploSmall";
_exp createVehicle [_posx + (1 + random 1), _posy + (1 + random 1), _posz + (10 + random 3)];
_exp createVehicle [_posx + (1 + random 1), _posy + (1 + random 1), _posz + (10 + random 3)];
_exp createVehicle [_posx + (1 + random 1), _posy + (1 + random 1), _posz + (10 + random 3)];
_exp createVehicle [_posx + (1 + random 1), _posy + (1 + random 1), _posz + (10 + random 3)];
sleep 0.75;

sleep 1;

[[_bomb, _yield, _radius], "pr_fnc_nuke_afterShock", true, false, false] call bis_fnc_MP;
[_bomb] spawn {
    _bomb = _this select 0;
    _sound = 10;
    _vol = 2;
    sleep 0.5;

    _loc = "a3\Sounds_F\environment\ambient\quakes\";
    _eq1 = (_loc + "earthquake1.wss");
    _eq2 = (_loc + "earthquake2.wss");
    _eq3 = (_loc + "earthquake3.wss");
    _eq4 = (_loc + "earthquake4.wss");

    playsound3D [_eq2,_bomb,false,_bomb,_sound+_vol]; _vol = _vol + 1;
    sleep 1.5;
    playsound3D [_eq2,_bomb,false,_bomb,_sound+_vol]; _vol = _vol + 2;
    sleep 1.5;
    playsound3D [_eq2,_bomb,false,_bomb,_sound+_vol]; _vol = _vol + 3;
    sleep 1.5;
    playsound3D [_eq3,_bomb,false,_bomb,_sound+_vol]; _vol = _vol + 3;
    sleep 1.5;
    playsound3D [_eq4,_bomb,false,_bomb,_sound+_vol]; _vol = _vol - 2;

    for [{ _volDown = 0 }, { _volDown < _vol }, { _vol = _vol - 1 }] do {
        sleep 3;
        playsound3D [_eq4,_bomb,false,_bomb,_sound+_vol];
    };
};

if (_doDamage) then {
    _objects = (nearestObjects [_bomb,[],_destroy]);
    _objects = _objects - _blacklist;
    {
        if (alive _x) then {
            _x setDamage 1;
            //if (((_x isKindOf "Building") || (_fire)) && (random (4) > 3.5)) then {
            if (_fire) then {
                //if ((_x isKindOf "House") && (random (4) > 3.2)) then {
                if ((_x isKindOf "House") && (random (4) > (3.7 - _fireScale))) then {
                    [_x] spawn {
                        _obj = _this select 0;
                        sleep 60 + (random 60) max 10;
                        [_obj, 0] spawn pr_fnc_fireRandomizer;
                    };
                };
            };
        };
    } forEach _objects;
};

sleep 3;
pr_destruction = true; publicVariable "pr_destruction";
sleep 3; // new adjust
[_bomb, _vehicles, _airs, _land, _house, _lights, _objectsSkip, _objects] spawn pr_fnc_nuke_damage;
sleep 120;
_bomb setVariable ["nukeUsed", "false", true];
