private ["_object","_radius","_rad1","_rad2","_rad3"]; 

_object    = _this select 0; 
_radius    = _this select 1; 
_reqMineDt = _this select 2; 
_rad1  = _radius; 
_rad2  = _radius / 2; 
_rad3  = _radius / 4; 
_sleep = 0; 

while { alive _object } do { 
    waitUntil { player distance _object < _rad1 || !alive _object }; 
    if (_reqMineDt) then { 
        if ("MineDetector" in (items player + assignedItems player)) then { 
            if (player distance _object < _rad1) then { 
                if (player distance _object < _rad2) then { 
                    if (player distance _object < _rad3) then { 
                        playSound "geiger_3"; 
                        _sleep = random 1; 
                    } else { 
                        playSound "geiger_2"; 
                        _sleep = random 2; 
                    }; 
                } else { 
                    playSound "geiger_1"; 
                    _sleep = random 3; 
                }; 
            }; 
        }; 
    } else { 
        if (player distance _object < _rad1) then { 
            if (player distance _object < _rad2) then { 
                if (player distance _object < _rad3) then { 
                    playSound "geiger_3"; 
                    _sleep = random 1; 
                } else { 
                    playSound "geiger_2"; 
                    _sleep = random 2; 
                }; 
            } else { 
                playSound "geiger_1"; 
                _sleep = random 3; 
            }; 
        }; 
    }; 
    sleep _sleep; 
}; 