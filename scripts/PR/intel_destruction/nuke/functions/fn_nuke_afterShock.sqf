if (isDedicated || !hasInterface) exitWith {}; 

_bomb = _this select 0; 
_pos = getpos _bomb; 
_yield = _this select 1; 
_radius = _this select 2; 

_posx = _pos select 0; 
_posy = _pos select 1; 

_unit = player; 
_unit setVariable ["getSmoke", [_bomb,_yield,_radius], false]; 


_unit addMPEventHandler ["MPRespawn", { 
    _smokes = []; 
    _smokes = _unit getVariable "getSmoke"; 
    _bomb = _smokes select 0; 
    _yield = _smokes select 1; 
    _radius = _smokes select 2; 
    [_bomb, _yield, _radius] spawn pr_fnc_nuke_athmo; 
    [_bomb, _yield, _radius] spawn pr_fnc_nuke_fallout; 
}]; 

while { isNil "pr_destruction" } do { 
    [_bomb, _yield, _radius] spawn pr_fnc_nuke_athmo; 
    sleep 0.5 + (random 0.25); 
}; 

[_bomb, _yield, _radius] spawn pr_fnc_nuke_athmo; 
[_bomb, _yield, _radius] spawn pr_fnc_nuke_smoke; 

sleep 0.5 + (random 0.25); 
[_bomb, _yield, _radius] spawn pr_fnc_nuke_athmo; 
enableCamShake true; 
addCamShake [2.1, 20, 30]; 

sleep 0.75; 
[_bomb, _yield, _radius] spawn pr_fnc_nuke_shockwave3; 
[_bomb, _yield, _radius] spawn pr_fnc_nuke_shockwave2; 

sleep 0.25; 
[_bomb, _yield, _radius] spawn pr_fnc_nuke_shockwave2; 
[_bomb, _yield, _radius] spawn pr_fnc_nuke_dust; 

sleep 4; 
[_posx, _posy] exec "scripts\PR\intel_destruction\nuke\functions\ring1.sqs"; // need to convert to sqf 
//[_posx, _posy] execVM "scripts\PR\scripts\misc\intel_destruction\nuke\functions\fn_nuke_ring1.sqf"; 
//[_posx, _posy] spawn pr_fnc_nuke_ring1; 
sleep 1.85; 
[_posx, _posy] exec "scripts\PR\intel_destruction\nuke\functions\ring2.sqs"; // need to convert to sqf 

sleep 60; 
[_bomb, _yield, _radius] spawn pr_fnc_nuke_fallout; 
