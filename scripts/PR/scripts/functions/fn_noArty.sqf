
// [[pad1, 100], "fnc_noArty"] call BIS_fnc_MP; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_noArty start"]; }; 

fnc_noArty = { 
//if (!isDedicated) Then { 
    if ((isDedicated) || !(hasInterface)) exitWith {}; 
    waitUntil { !isNull Player }; 
    if (local player) then { 



    //if ((isDedicated) || !(hasInterface)) exitWith {}; 
    //if (player == player) then { 
        private ["_obj","_radius","_unit","_b"]; 
        _obj      = _this select 0; pr_obj = _obj; 
        _radius   = _this select 1; pr_radius = _radius; 
        _unit     = player; pr_unit = _unit; 
        _b        = 0; 

        while { true } do { 
            if ((_unit distance _obj < _radius) && (_b == 0)) then { 
                noArty = noArty + [_unit]; 
                _b = 1; 
            }; 

            if ((_unit distance _obj > _radius) && (_b == 1)) then { 
                noArty = noArty - [_unit]; 
                _b = 0; 
            }; 

            sleep 4; 
            publicVariable "noArty"; 
        }; 
    }; 
}; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_noArty complete"]; }; 
