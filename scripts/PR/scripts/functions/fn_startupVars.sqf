/* fn_startup.sqf
*  Author: PapaReap
*  Sets initial startup variables
*  ver 1.0 2016-01-31
*/

//if (isNil "startupVarsCompiled") then { startupVarsCompiled = false; };
preInitCompiled = false;
hcCheckCompiled = false;
moreVarsCompiled = false;
postInitComplete = false;
gearCompiled = false;
defusedInitCompiled = false;

//if ((isServer) && (isNil "sCntr")) then {
    //--- checking mods running
    if (isNil "aceOn") then { aceOn = (isClass (configFile >> "CfgPatches" >> "ace_main")); /*publicVariable "aceOn";*/ };              //--- ACE 3 Mod
    if (isNil "aliveOn") then { aliveOn = (isClass (configFile >> "CfgPatches" >> "alive_main")); /*publicVariable "aliveOn";*/ };      //--- Alive Mod
    if (isNil "cbaOn") then { cbaOn = (isClass (configFile >> "CfgPatches" >> "cba_main")); /*publicVariable "cbaOn";*/ };              //--- CBA Mod
    if (isNil "masOn") then { masOn = (isClass (configFile >> "CfgPatches" >> "mas_weapons")); /*publicVariable "masOn";*/ };           //--- Massi Mod
    if (isNil "nqdyOn") then { nqdyOn = (isClass (configfile >> "CfgVehicles" >> "nqdy_medic")); /*publicVariable "nqdyOn";*/ };        //--- NQDY Mod
    if (isNil "rhsOn") then { rhsOn = (isClass (configFile >> "CfgPatches" >> "rhs_main")); /*publicVariable "rhsOn";*/ };              //--- RHS Mod
    if (isNil "tfarOn") then { tfarOn = (isClass (configFile >> "CfgPatches" >> "task_force_radio")); /*publicVariable "tfarOn";*/ };   //--- TFAR Mod
    if (isNil "unsungOn") then { unsungOn = (isClass (configFile >> "CfgPatches" >> "uns_main")); /*publicVariable "unsungOn";*/ };     //--- Unsung Mod
if ((isServer) && (isNil "sCntr")) then {
    if (isNil "noHCSwap") then { noHCSwap = []; publicVariable "noHCSwap"; };                        //--- used for passToHCs.sqf
    if (isNil "deadArray") then { deadArray = []; publicVariable "deadArray"; };                     //--- used for passToHCs.sqf
    if (isNil "hcUnits") then { hcUnits = []; publicVariable "hcUnits"; };                           //--- used for passToHCs.sqf
    if (isNil "hc2Units") then { hc2Units = []; publicVariable "hc2Units"; };                        //--- used for passToHCs.sqf
    if (isNil "serverUnits") then { serverUnits = []; publicVariable "serverUnits"; };               //--- used for passToHCs.sqf
    if (isNil "noArty") then { noArty = []; publicVariable "noArty"; };                              //---
    if (isNil "headlessClients") then { headlessClients = []; publicVariable "headlessClients"; };
    if (isNil "HCPresent") then { HCPresent = false; publicVariable "HCPresent"; };                  //--- used for fn_hcCheck.sqf
    if (isNil "HC_here") then { HC_here = false; publicVariable "HC_here"; };                        //--- used for fn_hcTracker.sqf
    if (isNil "HC2_here") then { HC2_here = false; publicVariable "HC2_here"; };                     //--- used for fn_hcTracker.sqf
    if (isNil "aiSpawnOwner") then { aiSpawnOwner = objNull; publicVariable "aiSpawnOwner"; };       //--- used for fn_aiSpawner.sqf
    if (isNil "sCntr") then { sCntr = 0; publicVariable "sCntr" };                                   //--- used for fn_aiSpawner.sqf
    if (isNil "pr_noPushArray") then { pr_noPushArray = []; publicVariable "pr_noPushArray"; };      //--- used for fn_noPush.sqf
    if (isNil "pr_addDragToAll") then { pr_addDragToAll = false; publicVariable "pr_addDragToAll"; };//--- used for fn_dragBody.sqf

    if (isNil "noNukeDestroy") then { noNukeDestroy = []; publicVariable "noNukeDestroy"; };         //--- used for fn_nuke.sqf
    if (isNil "noRadiation") then { noRadiation = []; publicVariable "noRadiation"; };               //--- used for fn_nuke.sqf
};

diag_log format ["*PR* fn_startupVars complete"];
