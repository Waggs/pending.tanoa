/* 
* Author: PapaReap 
* Function name: pr_fnc_moveObject 
* Move any object to another object or position 
* ver 1.0 - 2016-01-31 
*
* Arguments: 
* 0: <OBJECT> 
* 1: <POSITION> 

* Examples: 
* 0 = [player, car] call pr_fnc_moveObject; 
* [["respawn_west","rwm"], "pr_fnc_moveObject"] call BIS_fnc_MP; 
*/

//private ["_object", "_pos"]; 

//if (isServer) then { 
_object = _this select 0; 
_pos = _this select 1; 
_newPos = [0,0,0]; 

if (typeName _pos == "STRING") then { 
    _newPos = getMarkerPos _pos; 
} else { 
    if (typeName _pos == "ARRAY") then { 
        _newPos = _pos; 
    } else { 
        _newPos = getPos _pos; 
    }; 
}; 

if (typeName _object == "STRING") then { 
    _object setMarkerPos _newPos; 
} else { 
    _object setPos _newPos; 
}; 
//}; 
