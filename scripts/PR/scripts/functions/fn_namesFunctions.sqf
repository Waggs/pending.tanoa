/* fn_namesFunctions.sqf 
    Note: Feel free to add more classes to watch out for following the pattern 
    Usage: 
    (_displayName) call fnc_g_findClassname; //returns className as a string 
    Example: 
    "RGO Grenade" call fnc_g_findClassname; //returns "HandGrenade" 
*/ 
if !(pr_deBug == 0) then { diag_log format ["*PR* fn_namesFunctions start"]; }; 

fnc_g_findClassname = { 
    private "_findClassname"; 
    _findClassname = "getText (_x >> 'displayName') == _this" configClasses (configFile >> "cfgWeapons"); 
    if (count _findClassname > 0) exitWith {configName (_findClassname select 0)}; 
    _findClassname = "getText (_x >> 'displayName') == _this" configClasses (configFile >> "cfgMagazines"); 
    if (count _findClassname > 0) exitWith {configName (_findClassname select 0)}; 
    _findClassname = "getText (_x >> 'displayName') == _this" configClasses (configFile >> "cfgVehicles"); 
    if (count _findClassname > 0) exitWith {configName (_findClassname select 0)}; 
    _findClassname = format ["Item %1 not found", _this]; 
    _findClassname 
}; 

ISSE_Cfg_WeaponInfo = { 
    private["_cfg", "_name", "_DescShort", "_DescLong", "_Pic", "_Type"]; 
    _name = _this; 
    _cfg = (configFile >> "CfgWeapons" >> _name); 
    _DescShort = if (isText(_cfg >> "displayName")) then { getText(_cfg >> "displayName") } else { "/" }; 
    _DescLong  = if (isText(_cfg >> "Library" >> "libTextDesc")) then { getText(_cfg >> "Library" >> "libTextDesc") } else { "/" }; 
    _Pic = if (isText(_cfg >> "picture")) then { getText(_cfg >> "picture") } else { "/" }; 
    _Type = if (isText(_cfg >> "type")) then { parseNumber(getText(_cfg >> "type")) } else { getNumber(_cfg >> "type") }; 
    [_DescShort, _DescLong, _Type, _Pic] 
}; 

ISSE_Cfg_Weapons_GetName  = { (_this call ISSE_Cfg_WeaponInfo) select 0 }; 

ISSE_Cfg_Weapons_GetDesc  = { (_this call ISSE_Cfg_WeaponInfo) select 1 }; 

ISSE_Cfg_Weapons_GetType  = { (_this call ISSE_Cfg_WeaponInfo) select 2 }; 

ISSE_Cfg_Weapons_GetPic  = { (_this call ISSE_Cfg_WeaponInfo) select 3 }; 

ISSE_Cfg_MagazineInfo = { 
    private["_cfg", "_name", "_DescShort", "_DescLong", "_Type", "_Count", "_Pic"]; 
    _name = _this; 
    _cfg = (configFile >> "CfgMagazines" >> _name); 
    _DescShort = if (isText(_cfg >> "displayName")) then { getText(_cfg >>"displayName") } else { "/" }; 
    _DescLong  = if (isText(_cfg >> "Library" >> "libTextDesc")) then { getText(_cfg >> "Library" >>"libTextDesc") } else { "/" }; 
    _Pic = if (isText(_cfg >> "picture")) then { getText(_cfg >> "picture") } else { "/" }; 
    _Type = if (isText(_cfg >> "type")) then { parseNumber(getText(_cfg >>"type")) } else { getNumber(_cfg >> "type") }; 
    _Count = if (isText(_cfg >> "count")) then { parseNumber(getText(_cfg >> "count")) } else { getNumber(_cfg >> "count") }; 
    [_DescShort, _DescLong, _Type, _Pic] 
}; 

// "8Rnd_82mm_Mo_shells" call ISSE_Cfg_Magazine_GetName 
ISSE_Cfg_Magazine_GetName = { (_this call ISSE_Cfg_MagazineInfo) select 0 }; 

ISSE_Cfg_Magazine_GetDesc = { (_this call ISSE_Cfg_MagazineInfo) select 1 }; 

ISSE_Cfg_Magazine_GetType = { (_this call ISSE_Cfg_MagazineInfo) select 2 }; 

ISSE_Cfg_Magazine_GetPic  = { (_this call ISSE_Cfg_MagazineInfo) select 3 }; 

ISSE_Cfg_VehicleInfo = { 
    private["_cfg", "_name", "_DescShort", "_DescLong", "_Type", "_MaxSpeed", "_MaxFuel", "_Pic"]; 
    _name = _this; 
    _cfg  = (configFile >>  "CfgVehicles" >>  _name); 
    _DescShort = if (isText(_cfg >> "displayName")) then { getText(_cfg >> "displayName") } else { "/" }; 
    _DescLong = if (isText(_cfg >> "Library" >> "libTextDesc")) then { getText(_cfg >> "Library" >> "libTextDesc") } else { "/" }; 
    _Pic = if (isText(_cfg >> "picture")) then { getText(_cfg >> "picture") } else { "/" }; 
    _Type = if (isText(_cfg >> "type")) then { parseNumber(getText(_cfg >> "type")) } else { getNumber(_cfg >> "type") }; 
    _MaxSpeed = if (isText(_cfg >> "maxSpeed")) then { parseNumber(getText(_cfg >> "maxSpeed")) } else { getNumber(_cfg >> "maxSpeed") }; 
    _MaxFuel = if (isText(_cfg >>    "fuelCapacity")) then { parseNumber(getText(_cfg >> "fuelCapacity")) } else { getNumber(_cfg >>"fuelCapacity") }; 
    [_DescShort, _DescLong, _Type, _Pic, _MaxSpeed, _MaxFuel] 
}; 

/*
ISSE_Cfg_Weapons_GetName  = { (_this call ISSE_Cfg_WeaponInfo) select 0 }; 

ISSE_Cfg_Weapons_GetDesc  = { (_this call ISSE_Cfg_WeaponInfo) select 1 }; 

ISSE_Cfg_Weapons_GetType  = { (_this call ISSE_Cfg_WeaponInfo) select 2 }; 

ISSE_Cfg_Weapons_GetPic   = { (_this call ISSE_Cfg_WeaponInfo) select 3 }; 
*/

/*
ISSE_Cfg_Magazine_GetName = { // "8Rnd_82mm_Mo_shells" call ISSE_Cfg_Magazine_GetName (_this call ISSE_Cfg_MagazineInfo) select 0 }; 

ISSE_Cfg_Magazine_GetDesc = { (_this call ISSE_Cfg_MagazineInfo) select 1 }; 

ISSE_Cfg_Magazine_GetType = { (_this call ISSE_Cfg_MagazineInfo) select 2 }; 

ISSE_Cfg_Magazine_GetPic  = { (_this call ISSE_Cfg_MagazineInfo) select 3 }; 
*/

ISSE_Cfg_Vehicle_GetName  = { (_this call ISSE_Cfg_VehicleInfo) select 0 }; 

ISSE_Cfg_Vehicle_GetDesc  = { (_this call ISSE_Cfg_VehicleInfo) select 1 }; 

ISSE_Cfg_Vehicle_GetType  = { (_this call ISSE_Cfg_VehicleInfo) select 2 }; 

ISSE_Cfg_Vehicle_GetPic   = { (_this call ISSE_Cfg_VehicleInfo) select 3 }; 

ISSE_Cfg_Vehicle_GetSpeed = { (_this call ISSE_Cfg_VehicleInfo) select 4 }; 

ISSE_Cfg_Vehicle_GetFuel  = { (_this call ISSE_Cfg_VehicleInfo) select 5 }; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_namesFunctions complete"]; }; 