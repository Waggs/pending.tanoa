/* 
*  Author: PapaReap 
*  function name: pr_fnc_upsSpawner 
*  ver 1.0 2016-01-19 

*  [_grp, _area, ""] call pr_fnc_upsSpawner; 
*  [_grp, _area, _upsmonA] call pr_fnc_upsSpawner; 
*/ 

_area = ""; 
_upsmonA = _this; 
_grp  = [_upsmonA, 0, []] call BIS_fnc_paramIn; 
_area = [_upsmonA, 1, "", []] call BIS_fnc_paramIn; 
_dir  = 0; 

if (isNil "area") then { area = 200 }; 
_areaNumber = area; 

if (typeName _area == "ARRAY") then { 
    area = area + 1; 
    _aDir = _area select 0; 
    _bDir = _area select 1; 
    if (count _area > 2) then { _dir = _area select 2 }; 
    _markerPos = [0,0,0]; 
    _markerPos = position (leader _grp); 
    _mkr = format ["area%1", _areaNumber]; 
    createMarker [_mkr, _markerPos]; 
    _mkr setMarkerShape "RECTANGLE"; 
    _mkr setMarkerAlpha 0; 
    _mkr setMarkerSize [_aDir, _bDir]; 
    _mkr setMarkerDir _dir; 
    _area = _mkr; 
} else { 
    if (_area == "") then { 
        area = area + 1; 
        _markerPos = [0,0,0]; 
        _markerPos = position (leader _grp); 
        _mkr = format ["area%1",_areaNumber]; 
        createMarker [_mkr, _markerPos]; 
        _mkr setMarkerShape "RECTANGLE"; 
        _mkr setMarkerAlpha 0; 
        _mkr setMarkerSize [50,50]; 
        _area = _mkr; 
    }; 
}; 

//--- Required arguments 
_spawned = "SPAWNED"; 
_showmarker = "HIDEMARKER"; if (pr_deBug == 2) then { _showmarker = "SHOWMARKER"; }; 
_r_final_array = []; 
_r_final_array = [_grp, _area, _spawned, _showmarker]; 

//--- Optional parameters 
if (count _upsmonA > 2) then { 
    _optionsA = []; 
    _optionsA = _upsmonA select 2; 
    //_r_final_array = _r_array + _optionsA; 
    _r_final_array = _r_final_array + _optionsA; 
}; 

_grp setVariable ["upsArray", _r_final_array, true]; 

[_r_final_array] spawn { 
    private ["_r_final_array"]; 
    _r_final_array = _this select 0; 
    sleep 1; 
    _nil = _r_final_array execVM "scripts\UPSMON\upsmon.sqf"; 
}; 

if !(pr_deBug == 0) then { 
    pr_upsSpawnerUpsmonA = _upsmonA;
    pr_upsSpawnerArea = _area; 
    upsSpawnerArray = _r_final_array; 
    if (pr_deBug == 2) then { hint str _r_final_array; }; 
    diag_log format ["*PR* upsmonArray = %1", _r_final_array]; 
}; 
