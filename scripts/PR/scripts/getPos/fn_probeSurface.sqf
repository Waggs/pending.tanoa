
// Probe function
// A workaround for hasSurface
// Creates probe object at a given position, and checks to see if this position has solid ground (diff in height less than 1m) after simulation 
//RUFS_ProbeSurface = {
// pr_fnc_probeSurface
    private ["_pos", "_bball", "_probe", "_zi", "_zf", "_zdiff", "_vel", "_hasSurface"];
    _object = _this select 0;
	_pos = position _object;
    _zi = _pos select 2;
    _bball = "Rabbit"; // our furry little friend
    _probe = _bball createVehicle _pos;
    _probe setpos _pos;
    _vel = -60;
    _probe setVelocity [0, 0 , -60]; // force the object to crash downward
    while { _vel > 0.1 && _vel < -0.1 } do {
        _vel = velocity _probe select 2;
    };
    _zf = getposATL _probe select 2;
    _zdiff = _zi - _zf;
    // find difference in height
    if (_zdiff > 0.5 || _zdiff < -1) then {
        _hasSurface = false;
        hint "surface unsuitable";
    } else {
        _hasSurface = true;
        hint "surface suitable";
    };
    //deletevehicle _probe;
    // output result
    _hasSurface
//};
