/* fn_pgSwitcher.sqf 
*  Author: PapaReap 

*  [] call compile preprocessFileLineNumbers "scripts\pr\scripts\init\fn_pgSwitcher.sqf"; 
*  script to keep a player group alive by putting a VR Entity into the group if no players are in the group 
*  this can help things like tasks to be persistent even when players don't slot until after game starts 

*  ver 1.0  2014-02-13 initial release: thanks to Skull for his help with this 
*  ver 1.1  2016-03-13 rework code, moved to function 
*/

//if !(isServer) exitWith {}; 

fnc_pgSwitcher = { 
    if (!isServer) exitWith {}; 

    _gameLogic = _this select 0; 
    _groupName = _this select 1; 
    noHCSwap = noHCSwap + [_gameLogic]; publicVariable "noHCSwap"; 
    //if !(pr_deBug == 0) then { diag_log ["*** %1", _groupName]; }; // check why pr_debug undefined

    while { true } do { 
        _group = call compile format ["%1", _groupName]; 
        _count = count units _group; 
        if (_count > 1) then { 
            [_gameLogic] join grpNull; 
        }; 
        if (_count < 1) then { 
            if (isNull _group) then { 
                _group = createGroup west; 
                _group call compile format ["%1 = _this; publicVariable '%1' ",_groupName]; 
            }; 
             [_gameLogic] joinSilent _group; 
        }; 
        sleep 1; 
    }; 
}; 

if !(isServer) exitWith {}; 

if (getMarkerColor "gl" == "") then { 
    _marker = createMarker ["gl",[0,0,0]]; 
}; 

if (!isNil "PG_ai") then { 
    PG_ai setPos getMarkerPos "gl"; 
    _nil = [PG_ai, "PG"] spawn fnc_pgSwitcher; 
}; 

if (!isNil "PG1_ai") then { 
    PG1_ai setPos getMarkerPos "gl"; 
    _nil = [PG1_ai, "PG1"] spawn fnc_pgSwitcher; 
}; 

if (!isNil "PG2_ai") then { 
    PG2_ai setPos getMarkerPos "gl"; 
    _nil = [PG2_ai, "PG2"] spawn fnc_pgSwitcher; 
}; 

if (!isNil "PG3_ai") then { 
    PG3_ai setPos getMarkerPos "gl"; 
    _nil = [PG3_ai, "PG3"] spawn fnc_pgSwitcher; 
}; 

if (!isNil "PG4_ai") then { 
    PG4_ai setPos getMarkerPos "gl"; 
    _nil = [PG4_ai, "PG4"] spawn fnc_pgSwitcher; 
}; 

if (!isNil "PG5_ai") then { 
    PG5_ai setPos getMarkerPos "gl"; 
    _nil = [PG5_ai, "PG5"] spawn fnc_pgSwitcher; 
}; 
