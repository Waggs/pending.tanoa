//--- TaskForceRadio 
if (isClass (configFile >> "CfgPatches" >> "task_force_radio")) then { 
    #include "task_force_radio\functions\common.sqf"; 

    if ((isServer) or (isDedicated)) then { 
        tf_no_auto_long_range_radio = true;  publicVariable "tf_no_auto_long_range_radio"; 
        TF_give_personal_radio_to_regular_soldier = true;  publicVariable "TF_give_personal_radio_to_regular_soldier"; 
        tf_same_sw_frequencies_for_side = true;  publicVariable "tf_same_sw_frequencies_for_side"; 
        tf_same_lr_frequencies_for_side = true;  publicVariable "tf_same_lr_frequencies_for_side"; 
        tf_freq_west = false call TFAR_fnc_generateSwSettings; 
        tf_freq_west set [2,["101.1","102.1","103.1","104.1","105.1","106.1","107.1","108.1"]]; 
        tf_freq_west_lr = false call TFAR_fnc_generateLrSettings; 
        tf_freq_west_lr set [2,["31.1","32.1","33.1","34.1","35.1","36.1","37.1","38.1","39.1"]]; 
        publicVariable "tf_freq_west"; 
        publicVariable "tf_freq_west_lr"; 

        tf_freq_east = false call TFAR_fnc_generateSwSettings; 
        tf_freq_east set [2,["101.1","102.1","103.1","104.1","105.1","106.1","107.1","108.1"]]; 
        tf_freq_east_lr = false call TFAR_fnc_generateLrSettings; 
        tf_freq_east_lr set [2,["31.1","32.1","33.1","34.1","35.1","36.1","37.1","38.1","39.1"]]; 
        publicVariable "tf_freq_east"; 
        publicVariable "tf_freq_east_lr"; 
    }; 
}; 