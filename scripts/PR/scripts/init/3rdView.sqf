/* 
*  Author: PapaReap 
*  Use with ParamsArray.ext 
* 
*  Add to your init.sqf: [] execVM "scripts\PR\scripts\init\3rdView.sqf";   
*  To call from a trigger: 0 = [this,3] execVM "scripts\PR\scripts\init\3rdView.sqf"; 
* 
*  To pause/unpause script during mission, toggle the variable (pause3rdView = bool). example: pause3rdView = true; publicVariable "pause3rdView"; 
*  To exit script completly set the variable (stop3rdView = bool). example: stop3rdView = true; publicVariable "stop3rdView"; 
* 
*  Ver 1.3 2016-04-22 arma changed difficultyEnabled to difficultyOption 
*  Ver 1.2 2016-03-06 code optimization and tweaks 
*  Ver 1.0 2015-05-20 Added ability to call from trigger 
*  Ver 1.0 2014-06-13 initial release 
*/

if (difficultyOption "3rdPersonView" == 0) then { 
    private ["_CameraView","_3rdView","_sleep"]; 
    scopename "camView"; 
    if (isNil "pause3rdView") then { pause3rdView = false; }; 
    if (isNil "stop3rdView") then { stop3rdView = false; }; 
    _CameraView = 0; 

    if ((count _this) > 1) then { 
        _CameraView = _this select 1; 
    } else { 
        _CameraView = (["3rdCameraView", 0] call BIS_fnc_getParamValue); 
    }; 
    _3rdView = 0; 
    _sleep = 0.2; 

    switch (_CameraView) do {      // 3rd Person View 
        case 0: { _3rdView = 0 };  // Allow All Views 
        case 1: { _3rdView = 1 };  // Allow Infantry Only 
        case 2: { _3rdView = 2 };  // Allow Vehicle Only 
        case 3: { _3rdView = 3 };  // Disabled All Views 
    }; 

    if (_3rdView == 0) exitWith {}; 

    if(_3rdView == 1) then { 
        while { (true) } do { 
            while { !(pause3rdView) } do { 
                if (CameraView == "External") then { 
                    if ((vehicle player) != player) then { 
                        (vehicle player) switchCamera "Internal"; 
                    }; 
                }; 
                if (stop3rdView) then { breakto "camView"; }; 
                sleep _sleep; 
            }; 
        }; 
    } else {  
        if(_3rdView == 2) then { 
            while { (true) } do { 
                while { !(pause3rdView) } do { 
                    if (CameraView == "External") then { 
                        if ((vehicle player) == player) then { 
                            player switchCamera "Internal"; 
                        }; 
                    }; 
                    if (stop3rdView) then { breakto "camView"; }; 
                    sleep _sleep; 
                }; 
            }; 
        } else { 
            if(_3rdView == 3) then { 
                while { (true) } do { 
                    while { !(pause3rdView) } do { 
                        if (CameraView == "External") then { 
                            if ((vehicle player) == cameraOn) then { 
                                (vehicle player) switchCamera "Internal"; 
                            }; 
                        }; 
                        if (stop3rdView) then { breakto "camView"; }; 
                        sleep _sleep; 
                    }; 
                }; 
            }; 
        }; 
    }; 
}; 
