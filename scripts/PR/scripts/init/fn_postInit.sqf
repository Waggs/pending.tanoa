/* fn_postInit.sqf
*  Author: PapaReap
*  Call the function upon mission start, after objects are initialized. Passed arguments are ["postInit"]
*  ver 1.0 2016-01-31
*/

if (isNil "fnc_initCompiled") then { fnc_initCompiled = false; };
if (fnc_initCompiled) exitWith {};

//--- more vars
if (isNil "pr_deBug") then { pr_deBug = ["PRdeBug", 0] call BIS_fnc_getParamValue; publicVariable "pr_deBug"; };
if (isNil "PersistTasks") then { PersistTasks = false; publicVariable "PersistTasks"; };
if (["PersistantTasks", 0] call BIS_fnc_getParamValue == 1) then { PersistTasks = true; publicVariable "PersistTasks"; };
if (isNil "prEnemyUnits") then {
    prEnemyUnits = ["EnemyUnits", 1] call BIS_fnc_getParamValue;
    if (prEnemyUnits <= 0) then { prEnemyUnits = 1; };
    publicVariable "prEnemyUnits";
};
if (isNil "prEnemySide") then { prEnemySide = ["EnemySide", 2] call BIS_fnc_getParamValue; publicVariable "EnemySide"; };
if (isNil "prWeapons") then { prWeapons = ["Weapons", 0] call BIS_fnc_getParamValue; publicVariable "prWeapons"; };
if (isNil "prWeather") then { prWeather = ["missionWeather", 0] call BIS_fnc_getParamValue; publicVariable "prWeather"; };
if (isNil "prReviveTime") then { prReviveTime = ["ace_medical_maxReviveTime", 0] call BIS_fnc_getParamValue; publicVariable "prReviveTime"; };

[] call compile preprocessFileLineNumbers "scripts\PR\loadouts\fn_objectVars.sqf";

//--- ai "scripts\PR\scripts\ai\
[] call compile preprocessFileLineNumbers (prAIf + "fn_unitArray.sqf");
[] call compile preprocessFileLineNumbers (prAIf + "fn_redUnits.sqf");
[] call compile preprocessFileLineNumbers (prAIf + "fn_bluUnits.sqf");

//--- functions "scripts\PR\scripts\functions\
[] call compile preprocessFileLineNumbers (prFUNf + "fn_namesFunctions.sqf");  //--- ClassName Finder and Converter
[] call compile preprocessFileLineNumbers (prFUNf + "fn_noArty.sqf");

//--- MISC ---//
[] call compile preprocessFileLineNumbers (prMISCf + "fn_tfrTweeks.sqf");  //--- Task Force Radio Tweeks

//--- "scripts\PR\intel_destruction\
call compile preprocessFile (prINDEf + "dataDownload\downloadData.sqf");  //--- DownloadData
call compile preprocessFile (prINDEf + "dataDownload\uploadData.sqf");  //--- UploadData
call compile preProcessFileLineNumbers (prINDEf + "defuse\fn_defuse.sqf");  //--- Defuse
[] call compile preprocessFileLineNumbers (prNUKEf + "functions\fn_nuke_cover.sqf");  //--- Nuke, line of sight script to avoid nuclear blast

//--- Push
//[] call compile preprocessFileLineNumbers (prDRAGf + "fn_dragBody.sqf");  //--- Can drag dead units

//--- VEHICLE ---//
//--- vehicle "scripts\PR\scripts\vehicle\
[] call compile preprocessFileLineNumbers (prVEHf + "fn_airCargoTransport.sqf");  //--- note: should probably convert to several functions?
//--- script for adding paradrop to vehicles not covered by igiload
[] call compile preprocessFileLineNumbers (prVEHf + "fn_airParaDrop.sqf");

//--- medFunctions "scripts\PR\healthCare\medFunctions\
[] call compile preprocessFileLineNumbers (prHCMFf + "fn_healthCareInit.sqf");  //--- Server healthcare
[player] call compile preprocessFileLineNumbers (prHCMFf + "fn_playerState.sqf");  //--- Player health & crawling
[player] call compile preprocessFileLineNumbers (prHCMFf + "fn_mediCare.sqf");  //--- Medic duties

//--- Loads scripts for PR_Commander
if !(isNil "p1") then { [p1] call compile preprocessFileLineNumbers (prCOMFf + "fn_leadersChoice.sqf"); };

//--- Monitors the state of Headless Clients
[] execVM (prINITf + "fn_hcTracker.sqf");

postInitComplete = true; publicVariable "postInitComplete";
if !(pr_deBug == 0) then { diag_log format ["*PR* fn_postInit complete"]; };

fnc_initCompiled = true;
