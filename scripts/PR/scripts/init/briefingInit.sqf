// setVariables 
//set in previous mission by trigger 
//if isServer then { ["task1complete", "true"] call ALiVE_fnc_setData; }; 
//if isServer then { ["ALIVE_SYS_PROFILE"] call ALiVE_fnc_PauseModule; }; 
//if isServer then { ["ALIVE_SYS_PROFILE","ALIVE_MIL_OPCOM","alive_mil_cqb","ALIVE_MIL_LOGISTICS"] call ALiVE_fnc_pauseModule; }; 
//if isServer then { ["ALIVE_SYS_PROFILE","ALIVE_MIL_OPCOM","alive_mil_cqb","ALIVE_MIL_LOGISTICS"] call ALiVE_fnc_unpauseModule; }; 

if !(isServer) exitWith {}; 

if (isNil "isTask1Complete") then { isTask1complete = "false"; tsk1 = false; publicVariable "tsk1"; }; 
if (isNil "isTask2Complete") then { isTask2complete = "false"; tsk2 = false; publicVariable "tsk2"; }; 
if (isNil "isTask3Complete") then { isTask3complete = "false"; tsk3 = false; publicVariable "tsk3"; }; 
if (isNil "isTask4Complete") then { isTask4complete = "false"; tsk4 = false; publicVariable "tsk4"; }; 
if (isNil "isTask5Complete") then { isTask5complete = "false"; tsk5 = false; publicVariable "tsk5"; }; 
if (isNil "isTask6Complete") then { isTask6complete = "false"; tsk6 = false; publicVariable "tsk6"; }; 
if (isNil "isTask7Complete") then { isTask7complete = "false"; tsk7 = false; publicVariable "tsk7"; }; 
if (isNil "isTask8Complete") then { isTask8complete = "false"; tsk8 = false; publicVariable "tsk8"; };    
if (isNil "isTask9Complete") then { isTask9complete = "false"; tsk9 = false; publicVariable "tsk9"; }; 
if (isNil "isTask10Complete") then { isTask10complete = "false"; tsk10 = false; publicVariable "tsk10"; }; 
if (isNil "isTask11Complete") then { isTask11complete = "false"; tsk11 = false; publicVariable "tsk11"; }; 
if (isNil "isTask12Complete") then { isTask12complete = "false"; tsk12 = false; publicVariable "tsk12"; }; 
if (isNil "isTask13Complete") then { isTask13complete = "false"; tsk13 = false; publicVariable "tsk13"; }; 
if (isNil "isTask14Complete") then { isTask14complete = "false"; tsk14 = false; publicVariable "tsk14"; }; 
if (isNil "isTask15Complete") then { isTask15complete = "false"; tsk15 = false; publicVariable "tsk15"; }; 
if (isNil "isTask16Complete") then { isTask16complete = "false"; tsk16 = false; publicVariable "tsk16"; }; 
if (isNil "isTask17Complete") then { isTask17complete = "false"; tsk17 = false; publicVariable "tsk17"; }; 
if (isNil "isTask18Complete") then { isTask18complete = "false"; tsk18 = false; publicVariable "tsk18"; }; 
if (isNil "isTask19Complete") then { isTask19complete = "false"; tsk19 = false; publicVariable "tsk19"; }; 
if (isNil "isTask20Complete") then { isTask20complete = "false"; tsk20 = false; publicVariable "tsk20"; }; 

if ((isDedicated) && (aliveOn) && (PersistTasks)) then { 
    waitUntil { (!isnil "alive_sys_player") }; 

    if (!isNil {["task1complete"] call ALiVE_fnc_getData}) then { isTask1complete = ["task1complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task2complete"] call ALiVE_fnc_getData}) then { isTask2complete = ["task2complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task3complete"] call ALiVE_fnc_getData}) then { isTask3complete = ["task3complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task4complete"] call ALiVE_fnc_getData}) then { isTask4complete = ["task4complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task5complete"] call ALiVE_fnc_getData}) then { isTask5complete = ["task5complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task6complete"] call ALiVE_fnc_getData}) then { isTask6complete = ["task6complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task7complete"] call ALiVE_fnc_getData}) then { isTask7complete = ["task7complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task8complete"] call ALiVE_fnc_getData}) then { isTask8complete = ["task8complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task9complete"] call ALiVE_fnc_getData}) then { isTask9complete = ["task9complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task10complete"] call ALiVE_fnc_getData}) then { isTask10complete = ["task10complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task11complete"] call ALiVE_fnc_getData}) then { isTask11complete = ["task11complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task12complete"] call ALiVE_fnc_getData}) then { isTask12complete = ["task12complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task13complete"] call ALiVE_fnc_getData}) then { isTask13complete = ["task13complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task14complete"] call ALiVE_fnc_getData}) then { isTask14complete = ["task14complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task15complete"] call ALiVE_fnc_getData}) then { isTask15complete = ["task15complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task16complete"] call ALiVE_fnc_getData}) then { isTask16complete = ["task16complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task17complete"] call ALiVE_fnc_getData}) then { isTask17complete = ["task17complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task18complete"] call ALiVE_fnc_getData}) then { isTask18complete = ["task18complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task19complete"] call ALiVE_fnc_getData}) then { isTask19complete = ["task19complete"] call ALiVE_fnc_getData; }; 
    if (!isNil {["task20complete"] call ALiVE_fnc_getData}) then { isTask20complete = ["task20complete"] call ALiVE_fnc_getData; }; 

    publicVariable "isTask1complete"; diag_log format ["*PR* isTask1complete, %1", isTask1complete]; 
    publicVariable "isTask2complete"; diag_log format ["*PR* isTask2complete, %1", isTask2complete]; 
    publicVariable "isTask3complete"; diag_log format ["*PR* isTask3complete, %1", isTask3complete]; 
    publicVariable "isTask4complete"; diag_log format ["*PR* isTask4complete, %1", isTask4complete]; 
    publicVariable "isTask5complete"; diag_log format ["*PR* isTask5complete, %1", isTask5complete]; 
    publicVariable "isTask6complete"; diag_log format ["*PR* isTask6complete, %1", isTask6complete]; 
    publicVariable "isTask7complete"; diag_log format ["*PR* isTask7complete, %1", isTask7complete]; 
    publicVariable "isTask8complete"; diag_log format ["*PR* isTask8complete, %1", isTask8complete]; 
    publicVariable "isTask9complete"; diag_log format ["*PR* isTask9complete, %1", isTask9complete]; 
    publicVariable "isTask10complete"; diag_log format ["*PR* isTask10complete, %1", isTask10complete]; 
    publicVariable "isTask11complete"; diag_log format ["*PR* isTask11complete, %1", isTask11complete]; 
    publicVariable "isTask12complete"; diag_log format ["*PR* isTask12complete, %1", isTask12complete]; 
    publicVariable "isTask13complete"; diag_log format ["*PR* isTask13complete, %1", isTask13complete]; 
    publicVariable "isTask14complete"; diag_log format ["*PR* isTask14complete, %1", isTask14complete]; 
    publicVariable "isTask15complete"; diag_log format ["*PR* isTask15complete, %1", isTask15complete]; 
    publicVariable "isTask16complete"; diag_log format ["*PR* isTask16complete, %1", isTask16complete]; 
    publicVariable "isTask17complete"; diag_log format ["*PR* isTask17complete, %1", isTask17complete]; 
    publicVariable "isTask18complete"; diag_log format ["*PR* isTask18complete, %1", isTask18complete]; 
    publicVariable "isTask19complete"; diag_log format ["*PR* isTask19complete, %1", isTask19complete]; 
    publicVariable "isTask20complete"; diag_log format ["*PR* isTask20complete, %1", isTask20complete]; 
} else { 
    publicVariable "isTask1complete"; diag_log format ["*PR* isTask1complete, %1", isTask1complete]; 
    publicVariable "isTask2complete"; diag_log format ["*PR* isTask2complete, %1", isTask2complete]; 
    publicVariable "isTask3complete"; diag_log format ["*PR* isTask3complete, %1", isTask3complete]; 
    publicVariable "isTask4complete"; diag_log format ["*PR* isTask4complete, %1", isTask4complete]; 
    publicVariable "isTask5complete"; diag_log format ["*PR* isTask5complete, %1", isTask5complete]; 
    publicVariable "isTask6complete"; diag_log format ["*PR* isTask6complete, %1", isTask6complete]; 
    publicVariable "isTask7complete"; diag_log format ["*PR* isTask7complete, %1", isTask7complete]; 
    publicVariable "isTask8complete"; diag_log format ["*PR* isTask8complete, %1", isTask8complete]; 
    publicVariable "isTask9complete"; diag_log format ["*PR* isTask9complete, %1", isTask9complete]; 
    publicVariable "isTask10complete"; diag_log format ["*PR* isTask10complete, %1", isTask10complete]; 
    publicVariable "isTask11complete"; diag_log format ["*PR* isTask11complete, %1", isTask11complete]; 
    publicVariable "isTask12complete"; diag_log format ["*PR* isTask12complete, %1", isTask12complete]; 
    publicVariable "isTask13complete"; diag_log format ["*PR* isTask13complete, %1", isTask13complete]; 
    publicVariable "isTask14complete"; diag_log format ["*PR* isTask14complete, %1", isTask14complete]; 
    publicVariable "isTask15complete"; diag_log format ["*PR* isTask15complete, %1", isTask15complete]; 
    publicVariable "isTask16complete"; diag_log format ["*PR* isTask16complete, %1", isTask16complete]; 
    publicVariable "isTask17complete"; diag_log format ["*PR* isTask17complete, %1", isTask17complete]; 
    publicVariable "isTask18complete"; diag_log format ["*PR* isTask18complete, %1", isTask18complete]; 
    publicVariable "isTask19complete"; diag_log format ["*PR* isTask19complete, %1", isTask19complete]; 
    publicVariable "isTask20complete"; diag_log format ["*PR* isTask20complete, %1", isTask20complete]; 
}; 

if (isNil "taskHold") then { taskHold = false; publicVariable "taskHold"; }; 
