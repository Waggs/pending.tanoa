//--- Headless Client check 
//diag_log format ["*PR* fn_hcCheck start"]; 

if (isNil "paramsArray") then { paramsArray = [0,0,0]; }; 
if (isNil "prHCs") then { prHCs = ["HeadlessClient", 0] call BIS_fnc_getParamValue; publicVariable "prHCs"; }; 

if (prHCs == 1) then { 
    if (isServer) then { 
        if (isNil "headlessClients") then { headlessClients = []; }; 
        if (!isNil "HC") then { if (!isNull HC) then { headlessClients = headlessClients + [HC]; HC_here = true; PublicVariable "HC_here"; }; }; 
        if (!isNil "HC2") then { if (!isNull HC2) then { headlessClients = headlessClients + [HC2]; HC2_here = true; PublicVariable "HC2_here"; }; }; 
    }; 

    if (!hasInterface && !isServer) then { 
        if (name player == "HC") then { 
            HCPresent = true; publicVariable "HCPresent"; 
            diag_log format ["*PR* %1 HCPresent = %2", name player, HCPresent]; 
        }; 
        if (name player == "HC2") then { 
            HCPresent = true; publicVariable "HCPresent"; 
            diag_log format ["*PR* %1 HCPresent = %2", name player, HCPresent]; 
        }; 
    }; 

    if (isServer) then { 
        if (count headlessClients == 0) then { 
            HCPresent = false; publicVariable "HCPresent"; 
            diag_log format ["*PR* Server HCPresent = %1", HCPresent]; 
        } else { 
            HCPresent = true; publicVariable "HCPresent"; 
            diag_log format ["*PR* Server HCPresent = %1", HCPresent]; 
        }; 
    }; 
} else { 
    if (isServer) then { 
        if (isNil "headlessClients") then { headlessClients = []; }; 
        HCPresent = false; publicVariable "HCPresent"; 

        diag_log format ["*PR* Server HCPresent = %1", HCPresent]; 
    }; 
}; 

publicVariable "headlessClients"; 

diag_log format ["*PR* Headless Clients = %1", headlessClients]; 
//diag_log format ["*PR* fn_hcCheck complete"]; 
