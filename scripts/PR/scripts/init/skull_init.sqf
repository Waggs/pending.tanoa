//--- fn_skull_init.sqf

//--- Skull PlayerMenu 
//--- The entire player menu can be blocked with: false 
//SKL_PM_ALLOW_PLAYERMENU = false; 

//--- Enemy Markers can be forced off with: false 
SKL_PM_ALLOW_ENEMY = false; 

//--- use one of: "NONE" "SELF" "TEAM" "TEAM_N" "SQUAD" "SQUAD_N" "SIDE" "FRIEND" // or SKL_FORCE_MARKER = ""; to return to allowing user selection 
//SKL_PM_FORCE_MARKER = "TEAM"]; 

//--- Skull's Debug Menu Setup 
[] execVM "special\SKL_DebugSetup.sqf"; 

//--- Skulls - Zeus scripts 
execVM "scripts\SKULL\DebugMenu\SKL_Zeus.sqf"; 

//--- Skulls - Reset player rating - allow to enter vehicle 
execVM "scripts\SKULL\SKL_RatingMinimum.sqf"; 

//--- Skulls AI Spawn Script (LV) loader 
SKL_FH_MZ = compile preprocessFileLineNumbers "scripts\SKULL\SKL_FhMz.sqf"; 