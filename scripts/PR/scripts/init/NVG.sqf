/*  NVG.sqf
    Add to your init.sqf: [] execVM "scripts\PR\scripts\init\NVG.sqf";
    Use with ParamsArray.ext
    ver 1.5 2016-03-06  code cleanup
    ver 1.4 2015-11-26  code cleanup
    ver 1.3 2015-05-09  added ace nvg's, removeed agm
    ver 1.2 2015-04-04  added agm nvg's, cleanup script a bit
    ver 1.1 2014-04-23  added - Forced Flashlights On
    ver 1.0 2014-03-15
*/

private ["_nvg","_nvgtrue","_sleep","_removeWest","_removeEast","_removeIndependent","_addLight"];

_nvg = (["missionNVGs", 0] call BIS_fnc_getParamValue);

switch (_nvg) do {
    case 0: { pr_nvg = 0 };  //"NVG On / Off"
    case 1: { pr_nvg = 1 };  //"NVG Off (ALL) / Off"
    case 2: { pr_nvg = 2 };  //"NVG Off (ALL) / Force Flashlights On (ALL)"
    case 3: { pr_nvg = 3 };  //""
    case 4: { pr_nvg = 4 };  //"NVG Off (West) / Off"
    case 5: { pr_nvg = 5 };  //"NVG Off (West) / Force Flashlights On (West)"
    case 6: { pr_nvg = 6 };  //"NVG Off (East) / Off"
    case 7: { pr_nvg = 7 };  //"NVG Off (East) / Force Flashlights On (East)"
    case 8: { pr_nvg = 8 };  //"NVG Off (Independent / Off"
    case 9: { pr_nvg = 9 };  //"NVG Off (Independent) / Force Flashlights On (Independent)"
};
_nvgtrue = pr_nvg;

if (_nvgtrue == 0) exitWith {};

_sleep = 5;

_removeWest = {
    _x unassignItem "NVGoggles";
    _x unassignItem "NVGoggles_mas_mask3";
    _x unassignItem "ACE_NVG_Wide";
    _x unassignItem "ACE_NVG_Gen1";
    _x unassignItem "ACE_NVG_Gen2";
    _x unassignItem "ACE_NVG_Gen4";
    _x removeItem "NVGoggles";
    _x removeItem "NVGoggles_mas_mask3";
    _x removeItem "ACE_NVG_Wide";
    _x removeItem "ACE_NVG_Gen1";
    _x removeItem "ACE_NVG_Gen2";
    _x removeItem "ACE_NVG_Gen4";
};

_removeEast = {
    _x unassignItem "NVGoggles_OPFOR";
    _x removeItem "NVGoggles_OPFOR";
    _x unassignItem "H_HelmetO_ViperSP_ghex_F";
    _x removeItem "H_HelmetO_ViperSP_ghex_F";
    _x unassignItem "H_HelmetO_ViperSP_hex_F";
    _x removeItem "H_HelmetO_ViperSP_hex_F";
};

_removeIndependent = {
    _x unassignItem "NVGoggles_INDEP";
    _x removeItem "NVGoggles_INDEP";
};

_addLight = {
    _x addPrimaryWeaponItem "acc_flashlight";
    _x enableGunLights "forceOn";
};

if (_nvgtrue == 1) then {  //"NVG Off (ALL) / Off"
    while { true } do {
        sleep _sleep;
        {
            if (side _x == west) then {
                [_x] call _removeWest;
            } else {
                if (side _x == east) then {
                    [_x] call _removeEast;
                } else {
                    if (side _x == independent) then {
                        [_x] call _removeIndependent;
                    };
                };
            };
        } forEach (allUnits);
    };
} else {
    if (_nvgtrue == 2) then {  //"NVG Off (ALL) / Force Flashlights On (ALL)"
        while { true } do {
            sleep _sleep;
            {
                if (side _x == west) then {
                    [_x] call _removeWest;
                } else {
                    if (side _x == east) then {
                        [_x] call _removeEast;
                        [_x] call _addLight;
                    } else {
                        if (side _x == independent) then {
                            [_x] call _removeIndependent;
                            [_x] call _addLight;
                        };
                    };
                };
            } forEach (allUnits);
        };
    } else {
        if (_nvgtrue == 4) then {  //"NVG Off (West) / Off"
            while { true } do {
                sleep _sleep;
                {
                    if (side _x == west) then {
                        [_x] call _removeWest;
                    };
                } forEach (allUnits);
            };
        } else {
            if (_nvgtrue == 5) then {  //"NVG Off (West) / Force Flashlights On (West)"
                while { true } do {
                    sleep _sleep;
                    {
                        if (side _x == west) then {
                            [_x] call _removeWest;
                        };
                    } forEach (allUnits);
                };
            } else {
                if (_nvgtrue == 6) then {  //"NVG Off (East) / Off"
                    while { true } do {
                        sleep _sleep;
                        {
                            if (side _x == east) then {
                                [_x] call _removeEast;
                            };
                        } forEach (allUnits);
                    };
                } else {
                    if (_nvgtrue == 7) then {  //"NVG Off (East) / Force Flashlights On (East)"
                        while { true } do {
                            sleep _sleep;
                            {
                                if (side _x == east) then {
                                    [_x] call _removeEast;
                                    [_x] call _addLight;
                                };
                            } forEach (allUnits);
                        };
                    } else {
                        if (_nvgtrue == 8) then {  //"NVG Off (Independent / Off"
                            while { true } do {
                                sleep _sleep;
                                {
                                    if (side _x == independent) then {
                                        [_x] call _removeIndependent;
                                    };
                                } forEach (allUnits);
                            };
                        } else {
                            if (_nvgtrue == 9) then {  //"NVG Off (Independent) / Force Flashlights On (Independent)"
                                while { true } do {
                                    sleep _sleep;
                                    {
                                        if(side _x == independent) then {
                                            [_x] call _removeIndependent;
                                            [_x] call _addLight;
                                        };
                                    } forEach (allUnits);
                                };
                            };
                        };
                    };
                };
            };
        };
    };
};
