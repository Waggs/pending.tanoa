/* fn_hcTracker.sqf 
*  Author: PapaReap 
*  Monitors the state of Headless Clients 
*  ver 1.0 2016-01-31 
*/ 
//if !(pr_deBug == 0) then { diag_log format ["*PR* fn_hcTracker start"]; }; 

if (prHCs == 0) exitWith {}; 

trgHC = createTrigger ["EmptyDetector", [0,0,0]]; 
trgHC setTriggerArea [3, 3, 0, false]; 
trgHC setTriggerActivation ["ANY", "PRESENT", true]; 
trgHC setTriggerStatements [ 
    "this && (((HC) in thisList) && !(isNil Name HC))", 
    "HC_here = true; PublicVariable 'HC_here';", 
    "HC_here = false; PublicVariable 'HC_here'; HC = nil; " 
]; 

trgHCmove = createTrigger ["EmptyDetector", [0,0,0]]; 
trgHCmove setTriggerArea [0, 0, 0, false]; 
trgHCmove setTriggerActivation ["NONE", "PRESENT", false]; 
trgHCmove setTriggerStatements [ 
    "!(isNil ""HC"")", 
    "trgHC setpos getPos HC;", 
    "" 
]; 

trgHC2 = createTrigger ["EmptyDetector", [0,0,0]]; 
trgHC2 setTriggerArea [3, 3, 0, false]; 
trgHC2 setTriggerActivation ["ANY", "PRESENT", true]; 
trgHC2 setTriggerStatements [ 
    "this && (((HC2) in thisList) && !(isNil Name HC2))", 
    "HC2_here = true; PublicVariable 'HC2_here';", 
    "HC2_here = false; PublicVariable 'HC2_here'; HC2 = nil; " 
]; 

trgHC2move = createTrigger ["EmptyDetector", [0,0,0]]; 
trgHC2move setTriggerArea [0, 0, 0, false]; 
trgHC2move setTriggerActivation ["NONE", "PRESENT", false]; 
trgHC2move setTriggerStatements [ 
    "!(isNil ""HC2"")", 
    "trgHC2 setpos getPos HC2;", 
    "" 
]; 


if !(isServer) exitWith {}; 

_hc   = 0; 
_hc2  = 0; 
_noHC = 0; 

while { true } do { 
    if !(isNil "HC") then { 
        if ((_hc == 0) && (HC_here)) then { 
            if !(HC in headlessClients) then { 
                headlessClients = headlessClients + [HC]; publicVariable "headlessClients"; 
                aiSpawnOwner = HC; publicVariable "aiSpawnOwner"; 
                _HCbackHint = format ["%1 is back in action", HC]; 
                logic_HQ sideChat _HCbackHint; 
                logic_HQ_SideChat = _HCbackHint; publicVariable "logic_HQ_SideChat"; 
            }; 
            if !(HCPresent) then { HCPresent = true; publicVariable "HCPresent"; }; 
            _hc = 1; 
            _noHC = 1; 
        } else { 
            if ((_hc == 1) && !(HC_here)) then { 
                if (HC2_here) then { 
                    headlessClients = []; publicVariable "headlessClients"; 
                    headlessClients = headlessClients + [HC2]; publicVariable "headlessClients"; 
                    aiSpawnOwner = HC2; publicVariable "aiSpawnOwner"; 
                } else { 
                    headlessClients = []; 
                    aiSpawnOwner = objNull; publicVariable "aiSpawnOwner"; 
                }; 
                _hc = 0; 
                _HCoutHint = format ["%1 is out of action", HC]; 
                logic_HQ sideChat _HCoutHint; 
                logic_HQ_SideChat = _HCoutHint; publicVariable "logic_HQ_SideChat"; 
            }; 
        }; 
    }; 

    if !(isNil "HC2") then { 
        if ((_hc2 == 0) && (HC2_here)) then { 
            if !(HC2 in headlessClients) then { 
                headlessClients = headlessClients + [HC2]; publicVariable "headlessClients"; 
                aiSpawnOwner = HC2; publicVariable "aiSpawnOwner"; 
                _HC2backHint = format ["%1 is back in action", HC2]; 
                logic_HQ sideChat _HC2backHint; 
                logic_HQ_SideChat = _HC2backHint; publicVariable "logic_HQ_SideChat"; 
            }; 
            if !(HCPresent) then { HCPresent = true; publicVariable "HCPresent"; }; 
            _hc2 = 1; 
            _noHC = 1; 
        } else { 
            if ((_hc2 == 1) && !(HC2_here)) then { 
                if (HC_here) then { 
                    headlessClients = []; publicVariable "headlessClients"; 
                    headlessClients = headlessClients + [HC]; publicVariable "headlessClients"; 
                    aiSpawnOwner = HC; publicVariable "aiSpawnOwner"; 
                } else { 
                    headlessClients = []; 
                    aiSpawnOwner = objNull; publicVariable "aiSpawnOwner"; 
                }; 
                _hc2 = 0; 
                _HC2outHint = format ["%1 is out of action", HC2]; 
                logic_HQ sideChat _HC2outHint; 
                logic_HQ_SideChat = _HC2outHint; publicVariable "logic_HQ_SideChat"; 
            }; 
        }; 
    }; 

    if (_noHC == 1) then { 
        if (!(HC_here) && !(HC2_here)) then { 
            if (HCPresent) then { 
                HCPresent = false; publicVariable "HCPresent"; 
                aiSpawnOwner = objNull; publicVariable "aiSpawnOwner"; 
                _HCSoutHint = format [" All %1 s are out of action", HC]; 
                logic_HQ sideChat _HCSoutHint; 
                logic_HQ_SideChat = _HCSoutHint; publicVariable "logic_HQ_SideChat"; 
            }; 
            _noHC = 0; 
        }; 
    }; 
    sleep 2; 
}; 


