/*  pilotCheck.sqf 
    Add to your init.sqf: [] execVM "scripts\PR\Scripts\pilotCheck.sqf"; 
    Use with ParamsArray.ext 
    Version 1.1 4-23-64  added - Ability to be called from ParamsArray 
    Version 1.0  2-13-14 
    1.0: initial release 

    *** Blufor ***  AH-9 Pawnee, MH-9 Hummingbird, AH-99 Blackfoot, UH-80 Ghost Hawk, UH-80 Ghost Hawk(Camo 
    *** Indep ***   CH-49 Mohawk, A-143 Buzzard (AA), A-143 Buzzard (CAS), WY-55 Helicat, WY-55 Helicat (Green) 
    *** Opfor ***   PO-30 Orca, PO-30 Orca (Black), MI-48 Kalman, MI-48 Kalman (Black) 
*/

if (isDedicated) exitWith {}; 
private ["_allowed_pilots","_crewType","_pc","_trainedAir","_veh"]; 
_pc = (["PilotCheck",0] call BIS_fnc_getParamValue); 
if (_pc == 0) exitWith {}; 

_trainedAir = [ 
"B_Heli_Light_01_armed_F","B_Heli_Light_01_F","B_Heli_Attack_01_F","B_Heli_Transport_01_F","B_Heli_Transport_01_camo_F","B_Plane_CAS_01_F",
"I_Heli_Transport_02_F","I_Plane_Fighter_03_AA_F","I_Plane_Fighter_03_CAS_F","I_Heli_light_03_F","I_Heli_light_03_unarmed_F",
"O_Heli_Light_02_F","O_Heli_Light_02_unarmed_F","O_Heli_Attack_02_F","O_Heli_Attack_02_black_F","O_Plane_CAS_02_F",
"kyo_MH47E_base","kyo_MH47E_Ramp","kyo_MH47E_HC","nqdy_camo_mowhawk_helicopter","nqdy_arctic_mowhawk_helicopter","nqdy_medic_helicopter","nqdy_camo_helicopter"
]; 

_allowed_pilots = ["B_Helipilot_F","B_helicrew_F","B_Pilot_F","O_helipilot_F","O_helicrew_F","O_Pilot_F","I_helipilot_F","I_helicrew_F","I_Pilot_F","C_man_pilot_F"]; 

_pilot = (typeOf player) in _allowed_pilots; 

while {true} do { 
    if(vehicle player != player) then { 
        _veh = vehicle player; 
        if ((typeOf _veh) in _trainedAir && !_pilot) then { 
            _unTrained = [driver _veh] + [_veh turretUnit [0]]; 	//_unTrained = [driver _veh]/*pilot*/ + [_veh turretUnit [0]]/*copilot*/ + [gunner _veh]/*gunner*/;
            if (player in _unTrained) then { 
                hint "You have not been trained to fly this aircraft"; 
                player action ["getOut", _veh]; 
                if (isEngineOn _veh) then {_veh engineOn false}; 
                sleep 1; 
            }; 
        }; 
    }; 
}; 
