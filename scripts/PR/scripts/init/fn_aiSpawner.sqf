/* 
*  Author: PapaReap 
*  function name: pr_fnc_aiSpawner 

*  ver 1.0 2015-10-01 
*  ver 1.1 2016-03-05 optimize for eden update 
*/ 

//if (!(pr_deBug == 0) && (isNil "aiSpawnerInit")) then { diag_log format ["*PR* aiSpawnerInit start"]; }; 
if (prHCs == 0) exitWith {}; 
if !(isServer) exitWith {}; 

//if (isNil "sCntr") then { sCntr = 0; publicVariable "sCntr" }; 

if (HCPresent && (!isNil "HC") && (!isNil "HC2")) then { 
    if ((HC_here) && (HC2_here)) then { 
        if (sCntr == 0) then { 
            aiSpawnOwner = HC; sCntr = 1; 
        } else { 
            if (sCntr == 1) then { aiSpawnOwner = HC2; sCntr = 0 }; 
        }; 
    } else { 
        if (HC_here) then { 
            if (sCntr == 0) then { aiSpawnOwner = HC; sCntr = 0 }; 
        } else { 
            if (HC2_here) then { 
                if (sCntr == 0) then { aiSpawnOwner = HC2; sCntr = 0 }; 
            } else {  
                if (isServer) then { aiSpawnOwner = objNull; sCntr = 0 }; 
            }; 
        }; 
    }; 
} else { 
    if (HCPresent && ((!isNil "HC") || (!isNil "HC2"))) then { 
        if (HC_here) then { 
            if (sCntr == 0) then { 
                aiSpawnOwner = HC; sCntr = 0; 
            } else { 
                if (HC2_here) then { 
                    if (sCntr == 0) then { 
                        aiSpawnOwner = HC2; sCntr = 0; 
                    } else { 
                        if (isServer) then { aiSpawnOwner = objNull; sCntr = 0 }; 
                    }; 
                }; 
            }; 
        }; 
    } else { 
        if (HCPresent && ((isNil "HC") && (isNil "HC2"))) then { 
            aiSpawnOwner = objNull; sCntr = 0; 
        } else { 
            if (isServer && !HCPresent) then { 
                aiSpawnOwner = objNull; sCntr = 0; 
            }; 
        }; 
    }; 
}; 
publicVariable "aiSpawnOwner"; 

if !(pr_deBug == 0) then { diag_log format ["*PR* aiSpawnOwner = %1", aiSpawnOwner]; }; 

//if (!(pr_deBug == 0) && (isNil "aiSpawnerInit")) then { diag_log format ["*PR* aiSpawnerInit complete"]; }; 
//if (isNil "aiSpawnerInit") then { aiSpawnerInit = true; publicVariable "aiSpawnerInit"; }; 
