/* fn_tasker.sqf
   [pr_task1, ""] call fnc_tasker;
   [pr_task1, "assigned"] call fnc_tasker;
   [pr_task1, "update", "assigned"] call fnc_tasker;
*/

if !(isServer) exitWith {};

if (isNil "pr_tasks") then { pr_tasks = []; publicVariable "pr_tasks"; };

private ["_taskArray","_group","_taskName","_taskTitle","_taskDesc","_taskWP","_taskDest","_taskState"];

_taskArray = [_this, 0, [], [[]]] call BIS_fnc_param;  // Task Array
_group = [_taskArray, 0, west] call BIS_fnc_paramIn;   // Group
_taskName = [_taskArray, 1, ""] call BIS_fnc_paramIn;  // Task Name
_taskTitle = [_taskArray, 2, ""] call BIS_fnc_paramIn; // Task Title
_taskDesc = [_taskArray, 3, ""] call BIS_fnc_paramIn;  // Task Description
_taskWP = [_taskArray, 4, ""] call BIS_fnc_paramIn;    // Waypoint Markername: Caret shown in mission
_taskDest = [_taskArray, 5, ""] call BIS_fnc_paramIn;  // Task Markername: Destination/Target shown on map
_taskState = [_taskArray, 6, ""] call BIS_fnc_paramIn; // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed

//if (_taskDest == "") then { };
_taskNameChild = "";
_taskNameParent = "";

if (typename (_taskName) == "ARRAY") then {
    _taskNameChild = [_taskName, 0, ["",""]] call BIS_fnc_paramIn;
    _taskNameParent = [_taskName, 1, ["",""]] call BIS_fnc_paramIn;
    _taskName = [_taskNameChild, _taskNameParent];
};
//  * Example:
//  * ["taskGetVodka", "succeeded", "taskDrink", "taskBeMerry"] call FHQ_TT_setTaskStateAndNext; // need to work on parent/child tasks

_stateOverRide = [_this, 1, ""] call BIS_fnc_param;     // Overrides Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed or Update
_stateUpdate = [_this, 2, ""] call BIS_fnc_param;       // Updates Existing Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed

// [_filter, [_taskName|[_taskName, _parent], _description, _title, _waypoint, _destination|_target, _state]]

if (_taskDest != "") then {
    if (_stateOverRide == "Assigned") then {
                //[_taskId,_description,     _title,_shortTitle,             _target,       _initial, _type]
        [ _group, [_taskName, _taskDesc, _taskTitle, _taskWP, getMarkerPos _taskDest, _stateOverRide ]] call FHQ_fnc_ttAddTasks;
        if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 1: Group_%1, TaskName_%2, TaskTitle_%3, StateOverRide_%4", _group, _taskName, _taskTitle, _stateOverRide]; };
    } else {
        if (_stateOverRide == "Created") then {
            [ _group, [_taskName, _taskDesc, _taskTitle, _taskWP, getMarkerPos _taskDest, _stateOverRide ]] call FHQ_fnc_ttAddTasks;
            if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 2: Group_%1, TaskName_%2, TaskTitle_%3, StateOverRide_%4", _group, _taskName, _taskTitle, _stateOverRide]; };
        } else {
            if ((_stateOverRide == "Succeeded") || (_taskState == "Canceled") || (_taskState == "Cancelled") || (_taskState == "Failed")) then {
                if (typename (_taskName) == "ARRAY") then {
                    [_taskNameChild, _stateOverRide] call FHQ_fnc_ttSetTaskState;
                    if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 3: ChildName_%1, StateOverRide_%2", _taskNameChild, _stateOverRide]; };
                } else {
                    [_taskName, _stateOverRide] call FHQ_fnc_ttSetTaskState;
                    if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 4: TaskName_%1, StateOverRide_%2", _taskName, _stateOverRide]; };
                };
            } else {
                if (_stateOverRide == "Update") then {
                    if (typename (_taskName) == "ARRAY") then {
                        [_taskNameChild, _stateUpdate] call FHQ_fnc_ttSetTaskState;
                        if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 5: TaskNameChild_%1, StateUpdate_%2", _taskNameChild, _stateUpdate]; };
                    } else {
                        [_taskName, _stateUpdate] call FHQ_fnc_ttSetTaskState;
                        if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 6: TaskName_%1, StateUpdate_%2", _taskName, _stateUpdate]; };
                    };
                } else {
                    [ _group, [_taskName, _taskDesc, _taskTitle, _taskWP, getMarkerPos _taskDest, _taskState ]] call FHQ_fnc_ttAddTasks;
                    if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 7: Group_%1, TaskName_%2, TaskTitle_%3, TaskState_%4", _group, _taskName, _taskTitle, _taskState]; };
                };
            };
        };
    };
} else {
    if (_stateOverRide == "Assigned") then {
        [ _group, [_taskName, _taskDesc, _taskTitle, _taskWP, _stateOverRide ]] call FHQ_fnc_ttAddTasks;
        if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 8: Group_%1, TaskName_%2, TaskTitle_%3, StateOverRide_%4", _group, _taskName, _taskTitle, _stateOverRide]; };
    } else {
        if (_stateOverRide == "Created") then {
            [ _group, [_taskName, _taskDesc, _taskTitle, _taskWP, _stateOverRide ]] call FHQ_fnc_ttAddTasks;
            if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 9: Group_%1, TaskName_%2, TaskTitle_%3, StateOverRide_%4", _group, _taskName, _taskTitle, _stateOverRide]; };
        } else {
            if ((_stateOverRide == "Succeeded") || (_taskState == "Canceled") || (_taskState == "Cancelled") || (_taskState == "Failed")) then {
                if (typename (_taskName) == "ARRAY") then {
                    [_taskNameChild, _stateOverRide] call FHQ_fnc_ttSetTaskState;
                    if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 10: TaskNameChild_%1, StateOverRide_%2", _taskNameChild, _stateOverRide]; };
                } else {
                    [_taskName, _stateOverRide] call FHQ_fnc_ttSetTaskState;
                    if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 11: TaskName_%1, StateOverRide_%2", _taskName, _stateOverRide]; };
                };
            } else {
                if (_stateOverRide == "Update") then {
                    if (typename (_taskName) == "ARRAY") then {
                        [_taskNameChild, _stateUpdate] call FHQ_fnc_ttSetTaskState;
                        if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 12: TaskNameChild_%1, StateOverRide_%2", _taskNameChild, _stateOverRide]; };
                    } else {
                        [_taskName, _stateUpdate] call FHQ_fnc_ttSetTaskState;
                        if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 13: TaskName_%1, StateUpdate_%2", _taskName, _stateUpdate]; };
                    };
                } else {
                    [ _group, [_taskName, _taskDesc, _taskTitle, _taskWP, _taskState ]] call FHQ_fnc_ttAddTasks;
                    if !(pr_deBug == 0) then { diag_log format["*PR* fnc_tasker 14: Group_%1, TaskName_%2, TaskTitle_%3, TaskState_%4", _group, _taskName, _taskTitle, _taskState]; };
                };
            };
        };
    };
};

if !(_taskName in pr_tasks) then { pr_tasks = pr_tasks + [_taskName]; }; publicVariable "pr_tasks";
pr_stateOverRide = _stateOverRide;
pr_stateUpdate = _stateUpdate;
pr_taskName = _taskName;
pr_taskNameChild = _taskNameChild;
