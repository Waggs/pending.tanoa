// --------------------- READ FIRST --------------------- //

// This file is for limiting BlueHUD usage on your server.
// It does not have any effects on singleplayer missions.

// If you're not running a server you can completly ignore
// this file and do not need to change any settings. Use
// the in-game settings instead, you can open them by
// pressing Ctrl-Shift-B by default.

// ---------------------- Settings ---------------------- //

// Allowed HUD modes
// Possible Values: BLUEHUD_360 | BLUEHUD_MIDWAY | BLUEHUD_180
// Combine by using +, for example: BLUEHUD_360 + BLUEHUD_180
BlueHUD_allowedHUDModes = BLUEHUD_180 + BLUEHUD_MIDWAY + BLUEHUD_360;

// Show fireteam colors in HUD
BlueHUD_allowTeamColors = true;

// Allow compass in HUD
BlueHUD_allowCompass = true;