class BlueHud { 
    class BlueHud { 
        file = "scripts\PR\scripts\hud_tags\BlueHud\functions"; 
        class initHud {}; 
        class getRole {}; 
        class setRole {}; 
        class vectorRotate {}; 
        class getEyeDir {}; 
        class getColor {}; 
        class initSettingsDialog {}; 
        class unfade {}; 
        class watchColor {}; 
        class buildEH {}; 
        class setServerSettings {}; 
    }; 
}; 