if !(pr_deBug == 0) then { diag_log format ["*PR* bluHudInit start"]; }; 

//_unit = _this; 

//Distribute settings if server 
if (isServer && isMultiplayer) then { 
    //Set constants 
    BLUEHUD_180 = 1; 
    BLUEHUD_MIDWAY = 2; 
    BLUEHUD_360 = 4; 

    //Set default values 
    BlueHUD_allowedHUDModes = BLUEHUD_360 + BLUEHUD_MIDWAY + BLUEHUD_180; 
    BlueHUD_allowTeamColors = true; 
    BlueHUD_allowCompass = true; 

    [[BlueHUD_allowedHUDModes, BlueHUD_allowTeamColors, BlueHUD_allowCompass], "BlueHud_fnc_setServerSettings", true, true] call BIS_fnc_MP; 
}; 

//Shutdown on HC and Dedis 
if (!hasInterface) exitWith {}; 

[] spawn { 
    if !(pr_deBug == 0) then { diag_log format ["*PR* bluHudInit - spawned"]; }; 
    //Load settings stored in profile, if not present use default settings 
    BlueHudSettings = profileNamespace getVariable ["BlueHudSettings", [false, false, true, [0,0,0], false, 0.08]]; 
    //Backwards compatibility 
    if (count BlueHudSettings < 4) then { 
        BlueHudSettings pushBack [0,0,0]; 
        profileNamespace setVariable ['BlueHudSettings', BlueHudSettings]; 
    }; 
    if (count BlueHudSettings < 5) then { 
        BlueHudSettings pushBack false; 
        profileNamespace setVariable ['BlueHudSettings', BlueHudSettings]; 
    }; 
    if (count BlueHudSettings < 6) then { 
        BlueHudSettings pushBack 0.08; 
        profileNamespace setVariable ['BlueHudSettings', BlueHudSettings]; 
    }; 

    BlueHudShift = BlueHudSettings select 3; 
    if (BlueHudSettings select 4) then { 
        BlueHudCurrentAlpha = 0; 
    } else { 
        BlueHudCurrentAlpha = 0.8; 
    }; 
    BlueHudLastUnfade = -100; 

    //Set default server settings 
    BlueHUD_allowedHUDModes = 7; 
    BlueHUD_allowTeamColors = true; 
    BlueHUD_allowCompass = true; 

    //For some reason, calling this to early kills the HUD 
    sleep 2; 
    ("BHUDLayer" call BIS_fnc_rscLayer) cutRsc ["BlueHud", "PLAIN"]; 

    //Center map on [0,0] for map zoom constant calculation 
    (uiNamespace getVariable "BlueHudMap") ctrlMapAnimAdd [0, 0.001, [0, 0, 0]]; 
    ctrlMapAnimCommit (uiNamespace getVariable "BlueHudMap"); 
    waitUntil {ctrlMapAnimDone (uiNamespace getVariable "BlueHudMap")}; 

    //How many square meters per pixel are drawn at a certain zoom level unfortunately depends on the map, 
    //thus we need to calculate the correct map zoom. This is done by checking where it draws [0,0] and [0,10] 
    //on the screen and comparing said distance to the zoom level. 
    _delta = (((uiNamespace getVariable "BlueHudMap") ctrlMapWorldToScreen [0,0]) select 1) - (((uiNamespace getVariable "BlueHudMap") ctrlMapWorldToScreen [0,10]) select 1); 
    //Now we calculate the map's zoom factor (which is directly related to the map size): 
    BlueHUDMapZoomConstant = _delta * 0.001; 
    //And finally the correct zoom factor (the setting saves the distance we want between [0,0] and [0,10] on the screen): 
    _zoom = BlueHUDMapZoomConstant / (BlueHudSettings select 5); 

    //We center this time on a custom "zero point" to make sure we don't get any terrain markers - this can be an issue if we stay at [0, 0] 
    //This way everything seen on screen is actually drawn outside of the map 
    BlueHUDMapZero = [(getNumber (configFile>>"CfgWorlds">>worldName>>"Grid">>"OffsetX")), (getNumber (configFile>>"CfgWorlds">>worldName>>"Grid">>"OffsetY")), 0]; 
    (uiNamespace getVariable "BlueHudMap") ctrlMapAnimAdd [0, _zoom, BlueHUDMapZero]; 
    ctrlMapAnimCommit (uiNamespace getVariable "BlueHudMap"); 
    waitUntil { ctrlMapAnimDone (uiNamespace getVariable "BlueHudMap") }; 

    //Start team color watcher thread 
    [] call BlueHud_fnc_watchColor; 

    //Create EventHandler to draw HUD 
    [] call BlueHud_fnc_buildEH; 

    //Deactivate ST HUD if present 
    if ("st_sthud" in activatedAddons) then { 
        [] spawn { 
            waitUntil { missionNamespace getVariable ["ST_STHud_ShownUI", -1] != -1 }; 
            ST_STHud_ShownUI = 0; 
        }; 
    }; 

    //--- Shift + Ctrl + B = Show HUD settings 
    pr_settingKey = 48; // B 
    pr_settingShift = 1; 
    pr_settingCtrl = 1; 
    pr_settingAlt = 0; 

    _settingKey = pr_settingKey; 
    if (_settingKey == 0) then { _settingKey = 48 }; 
    _settingShift = pr_settingShift == 1; 
    _settingCtrl  = pr_settingCtrl == 1; 
    _settingAlt   = pr_settingAlt == 1; 

    prSettingKeys = [_settingKey, [_settingShift, _settingCtrl, _settingAlt]]; 
    prSettingKeyHandler = { 
        _settingHandled = false; 
        if ((prSettingKeys select 0) == (_this select 1)) then { 
            if ((prSettingKeys select 1) isEqualTo [(_this select 2), (_this select 3), (_this select 4)]) then { 
                createDialog "dlgBlueHudSettings"; 
                _settingHandled = true; 
            }; 
        }; 
        _settingHandled 
    }; 

    waitUntil { !isNull(findDisplay 46) }; 
    (findDisplay 46) displayAddEventHandler ["KeyDown", "_this call prSettingKeyHandler;"]; 

    //--- Shift + Alt + B = Hide HUD 
    pr_hideHudKey = 48; // B 
    pr_hideHudShift = 1; 
    pr_hideHudCtrl = 0; 
    pr_hideHudAlt = 1; 

    _hideHudKey = pr_hideHudKey; 
    if (_hideHudKey == 0) then { _hideHudKey = 48 }; 
    _hideHudShift = pr_hideHudShift == 1; 
    _hideHudCtrl  = pr_hideHudCtrl == 1; 
    _hideHudAlt   = pr_hideHudAlt == 1; 

    pr_hideHudKeys = [_hideHudKey, [_hideHudShift, _hideHudCtrl, _hideHudAlt]]; 
    prHideHudKeyHandler = { 
        _hideHudHandled = false; 
        if ((pr_hideHudKeys select 0) == (_this select 1)) then { 
            if ((pr_hideHudKeys select 1) isEqualTo [(_this select 2),(_this select 3),(_this select 4)]) then { 
                (uiNamespace getVariable "BlueHudMap") ctrlShow !(ctrlShown (uiNamespace getVariable "BlueHudMap")); //true; 
                _hideHudHandled = true; 
            }; 
        }; 
        _hideHudHandled 
    }; 

    waitUntil { !isNull(findDisplay 46) }; 
    (findDisplay 46) displayAddEventHandler ["KeyDown", "_this call prHideHudKeyHandler;"]; 

    //--- Shift + Space = Unfade Hud 
    pr_unFadeKey = 57; // Space 
    pr_unFadeShift = 1; 
    pr_unFadeCtrl = 0; 
    pr_unFadeAlt = 0; 

    _unFadeKey = pr_unFadeKey; 
    if (_unFadeKey == 0) then { _unFadeKey = 48 }; 
    _unFadeShift = pr_unFadeShift == 1; 
    _unFadeCtrl  = pr_unFadeCtrl == 1; 
    _unFadeAlt   = pr_unFadeAlt == 1; 

    prUnFadeKeys = [_unFadeKey, [_unFadeShift, _unFadeCtrl, _unFadeAlt]]; 
    prUnFadeKeyHandler = { 
        _unFadeHandled = false; 
        if ((prUnFadeKeys select 0) == (_this select 1)) then { 
            if ((prUnFadeKeys select 1) isEqualTo [(_this select 2),(_this select 3),(_this select 4)]) then { 
                [] call BlueHud_fnc_unfade; 
                _unFadeHandled = true; 
            }; 
        }; 
        _unFadeHandled 
    }; 

    waitUntil { !isNull(findDisplay 46) }; 
    (findDisplay 46) displayAddEventHandler ["KeyDown", "_this call prUnFadeKeyHandler;"]; 

    BlueHud_initialized = true; 
    if !(pr_deBug == 0) then { diag_log format ["*PR* bluHudInit BlueHud_initialized"]; }; 
}; 
