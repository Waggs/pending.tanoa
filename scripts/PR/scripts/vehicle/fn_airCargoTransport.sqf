/*  fn_airCargoTransport.sqf - Attached object paradrop script 
*  Author: PapaReap 
*  //Function name: fnc_carSpawn 
*  Spawns vehicle and follows waypoints 
*  ver 1.0 2015-12-14 
*  ver 1.1 2016-03-20 tweaked scripts 

* use code to find positions: 
    hv attachTo [c130, [0,-0.5,3.1]] 
    ammo1 attachTo [c130, [0,-2,2.15]] 

*  fnc_cargoAttach 
*  example: quad_1 is loaded into c130_1, slot 1 
*  [c130_1, [quad_1, 1]] spawn fnc_cargoAttach; 

*  fnc_cargoPara
*  example: c130_1 opens cargo door and paradrops all cargo 
*  0 = [c130_1] spawn fnc_cargoPara; 
*  if (isServer) then { [[c130_1],"fnc_cargoPara"] call BIS_fnc_MP; }; 
*/ 

//--- Vehicles 
para_C130J = ["C130J_Cargo", "C130J", "RHS_C130J"];
para_BlackFish = ["B_T_VTOL_01_infantry_F"];
para_MOHAWK = ["I_Heli_Transport_02_F","CH49_Mohawk_FG", "Marinir_CH49_Mohawk_FG","nqdy_arctic_mohawk_helicopter","nqdy_desert_mohawk_helicopter","nqdy_camo_mohawk_helicopter","nqdy_black_mohawk_helicopter","B_mas_uk_Heli_Transport_02_F"]; 
para_UH1Y = ["B_mas_mar_UH1Y_F"]; 
para_Med = ["RHS_UH60M_MEV","RHS_UH60M_MEV2_d"]; 

//--- Slots 
slot_C130J_Quad_1 = [0, 3.5,2.6]; 
slot_C130J_Quad_2 = [0, 1.3,2.6]; 
slot_C130J_Quad_3 = [0,-0.9,2.6]; 
slot_C130J_Quad_4 = [0,-3.1,2.6]; 

//--- Crates 
para_BoxSml = ["Box_NATO_WpsLaunch_F","ACE_medicalSupplyCrate"]; 
para_BoxMed = ["B_supplyCrate_F","rhsusf_mags_crate"]; 
para_BoxLrg = []; 
para_Quad = ["B_Quadbike_01_F"]; 

//--- Objects to not detach 
paraNoDetach = ["ModuleChemlight_F"]; 

if (isNil "newCrate") then { newCrate = "Box_NATO_Ammo_F"; }; 

fnc_cargoAttach = { 
    _veh = _this select 0; 
    _cargoType = _this select 1; 

    _cargoSlot = 0; 
    _cargoInit = ""; 
    _cargo = []; 

    if (typeName _cargoType == "ARRAY") then { 
        _cargoArray = _cargoType; 
        _cargoType  = _cargoArray select 0; 
        _cargoSlot  = _cargoArray select 1; 
    }; 

    if (count _this > 2) then { 
        _cargoInit = _this select 2; 
    }; 

    pr_cargoInit2 = _cargoInit; publicVariable "pr_cargoInit2"; //[0,{ [newCrate] call fnc_natoBoxAmmo; },"BIS_fnc_spawn",true,false,false]
    _veh_type = (typeOf _veh); 

    if (typeName _cargoType == "STRING") then { 
        if (isNil "crateID") then { crateID = 0; }; 
        crateID = crateID + 1; 
        newCrate = formatText ["newCrate_%1", crateID]; 
        newCrate = _cargoType createVehicle [0, 0, 0]; publicVariable "newCrate"; 
        _cargo = newCrate; 
        if !(_cargoInit == "") then { 
            [_cargoInit] spawn { 
                _cargoInit = _this select 0; 
                sleep 2; 
                _cargoInit; //[true, "Box_NATO_WpsLaunch_F",[{ [newCrate] call fnc_natoBoxAmmo; },'BIS_fnc_spawn',true,false] call BIS_fnc_MP]
                pr_cargoInit = _cargoInit; publicVariable "pr_cargoInit"; 
            }; 
        }; 
    } else { 
        _cargo = _cargotype; 
    }; 

    _box_type = (typeOf _cargo); pr_cargo = _cargo; // remove after testing
    _attachPos = [0,0,0]; 

    if (_veh_type in para_C130J) then { 
        if (_box_type in para_BoxMed) then { 
            _attachPos = [0, -2, 2.15]; // RHS Ammo box
            _nil = [_cargo attachTo [(_veh), _attachPos]]; //_nil = [ammo1 attachTo [(c130), [0,-2,2.15]]]; 

        } else { 
            if (_box_type in para_Quad) then { 
                if (_cargoSlot == 1) then { 
                    _attachPos = slot_C130J_Quad_1; 
                } else { 
                    if (_cargoSlot == 2) then { 
                        _attachPos = slot_C130J_Quad_2; 
                    } else { 
                        if (_cargoSlot == 3) then { 
                            _attachPos = slot_C130J_Quad_3; 
                        } else { 
                            _attachPos = slot_C130J_Quad_4; 
                        }; 
                    }; 
                }; 
                _nil = [_cargo attachTo [(_veh), _attachPos]]; 
                _cargo setDir 180; 
            }; 
        }; 
    } else { 
        if (_veh_type in para_MOHAWK) then { 
            //_nil = [_cargo attachTo [(_veh), [0,-2.0,-2.0]]]; //_nil = [ammo1 attachTo [mo, [0,-1.2,-1.23]]]; //launcher
            if (_box_type in para_BoxMed) then { 
                _attachPos = [0, -1.5, -1.32]; // supply box
                _nil = [_cargo attachTo [(_veh), _attachPos]]; // MED CHOPPER
            }; 
        } else { 
            if (_veh_type in para_Med) then { 
                if (_box_type in para_BoxSml) then { 
                    _attachPos = [-0.7, -4.18, -1.78]; // Ace medbox 
                    _nil = [_cargo attachTo [(_veh), _attachPos]]; // MED CHOPPER 
                    _cargo setDir 90; 
                }; 
            }; 
        }; 
    }; 
}; 

fnc_releaseCargo = { 
    _veh   = _this select 0; 
    _cargo = _this select 1; 
    _veh_type = _this select 2; 
    _cargo allowDamage false; 
    _pos = _veh worldToModel [getPosATL _cargo select 0, getPosATL _cargo select 1, (getPosATL _cargo select 2) - ((_cargo worldToModel (getPosATL _cargo)) select 2)]; 

    while { (_pos select 1 > -7) } do { 
        _pos = _pos vectorDiff [0, 0.05, 0]; 
        _cargo attachTo [_veh, _pos]; 
        sleep 0.01; 
    }; 

    detach _cargo; 
    sleep 0.1; 
    sleep 2.1; 
    // enter height check before attaching chute 
    if ((getPosATL _veh select 2) > 10) then { 
        _chute = "B_Parachute_02_F" createVehicle [0, 0, 100]; 
        _chute setPos (getPosATL _cargo); 
        _cargo attachTo [_chute, [0, 0, -.5]]; 

        if (_veh_type in para_C130J) then { 
            sleep 5; 
            _veh animateDoor ["ramp", 0]; 
        } else { 
            if (_veh_type in para_MOHAWK) then { 
                sleep 5; 
                _veh animateDoor ["CargoRamp_Open", 0]; 
            }; 
        }; 

        waitUntil { ((getPosATL _cargo select 2) < 10) }; 
        _time = serverTime + 20; 
        waitUntil { (((getPosATL _cargo select 2) < 1) || (serverTime > _time)) }; 
        detach _cargo; 
        sleep 0.1; 
        deleteVehicle _chute; 
        _veh allowdamage true; 
        sleep 2; 
        _cargo allowDamage true; 
    }; 
}; 

fnc_cargoPara = { 
    _veh = _this select 0; 
    _veh_type = (typeOf _veh); 

    if (_veh_type in para_C130J) then { 
        _veh animateDoor ["ramp", 1]; 
        waitUntil { _veh doorPhase "ramp" == 1 }; 
    } else { 
        if (_veh_type in para_MOHAWK) then { 
            _veh animateDoor ["CargoRamp_Open", 1]; 
            waitUntil { _veh animationPhase "CargoRamp_Open" == 1 }; 
        }; 
    }; // MED CHOPPER mo_1 animateDoor ["doorLB", 1]; 

    _cargo = ""; 
    _cargoAll = []; 
    _noDetach = ""; 
    _noDetachArray = []; 
    _cargo = (attachedObjects _veh) select 0; pr_cargo = _cargo; // chem 
    _cargoAll = attachedObjects _veh; 
    _count = count _cargoAll - 1; 

    for "_i" from 0 to _count do { 
        _noDetach = (attachedObjects _veh) select _i; 
        if ((typeOf _noDetach) in paraNoDetach) then { 
            _noDetachArray = _noDetachArray + [_noDetach]; 
        }; 
    }; 
    _cargoAll = _cargoAll - _noDetachArray; 

    _count = count _cargoAll - 1; 
    for "_i" from 0 to _count do { 
        _cargo = _cargoAll select _i; 
        [_veh, _cargo, _veh_type] spawn fnc_releaseCargo; 
        sleep 0.3; 
    }; 
}; 
