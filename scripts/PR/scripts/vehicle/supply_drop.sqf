/*
supply_drop.sqf 
ver 1.3 - 2015-07-18 compacted all scripts into 1
ver 1.2 - 2015-01-01 more refinement
ver 1.1 - 2014-12-26 complete medical system rework in progress
ver 1.0 - 2014-12-15

"medHeliSpawn" marker required in mission for spawn location // side  1 = Blufor, 2 = Opfor, 3 = Indedpendent // gunner crew  0 = off, 1 = ON
["spawn marker", "drop marker", side, gunner crew, "type of crate", custom crate, grid location of caller]

["medHeliSpawn", "medDrop", 1, 1, Box_East_AmmoVeh_F", 0, 0] execVM "scripts\PR_HealthCare\PR_misc\PR_AiLift\PR_med_drop.sqf";

From a trigger use:
if !(heliSupplyDrop) then {["medHeliSpawn", "medDrop", 1, 1, "AGM_Box_Medical", 0, 0] execVM "scripts\PR_HealthCare\PR_misc\PR_AiLift\PR_med_drop.sqf"} else {hint format["Only run one instance of AiLift at a time"];};

if (true) then {["medHeliSpawn", "medDrop", 1, 1, "Box_East_AmmoVeh_F", 0, 0] execVM "scripts\PR_HealthCare\PR_misc\PR_AiLift\PR_med_drop.sqf"};
*/ //diag_log format ["***test***"];

fn_findBoxAir = { 
    private["_dropbox", "_c", "_smoke", "_flare"]; 
    _dropbox = _this select 0; 
    _c = 0; 
    _smoke = ""; 
    _flare = ""; 

    if (missionNameSpace getVariable "typeSupplyDrop" == "Ammo") then { 
        _smoke = "SmokeShellBlue"; 
        _flare = "F_40mm_white"; 
    } else { 
        if (missionNameSpace getVariable "typeSupplyDrop" == "Med") then { 
            _smoke = "SmokeShellRed"; 
            _flare = "F_40mm_red"; 
        } else { 
            if (missionNameSpace getVariable "typeSupplyDrop" == "Mash") then { 
                _smoke = "SmokeShellGreen"; 
                _flare = "F_40mm_white"; 
            } else { 
                if (missionNameSpace getVariable "typeSupplyDrop" == "MH9") then { 
                    _smoke = "SmokeShellBlue"; 
                    _flare = ""; 
                }; 
            }; 
        }; 
    }; 

    while {((getPos _dropbox) select 2) > 3} do { 
        if (((getPos _dropbox) select 2) > 3) then { 
            _smokeAir = _smoke createVehicle position _dropbox; 
            _smokeAir attachTo [_dropbox,[0,0,0]]; 
            _flareAir = _flare createVehicle position _dropbox; 
            _flareAir attachTo [_dropbox,[0,0,0]]; 
            if (((getPos _dropbox) select 2) > 3) then { 
                sleep 10.9; 
            }; 
        }; 
        if (((getPos _dropbox) select 2) < 3) then { 
            _c = 1; 
        }; 
    }; 
}; 

fn_findBoxLand = { 
    private["_dropbox", "_c", "_chemlight", "_smoke"]; 
    _dropbox = _this select 0; 
    _c = 0; 
    _chemlight = ""; 
    _smoke = ""; 
    _flare = ""; 

    if (missionNameSpace getVariable "typeSupplyDrop" == "Ammo") then { 
        _chemlight = "Chemlight_Blue"; 
        _smoke = "SmokeShellBlue"; 
        _flare = "F_40mm_white"; 
    } else { 
        if (missionNameSpace getVariable "typeSupplyDrop" == "Med") then { 
            _chemlight = "Chemlight_Red"; 
            _smoke = "SmokeShellRed"; 
            _flare = "F_40mm_red"; 
        } else { 
            if ((missionNameSpace getVariable "typeSupplyDrop" == "Mash") || (missionNameSpace getVariable "typeSupplyDrop" == "MH9")) then { 
                _chemlight = "Chemlight_Green"; 
                _smoke = "SmokeShellGreen"; 
                _flare = "F_40mm_white"; 
            }; 
        }; 
    }; 

    while {({_x distance _dropbox > 50} count playableUnits >0 ) && (_c == 0)} do { 
        if ({_x distance _dropbox > 50} count playableUnits >0 ) then { 
            if !(isNil "_lightLand") then {deleteVehicle _lightLand}; 
            _lightLand =  _chemlight createVehicle position _dropbox; 
            _smokeLand =  _smoke createVehicle position _dropbox; 
            _smokeLand attachTo [_dropbox,[0,0,0]]; 
            sleep 50; 
            if ({_x distance _dropbox > 50} count playableUnits >0 ) then { 
                sleep 50; 
                _smokeLand =  _smoke createVehicle position _dropbox; 
                _smokeLand attachTo [_dropbox,[0,0,0]]; 
                _flareLand = _flare createVehicle position _dropbox; 
                _flareLand attachTo [_dropbox,[0,0,0]]; 
                sleep 50; 
                if ({_x distance _dropbox > 50} count playableUnits >0 ) then { 
                    sleep 50; 
                    deleteVehicle _lightLand; 
                }; 
            }; 
        }; 
        if ({_x distance _dropbox < 50} count playableUnits >0 ) then { 
            _c = 1; 
        }; 
    }; 
}; 

private ["_spawn","_drop","_side","_crew","_crate","_custom","_gridPos"]; 

if (isNil "heliSupplyDrop") then {heliSupplyDrop = false}; 
heliSupplyDrop = true; publicVariable "heliSupplyDrop"; 

_spawn      = _this select 0; 
_drop       = _this select 1; 
_side       = _this select 2; 
_crew       = _this select 3; 
_crate      = _this select 4; 
_custom     = _this select 5; 
_gridPos    = _this select 6; 

_dropbox    = _crate createVehicle (getMarkerPos _spawn); 
_rtb        = "Land_HelipadEmpty_F" createVehicle (getMarkerPos _spawn); 
_dropland1  = "Land_HelipadEmpty_F" createVehicle (getMarkerPos _drop); 
_supplyHeli    = objNull; 

if (isNil "noHCSwap") then { 
    noHCSwap = []; publicVariable "noHCSwap"; 
}; 

sleep 1; 

if (_custom == 1) then { 
    if (missionNameSpace getVariable "typeSupplyDrop" == "Ammo") then { 
        _nil = _dropbox execVM "scripts\PR\loadouts\box\natoBox_ammo.sqf"; 
    } else { 
        if (missionNameSpace getVariable "typeSupplyDrop" == "Med") then { 
            _nil = _dropbox execVM "scripts\PR\loadouts\box\aceBox_med.sqf"; 
        } else { 
            if (missionNameSpace getVariable "typeSupplyDrop" == "Mash") then { 
                _nil = _dropbox execVM "scripts\PR\loadouts\box\aceBox_med.sqf"; 
            } else { 
                if (missionNameSpace getVariable "typeSupplyDrop" == "MH9") then { 
                    _nil = _dropbox execVM "scripts\PR\loadouts\box\natoBox_ammo.sqf"; 
                }; 
            }; 
        }; 
    }; 
}; 

sleep 10; 
//if (_side == 1) then {
if ((missionNameSpace getVariable "typeSupplyDrop" == "Ammo") || (missionNameSpace getVariable "typeSupplyDrop" == "Med")) then { 
    _supplyHeli = "b_heli_Transport_01_camo_F" createVehicle (getMarkerPos _spawn); 
    if (true) then {_supplyHeli animateDoor ['door_R', 1]; _supplyHeli animateDoor ['door_L', 1];}; 
    missionNameSpace setVariable ["supplyHeli", _supplyHeli]; 
} else { 
    if ((missionNameSpace getVariable "typeSupplyDrop" == "Mash") || (missionNameSpace getVariable "typeSupplyDrop" == "MH9")) then { 
        _supplyHeli = "B_Heli_Transport_03_F" createVehicle (getMarkerPos _spawn); 
        missionNameSpace setVariable ["supplyHeli", _supplyHeli]; 
    }; 
}; 

missionNameSpace setVariable ["scriptFinished", 0]; 

while { ((alive (missionNameSpace getVariable "supplyHeli")) && (canMove (missionNameSpace getVariable "supplyHeli")) && (missionNameSpace getVariable "scriptFinished" == 0)) } do { 
    scopeName "loop1";
    _supplyHeli allowDamage false; 
    _grouppilots = createGroup West; 
    "b_helipilot_F" createUnit [getMarkerPos _spawn, _grouppilots, "this moveInDriver _supplyHeli", 0.6, "corporal"]; 
    "b_helipilot_F" createUnit [getMarkerPos _spawn, _grouppilots, "this moveInTurret [_supplyHeli, [0]]", 0.6, "corporal"]; 
    missionNameSpace setVariable ["supplyHeliPilots", _grouppilots]; 
    _grouppilots allowFleeing 0; 
    {_x allowDamage false} forEach units _grouppilots; 
    { noHCSwap = noHCSwap + [_x]; publicVariable "noHCSwap"; } forEach units _grouppilots; 

    missionNameSpace setVariable ["supplyHeliCrew", grpNull]; 
    if (_crew == 1) then { 
        _grpCrew = createGroup West; 
        "b_helicrew_F" createUnit [getMarkerPos _spawn, _grpCrew, "this moveInTurret [_supplyHeli, [1]]", 0.6, "corporal"]; 
        "b_helicrew_F" createUnit [getMarkerPos _spawn, _grpCrew, "this moveInTurret [_supplyHeli, [2]]", 0.6, "corporal"]; 
        missionNameSpace setVariable ["supplyHeliCrew", _grpCrew]; 
        _grpCrew allowFleeing 0; 
        _grpCrew setBehaviour "COMBAT"; 
        { noHCSwap = noHCSwap + [_x]; publicVariable "noHCSwap"; } forEach units _grpCrew; 
    }; 

    _typeSupply = missionNameSpace getVariable "supplyDropHint"; 
    _supplyDropRequestHint = format ["Carrier Pigeon, Squad Lead is Requesting a %1 Drop at Grid %2", _typeSupply, _gridPos]; 
    logic_firefly sideChat _supplyDropRequestHint; 
    logic_firefly_SideChat = _supplyDropRequestHint; publicVariable "logic_firefly_SideChat"; 

    _dropbox allowDamage false; 

    if ((missionNameSpace getVariable "typeSupplyDrop" == "Ammo") || (missionNameSpace getVariable "typeSupplyDrop" == "Med")) then { 
        _dropbox attachTo [_supplyHeli, [0, 2, -5]]; 
    } else { 
        if ((missionNameSpace getVariable "typeSupplyDrop" == "Mash") || (missionNameSpace getVariable "typeSupplyDrop" == "MH9")) then { 
            _dropbox attachTo [_supplyHeli, [0, 2, -7.5]]; 
        }; 
    }; 
    sleep 1; 

    //_dropbox allowDamage true; 
    _supplyHeli flyInHeight 250; 
    _waypoint1 = _grouppilots addWaypoint [getMarkerPos _Drop, 5]; 
    _waypoint1 setWayPointBehaviour "CARELESS"; 
    _waypoint1 setWayPointSpeed "NORMAL"; 
    _waypoint1 setWayPointType "MOVE"; 
    _waypoint1 setWayPointCombatMode "WHITE"; 
    sleep 5; 

    _copyBaseFireflyHint = format ["Copy Base Firefly, %1 Drop in Route to Grid Location %2", _typeSupply, _gridPos]; 
    logic_pigeon sideChat _copyBaseFireflyHint; 
    logic_pigeon_SideChat = _copyBaseFireflyHint; publicVariable "logic_pigeon_SideChat"; 

    _waypoint2 = _grouppilots addWaypoint [getMarkerPos _Drop, 2]; 
    _supplyHeli flyInHeight 250; 
    //_dropbox allowDamage false; 

    waitUntil { ((_supplyHeli distance _dropland1 < 500) || (!alive (missionNameSpace getVariable "supplyHeli")) || (!canMove (missionNameSpace getVariable "supplyHeli"))) }; 
    _supplyHeli allowDamage true; 
    {_x allowDamage true} forEach units _grouppilots; 
    if ((!alive (missionNameSpace getVariable "supplyHeli")) || (!canMove (missionNameSpace getVariable "supplyHeli"))) then { breakOut "loop1" }; 

    waitUntil { ((_supplyHeli distance _dropland1 < 300) || (speed _supplyHeli < 2) || (!alive (missionNameSpace getVariable "supplyHeli")) || (!canMove (missionNameSpace getVariable "supplyHeli"))) }; 
    if ((!alive (missionNameSpace getVariable "supplyHeli")) || (!canMove (missionNameSpace getVariable "supplyHeli"))) then { breakOut "loop1" }; 
    detach _dropbox; 
    sleep 4.5; 

    //_pilot = _dropbox; 
    _chute = createVehicle ["B_Parachute_02_F", [100, 100, 200], [], 0, 'FLY']; 
    _chute setPos [position _dropbox select 0, position _dropbox select 1, (position _dropbox select 2) - 4]; 
    _dropbox attachTo [_chute, [0, 0, -0.6]]; 

    _packageDroppedHint = format ["Base Firefly, %1 Dropped at Grid %2", _typeSupply, _gridPos]; 
    logic_pigeon sideChat _packageDroppedHint; 
    logic_pigeon_SideChat = _packageDroppedHint; publicVariable "logic_pigeon_SideChat"; 
    sleep 5; 

    _copyRTBHint = format ["Copy Carrier Pigeon, RTB"]; 
    logic_firefly sideChat _copyRTBHint; 
    logic_firefly_SideChat = _copyRTBHint; publicVariable "logic_firefly_SideChat"; 

    _waypoint3 = _grouppilots addWaypoint [getPos _rtb, 5]; 
    _waypoint3 setWayPointBehaviour "CARELESS"; 
    _waypoint3 setWayPointSpeed "FULL"; 
    _waypoint3 setWayPointType "MOVE"; 
    _waypoint3 setWayPointCombatMode "WHITE"; 

    if (((getPos _dropbox) select 2) > 3) then { [_dropbox] spawn fn_findBoxAir; }; 

    waitUntil { position _dropbox select 2 < 0.5 || isNull _chute; }; 
    detach _dropbox; 
    _dropbox setPos [position _dropbox select 0, position _dropbox select 1, 0]; 
    sleep 5; 
    _dropbox allowDamage true; 

    if ({_x distance _dropbox > 50} count playableUnits >0 ) then { [_dropbox] spawn fn_findBoxLand; }; 

    waitUntil { ((_supplyHeli distance _rtb < 400) || (!alive (missionNameSpace getVariable "supplyHeli")) || (!canMove (missionNameSpace getVariable "supplyHeli"))) }; 
    if ((!alive (missionNameSpace getVariable "supplyHeli")) || (!canMove (missionNameSpace getVariable "supplyHeli"))) then { breakOut "loop1" }; 
    _supplyHeli land "Land"; 
    waitUntil { ((((getPos _supplyHeli) select 2) < 0.5) || (!alive (missionNameSpace getVariable "supplyHeli")) || (!canMove (missionNameSpace getVariable "supplyHeli"))) }; 
    missionNameSpace setVariable ["scriptFinished", 1]; 
    if ((!alive (missionNameSpace getVariable "supplyHeli")) || (!canMove (missionNameSpace getVariable "supplyHeli"))) then { breakOut "loop1" }; 
    sleep 5; 

    _pigeonHasRTBHint = format ["Base Firefly, Carrier Pigeon has Returned to Base. Resupplying in Progress..."]; 
    logic_pigeon sideChat _pigeonHasRTBHint; 
    logic_pigeon_SideChat = _pigeonHasRTBHint; publicVariable "logic_pigeon_SideChat"; 
    sleep 5; 
    _copyPigeonRTBHint = format ["Copy Carrier Pigeon, Respond When Ready for New Assignment, Out"]; 
    logic_firefly sideChat _copyPigeonRTBHint; 
    logic_firefly_SideChat = _copyPigeonRTBHint; publicVariable "logic_firefly_SideChat"; 
}; 

sleep 120; 
if ((alive (missionNameSpace getVariable "supplyHeli")) && (canMove (missionNameSpace getVariable "supplyHeli")) && (missionNameSpace getVariable "scriptFinished" == 1)) then { 
    _pigeonIsReadyHint = format ["Base Firefly, Carrier Pigeon is Ready for Next Assignment"]; 
    logic_pigeon sideChat _pigeonIsReadyHint; 
    logic_pigeon_SideChat = _pigeonIsReadyHint; publicVariable "logic_pigeon_SideChat"; 
    sleep 6; 

    _copyStandbyHint = format ["Copy Carrier Pigeon, Standby for Further Orders"]; 
    logic_firefly sideChat _copyStandbyHint; 
    logic_firefly_SideChat = _copyStandbyHint; publicVariable "logic_firefly_SideChat"; 
} else { 
    if ((!alive (missionNameSpace getVariable "supplyHeli")) || (!canMove (missionNameSpace getVariable "supplyHeli")) || (missionNameSpace getVariable "scriptFinished" != 1)) then { 
        _noResponseHint = format ["Carrier Pigeon, Do You Copy"]; 
        logic_firefly sideChat _noResponseHint; 
        logic_firefly_SideChat = _noResponseHint; publicVariable "logic_firefly_SideChat"; 
        sleep 30; 
        _noResponseHint2 = format ["Carrier Pigeon, Repeat, Do You Copy"]; 
        logic_firefly sideChat _noResponseHint2; 
        logic_firefly_SideChat = _noResponseHint2; publicVariable "logic_firefly_SideChat"; 
        sleep 60; 
        _newPigeonHint = format ["Squad Lead, Carrier Pigeon is Non-Responsive, Assigning New Carrier, Out"]; 
        logic_firefly sideChat _newPigeonHint; 
        logic_firefly_SideChat = _newPigeonHint; publicVariable "logic_firefly_SideChat"; 
    }; 
}; 
//--- clean-up time
deleteVehicle _dropland1; 
deleteVehicle _rtb; 
deleteVehicle _supplyHeli; 
_grouppilots = missionNameSpace getVariable "supplyHeliPilots"; 
{deleteVehicle _x} forEach units _grouppilots; 
deleteGroup _grouppilots; 
{ _x = nil } forEach (units _grouppilots); 

if (missionNameSpace getVariable "supplyHeliCrew" != grpNull) then { 
    _grpCrew = missionNameSpace getVariable "supplyHeliCrew"; 
    {deleteVehicle _x} forEach units _grpCrew; 
    deleteGroup _grpCrew; 
    { _x = nil } forEach (units _grpCrew); 
}; 

if (missionNameSpace getVariable "supplyDropMarker" != "") then { 
    _supplyDrop = missionNameSpace getVariable "supplyDropMarker"; 
    if (getMarkerColor _supplyDrop == "ColorBlue") then { deleteMarker _supplyDrop}; 
    missionNameSpace setVariable ["supplyDropMarker", ""]; 
}; 

if (true) exitWith { heliSupplyDrop = false; publicVariable "heliSupplyDrop"; }; 
