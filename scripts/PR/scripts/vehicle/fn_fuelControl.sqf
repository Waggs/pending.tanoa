/* fn_fuelControl.sqf
*  Author: PapaReap
*  function name: pr_fnc_fuelControl
*  Arguments:
*  0: VEHICLE    - Required, vehicle to get/set fuel
*  1: FUEL TYPE  - Required, 0 to get fuel, 1 to set fuel (scalar)

*  usage:    - [pr_heliTrans, 0] call pr_fnc_fuelControl, [pr_heliTrans, 1] call pr_fnc_fuelControl
*/


_vehicle = _this select 0;
_type = _this select 1;

private _fuel = _vehicle getVariable "vehFuel";
if (isNil "_fuel") then {
    _vehicle setVariable ["vehFuel", (fuel _vehicle)];
    _fuel = (fuel _vehicle);
};

if (_type == 0) then {
    // set fuel
    _fuel = fuel _vehicle;
    _vehicle setVariable ["vehFuel", _fuel, true];
    _vehicle setFuel 0;
} else {
    // get fuel
    _fuel = _vehicle getVariable "vehFuel";
    _vehicle setFuel _fuel;
};

_fuel
