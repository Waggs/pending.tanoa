/* 
*  Author: PapaReap 
*  function name: pr_fnc_fuelDrain 

*  ver 1.1 - 2015-06-10 convert to function
*  ver 1.0 - 2014-12-20 

*  Veh Init: 
*  0 = [this,15] spawn pr_fnc_fuelDrain; 
*  0 = [this,15] spawn pr_fnc_fuelDrain; 
*  0 = [this, Runtime in minutes] spawn pr_fnc_fuelDrain; 
*  30 min accelerated vehicle with random +/- 3.33 min.  vrs regular vehicle, fuel = 96% full 
*/

private ["_c","_veh","_min","_fuelLoss","_sleep","_random","_adjFuel"]; 
_veh       = _this select 0; 
_min       = _this select 1; 
_fuelLoss  = 0.002; 
_sleep     = _min * 60 * _fuelLoss; 
_random    = [0.0002, 0.0001, 0.0000, -0.0001, -0.0002] call BIS_fnc_selectRandom; 
_adjFuel   = _fuelLoss + _random; 
_c         = 0; 

while { alive _veh } do { 
    if ((isEngineOn _veh) && (_c == 0)) then { 
        _fuel = Fuel _veh - _adjFuel; 
        _veh setFuel _fuel; 
    }; 
    sleep _sleep; 
}; 
