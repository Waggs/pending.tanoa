
//[["B_Plane_CAS_01_F", thisTrigger, thisTrigger], "pr_fnc_emptyVeh"] call BIS_fnc_MP; 
//[randomUnit, thisTrigger, jam1] remoteExec ["pr_fnc_emptyVeh", 0, true];
if !(isServer) exitWith {};

// [r_transportSm] call r_randomSpawn;
// [randomUnit select 0, thisTrigger, jam1] remoteExec ["pr_fnc_emptyVeh", 0, true];
//[r_transportSm] call r_randomSpawn; [randomUnit select 0, thisTrigger, jam2] remoteExec ["pr_fnc_emptyVeh", 2, true];
//if (isServer) then { [r_transportSm] call r_randomSpawn; [randomUnit select 0, thisTrigger, jam2] call pr_fnc_emptyVeh; };
//if (isServer) then { [r_transportSm] call r_randomSpawn; [randomUnit select 0, thisTrigger, jam2] call pr_fnc_emptyVeh; };

//if (isServer) then { [[b_littleBird, lb1, "", 0, ""], lb2dir ] call pr_fnc_emptyVeh; };
//if (isServer) then { [[b_littleBird, lb1], [90, "wolf = _this; _this"] ] call pr_fnc_emptyVeh; };
// [type, position, markers, placement, special]: Array
mainArr = _this;

_objectArr = _this select 0; pr_objectArr = _objectArr;
_object = _objectArr select 0; pr_object = _object;
_spawn = _objectArr select 1; pr_spawn = _spawn;

if (count _objectArr > 2) then { _markers = _this select 2; };
if (count _objectArr > 3) then { _placement = _this select 3; };
if (count _objectArr > 4) then { _special = _this select 4; };

_arr2 = _this select 1;
_dir = _arr2 select 0;
//if (count _arr2 > 1) then { _name = _arr2 select 1; pr_name = _name; };

_spawnPos = [0,0,0];
_dirTo = [0,0,0];
//_newDir = 0;


if (typeName _object == "ARRAY") then { _object = _object select 0; };
if (typeName _spawn == "STRING") then { _spawnPos = getMarkerPos _spawn } else { _spawnPos = getPos _spawn };
//if (typeName _dir == "STRING") then { _dirTo = getMarkerPos _dir; } else { _dirTo = getPos _dir; };

//_newDir = [_spawnPos ,_dirTo] call BIS_fnc_dirTo; pr_newDir = _newDir;
_newDir = _dir;

_newObject = _object createVehicle _spawnPos; pr_spawnPos = _spawnPos;
_newObject setDir _newDir;
_name = _newObject; pr_newObject = _newObject;

vehInit = {(_this select 0) call compile (_this select 1)};
_init = "";
if (count _arr2 > 1) then {
    _init = _arr2 select 1;
    if (_init != "") then {
        [[_newObject, _init], "vehInit", true, false, false] spawn BIS_fnc_MP;
    };
};

