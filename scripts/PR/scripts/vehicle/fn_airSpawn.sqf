/* fn_airSpawn.sqf
*  Author: PapaReap 
*  Function name: PR_fnc_airSpawn 
* 
*  ver 1.0 2015-11-22
*
*  Arguments: 
* 0: <OBJECT OR MARKER> 
* 1: <SECONDS>
* Examples: 
* 0 = [this, Runtime in minutes] spawn fnc_delObject; // old PR_scripts_fnc_delObject; 
* 0 = [car1, 30] spawn fnc_delObject; 
* 0 = [thisTrigger, 15] spawn fnc_delObject; 
*/

/* 
usage: 
[spawnObject, [unit, height, direction, (optional positions; "down","up","middle","auto"), (optional unit movement; 0 = no movement; 1 = movement allowed)],[optional upsmon parameters; ""]] call fnc_airSpawn; 

[mortSpawn_1, [[b_mortar,"[this,5] spawn PR_vehicle_fnc_unlimitedAmmo; this allowDamage false"], 1, 0,"up",0],["combat"]] call f_preciseSpawn;
[[ss_1, "west"], [[r_menDiver], 1, 0, "up", 0, "nul = [this,3] execVM 'scripts\PR\scripts\vehicle\functions\fn_unlimitedAmmo.sqf'; this allowDamage false"],["combat"]] call fnc_airSpawn;
                                                 //_nil = this execVM 'scripts\PR\loadouts\box\natoBox_ammo.sqf'; 
[ss_1_1, ["O_Soldier_F", 0, 180, "up", 0, "nul = [this,3] execVM 'scripts\PR\scripts\vehicle\functions\fn_unlimitedAmmo.sqf'; this allowDamage false"],["combat"]] call fnc_airSpawn;
[[mortSpawn_1, "west"], ["B_Soldier_F", 0, 10, "auto", 1, "fred_1 = this;"]] call fnc_airSpawn; fred_1 moveInGunner mort1; 
*/

//if (isNil "airSpawnCount") then { airSpawnCount = 0; }; 

//if (!isServer && hasInterface ) exitWith {}; 

if (!isServer) exitWith {}; 
if (isNil "noHCSwap") then { noHCSwap = []; publicVariable "noHCSwap"; }; 

if (isServer) then { 
    //--- [[spawnObject], vehType, height, special init,[optional upsmon parameters; ""]] call fnc_airSpawn; 
    //     [[                   0,                                ], [    1   ], [    2      ], [   3  ], [    4    ]] call fnc_airSpawn; 

    // [Vehicle spawn position, 1st waypoint (sets direction), intermediate waypoint (area to drop), end waypoint, height, speed] 
    private ["_spawnArray","_spawn","_wp","_dlp","_end","_height","_speed","_spawnPos","_wpPos"];
    _spawnArray = [_this, 0, [], [[]]] call BIS_fnc_param;  // _this select 0
    _spawn = [_spawnArray, 0, [0, 0, 0]] call BIS_fnc_paramIn; 
    _wp = [_spawnArray, 1, [0, 0, 0]] call BIS_fnc_paramIn; 
    _dlp = [_spawnArray, 2, [0, 0, 0]] call BIS_fnc_paramIn; 
    _end = [_spawnArray, 3, [0, 0, 0]] call BIS_fnc_paramIn; 
    _height = [_spawnArray, 4, 100, [[]]] call BIS_fnc_paramIn; 
    _speed = [_spawnArray, 5, 300, [[]]] call BIS_fnc_paramIn; 
    _spawnPos = [0,0,0]; 
    _wpPos = [0,0,0]; 

    //--- Vehicle type, side 
    private ["_vehArray","_vehType","_side","_randomVehArray","_random_veh"]; 
    _vehArray = [_this, 1, [], [[]]] call BIS_fnc_param;  // _this select 1 
    _vehType = [_vehArray, 0, "RHS_C130J", [[]]] call BIS_fnc_paramIn; 
    _side = [_vehArray, 1, "WEST"] call BIS_fnc_paramIn; 
    if (typeName _vehType == "ARRAY") then { 
            _vehType = _vehType select 0; 
            _random_veh = _vehType call BIS_fnc_selectRandom; 
            _vehType = _random_veh; 
    }; 

    //--- SetCaptive, Allowdamage 
    private ["_vehStatus","_captive","_damage"]; 
    _vehStatus = [_this, 2, [], [[]]] call BIS_fnc_param;  // _this select 2 
    _captive = [_vehStatus, 0, false, [false]] call BIS_fnc_paramIn; 
    _damage = [_vehStatus, 1, true, [true]] call BIS_fnc_paramIn; 

    if (typeName _spawn == "STRING") then { _spawnPos = getMarkerPos _spawn; } else { _spawnPos = getPos _spawn; }; 
    if (typeName _wp == "STRING") then { _wpPos = getMarkerPos _wp; } else { _wpPos = getPos _wp; }; 

    if (typeName _side == "STRING") then { 
        _side = toUpper _side; 
        switch (_side) do { 
            case "EAST":       { _side = EAST }; 
            case "WEST":       { _side = WEST }; 
            case "RESISTANCE": { _side = RESISTANCE }; 
            case "CIVILIAN":   { _side = CIVILIAN }; 
            default            { _side = WEST }; 
        }; 
    }; 
    _vehContainer = [[_spawnPos select 0, _spawnPos select 1, _height], [_spawnPos, _wpPos] call BIS_fnc_dirTo, _vehType, _side] call BIS_fnc_spawnVehicle; 

    private ["_veh","_groupVeh"]; 
    _veh = _vehContainer select 0; 
    pr_veh = _veh; publicVariable "pr_veh"; // delete after testing 
    _groupVeh = _vehContainer select 2; 
    { noHCSwap = noHCSwap + [_x]; publicVariable "noHCSwap"; } forEach units _groupVeh; 

    //if (_side == EAST) then { 
    //    {[_x] execVM "scripts\PR\spawnUnits\r_changeUniform.sqf"} forEach units _grp; 
    //    { [_x] call pr_fnc_r_changeUniform } forEach units _grp; 
    //}; 

    //height & speed
    private "_dir"; 
    _dir = direction _veh; 
    _vel = _speed * 0.2778; 
    _veh setVelocity [sin (_dir) * _vel, cos (_dir) * _vel, 0]; 
    _veh limitSpeed _speed; 
    _veh flyInHeight _height; 
    _groupVeh allowFleeing 0; 
    _veh allowDamage _damage; 
    _veh setCaptive _captive; 

    //turn off collision lights
    [_veh] spawn { 
        private "_veh"; 
        _veh = _this select 0; 
        while {alive _veh} do { _veh action ["collisionlightOff", _veh]; sleep 0.01; }; 
    }; 

    _crateArray = [_this, 3, [true,[]], [[]]] call BIS_fnc_param;  // _this select 3 
    _allowCrate = [_crateArray, 0, true, [true]] call BIS_fnc_paramIn; 
    _crateType = [_crateArray, 1, "Box_NATO_Ammo_F", []] call BIS_fnc_paramIn; //make sure this default works, was []
    _crateInit = [_crateArray, 2, "", []] call BIS_fnc_paramIn; 
    if (_allowCrate) then { [_veh, _crateType, _crateInit] spawn fnc_crateAttach; }; 

    //--- Waypoint: 1 / array 
    private ["_waypoints","_loop","_wpL","_wpLPos"]; 
    _waypoints = []; 
    if (typeName _wp == "STRING") then { _waypoints = _wp call fnc_collectMarkers; } else { _waypoints = _wp call fnc_collectObjectsNum; }; 

    for [{ _loop = 0 }, { _loop < count _waypoints }, { _loop = _loop + 1 }] do { 
        {
            _wpL =  _waypoints select _loop; 
            if (typeName _wpL == "STRING") then { _wpLPos = getMarkerPos _wpL; } else { _wpLPos = getPos _wpL; }; 
            [_groupVeh, _wpLPos, [], ["CARELESS", "YELLOW", "FULL"]] call fnc_addWaypoint; 
        } forEach _waypoints; 
        _waypoints spawn { sleep 0.01; }; 
    }; 

    pr_waypoints = _waypoints; publicVariable "pr_waypoints"; // delete after testing
    _veh limitSpeed _speed; 
    _veh flyInHeight _height; 

    //--- Waypoint: Drop, Land or Passthrough 
    private ["_dlpPos","_statement","_statementdlp","_dlpWp"]; 
    _dlpPos = [0,0,0]; 
    _statement = ""; 
    if (typeName _dlp == "STRING") then { _dlpPos = getMarkerPos _dlp; } else { _dlpPos = getPos _dlp; }; 
    _statementdlp = [_this, 4, "", [[]]] call BIS_fnc_param;  // _this select 4 
    _dlpDest = "Land_HelipadEmpty_F" createVehicle _dlpPos; 

    if (_statementdlp == "land") then { _statement = " _veh = vehicle this; _veh land 'LAND'; _veh lock false; "; }; 

    [_groupVeh, _dlpDest, [], ["CARELESS", "YELLOW", "FULL"], _statement] call fnc_addWaypoint; 

    if (_statementdlp == "drop") then { 
        [_veh, _dlpDest] spawn { 
            _veh = _this select 0; 
            _dlpDest = _this select 1; 
            sleep 5; 
            waitUntil { 
                if (speed _veh < 105) then { 
                    waitUntil { (_veh distance2D _dlpDest < 500) }; 
                } else { 
                    if (speed _veh < 205) then { 
                        waitUntil { (_veh distance2D _dlpDest < 550) }; 
                    } else { 
                        if (speed _veh < 305) then { 
                            waitUntil { (_veh distance2D _dlpDest < 750) }; 
                        } else { 
                            if (speed _veh < 405) then { 
                                waitUntil { (_veh distance2D _dlpDest < 900) }; 
                            } else { 
                                waitUntil { (_veh distance2D _dlpDest < 1100) }; 
                            }; 
                        }; 
                    }; 
                }; 
            }; 
            _nil = [_veh] spawn fnc_vehPara; 
        }; 
    }; 
    _veh limitSpeed _speed; 
    _veh flyInHeight _height; 

    //--- Waypoint: End 
    //[group, position, ["mode", modifier, completition radius, force road], ["BEHAVIOUR", "COMBATMODE", "SPEED"], "code"] call ws_fnc_addWaypoint; 
    private ["_endPos","_end","_statementLand","_statementEnd","_statementMode","_statementMofidier","_endDest","_statementLast"]; 
    _endPos = [0,0,0]; 

    pr_end = _end; // delete after testing
    if (typeName _end == "STRING") then { _endPos = getMarkerPos _end; } else { _endPos = getPos _end; }; 
    //_modes = ["move","destroy","getin","sad","join","leader","getout","cycle","load","unload","tr unload","hold","sentry","guard","talk","scripted","support","getin nearest","dismiss","defend","garrison","patrol","ambush"]; 

    _statementLand = [_this, 5, "", [[]]] call BIS_fnc_param;  // _this select 5 

    _statementEnd = [_this, 6, ["", 0], [[]]] call BIS_fnc_param;  // _this select 6 
    _statementMode = [_statementEnd, 0, "", [""]] call BIS_fnc_paramIn; 
    _statementMofidier = [_statementEnd, 1, 0, []] call BIS_fnc_paramIn; 

    _endDest = "Land_HelipadEmpty_F" createVehicle _endPos; 
    _statementLast = ""; // "delete", "land", 

    //[["Supplies_spawn", "c_wp", "c130_land", "c130_end", 80, 300], ["RHS_C130J", "west"], [false, true], [true, "Box_NATO_WpsLaunch_F", [{ [newCrate] call fnc_natoBoxAmmo; },'BIS_fnc_spawn', true, false] call BIS_fnc_MP], "drop", "delete"] call fnc_airSpawn; 
    //[["Supplies_spawn", c_wp, "c130_drop", "c130_end", 80, 300], ["RHS_C130J", "west"], [FALSE, TRUE], [TRUE, "Box_NATO_WpsLaunch_F", [{ [newCrate] call fnc_natoBoxAmmo; },'BIS_fnc_spawn', true, false] call BIS_fnc_MP], "drop", "land", ["patrol", 900]] call fnc_airSpawn;
    //[["Supplies_spawn", "c_wp", "c130_land", "c130_end", 80, 300], ["RHS_C130J", "west"], [false, true], [true, "Box_NATO_WpsLaunch_F",[{ [newCrate] call fnc_natoBoxAmmo; },'BIS_fnc_spawn', true, false] call BIS_fnc_MP], "drop", "", ["patrol", 900]] call fnc_airSpawn; 

    pr_statementLand = _statementLand; // delete after testing
    pr_endDest = _endDest; // delete after testing
    if (_statementLand == "delete") then { 
        [_groupVeh, _endDest, [], ["CARELESS", "YELLOW", "FULL"]] call fnc_addWaypoint; 
        [_veh, _endDest] spawn { 
            _veh = _this select 0; 
            _endDest = _this select 1; 
            waitUntil { (_veh distance2D _endDest < 100) }; 
            { deleteVehicle _x; } forEach crew _veh; deleteVehicle _veh; deleteGroup _groupVeh; { _x = nil } forEach (units _groupVeh); 
        }; 
    } else { 
        if (_statementLand == "land") then { 
            _statementLast = " _veh = vehicle this; _veh land 'LAND'; _veh lock false; "; 
            [_groupVeh, _endDest, [], ["CARELESS", "YELLOW", "FULL"], _statementLast] call fnc_addWaypoint; 
        } else {  
            [_groupVeh, _endDest, [_statementMode, _statementMofidier], ["CARELESS", "YELLOW", "FULL"]] call fnc_addWaypoint; 
        }; 
    }; 
    pr_statementLast = _statementLast; // delete after testing
}; 

sleep 300; 

if (_groupVeh != grpNull) then { 
    { noHCSwap = noHCSwap - [_x]; publicVariable "noHCSwap"; } forEach units _groupVeh; 
}; 

