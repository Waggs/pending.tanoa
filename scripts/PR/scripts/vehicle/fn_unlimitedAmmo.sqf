/*  fn_unlimitedAmmo.sqf 
_nul = [this, 300] execVM "scripts\PR\scripts\vehicle\fn_unlimitedAmmo.sqf"; 
_nil = [this,5] spawn fnc_unlimitedAmmo; 
*/

_unit = _this select 0; 
_ReloadTime = _this select 1; 

while {alive _unit} do { 
    _unit setVehicleAmmo 1; 
    sleep _ReloadTime; 
}; 
