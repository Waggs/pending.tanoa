/* fn_radioBoost
SKLO setVariable ["tf_hasRadio", true, true];
SKLO setVariable ["tf_isolatedAmount", 0.0, true];
SKLO setVariable ["tf_range", 50000, true];
SKLO setVariable ["TF_RadioType", "tf_mr6000l", true];

// Player radio adjustments
player setVariable ["tf_receivingDistanceMultiplicator", 2]; // A multiplier for increasing, or lowering the distance from transmitter to receiver (player). Default 1.0
player setVariable ["tf_sendingDistanceMultiplicator", 0.5]; // A multiplier for increasing or lowering the range of transmission. Default 1.0

// Vehicle radio adjustments
_vehicle setVariable ["tf_side", _value, true]; // Forces side designation of a vehicle, and as a consequence, also the type of radios used. west/east/guer
_vehicle setVariable ["tf_hasRadio", true, true]; // Forces the presence of a vehicle-mounted radio.
_vehicle setVariable ["tf_isolatedAmount", 0.5, true]; // Sets the level of isolation for a vehicle. With values over 0.5 — it will be impossible to hear any outside speech from inside the vehicle (and vice versa). Default 0.0
_vehicle setVariable ["tf_range", 50000, true]; //Sets the maximum range of transmission for a vehicle-mounted radio. default: 30000
_vehicle setVariable ["TF_RadioType", "tf_mr6000l", true]; // Sets the type of a vehicle-mounted radio. Must be a radio from long range class. Long range radio of a faction.
TF_speakerDistance // The propagation distance of sound from radio speakers. Default 20
//player getVariable "tf_receivingDistanceMultiplicator",
//player getVariable "tf_sendingDistanceMultiplicator"
//player call TFAR_fnc_getTransmittingDistanceMultiplicator; // tf_sendingDistanceMultiplicator

Default radio is set at 1, any amount above this will boost radio strength. Any amount below 1 will weaken radio strength.
<object>, <radius min/max> , <boost/Jam amount> <action>
[[_radio, [2, 20], 4, "jam", true], "fnc_radioBoost"] call BIS_fnc_MP;


p1 setVariable ["tf_receivingDistanceMultiplicator", -1]; P1 setVariable ["tf_sendingDistanceMultiplicator", 5];

LOWER NUMBER BETTER RECEPTION
effective distance for receive = real distance * tf_receivingDistanceMultiplicator (smaller value - better quality) // 10000 * .2 = 2,000 10,000 * .5 = 5,000

// HIGHER NUMBER BETTER SENDING
effective distance for send = radio power distance * tf_sendingDistanceMultiplicator (bigger value - better quality) // 10000 * .2 =
*/
diag_log format ["*PR* fn_tfrTweeks start"];

fnc_radioBoost = {
    if ((isDedicated) || !(hasInterface)) exitWith {};
    if (player == player) then {
        private ["_obj","_radius","_multi","_action","_sender","_receiver","_d","_allow","_name"];
        _obj      = _this select 0;
        _rArray   = _this select 1;
        _radius   = _rArray select 0;
        _rMax     = _rArray select 1;
        _diff     = _rMax - _radius;
        _multi    = _this select 2;
        _action   = _this select 3;
        _sender   = 1;
        _receiver = 1;
        _amt      = 0;
        _unit     = player;
        _name     = name player;
        _b        = 0;
        _d        = 0;
        _showHint = false;
        if (_action == "jam") then { _d = 1; _action = "Jamming" } else { _action = "Boosting" };
        if (count _this > 4) then { _showHint = _this select 4 };
        _obj setVariable ["allow", true, true];

        scopename "radioScope";
        while { ((!isNull _obj) && (_obj getVariable "allow")) } do {
            if (_unit distance _obj < _radius) then {
                _b = 1;
                if (_d == 0) then { _sender = _multi; _receiver = 1 / _sender } else { _receiver = _multi; _sender = 1 / _receiver };
                _unit setVariable ["tf_sendingDistanceMultiplicator", _sender]; _unit setVariable ["tf_receivingDistanceMultiplicator", _receiver];
                if (_showHint) then { hintSilent parseText format ["%1<br/>%2 Radio 100 percent<br/>%2 power %3", _name, _action, _multi] };
                while { ((!isNull _obj) && (_obj getVariable "allow") && (_unit distance _obj < _radius)) } do { sleep 1 };
            };

            if ((_unit distance _obj > _radius) && (_unit distance _obj < _rMax)) then {
                _b = 1;
                _pct = 1 - (((_unit distance _obj) - _radius) / _diff);
                _adjust = 100 * _pct;
                _round = ((round _adjust) min 99) max 1;
                if (_d == 0) then {
                    _sender = ((_multi * _pct) + 1 - _pct); _receiver = 1 / _sender; _amt = _sender;
                } else {
                    _receiver = ((_multi * _pct) + 1 - _pct); _sender = 1 / _receiver; _amt = _receiver;
                };
                _unit setVariable ["tf_sendingDistanceMultiplicator", _sender]; _unit setVariable ["tf_receivingDistanceMultiplicator", _receiver];
                if (_showHint) then { hintSilent parseText format ["%1<br/>%2 Radio %3 percent<br/>%2 power %4", _name, _action, _round, _amt] };
            }; 

            if ((_unit distance _obj > _rMax) && (_b == 1)) then {
                if (_showHint) then { hintSilent parseText format ["%1<br/>%2 Radio Off", _name, _action] };
                _b = 0;
                _unit setVariable ["tf_sendingDistanceMultiplicator", 1]; _unit setVariable ["tf_receivingDistanceMultiplicator", 1];
            };

            sleep 1;
            if ((isNull _obj) || !(_obj getVariable "allow")) then { breakto "radioScope" };
        };
        _unit setVariable ["tf_sendingDistanceMultiplicator", 1]; _unit setVariable ["tf_receivingDistanceMultiplicator", 1];
        if (_showHint) then { hintSilent parseText format ["%1<br/>%2 Radio Off", _name, _action] };
    };
};

diag_log format ["*PR* fn_tfrTweeks complete"];
