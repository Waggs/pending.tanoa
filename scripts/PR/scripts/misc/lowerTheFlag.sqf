_height = 0; 
_flagUP = _this select 0; 
_unit  = _this select 1; 
_flagUP removeAction (_this select 2); 
_flagE = _flagUP getvariable "flagname"; 
_height = getpos _flagUP select 2; 
_unit action ["TakeFlag", _flagE]; 

while { (getpos _flagUP select 2 ) > -7 } do { 
    _flagUP setpos [ getpos _flagE select 0, getpos _flagE select 1, _height]; 
    _height =_height - 0.015; 
     sleep 0.01; 
}; 

_flagE addaction ["Raise Flag", "scripts\PR\scripts\misc\flyTheFlag.sqf", [], 1, false, true, "", " _this distance _target < 5"]; 
_flagE setvariable ["Flag",false,true];  
_flagUP setvariable ["FlagName",nil,true]; 

deletevehicle _flagUP; 
