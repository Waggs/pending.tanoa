//--- load_rscTitles.hpp 
//--- Adds Reaper screen at mission start 

class ReapPause { 
    idd = -1; 
    fadein = 0; 
    fadeout = 3; 
    duration = 6; 
    class controls { 
        class BackgroundBlack { 
            idc = -1; 
            type = 0; 
            style = 0; 
            x = safeZoneXAbs; 
            y = safeZoneY; 
            h = safeZoneH; 
            w = safeZoneWAbs; 
            font = "EtelkaNarrowMediumPro"; 
            sizeEx = 0.05; 
            colorBackground[] = {0,0,0,1}; 
            text = ""; 
            colorText[] = {0.5,0.5,0.5,1}; 
            lineSpacing = 1; 
        }; 
        class ReapControl { 
            idc = -1; 
            type = 0; 
            style = 48; 
            x = 0.25 * safeZoneW + safezoneX; 
            y = safeZoneY + (safezoneH - (0.5 * safeZoneW * 4/3)) / 2; 
            w = 0.5 * safeZoneW; 
            h = (0.5 * safeZoneW * 4/3); 
            font = "EtelkaNarrowMediumPro"; 
            sizeEx = 0.05; 
            colorBackground[] = {0,0,0,0}; 
            text = "scripts\PR\scripts\misc\loading\load.paa"; 
            colorText[] = {1,1,1,1}; 
            lineSpacing = 1; 
        }; 
    }; 
}; 

class ReapPauseAce { 
    idd = -1; 
    fadein = 0; 
    fadeout = 3; 
    duration = 30; 
    class controls { 
        class BackgroundBlack { 
            idc = -1; 
            type = 0; 
            style = 0; 
            x = safeZoneXAbs; 
            y = safeZoneY; 
            h = safeZoneH; 
            w = safeZoneWAbs; 
            font = "EtelkaNarrowMediumPro"; 
            sizeEx = 0.05; 
            colorBackground[] = {0,0,0,1}; 
            text = ""; 
            colorText[] = {0.5,0.5,0.5,1}; 
            lineSpacing = 1; 
        }; 
        class ReapControl { 
            idc = -1; 
            type = 0; 
            style = 48; 
            x = 0.25 * safeZoneW + safezoneX; 
            y = safeZoneY + (safezoneH - (0.5 * safeZoneW * 4/3)) / 2; 
            w = 0.5 * safeZoneW; 
            h = (0.5 * safeZoneW * 4/3); 
            font = "EtelkaNarrowMediumPro"; 
            sizeEx = 0.05; 
            colorBackground[] = {0,0,0,0}; 
            text = "scripts\PR\scripts\misc\loading\load.paa"; 
            colorText[] = {1,1,1,1}; 
            lineSpacing = 1; 
        }; 
    }; 
}; 