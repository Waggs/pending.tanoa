//--- usage: 0 = [this, 600, 1, []] execVM "scripts\PR\scripts\misc\destroyed_building_alive.sqf";
if !(isServer) exitWith {}; 
//sleep 5; 
waitUntil { sleep 1; !isNil "ALIVE_profileSystemInit" }; 

_center = _this select 0; 
_radius = _this select 1; 
_damage = _this select 2; 

_exclude = []; 
_exclude = _this select 3; 

if (isServer && isDedicated) then { 
    if (isNil {["task1complete"] call ALiVE_fnc_getData}) then { 
        _buildings = nearestObjects [_center, ["Static"], _radius]; {_x setDamage (random _damage) + 1/4;} forEach _buildings - _exclude; 
        buildings = _buildings; publicVariable "buildings"; 
    }; 
} else { 
    if (isServer && !(isDedicated)) then { 
        _buildings = nearestObjects [_center, ["Static"], _radius]; {_x setDamage (random _damage) + 1/4;} forEach _buildings - _exclude; 
        buildings = _buildings; publicVariable "buildings"; 
    }; 
}; 
