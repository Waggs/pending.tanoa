arp
cond: this && ((p5) in thisList) && (genStart)
on act: actionGenOff = p5 addAction ["<t color=""#00aeff"">" +"Disable Generator", "scripts\PR\scripts\misc\gen\gen_quad_off.sqf"];
on dea: p5 removeAction actionGenOff; 

arp
cond: this && ((p5) in thisList) && !(genStart)
on act: actionGenOn = p5 addAction ["<t color=""#00aeff"">" +"Repair Generator", "scripts\PR\scripts\misc\gen\gen_quad_on.sqf"];
on dea: p5 removeAction actionGenOn; 



genQuadO = true; publicVariable "genStart";



cond: genQuad
on act: gen_quad engineOn true; genStart = true;
on dea: gen_quad engineOn false; genStart = false;