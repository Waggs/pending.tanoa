/* fn_doorLocks.sqf
*  Author: PapaReap
*  function name: pr_fnc_doorLocks
*  ver 1.0 2016-08-24
*
*  Arguments:
*  0: <BUILDING>      e.g. - (REQUIRED) name of building to lock door
*
*  1: <DOOR NUMBER>   e.g. - (REQUIRED) building door number
*
*  2: <LOCK STATE>    e.g. - (REQUIRED) lock state of door, true = door locked, false = door unlocked. (bool)
*
*  EXAMPLES:
*  [house1, 2, true] remoteExec ["pr_fnc_doorLocks", 0, false];
*  [house1, 2, false] remoteExec ["pr_fnc_doorLocks", 0, false];
*/
if !(isServer) exitWith {};

_building = _this select 0;
_door = _this select 1;
_lock = _this select 2;

if (_lock) then { _lock = 1; } else { _lock = 0; };
_doorNum = "";

_doorNum = format [ "%1_%2", "bis_disabled_Door", _door ];
_building setVariable [_doorNum, _lock, true];
