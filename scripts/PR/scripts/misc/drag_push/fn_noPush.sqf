/* fn_noPush.sqf 
*  Author: PapaReap 
*  Removes object from pushable 
*  Object init: [this] call pr_fnc_noPush; 
*  ver 1.0 2016-04-23 
*/ 

_object = _this select 0; 
pr_noPushArray = pr_noPushArray + [_object]; publicVariable "pr_noPushArray"; 
