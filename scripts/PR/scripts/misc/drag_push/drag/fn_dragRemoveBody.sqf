/* fn_dragRemoveBody.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragRemoveBody

*  Remove body from vehicle
*/

_vehicle = _this select 0;
_player = _this select 1;
_vehUnloadId = _this select 2;
_loadedBodies = _vehicle getVariable "prLoadedBodies";
_unitToUnload = [];

{
        if (_x getVariable "pr_vehUnloadId" == _vehUnloadId) then {
            _unitToUnload = _unitToUnload + [_x];
            [_vehicle, _vehUnloadId] remoteExec ["pr_fnc_dragRemoveUnloadAction", 0, true];
        };
} forEach _loadedBodies;

_unit = _unitToUnload select 0;
if (_unit getVariable "pr_ghost") then {
    [_unit] remoteExec ["pr_fnc_dragReappear", 0, true];
    _unit setVariable ["pr_ghost", false, true];
};
[_unit, _player] call pr_fnc_dragActionWhileDrag;
_unit setVariable ["pvpfw_cleanUp_keep", false];
_loadedBodies = _loadedBodies - [_unit];
_vehicle setVariable ["prLoadedBodies", _loadedBodies, true];
