/* fn_dragLoad.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragLoad

*  Load dragged unit in vehicle
*/

_player = _this select 0;
_loadID = _this select 2;
_releaseID = _player getVariable "pr_releaseID";
_unit = _this select 3;
_vehicle = cursorTarget;
 
_loadedBodies = _vehicle getVariable "prLoadedBodies";
if (isNil "_loadedBodies") then {
        _loadedBodies = [];
        _vehicle setVariable ["prLoadedBodies", _loadedBodies, true];
};

_player removeAction _loadID;
_player removeAction _releaseID;
_player playMove "amovpknlmstpsraswrfldnon";
_player forceWalk false;

_unitID = _unit getVariable "pr_dragID";
[_unit, vehicle _player, _unitID] remoteExec ["pr_fnc_dragDetachDragged", 0, true];
sleep 1;
detach _unit;

if (((_vehicle emptyPositions "cargo") > 0) || (_vehicle isKindOf "Air")) then {
    if ((_vehicle emptyPositions "cargo") > 0) then {
        if (alive _unit) then {
            _unit assignAsCargo _vehicle; // test this
            _unit moveInCargo _vehicle;
            _cargoIndex = _vehicle getCargoIndex _unit;
            [_unit, _cargoIndex, _vehicle] remoteExec ["pr_fnc_dragMoveInCargo", 0, true];
        } else {
            [_unit] remoteExec ["pr_fnc_dragDisappear", 0, true];
            _unit setVariable ["pr_ghost", true, true];
        };
    };
    _loadedBodies = _loadedBodies + [_unit];
    _vehicle setVariable ["prLoadedBodies", _loadedBodies, true];
    [_vehicle, _unit] remoteExec ["pr_fnc_dragActionUnload", 0, true];
} else {
    ["<t size='0.7' color='#ff5000'>" + "No room left in vehicle" + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
};
