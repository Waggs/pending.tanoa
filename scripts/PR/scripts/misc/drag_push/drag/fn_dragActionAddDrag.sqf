/* fn_dragActionAddDrag.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragActionAddDrag

*  Drag, knockout or kill actions
*/

_dragCond = "";
_KOcond = "";
_getUnconscious = false;
{
    if (alive _x) then {
        _side = side _x;
        _x setVariable ["pr_dragSide", _side, true];
        _x setVariable ["pr_dragName", name _x, true];
        _x setVariable ["pr_ghost", false, true];

        [_x] remoteExec ["pr_fnc_dragKilled", 0, false];

        //--- drag action
        if !(aceOn) then {
            if (isNil { _x getVariable ["PR_randomUnconcious", Nil] }) then {
                _x setVariable ["PR_randomUnconcious", false, false];
                _getUnconscious = _x getVariable "PR_randomUnconcious";
            } else {
                _getUnconscious = _x getVariable "PR_randomUnconcious";
            };

            _dragCond = "(
                (vehicle _this != vehicle _target)
                && (isNull attachedTo _target)
                && (count attachedObjects _this == 0)
                && (_target distance _this < 2)
                && (alive _target)
                && (_target getVariable 'PR_randomUnconcious')
            )";
            _x addaction [[_x, "Drag"] call pr_fnc_dragActionText, { call pr_fnc_dragActionWhileDrag }, nil, 6, false, false, "", _dragCond];
        };

        //--- knockout or kill action
        _koCond = "(
            ((alive _target) && (_target isKindOf 'Man') && (speed _target < 0.3) &&  (player distance _target < 2 ))
            && (
                ((stance _target == 'STAND') && ((stance player == 'STAND') || (stance player == 'CROUCH')|| (stance player == 'PRONE')))
                || ((stance _target == 'CROUCH') && ((stance player == 'STAND') || (stance player == 'CROUCH') || (stance player == 'PRONE')))
                || ((stance _target == 'PRONE') && ((stance player == 'CROUCH') || (stance player == 'PRONE')))
            )
            && !(_target getVariable 'PR_randomUnconcious')
            && !(_target getVariable['pr_knockOut',false])
        )";

        if !([side player, side _x] call BIS_fnc_sideIsEnemy) then {
            _koID = _x addaction [[_x, "Knock Out"] call pr_fnc_dragActionText, { call pr_fnc_dragActionKnockOut }, nil, 6, false, false, "", _koCond];
            _x setVariable ["pr_koID", _koID];
            _x setVariable ["pr_koAction", true];
        } else {
            _koID = _x addaction ["<t color='#FC9512'>Kill Soldier</t>", { call pr_fnc_dragActionKnockOut }, nil, 6, false, false, "", _koCond];
            _x setVariable ["pr_koID", _koID];
            _x setVariable ["pr_koAction", false];
        };
    } else {
        //--- add drag to dead
        removeAllActions _x;
        _dragCond = "vehicle _this != vehicle _target && isNull attachedTo _target && count attachedObjects _this == 0 && _target distance _this < 2";
        _x addaction [[_x, "Drag"] call pr_fnc_dragActionText, { call pr_fnc_dragActionWhileDrag }, nil, 6, false, false, "", _dragCond];
    };
} forEach _this;
