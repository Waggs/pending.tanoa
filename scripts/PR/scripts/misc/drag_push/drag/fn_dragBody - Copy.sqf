/* fn_dragBody.sqf
*  Author: PapaReap

*  ver 1.0 - 2016-05-31 

 
*  Description: Drag uncnconscious or dead bodies out of sight to avoid detection. JIP/MP/SP/Dedicated compatible
*  Instructions: Add this line into the init.sqf. This will add a drag action to all editor placed units.
*  null = allUnits execVM "scripts\PR\scripts\misc\push\fn_dragBody.sqf";

*  Functions
*  0 = [this] spawn pr_fnc_dragAddDrag;
*  Add living units into the script. (Useful for units spawned mid-mission).

*  Credits to BangaBob (H8erMaker), Tajin, Norrin & Bohemia wiki for inspiration and guidance on developing this script
*/

waitUntil { !isNil "dragInitized" }; if !(dragInitized) exitWith {};

/*
//--- text to apply to add actions - function
pr_fnc_actionText = {
    _action = "";
    _unit = _this select 0;
    _action = _this select 1;

    if ((alive _unit) && ((_unit getVariable "pr_dragSide") == side player) || ((_unit getVariable "pr_dragSide") == Civilian)) then {
        format ["<t color='#FC9512'>%1 %2</t>", _action, _unit getVariable "pr_dragName"];
    } else {
        if (!(alive _unit) && ((_unit getVariable "pr_dragSide") == side player)) then {
            format ["<t color='#FC9512'>%1 %2's Corpse</t>", _action, _unit getVariable "pr_dragName"];
        } else {
            format ["<t color='#FC9512'>%1 Corpse</t>", _action];
        };
    };
};
*/

/*
//--- drag & knockout or kill actions
pr_addDragAction = {
    _dragCond = "";
    _KOcond = "";
    _getUnconscious = false;
    {
        if (alive _x) then {
            _side = side _x;
            _x setVariable ["pr_dragSide", _side, true];
            _x setVariable ["pr_dragName", name _x, true];
            _x setVariable ["pr_ghost", false, true];

            [_x] remoteExec ["pr_fnc_dragKilled", 2, false];
            //[_x] remoteExec ["pr_fnc_drag_killed", 2, false];

            //--- drag action
            if !(aceOn) then {
                if (isNil { _x getVariable ["PR_randomUnconcious", Nil] }) then {
                   _x setVariable ["PR_randomUnconcious", false, false];
                    _getUnconscious = _x getVariable "PR_randomUnconcious";
                } else {
                    _getUnconscious = _x getVariable "PR_randomUnconcious";
                };

                _dragCond = "(
                    (vehicle _this != vehicle _target)
                    && (isNull attachedTo _target)
                    && (count attachedObjects _this == 0)
                    && (_target distance _this < 2)
                    && (alive _target)
                    && (_target getVariable 'PR_randomUnconcious')
                )";
                _x addaction [[_x, "Drag"] call pr_fnc_dragActionText, { call pr_whileDragAction }, nil, 6, false, false, "", _dragCond];
            };

            //--- knockout or kill action
            _koCond = "(
                ((alive _target) && (_target isKindOf 'Man') && (speed _target < 0.3) &&  (player distance _target < 2 ))
                && (
                    ((stance _target == 'STAND') && ((stance player == 'STAND') || (stance player == 'CROUCH')|| (stance player == 'PRONE')))
                    || ((stance _target == 'CROUCH') && ((stance player == 'STAND') || (stance player == 'CROUCH') || (stance player == 'PRONE')))
                    || ((stance _target == 'PRONE') && ((stance player == 'CROUCH') || (stance player == 'PRONE')))
                )
                && !(_target getVariable 'PR_randomUnconcious')
                && !(_target getVariable['pr_knockOut',false])
            )";

            if !([side player, side _x] call BIS_fnc_sideIsEnemy) then {
                _koID = _x addaction [[_x, "Knock Out"] call pr_fnc_dragActionText, { call pr_knockOutAction }, nil, 6, false, false, "", _koCond];
                _x setVariable ["pr_koID", _koID];
                _x setVariable ["pr_koAction", true];
            } else {
                _koID = _x addaction ["<t color='#FC9512'>Kill Soldier</t>", { call pr_knockOutAction }, nil, 6, false, false, "", _koCond];
                _x setVariable ["pr_koID", _koID];
                _x setVariable ["pr_koAction", false];
            };
        } else {
            //--- add drag to dead
            removeAllActions _x;
            _dragCond = "vehicle _this != vehicle _target && isNull attachedTo _target && count attachedObjects _this == 0 && _target distance _this < 2";
            _x addaction [[_x, "Drag"] call pr_fnc_dragActionText, { call pr_whileDragAction }, nil, 6, false, false, "", _dragCond];
        };
    } forEach _this;
};
*/

/*
//--- adding killed eventhandler to unit
pr_fnc_killed = {
    _unit = _this select 0;
    _unit addEventHandler ["killed", {
        _unit = _this select 0;
        //removeAllActions _unit;
        //_dragCond = "vehicle _this != vehicle _target && isNull attachedTo _target && count attachedObjects _this == 0 && _target distance _this < 2 && !(alive _target)";
        //_unit addaction [[_unit, "Drag"] call pr_fnc_actionText, { call pr_whileDragAction }, nil, 6, false, false, "", _dragCond];
        //sleep 0.5;
        [_unit] remoteExec ["pr_fnc_addDrag", 2, false]; // possibly add above to remoteexec instead? will test this first
    }];
};
*/

/*
//--- knockout or kill unit action
pr_knockOutAction = {
    _unit = _this select 0;
    _player = _this select 1; pr_koDragger = _player;
    _koID = (_this select 2);
    _koAction = _unit getVariable "pr_koAction";
    //player switchMove "AwopPercMstpSgthWnonDnon_end";
    _player switchMove "AwopPercMstpSgthWnonDnon_end";
    sleep 0.5;

    if !(_koAction) then {
        _unit setDamage 1;
        _unit removeAction _koID;//untested
    } else {
        //[ [_unit, 30 + random 120, _player], "pr_fnc_knockOut", nil, true] spawn BIS_fnc_MP;
        [_unit, 30 + random 120, _player] remoteExec ["pr_fnc_dragKnockOut", 0, true]; // check this one out
    };
    //_unit removeAction _koID;
};
*/

/*
//--- knockout unit
pr_fnc_knockOut = {
    _unit = _this select 0;
    _time = _this select 1;
    _player = _this select 2;
    _unit setVariable ["pr_knockOut", true];
    [_unit, [true, _time, true], [false], [true]] spawn pr_fnc_randomBodyDamage;
    //[_unit, "PRONE_INJURED_U2"] call BIS_fnc_ambientAnim;
    //[_unit, "PRONE_INJURED"] call BIS_fnc_ambientAnim; 
    _unit playActionNow "agonyStart";
    sleep 0.5;
    [_unit, _player] call pr_whileDragAction;
    sleep _time;
    _unit setVariable ["PR_randomUnconcious", false, false];
    _unit setVariable ["pr_knockOut", false];
};
*/

/*
//--- while dragging, add action to release or load
pr_whileDragAction = {
    _unit = _this select 0;
    _player = _this select 1;

    //player playAction "grabDrag";
    //player forceWalk true;
    _player playAction "grabDrag";
    _player forceWalk true;

    _unitID = _unit getVariable "pr_dragID";
    //[_unit, vehicle _player, _unitID] remoteExec ["pr_fnc_attachWhileDragging", 0, true];
    [_unit, vehicle _player, _unitID] remoteExec ["pr_fnc_dragAttachWhileDragging", 0, true];
    _unit attachTo [_player, [0,1,0]];

    _releaseID = _player addAction [[_unit, "Release"] call pr_fnc_dragActionText, { call pr_fnc_dragRelease }, _unit, 6];
    _player setVariable ["pr_releaseID", _releaseID];

    _loadCond = "(
        (_target distance cursorTarget < 6)
        && (((cursorTarget isKindOf 'LandVehicle') || (cursorTarget isKindOf 'Air') || (cursorTarget isKindOf 'Ship')) && (cursorTarget emptyPositions 'cargo' > 0))
    )";
    _loadID = _player addAction [[_unit, "Load"] call pr_fnc_dragActionText, { call pr_fnc_dragLoad }, _unit, 6, false, false, "", _loadCond];
    _player setVariable ["pr_loadID", _loadID];

    //--- prevent player from entering vehicle while dragging //*********** issues here
    _player spawn { // needs testing on multiplayer
        private ["_attached","_player","_releaseID","_unit","_vehicle"];
        _player = _this;
        //_lockedVeh = [];
        while { (count (attachedObjects _player) > 0) } do { // need to add something to release if dead or unconscious
            if !(vehicle _player == _player) then {
                _attached = attachedObjects _player;
                _unit = _attached select 0;
                _vehicle = vehicle _player;
                _releaseID = _player getVariable "pr_releaseID";
                _unit = _attached select 0;
                [_player, _vehicle, _releaseID, _unit] call pr_fnc_dragRelease;
                hint "dragged detached";
            };
        };
    };
};
*/

/*
//--- release
pr_fnc_release = {
    _player = _this select 0;
    _releaseID = _this select 2;
    _loadID = _player getVariable "pr_loadID";
    _unit = _this select 3;


    _unitID = _unit getVariable "pr_dragID";
    [_unit, vehicle _player, _unitID] remoteExec ["pr_fnc_detachDragged", 0, true];

    detach _unit;
    if !(vehicle _player == _player) then {
        _vehicle = vehicle _player;
        sleep 0.5;
        _dropPos = [(getpos _vehicle select 0) - 0,(getpos _vehicle select 1) - 4, 0]; 
        _unit setPos _dropPos;
    };

    _player removeAction _releaseID;
    _player removeAction _loadID;
    if (vehicle _player == _player) then {
        _player playMove "amovpknlmstpsraswrfldnon";
    };
    _player forceWalk false;
};
*/

/*
//--- load
pr_fnc_load = {
    _player = _this select 0;
    _loadID = _this select 2;
    _releaseID = _player getVariable "pr_releaseID";
    _unit = _this select 3;
    _vehicle = cursorTarget;
 
    _loadedBodies = _vehicle getVariable "prLoadedBodies";
    if (isNil "_loadedBodies") then {
        _loadedBodies = [];
        _vehicle setVariable ["prLoadedBodies", _loadedBodies, true];
    };

    _player removeAction _loadID;
    _player removeAction _releaseID;
    _player playMove "amovpknlmstpsraswrfldnon";
    _player forceWalk false;

    _unitID = _unit getVariable "pr_dragID";
    [_unit, vehicle _player, _unitID] remoteExec ["pr_fnc_detachDragged", 0, true];
    sleep 1;
    detach _unit;

    if (((_vehicle emptyPositions "cargo") > 0) || (_vehicle isKindOf "Air")) then {
        if ((_vehicle emptyPositions "cargo") > 0) then {
            if (alive _unit) then {
                _unit assignAsCargo _vehicle; // test this
                _unit moveInCargo _vehicle;
                _cargoIndex = _vehicle getCargoIndex _unit; //character assignAsCargoIndex [vehicle, index]
                //[[_unit, _cargoIndex, _vehicle], "pr_fnc_moveInCargo", true] call BIS_fnc_MP;
                [_unit, _cargoIndex, _vehicle] remoteExec ["pr_fnc_moveInCargo", 0, true];
            } else {
                [_unit] remoteExec ["pr_fnc_disappear", 0, true]; //[[mark], "pr_fnc_disappear", true] call BIS_fnc_MP;
                _unit setVariable ["pr_ghost", true, true];
            };
        };
        _loadedBodies = _loadedBodies + [_unit];
        _vehicle setVariable ["prLoadedBodies", _loadedBodies, true];
        [_vehicle, _unit] remoteExec ["pr_unloadAction", 0, true];
    } else {
        ["<t size='0.7' color='#ff5000'>" + "No room left in vehicle" + "</t>" ,0.7,0.7,10,0] spawn BIS_fnc_dynamicText;
    };
};
*/

/*
pr_fnc_moveInCargo = {
    _unit = _this select 0;
    _cargoIndex = _this select 1;
    _vehicle = _this select 2;
    _unit assignAsCargoIndex [_vehicle, _cargoIndex];
    _unit moveInCargo _vehicle;
    _unit spawn {
        _unit = _this;
        sleep 0.1;
        _unit switchMove "KIA_Driver_low01";
    };
};
*/

/*
pr_fnc_disappear = {
    _unit = _this select 0;
    if (isServer) then { _unit hideObjectGlobal true; };
};
*/

/*
pr_fnc_reAppear = {
    _unit = _this select 0;
    if (isServer) then { _unit hideObjectGlobal false; };
};
*/

/*
//--- unload
pr_unloadAction = {
    _vehicle = _this select 0;
    _unit = _this select 1;
    _vehicle setVariable ["prLoadedBody", _unit, false];
    _unloadCond = "((_this distance _target < 6) && (vehicle _this == _this))";
    _vehUnloadId = _vehicle addAction [
        [_unit, "Unload"] call pr_fnc_dragActionText,
        //{ call pr_fnc_removeBody },
        { call pr_fnc_dragRemoveBody },
        nil, 0, false, false, "", _unloadCond
    ];
    _unit setVariable ["pr_vehUnloadId", _vehUnloadId];
};
*/

/*
//--- remove body from vehicle
pr_fnc_removeBody = {
    _vehicle = _this select 0;
    _player = _this select 1;
    _vehUnloadId = _this select 2;
    _loadedBodies = _vehicle getVariable "prLoadedBodies";
    _unitToUnload = [];

    //[_vehicle, _vehUnloadId] remoteExec ["pr_fnc_removeUnloadAction", 0, true];
 
    {
        if (_x getVariable "pr_vehUnloadId" == _vehUnloadId) then {
            _unitToUnload = _unitToUnload + [_x];
		    [_vehicle, _vehUnloadId] remoteExec ["pr_fnc_removeUnloadAction", 0, true]; // needs testing if don't work uncomment above
        };
    } forEach _loadedBodies;

    _unit = _unitToUnload select 0;
    if (_unit getVariable "pr_ghost") then {
        //[_unit] remoteExec ["pr_fnc_reAppear", 0, true];
        [_unit] remoteExec ["pr_fnc_dragReappear", 0, true];
        _unit setVariable ["pr_ghost", false, true];
    };
    [_unit, _player] call pr_whileDragAction;
    _unit setVariable ["pvpfw_cleanUp_keep", false];
    _loadedBodies = _loadedBodies - [_unit];
    _vehicle setVariable ["prLoadedBodies", _loadedBodies, true];
};
*/

/*
//--- remove unload action
pr_fnc_removeUnloadAction = {
    _vehicle = (_this select 0);
    _vehUnloadId = (_this select 1);
    _vehicle removeAction _vehUnloadId;
};
*/

/*
//--- attach while being dragged
pr_fnc_attachWhileDragging = {
    _unit = (_this select 0);
    _player = (_this select 1);
    _unitID = (_this select 2);

    _id = format ["h8EF%1", _unitID];
    0 = [_id, "onEachFrame", "pr_fnc_moveDragged", [_unit, _player]] call BIS_fnc_addStackedEventHandler;
};
*/

/*
//--- detach dragged unit
pr_fnc_detachDragged = {
    _unit = (_this select 0);
    _player = (_this select 1);
    _unitID = (_this select 2);

    _id = format ["h8EF%1", _unitID];
    0 = [_id, "onEachFrame"] call BIS_fnc_removeStackedEventHandler;
 
    sleep 0.05;
    _dirTo = [_unit, _player] call BIS_fnc_dirTo;


    _unit switchMove "AinjPpneMstpSnonWrflDb_release";
    //[_unit, "PRONE_INJURED_U2"] call BIS_fnc_ambientAnim;
    _unit setDir (_dirTo - 180);
};
*/

/*
//--- keep dragged unit moving with player
pr_fnc_moveDragged = {
    _unit = (_this select 0);
    _player = (_this select 1);
    _pos = _player modelToWorld [0,1,0];
    _unit setPos _pos;
    _unit setDir 180;
    _unit switchMove "AinjPpneMrunSnonWnonDb";
};
*/

/*
//--- add drag to unit, trigger or script
pr_fnc_addDrag = {
    {
        pr_dragTotal = pr_dragTotal + 1;
        //removeAllActions _x;// untested
        _x setVariable ["pr_dragID", pr_dragTotal, true];
        pr_dragArray set [count pr_dragArray, _x];
    } forEach _this;

    //[_this, "pr_addDragAction", true] call BIS_fnc_MP; //0 = [joe,joe_1,joe_2,mark] spawn pr_fnc_addDrag;
    _this remoteExec ["pr_addDragAction", 0, true];

    publicVariable "pr_dragTotal";
    publicVariable "pr_dragArray";
};
*/

//--- server variables
if (isServer) then {
    if (isNil ("pr_dragArray")) then { pr_dragArray = []; };
    if (isNil ("pr_dragTotal")) then { pr_dragTotal = 0; };

    {
        pr_dragTotal = pr_dragTotal + 1;
        _x setVariable ["pr_dragID", pr_dragTotal, true];
        //pr_dragArray set [count pr_dragArray, _x];
    } forEach pr_dragArray;

    publicVariable "pr_dragTotal";
    publicVariable "pr_dragArray";
    serverDragInit = true; publicVariable "serverDragInit";
};

if (!isServer && (player != player)) then {
    waitUntil { player == player }; 
};

if ((!isDedicated) && (hasInterface)) then {
    _this spawn {
        //waitUntil { !isNil "dragInitized" };
        waitUntil { !isNil "serverDragInit" };
        //0 = pr_dragArray spawn pr_addDragAction;
        0 = pr_dragArray spawn pr_fnc_dragActionAddDrag;
    };
};
