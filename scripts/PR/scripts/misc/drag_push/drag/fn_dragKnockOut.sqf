/* fn_dragKnockOut.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragKnockOut

*  Knockout unit
*/

_unit = _this select 0;
_time = _this select 1;
_player = _this select 2;

_unit setVariable ["pr_knockOut", true];
[_unit, [true, _time, true], [false], [true]] spawn pr_fnc_randomBodyDamage;
_unit playActionNow "agonyStart";
sleep 0.5;

[_unit, _player] call pr_fnc_dragActionWhileDrag;
sleep _time;

_unit setVariable ["PR_randomUnconcious", false, false];
_unit setVariable ["pr_knockOut", false];
