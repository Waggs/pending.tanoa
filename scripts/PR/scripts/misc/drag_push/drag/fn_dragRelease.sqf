/* fn_dragRelease.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragRelease

*  Release dragged unit
*/

_player = _this select 0;
_releaseID = _this select 2;
_loadID = _player getVariable "pr_loadID";
_unit = _this select 3;

_unitID = _unit getVariable "pr_dragID";
[_unit, vehicle _player, _unitID] remoteExec ["pr_fnc_dragDetachDragged", 0, true];

detach _unit;

if !(vehicle _player == _player) then {
    _vehicle = vehicle _player;
    sleep 0.5;
    _dropPos = [(getpos _vehicle select 0) - 0, (getpos _vehicle select 1) - 4, 0]; 
    _unit setPos _dropPos;
};

_player removeAction _releaseID;
_player removeAction _loadID;

if (vehicle _player == _player) then {
    _player playMove "amovpknlmstpsraswrfldnon";
};

_player forceWalk false;
