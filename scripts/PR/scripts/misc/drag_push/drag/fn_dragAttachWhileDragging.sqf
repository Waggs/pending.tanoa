/* fn_dragAttachWhileDragging.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragAttachWhileDragging

*  Attach unit to dragger while being dragged
*/

_unit = (_this select 0);
_player = (_this select 1);
_unitID = (_this select 2);

_id = format ["h8EF%1", _unitID];
0 = [_id, "onEachFrame", "pr_fnc_dragMoveDragged", [_unit, _player]] call BIS_fnc_addStackedEventHandler;
