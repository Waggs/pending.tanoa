/* fn_dragActionText.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragActionText

*  Text to apply to add actions
*/

_action = "";
_unit = _this select 0;
_action = _this select 1;

if ((alive _unit) && ((_unit getVariable "pr_dragSide") == side player) || ((_unit getVariable "pr_dragSide") == Civilian)) then {
    format ["<t color='#FC9512'>%1 %2</t>", _action, _unit getVariable "pr_dragName"];
} else {
    if (!(alive _unit) && ((_unit getVariable "pr_dragSide") == side player)) then {
        format ["<t color='#FC9512'>%1 %2's Corpse</t>", _action, _unit getVariable "pr_dragName"];
    } else {
        format ["<t color='#FC9512'>%1 Corpse</t>", _action];
    };
};
