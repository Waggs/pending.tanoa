


_position = _this select 0;
_radius = _this select 1;
_percent = _this select 2;
_blacklist = _this select 3;


_pos = [0,0,0];
_terrainobjects = [];
_total = 0;

if (typeName _position == "STRING") then {
    _pos = getMarkerPos _position;
} else {
    if (typeName _position == "ARRAY") then {
        _pos = _position; 
    } else {
        _pos = getPos _position; 
    };
};

_terrainobjects = nearestTerrainObjects [_pos, [], _radius];
_terrainobjects = _terrainobjects - _blacklist;

if !(_percent == 100) then {
    _count = count _terrainobjects;
    _total = _count * (_percent / 100);
    _i = 0;
    while { _i = _i + 1; (_i < _total) } do {
        _rTO = _terrainobjects call BIS_fnc_selectRandom;
        _terrainobjects = _terrainobjects - [_rTO];
        hideObjectGlobal _rTO;
    };
} else {
    { hideObjectGlobal _x } foreach _terrainobjects;
};

clearMapObject = true;
