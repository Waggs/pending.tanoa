//--- fn_windSound.sqf 

private ["_stormWindSound", "_stormWindSleep", "_mintime", "_maxtime", "_random","_timeSilence"];
_stormWindSound = ""; 
_stormWindSleep = 0; 

if ((["missionWeather", 1] call BIS_fnc_getParamValue == 19) || (["missionWeather"] call BIS_fnc_getParamValue == 20)) then { 
    _stormWindSound = "highWind"; 
    _stormWindSleep = 35; 
} else {  
    if (["missionWeather", 1] call BIS_fnc_getParamValue == 21) then { 
        _stormWindSound = "blizzard"; 
        _stormWindSleep = 51; 
    } else {  
        if (["missionWeather", 1] call BIS_fnc_getParamValue == 22) then { 
            _stormWindSound = "blizzard2"; 
            _stormWindSleep = 51; 
        }; 
    }; 
}; 

_random = true; 
_mintime = 0; 
_maxtime = 0; 
_timeSilence = _mintime; 

while {true} do { 
    playSound _stormWindSound; 
    sleep _stormWindSleep; 
    if(_random) then { 
        _timeSilence = _mintime + (random (_maxtime - _mintime)); 
    }; 
    sleep _timeSilence; 
}; 
