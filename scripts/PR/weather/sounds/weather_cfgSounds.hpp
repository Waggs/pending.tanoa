//--- weather_cfgSounds.hpp 
//--- Adds wind sounds for snow missions to class CfgSounds 

class highWind { 
    name = "highWind"; 
    sound[] = {"scripts\PR\scripts\weather\sounds\highWind.ogg", db 0, 2.0}; 
    titles[]={}; 
}; 

class blizzard { 
    name = "blizzard"; 
    sound[] = {"scripts\PR\scripts\weather\sounds\blizzard.ogg", db -10, 1.0}; 
    titles[]={}; 
}; 

class blizzard2 { 
    name = "blizzard"; 
    sound[] = {"scripts\PR\scripts\weather\sounds\blizzard.ogg", db 10, 1.0}; 
    titles[]={}; 
}; 
