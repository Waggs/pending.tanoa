

_mapClickOnly = false;
_nearestPlayer = false;
_nearestLeader = false;
_nearestRooftop = false;
_nearestSmoke = false;

_pickupUnload = false;
_fastRope = false;
_moveHover = false;
_landEngineOn = false;
_landEngineOff = false;

_transSpeed = "NORMAL";
//(startHeliTimer) && (heli_transport)
przzARr = _this;
_marker = _this select 0;

_heli = pr_heliTrans;
_heliGroup = pr_heliTransGroup;
[_heliGroup, true] call pr_fnc_deleteWaypoint;
{ deleteVehicle _x } forEach pr_landPadArray; 
pr_landPadArray = []; publicVariable "pr_landPadArray";
//missionNameSpace getVariable "wpMarker";

_quickMove = false;
if (missionNamespace getVariable "quickMove") then {
    _quickMove = true;
};

if !(_quickMove) then {
    _posType = _this select 1;    // "MapClickOnly", "NearestPlayer", "NearestLeader", "NearestRooftop", "NearestSmoke"
    _transType = _this select 2;  // "PickupUnload", "Fastrope", "MoveHover", "LandEngineOn", "LandEngineOff";
    _flyHeight = _this select 3;  // 10, 20, 50, 100, 200, 500
    _transSpeed = _this select 4;  // "UNCHANGED","LIMITED","NORMAL","FULL"

    _closestPlayer = objNull;
    _closestRoof = [0,0,0];
    _adjPos = [0,0,0];

    if (_posType == "MapClickOnly") then {
        _mapClickOnly = true;
    } else {
        if (_posType == "NearestPlayer") then {
            _nearestPlayer = true;
        } else {
            if (_posType == "NearestLeader") then {
                _nearestLeader = true;
            } else {
                if (_posType == "NearestRooftop") then {
                    _nearestRooftop = true;
                } else {
                    if (_posType == "NearestSmoke") then {
                        _nearestSmoke = true;
                    };
                };
            };
        };
    };

    if (_transType == "PickupUnload") then {
        _pickupUnload = true;
    } else {
        if (_transType == "Fastrope") then {
            _fastRope = true;
        } else {
            if (_transType == "MoveHover") then {
                _moveHover = true;
            } else {
                if (_transType == "LandEngineOn") then {
                    _landEngineOn = true;
                } else {
                    if (_transType == "LandEngineOff") then {
                        _landEngineOff = true;
                    };
                };
            };
        };
    };

    private ["_signal","_signalPos","_pickUpPos"];
    _signalPos = getMarkerPos _marker;
    _pickUpPos = createVehicle ["Land_HelipadEmpty_F", _signalPos, [], 0, "NONE"];
    pr_landPadArray = pr_landPadArray + [_pickUpPos]; publicVariable "pr_landPadArray";

    if ((_posType == "NearestPlayer") || (_posType == "NearestLeader")) then {
        _closestPlayer = ["Transport", 100] call pr_fnc_nearestPlayer;
        if !(isNull _closestPlayer) then {
            if (_posType == "NearestLeader") then {
                _adjPos = position (leader (group _closestPlayer));
            } else {
                _adjPos = position _closestPlayer;
            }; 
        } else {
            _adjPos = _signalPos;
        };
        _pickUpPos setPosATL [_adjPos select 0, _adjPos select 1, (_adjPos select 2) + 1];
    };

    if (_posType == "NearestRooftop") then {
        _closestRoof = ["Transport"] call pr_fnc_getBuildingtop;
        if (_closestRoof select 0 > 0) then {
            _adjPos = _closestRoof; pr_closestRoof=_closestRoof;
        } else {
            _adjPos = _signalPos;
        };
        _pickUpPos setPosATL [_adjPos select 0, _adjPos select 1, (_adjPos select 2) + 1];
    };

    // find safe landing position
    _safePos = [];

    if ((_posType == "MapClickOnly") && !(_fastRope)) then {
        if !(surfaceIsWater _signalPos) then {
            _range = 35;
            _maxGrad = 0.1;

            while { ((count _safePos) == 0) } do {
                _safePos = [
                    ["position", _signalPos],
                    ["number", 1],
                    ["objDistance", 9],
                    ["range", [0, _range]],
                    ["maxGradient", _maxGrad],
                    ["waterMode",1],
                    ["onShore",0]
                ] call pr_fnc_randomCirclePositions;
                _range = _range * 1.25;
                _maxGrad = _maxGrad + 0.01;
            };
            _adjPos = (_safePos select 0);
            _pickUpPos setPosATL _adjPos; //m[_adjPos select 0, _adjPos select 1, _adjPos select 2];
        } else {
            _pickUpPos setPosASL [getPosASL _pickUpPos select 0, getPosASL _pickUpPos select 1, 2];
        };
    };

    if (_fastRope) then {
        if !(surfaceIsWater _signalPos) then {
            _pickUpPos setPosATL [getPosATL _pickUpPos select 0, getPosATL _pickUpPos select 1, (getPosATL _pickUpPos select 2) + 40];
        } else {
            _pickUpPos setPosASL [getPosASL _pickUpPos select 0, getPosASL _pickUpPos select 1, (getPosASL _pickUpPos select 2) + 40];
        };
    };

    if (_transType == "MoveHover") then {
        if !(surfaceIsWater _signalPos) then {
            _pickUpPos setPosATL [getPosATL _pickUpPos select 0, getPosATL _pickUpPos select 1, (getPosATL _pickUpPos select 2) + _flyHeight];
        } else {
            _pickUpPos setPosASL [getPosASL _pickUpPos select 0, getPosASL _pickUpPos select 1, (getPosASL _pickUpPos select 2) + _flyHeight];
        };
    }; 

    pr_pickUpPos = _pickUpPos;

    _behaviour = ["CARELESS","YELLOW",_transSpeed,"FILE"];
    _wpS = "_heli = vehicle this; _group = group _heli; deleteWaypoint [(group this), currentWaypoint (group this)];";

    _wpSLand = "_heli land 'LAND';";
    _wpSLandIn = "_heli land 'GET IN';";
    _wpSLandOut = "_heli land 'GET OUT';";


    [_heliGroup, position _heli, ["MOVE"], _behaviour, _wpS] call pr_fnc_addWaypoint;
    [_heliGroup, _pickUpPos, ["LOAD"], _behaviour, _wpS + _wpSLandIn] call pr_fnc_addWaypoint;
    [_heliGroup, _pickUpPos, ["HOLD"], _behaviour, _wpS] call pr_fnc_addWaypoint;
    if (_flyHeight == 10) then {
        _flyHeight = 9.99;
    };
    _heli flyInHeight _flyHeight;

} else {
    [_heliGroup, getMarkerPos _marker, ["Move"], ["CARELESS","YELLOW","FULL","FILE"]] call pr_fnc_addWaypoint;
    missionNamespace setVariable ["quickMove", false];
};

