/* fn_atlasLanding.sqf
*  Author: PapaReap
*  function name: pr_fnc_atlasLanding
*/

_heli   = _this select 0;
_pos    = _this select 1;
_grpAll = _this select 2;
_group  = _this select 3;
_height = _this select 4;

_heli allowDamage false; //keep from getting damage from the following script
private "_asloc","_lockobj","_lock";

_fastropePos = getPos _pos;


//_height = 0.05;
sleep 0.1;
_asloc = [_fastropePos select 0, _fastropePos select 1, (_fastropePos select 2) - _height];
_lockobj = "Land_HelipadEmpty_F" createVehiclelocal _asloc;
_lock = getPosATL _lockobj select 2; deleteVehicle _lockobj;
_delay = 0.75;
_fastropePos = _asloc;
_heli disableAi "move";
_c = 0;

while { (!isNull _heli) && ((getPos _heli) distance _asloc > -1.0) && (alive _heli) && (_c == 0) } do {
    _xfall = sqrt(((getPosATL _heli select 2) - _lock)/0.95);
    _xvelx = (((_fastropePos select 0) - (getPos _heli select 0)) / _xfall) / _delay;
    _xvely = (((_fastropePos select 1) - (getPos _heli select 1)) / _xfall) / _delay;
    _xvelz = (((_fastropePos select 2) - (getPos _heli select 2)) / _xfall) / (_delay * 1.5);
    _heli setVelocity [_xvelx, _xvely, _xvelz];

    if (atlasPickup) then {
        if (count _grpAll > 0) then {
            if ((heli_liftOff) || ({ _x in _heli } count _grpAll == { alive _x } count _grpAll) || ((!heli_transport) && ({ _x in _heli } count _grpAll < 1))) then {
                _c = 1;
            };
        } else {
            if ((heli_liftOff) || ({ _x in _heli } count (units _group) == { alive _x } count (units _group)) || ((!heli_transport) && ({ _x in _heli } count (units _group) < 1))) then {
                _c = 1;
            };
        };
    } else {
        if (atlasDropoff) then {
            if (count _grpAll > 0) then {
                if ((heli_liftOff) || ({ _x in _heli } count _grpAll < 1)) then {
                    _c = 1;
                };
            } else {
                if ((heli_liftOff) || ({ _x in _heli } count (units _group) < 1)) then {
                    _c = 1;
                };
            };
        };
    };
    sleep 0.05;
};
_heli enableAi "move";
