/*

   [_heli, _heliGroup] spawn pr_fnc_transportMonitor;
*/

_heli = _this select 0;
_heliGroup = _this select 1;
_driver = driver _heli;

pr_transportAlive = true; publicVariable "pr_transportAlive";

if (isNil "noHCSwap") then { noHCSwap = []; publicVariable "noHCSwap" };
{ noHCSwap = noHCSwap + [_x]; } forEach units _heliGroup; publicVariable "noHCSwap";

//while { (!(isNull _heli) && (canMove _heli) && (alive (driver _heli))) } do { sleep 10; };
while { ((alive _heli) && (alive (driver _heli))) } do { sleep 2; };

pr_transportAlive = false; publicVariable "pr_transportAlive";
{ noHCSwap = noHCSwap - [_x]; } forEach units _heliGroup; publicVariable "noHCSwap";
if !(alive _heli) then { _heli = objNull; };
if !(alive (driver _heli)) then { _heliGroup = grpNull; };
