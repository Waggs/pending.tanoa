// Script which updates the text value below "SELECTED DELAY" in the right hand box of the GUI.
// Adds values first, then updates it frequently, as well as assigning missionNamespace variable "setTeam".

waitUntil { !isNull (findDisplay 1801) && { dialog } };

_display = 1822;
_string = ["Pickup / Unload","Fastrope","Move / Hover","Land - Engine On","Land - Engine Off"];
_arr = [0,1,2,3,4,5];
_choice = -1;
{
    _index = lbAdd [_display, _x];
} forEach _string;

while { dialog } do {

    waitUntil { lbCurSel _display != -1 };
    _count = lbCurSel _display;

    if (_count == 0) then {
        _choice = _arr select 0;
    } else {
        if (_count == 1) then {
            _choice = _arr select 1;
        } else {
            if (_count == 2) then {
                _choice = _arr select 2;
            } else {
                if (_count == 3) then {
                    _choice = _arr select 3;
                } else {
                    if (_count == 4) then {
                        _choice = _arr select 4;
                    };
                };
            };
        };
    };

    missionNamespace setVariable ["setChangeRouteType", _choice];
    missionNamespace setVariable ["changeRouteTypeSelected", 1];
};

missionNamespace setVariable ["changeRouteTypeSelected", 0];
