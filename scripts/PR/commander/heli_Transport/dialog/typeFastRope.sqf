
waitUntil { !isNull (findDisplay 1801) && { dialog } };

_display = 1830;
_string = ["Yes","No"];
_arr = [0,1];
_choice = -1;
{
    _index = lbAdd [_display, _x];
} forEach _string;

while { dialog } do {

    waitUntil { lbCurSel _display != -1 };
    _count = lbCurSel _display;

    if (_count == 0) then {
        _choice = _arr select 0;
    } else {
        if (_count == 1) then {
            _choice = _arr select 1;
        };
    };

    missionNamespace setVariable ["setFastRopeType", _choice];
    missionNamespace setVariable ["fastRopeSelected", 1];
};

missionNamespace setVariable ["fastRopeSelected", 0];