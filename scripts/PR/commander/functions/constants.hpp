#define MISSION

#ifdef MISSION

#else
#define PR_RscText RscText
#define PR_RscText2 RscText
#define PR_RscStructuredText RscStructuredText
#define PR_RscCombo RscCombo
#define PR_RscListBox RscListBox
#define PR_RscButton RscButton
#define PR_RscButton2 RscButton
#define PR_RscButton3 RscButton
#define PR_RscButton4 RscButton
#define PR_RscFrame RscFrame

class RscText;
class RscStructuredText;
class RscCombo;
class RscListBox;
class RscButton;
class RscFrame;

#endif

#include "constants_a3.hpp"
