// Debug Dialog
// To use, set a radio trigger to activate: _handle = CreateDialog "PR_RTB_DIALOG";
// In description.ext add:  #include "scripts\PR\Commander\commander.hpp"

#define GUI_GRID_X  (0)
#define GUI_GRID_Y  (0)
#define GUI_GRID_W  (0.025)
#define GUI_GRID_H  (0.04)
#define GUI_GRID_WAbs  (1)
#define GUI_GRID_HAbs  (1)

//#include "constants.hpp"
//#include "ids.h"
/*
class PR_RTB_BOX {
    type = 0;
    idc = -1;
    style = ST_CENTER;
    shadow = 2;
                       //Red Green Blue Alpha
    colorBackground[] = {0.0,0.2,0.3,0.2};
    colorText[] = {1,1,1,1};
    font = "PuristaMedium";
    sizeEx = 0.02;
    text = "";
};
*/
class PR_RTB_DIALOG {
    idd = 1901;
    movingenabled = true;
    onLoad = "((_this select 0) displayCtrl  1636) ctrlSetText (uiNamespace getVariable 'PR_SKL_PrevCommand');";
    
    class Controls {
        class PR_RTBBox: PR_BOX {
            idc = -1;
            text = "";
            x = -20 * GUI_GRID_W + GUI_GRID_X;
            y = 6 * GUI_GRID_H + GUI_GRID_Y;
            w = 19 * GUI_GRID_W;
            h = 5 * GUI_GRID_H;
        };

        class PR_RTBDialogeFRAME: PR_RscFrame {
            idc = 1900;
            text = "";
            x = -20 * GUI_GRID_W + GUI_GRID_X;
            y = 6 * GUI_GRID_H + GUI_GRID_Y;
            w = 19 * GUI_GRID_W;
            h = 5 * GUI_GRID_H;
            moving = true;
            sizeEx = 0.035;
        };

        // FRAMES
        class PR_RTBFRAME: PR_RscFrame {
            idc = 1902;
            text = "  CONFIRM   RETURN TRANSPORT TO BASE";
            x = -19.5 * GUI_GRID_W + GUI_GRID_X;
            y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 18 * GUI_GRID_W;
            h = 3.75 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        // CALL FOR TRANSPORT START

        class PR_RTB_ButtonClose: PR_RscButton {
            idc = 1913;
            text = "CANCEL";
            x = -19 * GUI_GRID_W + GUI_GRID_X;
            y = 8.0 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            action = "closeDialog 0;";
        };

        class PR_RTBBUTTON: PR_RscButton2 {
            idc = 1914;
            text = "YES RTB";
            x = -9 * GUI_GRID_W + GUI_GRID_X;
            y = 8.0 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; closeDialog 0; 0 = [pr_heliTrans, true] remoteExec ['pr_fnc_deleteWaypoint', 0]; 0 = [pr_heliTrans, pr_heliTransGroup, markerPos 'extractSpawn'] remoteExec ['pr_fnc_rtb', 0]; pr_rtb_order = true; publicVariable 'pr_rtb_order';";
        };
    };
};
