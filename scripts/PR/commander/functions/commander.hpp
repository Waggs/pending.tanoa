// Debug Dialog
// To use, set a radio trigger to activate: _handle = CreateDialog "COMMAND_DIALOG";
// In description.ext add:  #include "scripts\PR\Commander\commander.hpp"

#define GUI_GRID_X  (0)
#define GUI_GRID_Y  (0)
#define GUI_GRID_W  (0.025)
#define GUI_GRID_H  (0.04)
#define GUI_GRID_WAbs  (1)
#define GUI_GRID_HAbs  (1)

#include "constants.hpp"
#include "ids.h"

class PR_BOX {
    type = 0;
    idc = -1;
    style = ST_CENTER;
    shadow = 2;
                       //Red Green Blue Alpha
    colorBackground[] = {0.0,0.2,0.3,0.2};
    colorText[] = {1,1,1,1};
    font = "PuristaMedium";
    sizeEx = 0.02;
    text = "";
};

class COMMAND_DIALOG {
    idd = 1801;
    movingenabled = true;
    onLoad = "((_this select 0) displayCtrl 1636) ctrlSetText (uiNamespace getVariable 'PR_SKL_PrevCommand'); [] execVM 'scripts\PR\commander\heli_Transport\dialog\typeTeams.sqf'; [] execVM 'scripts\PR\commander\heli_Transport\dialog\typeTransport.sqf'; [] execVM 'scripts\PR\commander\heli_Transport\dialog\typeRoute.sqf'; [] execVM 'scripts\PR\commander\heli_Transport\dialog\typeChangeRoute.sqf'; [] execVM 'scripts\PR\commander\heli_Transport\dialog\typeSetPosition.sqf'; [] execVM 'scripts\PR\commander\heli_Transport\dialog\typeSetFlyHeight.sqf'; [] execVM 'scripts\PR\commander\heli_Transport\dialog\typeFastRope.sqf'; [] execVM 'scripts\PR\commander\heli_Transport\dialog\typeTransSpeed.sqf'; [] execVM 'scripts\PR\commander\heli_Transport\dialog\watchButtons.sqf'; ";
    
    class Controls {
        class PR_Box: PR_BOX {
            idc = -1;
            text = "";
            x = -20 * GUI_GRID_W + GUI_GRID_X;
            y = 6 * GUI_GRID_H + GUI_GRID_Y;
            w = 30.5 * GUI_GRID_W;
            h = 28 * GUI_GRID_H;
        };

        class PR_CommanderDialogeFRAME: PR_RscFrame {
            idc = 1800;
            text = " PR Commander Dialogue - v1.1 ";
            x = -20 * GUI_GRID_W + GUI_GRID_X;
            y = 6 * GUI_GRID_H + GUI_GRID_Y;
            w = 30.5 * GUI_GRID_W;
            h = 28 * GUI_GRID_H;
            moving = true;
            sizeEx = 0.035;
        };

        // FRAMES
        class PR_transportFRAME: PR_RscFrame {
            idc = 1802;
            text = " CALL FOR TRANSPORT           EDIT ROUTE                 QUICK ORDERS";
            x = -19.5 * GUI_GRID_W + GUI_GRID_X;
            y = 7.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 29.5 * GUI_GRID_W;
            h = 11.25 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        class PR_supportsFRAME: PR_RscFrame {
            idc = 1805;
            text = " CALL FOR SUPPORTS ";
            x = -19.5 * GUI_GRID_W + GUI_GRID_X;
            y = 20 * GUI_GRID_H + GUI_GRID_Y;
            w = 29.5 * GUI_GRID_W;
            h = 10.25 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        // CALL FOR TRANSPORT - START
        class PR_transportTeamTEXT: PR_RscText {
            text = "Team to Transport";
            x = -19 * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        class PR_transportTeamBOX: PR_RscCombo {
            idc = 1810;
            x = -19 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 0.9 * GUI_GRID_H;
        };

        class PR_transportTypeTEXT: PR_RscText {
            text = "Type of Transport";
            x = -19 * GUI_GRID_W + GUI_GRID_X;
            y = 10.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        class PR_transportTypeBOX: PR_RscCombo {
            idc = 1811;
            x = -19 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 0.9 * GUI_GRID_H;
        };

        class PR_transportActionTEXT: PR_RscText {
            text = "Transport Action";
            x = -19 * GUI_GRID_W + GUI_GRID_X;
            y = 12.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        class PR_transportActionBOX: PR_RscCombo {
            idc = 1812;
            x = -19 * GUI_GRID_W + GUI_GRID_X;
            y = 13.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 0.9 * GUI_GRID_H;
        };

        class PR_orderTransportBUTTON: PR_RscButton2 {
            idc = 1813;
            text = "Order Transport";
            x = -19 * GUI_GRID_W + GUI_GRID_X;
            y = 15.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; [[player], 'pr_fnc_heliTransportCall'] call BIS_fnc_MP; "; //TAll = true; publicVariable 'TAll';
        };

        class PR_Button1_ca2: PR_RscButton2 {
            idc = 1815;
            text = "Give Liftoff Order";
            x = -19 * GUI_GRID_W + GUI_GRID_X;
            y = 17 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; heli_liftOff = true; publicVariable 'heli_liftOff'; 0 = 0 spawn { sleep 4; heli_liftOff = false; publicVariable 'heli_liftOff'; }; hint 'Take Off'; ";
        };

        // CALL FOR TRANSPORT - END

        // EDIT ROUTE - START
        class PR_getPositionActionTEXT: PR_RscText {
            text = "Set Route Position";
            x = -9.25 * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        class PR_getPositionActionBOX: PR_RscCombo {
            idc = 1821;
            x = -9.25 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 0.9 * GUI_GRID_H;
        };

        class PR_changeRouteActionTEXT: PR_RscText {
            text = "Set Route Type";
            x = -9.25 * GUI_GRID_W + GUI_GRID_X;
            y = 10.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        class PR_changeRouteActionBOX: PR_RscCombo {
            idc = 1822;
            x = -9.25 * GUI_GRID_W + GUI_GRID_X;
            y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 0.9 * GUI_GRID_H;
        };

        class PR_flyHeightActionTEXT: PR_RscText {
            text = "Set Route Fly Height";
            x = -9.25 * GUI_GRID_W + GUI_GRID_X;
            y = 12.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        class PR_flyHeightActionBOX: PR_RscCombo {
            idc = 1823;
            x = -9.25 * GUI_GRID_W + GUI_GRID_X;
            y = 13.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 0.9 * GUI_GRID_H;
        };

        class PR_transSpeedActionTEXT: PR_RscText {
            text = "Set Transport Speed";
            x = -9.25 * GUI_GRID_W + GUI_GRID_X;
            y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        class PR_transSpeedActionBOX: PR_RscCombo {
            idc = 1824;
            x = -9.25 * GUI_GRID_W + GUI_GRID_X;
            y = 15.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 0.9 * GUI_GRID_H;
        };

        class PR_acceptRouteChangesButton: PR_RscButton2 {
            idc = 1825;
            text = "Use Route Changes";
            x = -9.25 * GUI_GRID_W + GUI_GRID_X;
            y = 17 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
            //action = "pr_acceptRouteChanges = true; publicVariable 'pr_acceptRouteChanges';";
            action = "closeDialog 0; 0 = [player] remoteExec ['pr_fnc_heliTransportNewWPCall', p1];";
        };
        // EDIT ROUTE - END

        // QUICK ORDERS - START
/*        class PR_useFastRopeActionTEXT: PR_RscText {
            text = "Use Fast Rope";
            x = 0.5 * GUI_GRID_W + GUI_GRID_X;
            y = 8.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
        };

        class PR_useFastRopeActionBOX: PR_RscCombo {
            idc = 1830;
            x = 0.5 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 0.9 * GUI_GRID_H;
        };*/
		/*
        class PR_useFastRopeTrueButton: PR_RscButton2 {
            idc = 1832;
            text = "Yes";
            x = 0.5 * GUI_GRID_W + GUI_GRID_X;
            y = 11.0 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; [[player], 'pr_fnc_heliTransportNewWPCall'] call BIS_fnc_MP; ";
        };
		*/
		
        class PR_Button1_ca3: PR_RscButton2 {
            idc = 1832;
            text = "Quick Move";
            x = 0.5 * GUI_GRID_W + GUI_GRID_X;
            y = 9 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; 0 = [player] remoteExec ['pr_fnc_heliTransportNewWPCall', p1]; missionNamespace setVariable ['quickMove', true];";
        };

        class PR_startEngineButton: PR_RscButton2 {
            idc = 1833;
            text = "Enable Fuel System";
            x = 0.5 * GUI_GRID_W + GUI_GRID_X;
            y = 14.0 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "0 = [pr_heliTrans, 1] remoteExec ['pr_fnc_fuelControl', 0]; pr_disAbleFuel = false; publicVariable 'pr_disAbleFuel';";
        };

        class PR_killEngineButton: PR_RscButton4 {
            idc = 1834;
            text = "Disable Fuel System";
            x = 0.5 * GUI_GRID_W + GUI_GRID_X;
            y = 15.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "0 = [pr_heliTrans, 0] remoteExec ['pr_fnc_fuelControl', 0]; pr_disAbleFuel = true; publicVariable 'pr_disAbleFuel'";
        };
		
        class PR_RTBButton: PR_RscButton4 {
            idc = 1835;
            text = "RTB Transport";
            x = 0.5 * GUI_GRID_W + GUI_GRID_X;
            y = 17 * GUI_GRID_H + GUI_GRID_Y;
            w = 8.95 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "_handle = CreateDialog 'PR_RTB_DIALOG'; ";
        };
        // QUICK ORDERS - END


/*
        class PR_Button1_cb2: PR_RscButton {
            idc = 1616;
            text = "Transport Alpha";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; if !(isNil 'pg1') then { [[player], 'pr_fnc_heliTransportCall'] call BIS_fnc_MP; TAlpha = true; publicVariable 'TAlpha' } else { hint 'No Alpha Units to Transport' }; ";
        };
        class PR_Button1_cb3: PR_RscButton {
            idc = 1617;
            text = "Transport Bravo";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 11 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; if !(isNil 'pg2') then { [[player], 'pr_fnc_heliTransportCall'] call BIS_fnc_MP; TBravo = true; publicVariable 'TBravo' } else { hint 'No Bravo Units to Transport' }; ";
        };
        class PR_SKLD_Button1_cb4: PR_RscButton {
            idc = 1618;
            text = "Transport Charlie";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 12.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; if !(isNil 'pg3') then { [[player], 'pr_fnc_heliTransportCall'] call BIS_fnc_MP; TCharlie = true; publicVariable 'TCharlie' } else { hint 'No Charlie Units to Transport' }; ";
        };
        class PR_SKLD_Button1_cb5: PR_RscButton {
            idc = 1619;
            text = "Transport Delta";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 14 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; if !(isNil 'pg4') then { [[player], 'pr_fnc_heliTransportCall'] call BIS_fnc_MP; TDelta = true; publicVariable 'TDelta' } else { hint 'No Delta Units to Transport' }; ";
        };

        class PR_Text2_ca1: PR_RscText {
            text = "Call Supports";
            x = -19.5 * GUI_GRID_W + GUI_GRID_X;
            y = 16.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 12.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
        };
*/
        class PR_Button2_cb1: PR_RscButton {
            idc = 1840;
            text = "Ammo Drop";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 21.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.0 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; [[player, 'ammo'], 'pr_fnc_supplyDropCall'] call BIS_fnc_MP; ";
        };
        class PR_Button2_cb2: PR_RscButton {
            idc = 1841;
            text = "Medical Drop";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 23 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.0 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; [[player, 'med'], 'pr_fnc_supplyDropCall'] call BIS_fnc_MP; ";
        };
        class PR_Button2_cb3: PR_RscButton {
            idc = 1842;
            text = "Mash Cargo Drop";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 24.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.0 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; [[player, 'mash'], 'pr_fnc_supplyDropCall'] call BIS_fnc_MP; ";
        };
        class PR_Button2_cb4: PR_RscButton {
            idc = 1843;
            text = "MH-9 Heli Drop";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 26 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.0 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; [[player, 'MH9'], 'pr_fnc_supplyDropCall'] call BIS_fnc_MP; ";
        };
        class PR_Button2_cb5: PR_RscButton {
            idc = 1844;
            text = "Open Artillery Command";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 27.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.0 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; 0 = [player] spawn TRT_fnc_artilleryComputer;";
        };
//        class PR_Text3_ca1: PR_RscText { 
//            text = "Choose New Task"; 
//            x = -19.5 * GUI_GRID_W + GUI_GRID_X; 
//            y = 23 * GUI_GRID_H + GUI_GRID_Y; 
//            w = 12.5 * GUI_GRID_W; 
//            h = 1.1 * GUI_GRID_H; 
//            sizeEx = 0.035; 
//        }; 
//        class PR_Button3_cb1: PR_RscButton { 
//            idc = 1630; 
//            text = "Check Tasks"; 
//            x = -7.5 * GUI_GRID_W + GUI_GRID_X; 
//            y = 23 * GUI_GRID_H + GUI_GRID_Y; 
//            w = 10.5 * GUI_GRID_W; 
//            h = 1.1 * GUI_GRID_H; 
//            sizeEx = 0.035; 
//          action = "closeDialog 0; 0 = [player] execVM 'scripts\PR\commander\aiLift\call_ammo_drop.sqf';"; 
//        }; 

        class PR_Text4_ca1: PR_RscText {
            text = "End Mission";
            x = -19.5 * GUI_GRID_W + GUI_GRID_X;
            y = 30.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 12.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
        };
        class PR_Button3_cb2: PR_RscButton {
            idc = 1870;
            text = "Call Mission End";
            x = -7.5 * GUI_GRID_W + GUI_GRID_X;
            y = 30.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 10.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "closeDialog 0; _nul = ['End1' call BIS_fnc_endMission],'BIS_fnc_execVM',true,true call BIS_fnc_MP;";
        };

        class PR_SKLD_Button4a: PR_RscButton {
            idc = 1880;
            text = "Open Map";
            x = -19.5 * GUI_GRID_W + GUI_GRID_X;
            y = 32 * GUI_GRID_H + GUI_GRID_Y;
            w = 5.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "showMap true; openMap true;";
        };
        class PR_SKLD_Button5a: PR_RscButton {
            idc = 1881;
            text = "Close Map";
            x = -13.5 * GUI_GRID_W + GUI_GRID_X;
            y = 32  * GUI_GRID_H + GUI_GRID_Y;
            w = 5.5 * GUI_GRID_W;
            h = 1.1 * GUI_GRID_H;
            sizeEx = 0.035;
            action = "openMap false;";
        };

        class PR_SKLD_ButtonClose: PR_RscButton {
            idc = 1890;
            text = "CLOSE";
            x = -4 * GUI_GRID_W + GUI_GRID_X;
            y = 32 * GUI_GRID_H + GUI_GRID_Y;
            w = 6.5 * GUI_GRID_W;
            h = 1.0 * GUI_GRID_H;
            action = "closeDialog 0;";
        };

        class PR_SKLD_Text: PR_RscText {
            text = "(leveraged from FHQ_DebugConsole by Varanon)";
            x = -10 * GUI_GRID_W + GUI_GRID_X;
            y = 33.5 * GUI_GRID_H + GUI_GRID_Y;
            w = 20 * GUI_GRID_W;
            h = 0.5 * GUI_GRID_H;
            sizeEx = 0.02;
        };
    };
};
