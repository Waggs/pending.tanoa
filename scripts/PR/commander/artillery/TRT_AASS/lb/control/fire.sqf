// This script controls the fire button, whether it is enabled or not. It serves as a bridge between the fire button and the rest of the scripts.
// Will unlock fire button automatically once all values are accepted.

waitUntil {!isNull (findDisplay 1111) && {dialog}}; 

_button = 1601; 
ctrlEnable [1601,false]; // Disable FIRE button.

while {dialog && {true}} do { 
    waitUntil { 
        ( 
            (missionNamespace getVariable "ArtillerySelected" == 1) 
            && 
            (missionNamespace getVariable "DelaySelected" == 1) 
            && 
            (missionNamespace getVariable "amountRoundsSelected" == 1) 
            && 
            (missionNamespace getVariable "typeShellSelected" == 1) 
        ) 
    }; 
    ctrlEnable [_button, true]; // Enable FIRE button. 
    false; 
}; 
