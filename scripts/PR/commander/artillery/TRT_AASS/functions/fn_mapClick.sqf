/*
By tryteyker
V1.0

Defines what happens on map click. Used in conjunction with BIS_fnc_addStackedEventHandler.
Do not call remotely. Does not work remotely.
Called by lb\setMarker.sqf.
*/
//_unit = player; 

_mkr = createMarker ["ARTY", _pos]; 
_mkr setMarkerShape "ICON"; 
_mkr setMarkerType "hd_destroy"; 
_mkr setMarkerColor "ColorRed"; 
_mkr setMarkerText "FIRE MISSION"; 

[_mkr] spawn { 
    _mkr = _this select 0; 
    waitUntil {getMarkerColor _mkr == "ColorRed"}; 
}; 

missionNamespace setVariable ["MarkerSetByClick", 1]; 
missionNamespace setVariable ["MarkerPositionUsed", (getMarkerPos _mkr)]; 
missionNamespace setVariable ["MarkerUsed", _mkr]; 