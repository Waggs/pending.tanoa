/*
By tryteyker
V1.0

Deletes map-marker defined through functions\fn_mapClick.sqf.
Does not work remotely. Do not use remotely.
Takes no parameters.
*/ 

_unit = player; 

_mkr = missionNamespace getVariable "MarkerUsed"; 
deleteMarker _mkr; 

[_unit] spawn { 
    sleep 1; 
}; 

missionNamespace setVariable ["MarkerSetByClick", 0]; 
missionNamespace setVariable ["MarkerUsed", nil]; 
missionNamespace setVariable ["MarkerPositionUsed", nil]; 
