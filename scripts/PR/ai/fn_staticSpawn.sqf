/* fn_staticSpawn.sqf
*  Author: PapaReap
*  function name: pr_fnc_staticSpawn
*  ver 1.1 2016-01-19
*  ver 1.0 2015-06-14

* ToDo - add artillery,

* UPSMON parameters - https://dev.withsix.com/projects/upsmon/wiki/UPSMONsqf
* Behavior          - "CARELESS", "SAFE", "AWARE", "COMBAT", "STEALTH"
* Unit Position     - "DOWN", "UP", "MIDDLE", "AUTO"

*  Arguments:
*  0: <SPAWN OBJECT/MARKER>       e.g. - (REQUIRED) "marker1" or gameLogic or thisTrigger
*  0: <OPT ARRAY>
*     0: <SPAWN OBJECT/MARKER>    e.g. - (REQUIRED) see: above
*     1: <RANDOM DISTANCE/SIDE>   e.g. - 200 or "west", use one or the other
*     2: <SIDE/RANDOM DISTANCE>   e.g. - "west" or 200, use one or the other

*  1: <TEAM/UNIT>                 e.g. - (REQUIRED) r_fireteam or r_sniper + r_fireteam or ["O_soldier_LAT_F"]
*  1: <OPT TEAM ARRAY>            e.g. - [r_fireteam, "area1", ["careless", "onroad"]] or [["O_soldier_LAT_F"], "area1", ["careless", "onroad"]]
*     0: <TEAM/UNIT>              e.g. - (REQUIRED) see: above
*     1: <OPT AREA MARKER>        e.g. - "area1" (existing area marker name) or "" (will use a default 50x50 marker) or [30, 45] (axis A, axis B) or [30, 45, 60] (axis A, axis B, direction)
*     2: <OPT UPSMON PARAMETERS>  e.g. - ["combat", "onroad"] or ["fortify"]  etc... --- no limit on upsmon parameters (see: UPSMON parameters)

*  2: <TYPE OF STATIC>            e.g. - (REQUIRED) "AA", "AT", "GMG", "HMG", "LIGHT", "MORTAR", "MORTARA", "MORTARS"
*  2: <STATIC ARRAY>              e.g. - ["light", true, 360, "SAFE", "AUTO", true, 1] or ["mortarA", true, 120, 1, false] or ["mortarS", true, 0, 1, true, 15] or ["HMG"]
*     0: <TYPE OF STATIC>         e.g. - (REQUIRED) see: above
*     1: <OPT BOOL>               e.g. - allow extra parameters for "light" & "mortarA" & "mortarS" (bool)
*     2-6: <OPT PARAMETERS>       e.g. - "light":  range in degrees (0-360), behaviour (see: Behavior), stance (see: Unit Position), look up/down (bool), min delay (seconds)
*                                                                                      type of shell - HE, smoke, flare (0, 1, 2),
*                                 e.g. - "mortarA": amount of time to shell (seconds), type of shell (0,1,2), number of shells (#), hit target (bool), sleep between shells (seconds)
*                                 e.g. - "mortarS":                                    type of shell (0,1,2), number of shells (#), hit target (bool), sleep between shells (seconds)

*  3: <OPT RESTOCK>               e.g. - (OPTIONAL) leave blank if not desired, (NOTE) use "", if not used and using <OPT DIRECTION> below
*  3: <OPT RESTOCK ARRAY>         e.g. - (OPTIONAL) ["ammo", 360]
*     0: <FUEL/AMMO/BOTH>         e.g. - "AMMO", "FUEL", "BOTH"
*     1: <SECONDS>                e.g. - 360, seconds between restock (seconds)

*  4: <OPT DIRECTION>             e.g. - (OPTIONAL) leave blank if not desired or 214, (0 - 360 degrees)

*   EXAMPLES
    [thisTrigger, [["O_Soldier_F"]], ["HMG"]] call pr_fnc_staticSpawn; // min required
    [thisTrigger, [["O_Soldier_F"]], ["light", true]] call pr_fnc_staticSpawn; // spawn light with defaults
    [thisTrigger, [r_sentry + r_sentry, ""], ["HMG"], "", 115] call pr_fnc_staticSpawn; // spawn fireteam with hmg with default upsmon marker and patrol
    [[thisTrigger, 0], [r_fireteam, [30,30]], ["mortar", true, 0, 1, true, 15], ["ammo", 300], 210] call pr_fnc_staticSpawn;

    [[thisTrigger, 0], [r_fireteam, [30,30]], [["mortarS", true, 0, 1, true, 15],["mortarA", true, 60, 0, 1, false]], ["ammo", 300], 210] call pr_fnc_staticSpawn;
    [[thisTrigger, 0], [r_fireteam, [50,80]], [["mortarS", true, 0, 1, true, 15],["mortarA", true, 300, 2, 1, false, 8]]] call pr_fnc_staticSpawn;
    [[thisTrigger, 0], [r_fireteam, [50,80]], ["mortarA", true, 300, 2, 1, false, 15],"",90] call pr_fnc_staticSpawn;

    [[thisTrigger, 0], [r_fireteam, "area10", ["careless", "onroad"]], ["mortar", true, 0, 1, true]] call pr_fnc_staticSpawn;
    [[thisTrigger, 0], [r_sentry, "", ["careless"]], ["light", true, 360, "SAFE", "AUTO", true, 1], "", 160] call pr_fnc_staticSpawn;
    [[thisTrigger, 0], [r_sniper + r_fireteam], ["mortarS", true, 0, 1, true, 15], ["ammo", 300], 210] call pr_fnc_staticSpawn;
    [[thisTrigger, 0], [r_sniper + r_fireteam], ["mortarS", true, 0, 1, true, (random 15) max 10], ["ammo", 300], 210] call pr_fnc_staticSpawn;
    [[thisTrigger, 0], [["O_soldier_LAT_F"], "area10", ["combat"]], ["mortar", true, 0, 1, true]] call pr_fnc_staticSpawn;
    [[thisTrigger, 50, "west"], [["B_Soldier_F","B_spotter_F"], "area10", ["aware"]], ["mortar", true, 0, 1, true]] call pr_fnc_staticSpawn;
*/

if (!isServer && hasInterface) exitWith {};
if ((isNull aiSpawnOwner) && !(isServer)) exitWith {};
if (!(isNull aiSpawnOwner) && !(aiSpawnOwner == player)) exitWith {};

_this spawn {

    fnc_staticDamage = {
        _grp = _this select 0;
        {
            _x allowDamage false;
        } forEach units _grp;
        sleep 30;
        {
            _x allowDamage true;
        } forEach units _grp;
    };

    _spawn = _this select 0;
    _spawnPos = [0,0,0];
    _staticPos = [0,0,0];

    _side = EAST;
    if (prEnemySide == 1) then { _side = WEST; };
    if (prEnemySide == 2) then { _side = EAST; };
    if (prEnemySide == 3) then { _side = RESISTANCE; };

    _name1 = 1;
    _name2 = 1;

    if (typeName _spawn == "ARRAY") then {
        if (typeName (_spawn select 0) == "STRING") then {
            _spawnPos = getMarkerPos (_spawn select 0);
        } else {
            if (typeName (_spawn select 0) == "ARRAY") then {
                _spawnPos = _spawn select 0;
            } else {
                _spawnPos = getPos (_spawn select 0);
            };
        };

        if (count _spawn > 1) then {
            _name1 = _spawn select 1;
            if (typeName _name1 == "STRING") then {
                _side = _spawn select 1;
                _side = toUpper _side;
                switch (_side) do {
                    case "EAST": { _side = EAST };
                    case "WEST": { _side = WEST };
                    case "RESISTANCE": { _side = RESISTANCE };
                    case "CIVILIAN": { _side = CIVILIAN };
                    default { _side = EAST };
                };
            } else {
                if !(_name1 == 0) then {
                    _pos = _spawnPos;
                    _randomDistance = _spawn select 1;
                    _rx = random (2 * _randomDistance) - _randomDistance;
                    _ry = random (2 * _randomDistance) - _randomDistance;
                    _randomSpawn = [(_pos select 0) + _rx, (_pos select 1) + _ry, _pos select 2];
                    _spawnPos = _randomSpawn;
                };
            };
        };

        if (count _spawn > 2) then {
            _name2 = _spawn select 2;
            if (typeName _name2 == "STRING") then {
                _side = _spawn select 2;
                _side = toUpper _side;
                switch (_side) do {
                    case "EAST": { _side = EAST };
                    case "WEST": { _side = WEST };
                    case "RESISTANCE": { _side = RESISTANCE };
                    case "CIVILIAN": { _side = CIVILIAN };
                    default { _side = EAST };
                };
            } else {
                if !(_name2 == 0) then {
                    _pos = _spawnPos;
                    _randomDistance = _spawn select 2;
                    _rx = random (2 * _randomDistance) - _randomDistance;
                    _ry = random (2 * _randomDistance) - _randomDistance;
                    _randomSpawn = [(_pos select 0) + _rx, (_pos select 1) + _ry, _pos select 2];
                    _spawnPos = _randomSpawn;
                };
            };
        };
    } else {
        if (typeName _spawn == "STRING") then {
            _spawnPos = getMarkerPos _spawn;
        } else {
            _spawnPos = getPos _spawn;
        };
    };
    _newPos = _spawnPos;
    _staticPos = _spawnPos;

    //if (!(_name1 == 0) && !(_name2 == 0)) then {
        // position isFlatEmpty [float minDistance, float precizePos, float maxGradient, float gradientRadius, float onWater, bool onShore, object skipobj]
        _newPos = _spawnPos isFlatEmpty [
            3,      //--- Minimal distance from another object
            3,      //--- If 0, just check position. If >0, select new one
            0.7,     //--- Max gradient
            2,       //--- Gradient area
            0,       //--- 0 for restricted water, 2 for required water,
            false,   //--- True if some water can be in 25m radius
            ObjNull  //--- Ignored object
        ];

        while { (count _newPos < 1) } do {  //Loop the following code so long as isFlatEmpty cannot find a valid position near the current _pos.
            _pos = _spawnPos;
            _randomDistance = 30;
            _rx = random (2 * _randomDistance) - _randomDistance;
            _ry = random (2 * _randomDistance) - _randomDistance;
            _randomSpawn = [(_pos select 0) + _rx, (_pos select 1) + _ry, _pos select 2];
            _spawnPos = _randomSpawn;
            _newPos = _spawnPos isFlatEmpty [3, 1, 0.7, 40, 0, false, ObjNull];
        };
    //}; 

    _teamA = _this select 1;
    _team = _teamA select 0;
    _grp = [_newPos, _side, _team] call BIS_fnc_spawnGroup;

    [_grp] spawn fnc_staticDamage;
    
    if (name aiSpawnOwner == "hc") then {
        { hcUnits = hcUnits + [_x] } forEach units _grp; publicVariable "hcUnits";
    } else {
        if (name aiSpawnOwner == "hc2") then {
            { hc2Units = hc2Units + [_x] } forEach units _grp; publicVariable "hc2Units";
        } else {
            { serverUnits = serverUnits + [_x] } forEach units _grp; publicVariable "serverUnits";
        };
    };

    //if (pr_addDragToAll) then { { _nil = [_x] spawn pr_fnc_dragAddDrag; } forEach units _grp; };
    if (pr_addDragToAll) then { { [_x] remoteExec ["pr_fnc_dragAddDrag", 2, false]; } forEach units _grp; };


    if ((prEnemyUnits == 2) || (prEnemyUnits == 3) || (prEnemyUnits == 4)) then { if (_side == EAST) then { { [_x] spawn pr_fnc_r_changeUniform } forEach units _grp } };
    if ((_team select 0) in r_sniper) then {
        if ((_team select 0) in r_sniper) then { [((units _grp) select 0)] spawn pr_fnc_r_Sniper };
        if ((_team select 1) in r_sniper) then { [((units _grp) select 1)] spawn pr_fnc_r_spotter };
    };
    _type = "";
    _extras   = false;

    //light
    _light    = false;
    _range    = 240;
    _beh      = "SAFE";
    _stance   = "AUTO";
    _height   = false;
    _delay    = 1;
    //mortar Ambiance
    _mortarA    = [];
    _artyAmb    = false;
    _extrasA    = false;
    _stopTimeA  = 60;
    _typeShellA = 0;
    _numShellsA = 1;
    _hitTargetA = false;
    _sleepA = 1 + random 3;
    //mortar Spotter
    _mortarS  = [];
    _artySpot = false;
    _extrasS  = false;
    _typeShell = 0;
    _numShells = 1;
    _hitTarget = false;
    _sleep = 30 + random 15;

    // static array
    _pre = "";
    if (_side == EAST) then {
        _pre = "O_";
    } else {
        if (_side == WEST) then {
            _pre = "B_";
        } else {
            if (_side == RESISTANCE) then {
                _pre = "I_";
            };
        };
    };

    _staticA = _this select 2;
    _type = _staticA select 0;
    if (typeName _type == "ARRAY") then {
        _type = format ["%1Mortar_01_F", _pre];

        // artyAmbiance stuff
        if ((_staticA select 0) select 0 == "MortarA") then {
            _artyAmb  = true;
            _mortarA = _staticA select 0;
        } else {
            if ((_staticA select 1) select 0 == "MortarA") then {
                _artyAmb  = true;
                _mortarA = _staticA select 1;
            };
        };
        if (count _mortarA > 1) then { _extrasA = _mortarA select 1 };
        if (_artyAmb) then {
            if (_extrasA) then {
                if (count _mortarA > 2) then { _stopTimeA  = _mortarA select 2 };
                if (count _mortarA > 3) then { _typeShellA = _mortarA select 3 };
                if (count _mortarA > 4) then { _numShellsA = _mortarA select 4 };
                if (count _mortarA > 5) then { _hitTargetA = _mortarA select 5 };
                if (count _mortarA > 6) then { _sleepA     = _mortarA select 6 };
            };
        };

        // artySpotter stuff
        if ((_staticA select 0) select 0 == "MortarS") then {
            _artySpot = true;
            _mortarS  = _staticA select 0;
        } else {
            if ((_staticA select 1) select 0 == "MortarS") then {
                _artySpot = true;
                _mortarS  = _staticA select 1;
            };
        };
        if (count _mortarS > 1) then { _extrasS = _mortarS select 1 };
        if (_artySpot) then {
            if (_extrasS) then {
                if (count _mortarS > 2) then { _typeShell = _mortarS select 2 };
                if (count _mortarS > 3) then { _numShells = _mortarS select 3 };
                if (count _mortarS > 4) then { _hitTarget = _mortarS select 4 };
                if (count _mortarS > 5) then { _sleep     = _mortarS select 5 };
            };
        };
    } else {
        if (count _staticA > 1) then { _extras = _staticA select 1 };

        if (_type == "AA") then {
            _type = format ["%1static_AA_F", _pre];
        } else {
            if (_type == "AT") then {
                _type = format ["%1static_AT_F", _pre];
            } else {
                if (_type == "GMG") then {
                    _type = format ["%1GMG_01_high_F", _pre];
                } else {
                    if (_type == "HMG") then {
                        _type = format ["%1HMG_01_high_F", _pre];
                    } else {
                        if (_type == "Light") then {
                            _type = format ["%1SearchLight", _pre];
                            _light = true;
                            if (_extras) then {
                                if (count _staticA > 2) then { _range  = _staticA select 2 };
                                if (count _staticA > 3) then { _beh    = _staticA select 3 };
                                if (count _staticA > 4) then { _stance = _staticA select 4 };
                                if (count _staticA > 5) then { _height = _staticA select 5 };
                                if (count _staticA > 6) then { _delay  = _staticA select 6 };
                            };
                        } else {
                            if (_type == "Mortar") then {
                                _type = format ["%1Mortar_01_F", _pre];
                            } else {
                                if (_type == "MortarA") then {
                                    _type = format ["%1Mortar_01_F", _pre];
                                    _artyAmb = true;
                                    if (_extras) then {
                                        if (count _staticA > 2) then { _stopTimeA  = _staticA select 2 };
                                        if (count _staticA > 3) then { _typeShellA = _staticA select 3 };
                                        if (count _staticA > 4) then { _numShellsA = _staticA select 4 };
                                        if (count _staticA > 5) then { _hitTargetA = _staticA select 5 };
                                        if (count _mortarA > 6) then { _sleepA     = _mortarA select 6 };
                                    };
                                } else {
                                    if (_type == "MortarS") then {
                                        _type = format ["%1Mortar_01_F", _pre];
                                        _artySpot = true;
                                        if (_extras) then {
                                            if (count _staticA > 2) then { _typeShell = _staticA select 2 };
                                            if (count _staticA > 3) then { _numShells = _staticA select 3 };
                                            if (count _staticA > 4) then { _hitTarget = _staticA select 4 };
                                            if (count _staticA > 5) then { _sleep     = _staticA select 5 };
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };

    if ((_name1 == 0) || (_name2 == 0)) then {
        _staticPos = _staticPos;
    } else {
        _staticPos = _newPos;
    };
    _veh = createVehicle [_type, _staticPos, [], 0, ""];
    _veh setpos _staticPos;
    _gunner = "";

    _cnt = count units _grp;
    if (_cnt > 1) then { _gunner = (units _grp) select (_cnt - 1); [_gunner] join grpNull } else { _gunner = (units _grp) select 0 };
    _gunner moveInGunner _veh;

    if !(pr_deBug == 0) then {  // for debugging
        if (typeName (_staticA select 0) == "ARRAY") then {
            _staticLogA = (_staticA select 0) select 0;
            _staticLogS = (_staticA select 1) select 0;
            diag_log format["*PR* aiSpawner: %1 has spawned a ""%2"" and ""%3"" with %4, pr_fnc_staticSpawn %5 units", aiSpawnOwner, _staticLogA, _staticLogS, _grp, _cnt];
        } else {
            diag_log format["*PR* aiSpawner: %1 has spawned a ""%2"" with %3, pr_fnc_staticSpawn %4 units", aiSpawnOwner, (_staticA select 0), _grp, _cnt];
        };
    };

    //upsmon stuff
    _isUpsmon = false;
    _area = "";
    if (count _teamA > 1) then {
        _isUpsmon = true;
        _area = _teamA select 1;
        if (count _teamA > 2) then {
            _upsmonA = _teamA select 2;
            [_grp, _area, _upsmonA] call pr_fnc_upsSpawner;
        } else {
            [_grp, _area] call pr_fnc_upsSpawner;
        };
    };
    // pr_restock stuff
    if (count _this > 3) then {
        _restock = _this select 3;
        if (typeName _restock == "ARRAY") then {
            _item = (_restock select 0);
            _ReloadTime = (_restock select 1);
            [[[_gunner, _item, _ReloadTime], "scripts\PR\scripts\vehicle\restock.sqf"], "BIS_fnc_execVM", _gunner, false] call BIS_fnc_MP;
        };
    };

    _nil = _gunner execVM "scripts\SKULL\SKL_SmartStaticDefence.sqf";

    if (_light) then {
        if (_extras) then { [[_gunner, _range, _beh, _stance, _height, _delay], "PR_fnc_searchLight"] call BIS_fnc_MP; _veh action ["lightOn", _veh] };
    };

    if (_artyAmb) then {
        if ((_extras) || (_extrasA)) then { [[_stopTimeA, [_veh], _typeShellA, _numShellsA, _hitTargetA, _sleepA], "pr_fnc_artyAmbience"] call BIS_fnc_MP };
    };

    if (_artySpot) then {
        if ((_extras) || (_extrasS)) then { [[[_grp, _isUpsmon], [_veh], _typeShell, _numShells, _hitTarget, _sleep], "pr_fnc_artySpotter"] call BIS_fnc_MP };
    };

    if (count _this > 4) then { _dir = _this select 4; _veh setDir _dir; _gunner setFormDir _dir; };
    _gunner allowDamage true; // trying to figure out why gunner takes no damage

    [[], "pr_fnc_aiSpawner"] call BIS_fnc_MP;
};

if (!(pr_deBug == 0) && (isNil "staticSpawnInit")) then { diag_log format ["*PR* fn_staticSpawn complete"]; staticSpawnInit = true; };
