//--- DUKE_TalibanClothing.sqf 
//--- will give a unit clothing that looks taliban-like 
//--- ver 1.1 - 2015-06-17 fixed items not placed back to unit 
//--- Usage in the units init:  _nil = [this] execVM "scripts\PR\scripts\ai\clothing\fn_r_TalibanClothing.sqf"; 

waitUntil {!isNil "bis_fnc_init"}; 

_civ_clothes = ["U_OG_Guerilla2_1","U_OG_Guerilla2_2","U_OG_Guerilla2_3","U_OG_Guerilla1_1","U_OG_Guerilla3_1","U_OG_Guerilla3_2"];
_civ_vests = ["V_BandollierB_blk","V_BandollierB_khk","V_Chestrig_oli","V_Chestrig_blk","V_TacVest_blk"]; 
_civ_hats = ["H_Shemag_olive","H_Shemag_olive_hs","H_Shemag_tan","H_ShemagOpen_khk","H_ShemagOpen_tan","H_Bandanna_khk_hs"];
 
_unit = _this select 0; 

_uniformContainer = uniformContainer _unit; 
_items = getItemCargo _uniformContainer; 
_magazines = getMagazineCargo _uniformContainer; 

_vestContainer = vestContainer _unit; 
_itemsVest = getItemCargo _vestContainer; 
_magazinesVest = getMagazineCargo _vestContainer; 

removeUniform _unit; 
removeVest _unit; 
removeHeadgear _unit; 

{ 
    _unit unassignItem _x; 
    _unit removeItem _x; 
} forEach ["NVGoggles","NVGoggles_mas_mask3","ACE_NVG_Wide","ACE_NVG_Gen1","ACE_NVG_Gen2","ACE_NVG_Gen4","NVGoggles_OPFOR","NVGoggles_INDEP"]; 
                
_cloth_item = _civ_clothes call BIS_fnc_selectRandom; 
_unit forceAddUniform  _cloth_item; 

_vest_item = _civ_vests call BIS_fnc_selectRandom; 
_unit addVest _vest_item; 

//--- add items and magazines back to uniform 
_uniformContainer = uniformContainer _unit; 
_count = count (_items select 0) - 1; 
for "_i" from 0 to _count do { 
    _uniformContainer addItemCargoGlobal [(_items select 0) select _i, (_items select 1) select _i]; 
}; 
_count = count (_magazines select 0) - 1; 
for "_i" from 0 to _count do { 
    _uniformContainer addMagazineCargoGlobal [(_magazines select 0) select _i, (_magazines select 1) select _i]; 
}; 

//--- add items and magazines back to vest
_vestContainer = vestContainer _unit; 
_count = count (_itemsVest select 0) - 1; 
for "_i" from 0 to _count do { 
    _vestContainer addItemCargoGlobal [(_itemsVest select 0) select _i, (_itemsVest select 1) select _i]; 
}; 
_count = count (_magazinesVest select 0) - 1; 
for "_i" from 0 to _count do { 
    _vestContainer addMagazineCargoGlobal [(_magazinesVest select 0) select _i, (_magazinesVest select 1) select _i]; 
}; 

//--- Random integer, if 1 instead of 0, add a random item from the array
if (round (random 1) == 1) then { 
    _hat_item = _civ_hats call BIS_fnc_selectRandom; 
    _unit addHeadgear _hat_item; 
}; 
