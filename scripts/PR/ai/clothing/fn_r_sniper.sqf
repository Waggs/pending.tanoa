//--- fn_r_sniper.sqf 
//--- ver 1.0 - 2016-01-19
//--- Usage in the units init:  _nil = [this] execVM "scripts\PR\scripts\ai\clothing\fn_r_GuerClothing.sqf"; 
//--- Usage in the units init:  _nil = [this] call pr_fnc_r_GuerClothing; 

waitUntil { !isNil "bis_fnc_init" }; 

fnc_r_Sniper = { 
    _unit = _this select 0; 
	//_uniform = uniformContainer _unit; 
	//_unit addWeapon "ItemGPS"; 
    _unit addItem "ItemGPS"; _unit assignItem "ItemGPS"; 
	//_unit addWeapon "Rangefinder"; 
    _unit addItem "Rangefinder"; _unit assignItem "Rangefinder"; 

}; 
