//--- r_GuerClothing.sqf 
//--- pr_fnc_r_GuerClothing 
//--- ver 1.1 - 2015-06-17 fixed items not placed back to unit 
//--- ver 1.0 - 2015-05-28
//--- Usage in the units init:  _nil = [this] execVM "scripts\PR\scripts\ai\clothing\fn_r_GuerClothing.sqf"; 
//--- Usage in the units init:  _nil = [this] call pr_fnc_r_GuerClothing; 

waitUntil {!isNil "bis_fnc_init"}; 

_r_guerClothes = ["U_OG_leader","U_OG_Guerilla1_1","U_OG_Guerilla2_1","U_OG_Guerilla2_2","U_OG_Guerilla3_1","U_OG_Guerilla3_2"]; 
_r_guerVests = ["V_BandollierB_blk","V_BandollierB_khk","V_Chestrig_oli","V_Chestrig_blk","V_TacVest_blk"]; 
_r_guerHats = ["H_Shemag_olive","H_Cap_oli","H_Watchcap_blk","H_Watchcap_camo","H_Booniehat_khk","H_Bandanna_khk"]; 
 
_unit = _this select 0; 

_unitormContainer = uniformContainer _unit; 
_items = getItemCargo _unitormContainer; 
_magazines = getMagazineCargo _unitormContainer; 

_vestContainer = vestContainer _unit; 
_itemsVest = getItemCargo _vestContainer; 
_magazinesVest = getMagazineCargo _vestContainer; 
 
removeUniform _unit; 
removeVest _unit; 
removeHeadgear _unit; 

_cloth_item = _r_guerClothes call BIS_fnc_selectRandom; 
_unit forceAddUniform  _cloth_item; 

_vest_item = _r_guerVests call BIS_fnc_selectRandom; 
_unit addVest _vest_item; 

//--- add items and magazines back to uniform 
_unitormContainer = uniformContainer _unit; 
_count = count (_items select 0) - 1; 
for "_i" from 0 to _count do { 
    _unitormContainer addItemCargoGlobal [(_items select 0) select _i, (_items select 1) select _i]; 
}; 
_count = count (_magazines select 0) - 1; 
for "_i" from 0 to _count do { 
    _unitormContainer addMagazineCargoGlobal [(_magazines select 0) select _i, (_magazines select 1) select _i]; 
}; 

//--- add items and magazines back to vest 
_vestContainer = vestContainer _unit; 
_count = count (_itemsVest select 0) - 1; 
for "_i" from 0 to _count do { 
    _vestContainer addItemCargoGlobal [(_itemsVest select 0) select _i, (_itemsVest select 1) select _i]; 
}; 
_count = count (_magazinesVest select 0) - 1; 
for "_i" from 0 to _count do { 
    _vestContainer addMagazineCargoGlobal [(_magazinesVest select 0) select _i, (_magazinesVest select 1) select _i]; 
}; 

//--- Random integer, if 1 instead of 0, add a random item from the array 
if (round (random 1) == 1) then { 
    _hat_item = _r_guerHats call BIS_fnc_selectRandom; 
    _unit addHeadgear _hat_item; 
}; 
 