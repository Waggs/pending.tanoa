// Credit to Skull for his contribution on this script 
// SKL_ArtyAmbiance - V1.0
//
// will fire a given number of shells near the players 
//
// in trigger: _nil = [durationSec,ArtyArray,numShells,hitTarget] execVM "custom_scripts\SKL_ArtyAmbiance.sqf"
//
//                       _nil = [240,[art1,art2,art3],2,true] execVM "custom_scripts\SKL_ArtyAmbiance.sqf" 
//
// durationSec is in seconds
//
// Optional arguments:
// numShells: default 1
//
// hitTarget: if true, then aim centered on unit +/- and error, if false, then don't actually ever hit the target (default false)

private ["_targets","_artys","_type","_numShells","_arty","_pos","_target","_error","_px","_y", "_hitTarget","_hitpos","_tooClose"];
_stopTime = time + (_this select 0);
_artys = _this select 1;
_type = "8Rnd_82mm_Mo_shells"; 
_numShells = 1;
_hitTarget = false; 
_sleep = 1 + random 3; 
if (count _this > 2) then { _type = ["8Rnd_82mm_Mo_shells","8Rnd_82mm_Mo_Smoke_white","8Rnd_82mm_Mo_Flare_white"] select (_this select 2) }; 
if (count _this > 3) then {_numShells = _this select 3 }; 
if (count _this > 4) then {_hitTarget = _this select 4 }; 
if (count _this > 5) then {_sleep     = _this select 5 }; 
_px = 0; _py = 0;

//if (!isServer) exitWith{}; 
//if (!local _unit) exitWith {}; ???

{
    gunner _x setUnitAbility 0.5; 
    gunner _x setSkill 0.5; 
    gunner _x setSkill ["spotDistance", 2]; 
    gunner _x setSkill ["spotTime", 1]; 
    gunner _x setCombatMode "RED";
    gunner _x setBehaviour "COMBAT";
} forEach _artys; 

while { ((time < _stopTime) && ({ alive gunner _x } count _artys > 0)) } do { 
    sleep 1; 
    { 
        _target = _x; 
        { 
            _arty = _x; 
            if (_arty distance _target < 4000) then { 
                _pos = getPos (_target); 
                _error = 175; 
                if (_target distance _arty > _error ) then { 
                    _px = random (2*_error) - _error; 
                    _py = random (2*_error) - _error; 
                    _hitpos = [(_pos select 0) + _px, (_pos select 1) + _py, _pos select 2]; 
                    
                    if (!_hitTarget) then { 
                        while { ({ _x distance _hitpos < 100 } count playableUnits) != 0 } do { 
                            _px = random (2*_error) - _error; 
                            _py = random (2*_error) - _error; 
                            _hitpos = [(_pos select 0) + _px, (_pos select 1) + _py, _pos select 2]; 
                        }; 
                    }; 
                    
                    _arty setVehicleAmmo 1; 
                    _arty commandArtilleryFire [_hitpos, _type, _numShells]; 
                    //hint format ["%1 firing on %2, error [%3,%4], %5 shells",_arty, _target, _px, _py, _numShells]; 
                    //sleep 1 + random 3; 
                    sleep _sleep; 
                }; 
            }; 
        } forEach _artys; 
        sleep 4 + random 1; 
    } forEach playableUnits - noArty; 
}; 

