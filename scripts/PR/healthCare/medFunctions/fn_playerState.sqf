/*  fn_playerState.sqf 
*  Author: PapaReap 
*  ver 1.3 - 2016-01-03 major script system rework 
*  ver 1.2 - 2015-06-07 fixes for new ACE
*  ver 1.1 - 2014-12-22 fixes for new agm v0.95
*  ver 1.0 - 2014-12-15
*/ 
if !(aceOn) exitWith {}; 
if !(pr_deBug == 0) then { diag_log format ["*PR* fn_playerState start"]; }; 

_unit = player; 
if !(local _unit) exitWith {}; 
_unit setVariable ["PR_CRAWLING", false, false]; 
_side = side _unit; _unit setVariable ["PR_side", _side, true]; 

fnc_playerState = { 
    _unit = _this select 0; 
    _d = 0; 
    while { true } do { 
        _c = 0; 
        shotsFired = 0; 
        shotsHit = 0; 
        if !([_unit] call fnc_isUncon) then { 
            _unit setVariable ["PR_CRAWLING", false, false]; 
            [_unit] call fnc_return_actionMenu; 
            if (_d == 1) then { [_unit] call fnc_wakeUp; _d = 0 }; 
        }; 

        if (([_unit] call fnc_isUncon) && (_c == 0)) then { 
            [_unit] call fnc_remove_actionMenu; 
            if (isServer) then { [_unit] call fnc_knockOut }; 
            sleep 10; 
            _unit setVariable ["PR_CRAWLING", true, false]; 
            [_unit] spawn fnc_crawlKeys; 
            [_unit] call fnc_onYourBelly; // probably need a public event handler here 
            [_unit] spawn fnc_randomWake; 

            _unit addEventHandler ["FIRED", { shotsFired = shotsFired + 1 }]; 
            _unit addEventHandler ["HIT", { shotsHit = shotsHit + 1 }]; 
            _d = 1; 
            while { (([_unit] call fnc_isUncon) && (_c == 0)) } do { 
                [false] call ACE_common_fnc_disableUserInput; 

                if (shotsFired > 0) then { 
                    if isServer then { 
                        [_unit, "ACE_isUnconscious", false] call ACE_common_fnc_setCaptivityStatus; 
                        _unit setCaptive false; 
                        _unit allowDamage true; 
                    }; 
                    [_unit, "ACE_isUnconscious", false] call ACE_common_fnc_setCaptivityStatus; 
                    _unit setCaptive false; 

                    if (shotsHit > 1) then { 
                        _unit setVariable ["PR_CRAWLING", false, false]; 
                        _unit setVariable ["ACE_isUnconscious", true]; 
                        _unit removeAllEventHandlers "FIRED"; 
                        _unit removeAllEventHandlers "HIT"; 
                        [_unit, "ACE_unconscious", true] call ACE_common_fnc_setCaptivityStatus; 
                        _anim = [_unit] call ACE_common_fnc_getDeathAnim; 
                        [_unit, _anim, 1, true] call ACE_common_fnc_doAnimation; 
                        [{ 
                            _unit = _this select 0; 
                            _anim = _this select 1; 
                            if (([_unit] call fnc_isUncon) and (animationState _unit != _anim)) then { 
                                [_unit, _anim, 2, true] call ACE_common_fnc_doAnimation; 
                            }; 
                        }, [_unit, _anim], 0.5, 0] call ACE_common_fnc_waitAndExecute; 
                        ["medical_onUnconscious", [_unit, true]] call ACE_common_fnc_globalEvent; 
                         sleep 20; 
                        _c = 1; 
                    }; 
                }; 
                sleep 0.01; 
            }; 
        }; 
        sleep 4; 
    }; 
}; 

//--- Restore speech 
fnc_speech = { // this needs testing with others on server, can I remove it? 
    _unit = _this select 0; 
    while { !(_unit getVariable ["ACE_isUnconscious", true]) } do { sleep 4 }; 
    [[], "pr_fnc_restoreSpeech"] call BIS_fnc_MP; 
}; 

// Unconscious, bleeding & pain - Usage: [_unit] call fnc_isUncon 
fnc_isUncon = { _unit = _this select 0; _unit getVariable "ACE_isUnconscious" }; 
fnc_blood = { _unit = _this select 0; _unit getVariable "ace_medical_bloodVolume" }; 
fnc_pain = { _unit = _this select 0; _unit getVariable "ace_medical_pain" }; 

//--- wake up, knock out 
fnc_wakeUp = { _unit = _this select 0; _unit setVariable ["ACE_isUnconscious", false, true] }; 
fnc_knockOut = { _unit = _this select 0; _unit setVariable ["ACE_isUnconscious", true, true] }; 

fnc_randomWake = { 
    _unit = _this select 0; _sleep = 0; 
    while { ([_unit] call fnc_isUncon) } do { 
        sleep 2; 
        if (([_unit] call fnc_blood > 95) && ([_unit] call fnc_pain < .70)) then { _sleep = (random 100) max 30 }; 
        if (([_unit] call fnc_blood > 80) && ([_unit] call fnc_pain < .70)) then { _sleep = (random 200) max 60 }; 
        if (([_unit] call fnc_blood > 65) && ([_unit] call fnc_pain < .70)) then { _sleep = (random 300) max 90 }; 
        if (([_unit] call fnc_blood > 50) && ([_unit] call fnc_pain < .70)) then { _sleep = (random 400) max 120 }; 
        if (([_unit] call fnc_blood > 35) && ([_unit] call fnc_pain < .70)) then { _sleep = (random 500) max 150 }; 
        if (([_unit] call fnc_blood < 35) || ([_unit] call fnc_pain > .70)) then { _sleep = (random 600) max 180 }; 
        sleep _sleep; 
        if (([_unit] call fnc_blood < 35) || ([_unit] call fnc_pain > .70)) then { 
            _sleep = (random 300) max 90; sleep _sleep; 
            if ([_unit] call fnc_blood < 35) then { _unit setVariable ["ace_medical_bloodVolume", 40, true] }; 
            if ([_unit] call fnc_pain > .70) then { _unit setVariable ["ace_medical_pain", 65, true] }; 
        }; 
        [_unit] call fnc_wakeUp; 
    }; 
}; 

fnc_onYourBelly = { 
    _unit = _this select 0; 
    _unit playMoveNow "AmovPpneMstpSrasWrflDnon_turnL"; 
    [[_unit, "AidlPpneMstpSrasWrflDnon_G01"], "switchMove"] call BIS_fnc_MP; 
    _playerDownHint = format ["%1 is down at grid %2", name _unit, mapGridPosition _unit]; 
    logic_HQ sideChat _playerDownHint; 
    logic_HQ_SideChat = _playerDownHint; publicVariable "logic_HQ_SideChat"; 
}; 

fnc_remove_actionMenu = { inGameUISetEventHandler ["PrevAction", "true"]; inGameUISetEventHandler ["NextAction", "true"] }; 
fnc_return_actionMenu = { inGameUISetEventHandler ["PrevAction", "false"]; inGameUISetEventHandler ["NextAction", "false"] }; 

fnc_crawlKeys = { 
    private ["_displayHandler","_displayNumber","_unit"]; 
    disableSerialization; 
    _unit = _this select 0; 
    showHud true; 
    disableUserInput false; 
    _displayNumber = [] call BIS_fnc_DisplayMission; 
    _displayHandler = _displayNumber displayAddEventHandler [ 
        "KeyDown", 
        " 
            DIK_G=0x22; DIK_I=0x17; DIK_V=0x2F; DIK_ESCAPE=0x01; 
            private ['_actions', '_returnvalue', '_actionKeys']; 
            _returnvalue = true; 
            _actions = [ 'MoveForward', 'MoveBack', 'curatorInterface']; 
            _actionKeys = [DIK_G, DIK_I, DIK_V, DIK_ESCAPE]; _actionKeys = _actionKeys + [getNumber (configFile >> 'task_force_radio_keys' >>  'tanget_sw'  >> 'key')]; 
            { 
               _actionkeys = _actionkeys + actionKeys _x; 
            } forEach _actions; 
            if( (_this select 1) in _actionKeys ) then { _returnvalue = false }; 
            _returnvalue 
        " 
    ]; 
    while { ((_unit getVariable "PR_CRAWLING") && (alive _unit)) } do {sleep 2}; 
    _displayNumber displayRemoveEventHandler [ "KeyDown", _displayHandler]; showHud true; 
}; 

fnc_setStanima = { _unit = _this select 0; _unit SetStamina 90; _unit setCustomAimCoef 0.4; }; // default 1 //_unit setAnimSpeedCoef 0.75; // default 1 

/*
fnc_cleanupRespawn = { 
    _unit = _this select 1; 
    _pos = getPos _unit;_xpos = _pos select 0;_ypos = _pos select 1; 
    _zpos = _pos select 2;sleep 0.3;for "_i" from 0 to 3 do {_xvel = 0;_yvel = 0;_zvel = 0;_tnt = 0; 
    drop[["A3\Data_F\ParticleEffects\Universal\universal.p3d",16,7,48],"","Billboard",0,1 + random 0.5,[_xpos,_ypos,_zpos], 
    [_xvel,_yvel,_zvel],1,1.2,1.3,0,[2],[[0.55,0.5,0.45,0],[_tnt + 0.55,_tnt + 0.5,_tnt + 0.45,0.16], 
    [_tnt + 0.55,_tnt + 0.5,_tnt + 0.45, 0.12],[_tnt + 0.5,_tnt + 0.45,_tnt + 0.4,0.08], 
    [_tnt + 0.45,_tnt + 0.4,_tnt + 0.35,0.04],[_tnt + 0.4,_tnt + 0.35,_tnt + 0.3,0.01]],[0],0.1,0.1,"","",""];}; 
    deleteVehicle _unit; 
}; 
*/

_unit addMPEventHandler ["MPRespawn", { 
    [_this select 0] spawn fnc_speech; 
    [_this select 0] spawn fnc_setStanima; 
    [_this select 0] spawn fnc_playerState; 
    disableUserInput false; 
}]; 

[_unit] spawn fnc_speech; 
[_unit] spawn fnc_setStanima; 
[_unit] spawn fnc_playerState; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_playerState complete"]; }; 
