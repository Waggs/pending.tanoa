/*  fn_MediCare.sqf 
*  Author: PapaReap 
*  PR Healthcare functions ran on clients at mission start
*  Init: [player] call compile preprocessFileLineNumbers "scripts\PR\healthCare\functions\fn_MediCare.sqf"; 
*  ver 1.1 - 2016-01-03 major script system rework in progress 
*  ver 1.2 - 2015-05-25 convert to function 
*  ver 1.1 - 2015-04-28 adding field mash 
*  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress 
*/ 
if !(aceOn) exitWith {}; 
if !(pr_deBug == 0) then { diag_log format ["*PR* fn_MediCare start"]; }; 
//fnc_mtInit = compile preprocessFileLineNumbers "scripts\PR\HealthCare\mash\mash_man\fn_mashManInit.sqf";          //--- medic tent init 
//fnc_fmInit = compile preprocessFileLineNumbers "scripts\PR\HealthCare\mash\fn_fmInit.sqf";                        //--- field mash init 
//fnc_mashTentCreate = compile preprocessFileLineNumbers "scripts\PR\HealthCare\mash\mashMan\deploy_mash_man.sqf"; //--- creates a mash tent 
//fnc_mashTentDelete = compile preprocessFileLineNumbers "scripts\PR\HealthCare\mash\mashMan\stow_mash_man.sqf";   //--- removes mash tent 
//fnc_fmBuild = compile preprocessFileLineNumbers "scripts\PR\HealthCare\Mash\fn_fmBuild.sqf";                      //--- Builds a field mash station 
//fnc_fmDelete = compile preprocessFileLineNumbers "scripts\PR\HealthCare\Mash\fn_fmDelete.sqf";                    //--- removes field mash station 
[] call compile preprocessFileLineNumbers "scripts\PR\HealthCare\mash\mashMan\fn_deployTentInit.sqf"; 
[] call compile preprocessFileLineNumbers "scripts\PR\HealthCare\mash\mashMan\fn_stowTentInit.sqf"; 

pr_fnc_initRead = true; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_MediCare complete"]; }; 
