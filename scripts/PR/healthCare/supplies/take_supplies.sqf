/*
take_supplies.sqf
ver 1.0 - 2015-04-17 complete ACE medical system rework in progress
*/
private ["_name","_medic"]; 
_name = player; 
_medic = (typeOf player) in pr_medicArray;	//from PR_HealthCare_Init.sqf

if _medic then { 
    if (["MedicSupplyDrop",0] call BIS_fnc_getParamValue == 1) then { 
        getDropAction = mash_man_tent addAction ["<t size='1.0' color=""#00ff65"">Call for Supply Drop</t>", "scripts\PR\HealthCare\misc\AiLift\call_med_drop.sqf",[],1,true,true,"","_this distance _target < 2.8"]; 
        publicVariable "getDropAction"; 
    }; 
    if (["MedicalSupplies",0] call BIS_fnc_getParamValue == 1) then { 
        placeMedsAction = mash_man_tent addAction ["<t size='1.0' color=""#00ddff"">Place Medical Supplies In Tent</t>", "scripts\PR\HealthCare\supplies\place_supplies.sqf",[],1,true,true,"","_this distance _target < 2.8"]; 
        publicVariable "placeMedsAction"; 
    }; 
}; 