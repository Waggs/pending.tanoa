/*
call_med_drop.sqf
ver 1.0 - 2015-04-17 complete ACE medical system rework in progress
*/
//private ["_tent","_mash_type","_medic"];

_tent       = _this select 0; 
_mash_type  = Mash_array select 3; 
_medic      = (typeOf player) in pr_medicArray; 
_name       = name player; 
_crate      = "ACE_medicalSupplyCrate"; 
_custom     = 1;  // 0= off; 1 = on; set your custom box in "scripts\PR\HealthCare\misc\AiLift\med_drop.sqf";

if _medic then { 
    if (heliDrop_med) then { 
        hint parseText format ["%1<br/><t color='#ff5d00'>No Supply Drops Available<br/>Wait for Heli to RTB</t>",_name]; 
        sleep 15; 
        hintSilent ""; 
    }; 

    if !(heliDrop_med) then { 
        _gridPos = mapGridPosition player; 
        //["spawn marker", "drop marker", side, gunner crew (no,yes), "type of crate", custom crate (no/yes), grid location of caller]
        ["medHeliSpawn", "medDrop", 1, 1, _crate, _custom, _gridPos] execVM "scripts\PR\HealthCare\misc\AiLift\med_drop.sqf"; 
        sleep 2; 
        player sideChat format ["Base Firefly, Requesting Medical Supplies on Grid Location %1", _gridPos]; 
        hint parseText format ["%1<br/><t color='#19e56e'>Medical Supply Drop Called</t>", _name]; 
        sleep 15; 
        hintSilent ""; 
    }; 
}; 