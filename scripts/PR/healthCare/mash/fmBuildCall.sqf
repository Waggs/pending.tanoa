/* 
*  Author: PapaReap 
*  Called from fn_fmInit.sqf" 
*  Call the mash station build script 
*  ver 1.0 - 2015-04-29 complete ACE medical system rework in progress 
*  ver 1.1 - 2016-01-03 major script system rework in progress 
*  ver 1.2 - 2016-02-07 system rework in progress 
* 
*  Arguments: 
*  0: <DEPLOYER>  --- Player that will deploy the Field Mash 
*/ 

private ["_deployer"]; 
_deployer = _this select 0; 
[[_deployer], "pr_fnc_fmBuild"] call BIS_fnc_MP; 
