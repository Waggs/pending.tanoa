/* fn_mashTentInit.sqf 
*  Author: PapaReap 
*  Function name: pr_fnc_mashTentInit 
*  ver 1.1 - 2016-01-03 major script system rework in progress 
*  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress 
*/ 
if !(aceOn) exitWith {}; 
private ["_deployer","_mash_type","_c"]; 
_deployer  = _this select 0; 
mash_reset  = false; 
mash_man_tent  = objNull; 
_mash_type  = mash_Array select 3; 
if ((_mash_type == 1) || (_mash_type == 2)) then { mash_man_deployed = false; publicVariable "mash_man_deployed" }; 
_deployer setVariable ["mash", 0, false]; 
_deployer setVariable ["tentAbort", 0, false]; 
_deployer setVariable ["stowAbort", 0, false]; 

sleep 4; 
waitUntil { !isNil "mash_man_deployed" }; 
if (mash_man_deployed) then { _deployer setVariable ["mash", 1, false] }; 

if (player == _deployer) then { 
    if (!local _deployer) exitWith{}; 
    while { true } do { 
        _nearestMash = nearestObject [_deployer, "B_Slingload_01_Medevac_F"]; 
        while { !alive _deployer } do { sleep 1 }; 

        /* action to deploy */
        if (vehicle _deployer == _deployer && speed _deployer == 0 && (_deployer getVariable "mash" == 0) && !mash_man_deployed && !(surfaceIsWater (getPos _deployer)) && !(_deployer getVariable ["ACE_isUnconscious",false]) && _deployer distance _nearestMash >= 20) then { 
            mash_man_deployAction = _deployer addAction ["<t color='" + (Mash_array select 1) +"'>Deploy Medic Tent</t>", "scripts\PR\HealthCare\mash\mashMan\fn_mashTentCreate.sqf", [_deployer], 10, false, true, ""]; 
            _deployer setVariable ["mash", 1, false]; 
        }; 

        /* temporarily remove action to deploy if player is in vehicle, moving, in water or unconscious */
        if ((vehicle _deployer != _deployer || speed _deployer != 0) && (_deployer getVariable "mash" == 1) && !mash_man_deployed || (_deployer getVariable "mash" == 1) && surfaceIsWater (getPos _deployer) || (_deployer getVariable "mash" == 1) && (_deployer getVariable ["ACE_isUnconscious",false])) then { 
            if (!isNil "mash_man_deployAction") then { 
                _deployer removeAction mash_man_deployAction; 
            }; 
            if (!mash_man_deployed) then { 
                _deployer setVariable ["mash", 0, false]; 
            } else { 
                _deployer setVariable ["mash", 1, false]; 
            }; 
        }; 

        /* action to stow */
        if (vehicle _deployer == _deployer && speed _deployer == 0 && (_deployer getVariable "mash" == 2) && mash_man_deployed && _deployer distance mash_man_tent <= 4 && !(_deployer getVariable ["ACE_isUnconscious",false])) then { 
            mash_man_stowAction = _deployer addAction ["<t color='" + (Mash_array select 1) +"'>Stow Medic Tent</t>", "scripts\PR\HealthCare\mash\mashMan\fn_mashTentDelete.sqf", [_deployer], 10, false, true, ""]; 
            _deployer setVariable ["mash", 3, false]; 
        }; 

        /* temporarily remove action to stow if player is in vehicle, moving, in water or unconscious*/
        if ((vehicle _deployer != _deployer || speed _deployer != 0) && (_deployer getVariable "mash" == 3) && mash_man_deployed || (_deployer getVariable "mash" == 3) && mash_man_deployed && _deployer distance mash_man_tent > 4 || (_deployer getVariable "mash" == 3) && (_deployer getVariable ["ACE_isUnconscious",false])) then { 
            if (!isNil "mash_man_stowAction") then { 
                _deployer removeAction mash_man_stowAction; 
            }; 
            _deployer setVariable ["mash", 2, false]; 
        }; 

        /* reset everything back to start */
        if (mash_reset) then { 
            mash_reset = false;	publicVariable "mash_reset"; 
            mash_man_deployed = false; 
            _deployer setVariable ["mash", 0, false]; 
        }; 
        sleep 1; 
    }; 
}; 
