/* fmInit.sqf
*  Author: PapaReap
*  Gives the medic actions to deploy or stow a field mash
*  ver 1.1 - 2016-01-03 major script system rework in progress
*  ver 1.0 - 2015-04-28 complete ACE medical system rework in progress
*/

if !(aceOn) exitWith {};

private ["_deployer","_mash_type","_c"];
_deployer = _this select 0;
_mash = "";
_tent = objNull;
_mash = nearestObject [_deployer, "B_Slingload_01_Medevac_F"];
_deployAction = 0;
_stowAction = 0;

fieldMash_tent = objNull;
_mash_type = Mash_Array select 3;
if (_mash_type == 2) then { fieldMash_deployed = false; publicVariable "fieldMash_deployed"; };
_deployer setVariable ["fieldMashAbort", 0, false];
_deployer setVariable ["stowFieldMashAbort", 0, false];

if !(isNil "mash_dropbox") then {
    mash_dropbox setVariable ["fieldMash", 0, true];
    mash_dropbox setVariable ["fieldMash_deployed", "false", true];
    mash_dropbox setVariable ["fieldMash_reset", "false", true];
    mash_dropbox setVariable ["mashID", mash_dropbox, true];
    mash_dropbox setVariable ["mashNew", 0, true];
};

fnc_fmAddAction = {
    _mash = _this select 0;
    _deployAction = _this select 1;
    _mash setVariable ["deployAction", _deployAction, true];
};

fnc_fmRemoveAction = {
    _mash = _this select 0;
    _stowAction = _this select 1;
    _mash setVariable ["stowAction", _stowAction, true];
};

fnc_fmID = {
    _mash = _this select 0;
    _mash setVariable ["mashID", _mash, true];
    _mash setVariable ["mashNew", 0, true];
};

sleep 4;
waitUntil { !isNil "fieldMash_deployed" };
if (_mash getVariable "fieldMash_deployed" == "true") then { _mash setVariable ["fieldMash", 1, true] };

if (player == _deployer) then {
    if (!local _deployer) exitWith{};
    while { true } do {
        _mash = nearestObject [_deployer, "B_Slingload_01_Medevac_F"];

        if (_mash getVariable "mashNew" == 0) then {
            _mash = _mash getVariable "mashID"; pr_oldmash = _mash;
        } else {
            [[_mash], "fnc_fmID"] call BIS_fnc_MP;
            sleep 0.5;
            _mash = _mash getVariable "mashID"; pr_newmash = _mash;
        };

        if !(isNull _tent) then {
            _tent = nearestObject [_mash, "CamoNet_BLUFOR_big_F"];
            if (_tent distance _mash < 10) then {
                _mash setVariable ["fieldMash_deployed", "true", true];
                _deployAction = _mash getVariable "deployAction";
                _deployer removeAction _deployAction;
            };

            if (_tent distance _mash > 10) then {
                _mash setVariable ["fieldMash_deployed", "false", true];
                _stowAction = _mash getVariable "stowAction";
                _deployer removeAction _stowAction;
            };
        };

        /* action to deploy */
        if (!([_deployer] call fnc_isUncon) && (_deployer distance _mash <= 5.5) && (_deployer distance mash_man_tent >= 20)) then {
            if ((_mash getVariable "fieldMash" == 0) && (_mash getVariable "fieldMash_deployed" == "false")) then {
                if ((vehicle _deployer == _deployer) && (speed _deployer == 0) && !(surfaceIsWater (getPos _deployer))) then {
                    _mash = nearestObject [_deployer, "B_Slingload_01_Medevac_F"];
                    _deployAction = _deployer addAction ["<t color='"+(Mash_array select 1)+"'>Deploy Field Mash</t>","scripts\PR\HealthCare\Mash\fmBuildCall.sqf",[_deployer],10,false,true,""];
                    [[_mash, _deployAction], "fnc_fmAddAction"] call BIS_fnc_MP;
                    _mash setVariable ["fieldMash", 1, true];
                };
            };
        };

        /* remove action to deploy if player is in vehicle, moving, in water or unconscious */
        if (((vehicle _deployer != _deployer) || (speed _deployer != 0)) && (_mash getVariable "fieldMash" == 1) && (_mash getVariable "fieldMash_deployed" == "false") || (_mash getVariable "fieldMash" == 1) && (surfaceIsWater (getPos _deployer)) || (_mash getVariable "fieldMash" == 1) && ([_deployer] call fnc_isUncon)) then {
            _mash = nearestObject [_deployer, "B_Slingload_01_Medevac_F"];
            _mash = _mash getVariable "mashID";
            _deployAction = _mash getVariable "deployAction";
            _deployer removeAction _deployAction;
            if (_mash getVariable "fieldMash_deployed" == "false") then { _mash setVariable ["fieldMash", 0, true] } else { _mash setVariable ["fieldMash", 1, true] };
        };

        /* action to stow */
        if (!([_deployer] call fnc_isUncon) && (_deployer distance _mash <= 5.5)) then {
            if ((_mash getVariable "fieldMash" == 2) && (_mash getVariable "fieldMash_deployed" == "true")) then {
                if ((vehicle _deployer == _deployer) && (speed _deployer == 0)) then {
                    _mash = nearestObject [_deployer, "B_Slingload_01_Medevac_F"];
                    _stowAction = _deployer addAction ["<t color='"+(Mash_array select 1)+"'>Stow Field Mash</t>","scripts\PR\HealthCare\Mash\fmDeleteCall.sqf",[_deployer],10,false,true,""];
                    [[_mash, _stowAction], "fnc_fmRemoveAction"] call BIS_fnc_MP;
                    _mash setVariable ["fieldMash", 3, true];
                };
            };
        };

        /* remove action to stow if player is in vehicle, moving, in water or unconscious */
        if (((vehicle _deployer != _deployer) || (speed _deployer != 0)) && (_mash getVariable "fieldMash" == 3) && (_mash getVariable "fieldMash_deployed" == "true") || (_mash getVariable "fieldMash" == 3) && (_mash getVariable "fieldMash_deployed" == "true") && (_deployer distance _mash >= 5.5) || (_mash getVariable "fieldMash" == 3) && ([_deployer] call fnc_isUncon)) then {
            _mash = nearestObject [_deployer, "B_Slingload_01_Medevac_F"];
            _mash = _mash getVariable "mashID";
            _stowAction = _mash getVariable "stowAction";
            _deployer removeAction _stowAction;
            _mash setVariable ["fieldMash", 2, true];
        };

        /* reset everything back to start */
        //_mash = nearestObject [_deployer, "B_Slingload_01_Medevac_F"];
        
        if (_mash getVariable "fieldMash_reset" == "true") then {
            _mash setVariable ["fieldMash_reset", "false", true];
            _mash setVariable ["fieldMash_deployed", "false", true];
            _mash setVariable ["fieldMash", 0, true];
        };
        sleep 1;
    };
};
