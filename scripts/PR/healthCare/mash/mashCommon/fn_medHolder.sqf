/* 
*  Author: PapaReap 
*  Function names: pr_fnc_medHolder 
*  Called after mission start from missionInit.sqf 
*  Places a weapon holder for Mashman Tent 
*  ver 1.0 - 2016-02-07 system rework in progress 
* 
*  Arguments: 
*  0: <PLAYER>  --- Container to track blood bags  
*/ 
if !(aceOn) exitWith {}; 
waitUntil { time > 0 }; 

_unit = _this select 0; 
_unit setVariable ["bloodHolder", 0, true]; 
_meds = ["MedicalSupplies"] call BIS_fnc_getParamValue; 
if (_meds == 0) then { 
    while { true } do { 
        if !(isNull medHolder) then { 
            if ("ACE_bloodIV" in ItemCargo medHolder) then { _unit setVariable ["bloodHolder", 1, true]; } else { _unit setVariable ["bloodHolder", 0, true]; }; 
        } else { 
            _unit setVariable ["bloodHolder", 0, true]; 
        }; 
        sleep 2; 
    }; 
}; 
