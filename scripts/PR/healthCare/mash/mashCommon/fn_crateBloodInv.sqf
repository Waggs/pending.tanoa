/* 
*  Author: PapaReap 
*  Function names: pr_fnc_crateBloodInv 
*  Called from pr_fn_fmBuild.sqf" 
*  Removes blood bag in container 
*  ver 1.0 - 2016-02-07 system rework in progress 
* 
*  Arguments: 
*  0: <CONTAINER>  --- Container to track blood bags 
*/ 
if !(aceOn) exitWith {}; 
_container = _this select 0; 

while { !(isNull _container) } do { 
    if ((_container getVariable "removeBlood" == 1) && !(isNull _container)) then { 
        _bloodbags = 0; 
        _items = []; 
        _bloodbags = (({"ACE_bloodIV" == _x} count (itemCargo _container)) - 1) max 0; 
        _items = (itemCargo _container) - ["ACE_bloodIV"]; 
        clearItemCargoGlobal _container; 
        for "_i" from 0 to (count _items) do { 
            _container addItemCargoGlobal [(_items select _i), 1]; 
        }; 
        _container addItemCargoGlobal ["ACE_bloodIV", _bloodbags]; 
        sleep 0.05; 
        _container setVariable ["removeBlood", 0, true]; 
    }; 
    sleep 2; 
}; 
