/* 
*  Author: PapaReap 
*  Function names: pr_fnc_restoreSpeech 
*  Called from fnc_speech 
*  Restores player speech on Ace Unconsciousness 
*  ver 1.0 - 2016-02-07 system rework in progress 
* 
*  Arguments: 
*/ 

if !(aceOn) exitWith {}; 
if (isServer) then { 
    while { true } do { 
        { 
            if (alive _x) then { 
                if (_x getVariable ["ACE_isUnconscious", true]) then { 
                    _x setVariable ["tf_voiceVolume", 0.6, true]; 
                }; 
            }; 
        } forEach playableUnits; 
        sleep 4; 
    }; 
}; 
