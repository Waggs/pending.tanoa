/* 
*  Author: PapaReap 
*  Function names: pr_fnc_bloodCounter 
*  Called from pr_fn_mashTreatment.sqf 
*  Counts and tracks units of blood used 
*  ver 1.0 - 2016-02-07 system rework in progress 
* 
*  Arguments: 
*  0: <COUNT>      --- Count blood used 
*  1: <CONTAINER>  --- Container to count 
*/ 
if !(aceOn) exitWith {}; 
if !(isServer) exitWith {}; 

scopename "healScope"; 
_cnt = _this select 0; 
_container = _this select 1; 
if (isNil "bloodCounter") then { bloodCounter = true; publicVariable "bloodCounter"; _container setVariable ["bloodCount", 0, true] }; 

if !(_container == mash_man_tent) then { 
    if (isNull _container) then { breakto "healScope" }; 
    _count = _container getVariable "bloodCount"; 
    _count = _count + _cnt; 
    _container setVariable ["bloodCount", _count, true]; 
    if (_container getVariable "bloodCount" > 99) then { 
        _count = 0; // delete after testing 
        _container setVariable ["bloodCount", 0, true]; 
        _container setVariable ["removeBlood", 1, true]; 
    }; pr_count = _count; publicVariable "pr_count"; // delete after testing 
} else { 
    if (isNull mash_man_tent) then { breakto "healScope" }; 
    _count = _container getVariable "bloodCount"; 
    _count = _count + _cnt; 
    _container setVariable ["bloodCount", _count, true]; 
    if (_container getVariable "bloodCount" > 99) then { 
        _count = 0; 
        _container setVariable ["bloodCount", 0, true]; removeBloodBag = true; publicVariable "removeBloodBag"; 
    }; pr_count = _count; publicVariable "pr_count"; // delete after testing 
}; 
