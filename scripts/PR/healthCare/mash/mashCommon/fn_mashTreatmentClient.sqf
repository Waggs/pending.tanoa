/* 
*  Author: PapaReap 
*  Function names: pr_fnc_mashTreatmentClient 
*  Called upon from fn_fmBuild.sqf & fn_mashTentCreate.sqf 
*  Gives player hint while blood is given  
*  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress 
*  ver 1.1 - 2016-01-02 system rework in progress 
*  ver 1.2 - 2016-02-07 system rework in progress 
* 
*  Arguments: 
*  0: <OBJECT>  --- Object the script shall be attached to
*  1: <RADIUS>  --- Radius around the object in which units blood is restored 
*  2: <SLEEP>   --- Time to sleep in seconds 
*
*  Can also be used through objects init: 0 = [this, 4, 1] execVM "scripts\PR\HealthCare\mash\mashCommon\fn_mashTreatmentClient.sqf";
*  If spawned use: [[_fieldMash, 8, 1], "pr_fnc_mashTreatmentClient"] call BIS_fnc_MP; 
*/ 
if !(aceOn) exitWith {}; 
private ["_obj","_radius","_sleepTime"]; 

_obj = _this select 0; 
_radius = _this select 1; 
_sleepTime = _this select 2; 
_b = 0; 
_unit = player; 

_meds = ["MedicalSupplies"] call BIS_fnc_getParamValue; 

if (player == player) then { 
    while { true } do { 
        { 
            if ((alive _unit) && (_unit distance _obj < _radius + 30) && ((_meds == 0) /*&& (_obj == mash_man_tent)*/)) then { 
                if (_unit getVariable "aceBloodBag" == 1) then { 
                    _name = name player; _damage = '---'; _blood = '---'; 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "ace_medical_bloodVolume" < 50)) then {  //high blood loss
                        hintSilent parseText format ["%1<br/><t color='#ff5d00'>Healing in Progress....<br/>%2 BLOOD %2</t>", _name, _blood]; sleep 2; _b = 1; 
                    }; 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "ace_medical_bloodVolume" < 75) && (_unit getVariable "ace_medical_bloodVolume" > 50.01)) then {  //medium blood loss
                        hintSilent parseText format ["%1<br/><t color='#ffb600'>Healing in Progress....<br/>%2 BLOOD %2</t>", _name, _blood]; sleep 2; _b = 1; 
                    }; 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "ace_medical_bloodVolume" != 100) && (_unit getVariable "ace_medical_bloodVolume" > 75.01)) then {  //low blood loss
                        hintSilent parseText format ["%1<br/><t color='#ccff00'>Healing in Progress....<br/>%2 BLOOD %2</t>", _name, _blood]; sleep 2; _b = 1; 
                    }; 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "ace_medical_bloodVolume" == 100) && (_b == 1)) then {  //no blood loss
                        hint parseText format ["%1<br/><t color='#19e56e'>Bleeding Stopped</t>", _name]; sleep 2; _b = 0; 
                    }; 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "aceBloodBag" == 0) && (_unit getVariable "ace_medical_bloodVolume" != 100)) then { 
                        hintSilent parseText format ["%1<br/>Mash is out of Blood", name player]; sleep 0.5; 
                        hintSilent parseText format ["%1<br/>Mash is out of Blood", name player]; 
                    }; 
                    if ((_unit distance _obj > _radius) && (_b == 1)) then { 
                        hint parseText format ["%1<br/>You Left Before Being Fully Healed", name player]; sleep 0.5; 
                        hint parseText format ["%1<br/>You Left Before Being Fully Healed", name player]; _b = 0; 
                    }; 
                    sleep 1; 
                } else { 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "aceBloodBag" == 0) && (_unit getVariable "ace_medical_bloodVolume" != 100)) then { 
                        hintSilent parseText format ["%1<br/>Mash is out of Blood", name player]; sleep 0.5; 
                    }; 
                    sleep 1; 
                }; 
            } else {  
                if ((alive _unit) && (_unit distance _obj < _radius + 30)) then { 
                    _name   = name player; _damage = '---'; _blood  = '---'; 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "ace_medical_bloodVolume" < 50)) then {  //high blood loss
                        hintSilent parseText format ["%1<br/><t color='#ff5d00'>Healing in Progress....<br/>%2 BLOOD %2</t>", _name, _blood]; sleep 2; _b = 1; 
                    }; 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "ace_medical_bloodVolume" < 75) && (_unit getVariable "ace_medical_bloodVolume" > 50.01)) then {  //medium blood loss
                        hintSilent parseText format ["%1<br/><t color='#ffb600'>Healing in Progress....<br/>%2 BLOOD %2</t>", _name, _blood]; sleep 2; _b = 1; 
                    }; 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "ace_medical_bloodVolume" != 100) && (_unit getVariable "ace_medical_bloodVolume" > 75.01)) then {  //low blood loss
                        hintSilent parseText format ["%1<br/><t color='#ccff00'>Healing in Progress....<br/>%2 BLOOD %2</t>", _name, _blood]; sleep 2; _b = 1; 
                    }; 
                    if ((_unit distance _obj < _radius) && (_unit getVariable "ace_medical_bloodVolume" == 100) && (_b == 1)) then {  //no blood loss
                        hint parseText format ["%1<br/><t color='#19e56e'>Bleeding Stopped</t>", _name]; sleep 2; _b = 2; 
                    }; 
                    if ((_unit distance _obj > _radius) && (_b == 1)) then { 
                        hint parseText format ["%1<br/>You Left Before Being Fully Healed", name player]; sleep 0.5; 
                        hint parseText format ["%1<br/>You Left Before Being Fully Healed", name player]; _b = 0; 
                    }; 
                    sleep 1; 
                }; 
            }; 
        } forEach ((getPos _obj) nearEntities [["Man"], _radius]); 
        sleep _sleepTime; 
    }; 
}; 
