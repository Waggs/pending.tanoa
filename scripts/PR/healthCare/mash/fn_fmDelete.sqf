/* 
*  Author: PapaReap
*  Function names: pr_fnc_fmDelete
*  Called from fmDeleteCall.sqf"
*  Removes field mash station
*  ver 1.1 - 2016-02-07 system rework in progress
*  ver 1.0 - 2015-04-29 complete ACE medical system rework in progress
*
*  Arguments:
*  0: <DEPLOYER>  --- Player that deployed the Field Mash
*/

if !(aceOn) exitWith {};
private ["_deployer"];

_deployer = _this select 0;
_time     = mash_Array select 11;
_mash     = nearestObject [_deployer, "B_Slingload_01_Medevac_F"];
_mash     = _mash getVariable "mashID";
_medArray = _mash getVariable "medArray";
_deployer setVariable ["stowFieldMashAbort", 0, false]; //need to figure this one out
[_deployer, _mash] spawn fnc_stowTent;
sleep (_time + 1);

if (_deployer getVariable "stowFieldMashAbort" == 1) then {
    _mash setVariable ["fieldMash_deployed", "false", true];
    _mash setVariable ["fieldMash_reset", "true", true];

    _stowAction = _mash getVariable "stowAction";
    _deployer removeAction _stowAction;
    _deployer setVariable ["stowFieldMashAbort", 0, false];
    { if ( !isNull _x ) then {deleteVehicle _x} } forEach _medArray;
} else {
    if (_deployer getVariable "stowFieldMashAbort" == 2) then {
        _deployer setVariable ["stowFieldMashAbort", 0, false];
    };
};
