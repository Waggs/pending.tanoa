//--- Add the following to the init field of an ammo box in the editor
//--- _nil = this execVM "scripts\PR\loadouts\box\natoBox_ammoLg.sqf";
//--- When adding items, make sure they are in the correct sections (weapons/ammo/items..) 
//--- and make sure the last item in each group doesn't have a comma at the end of the line

//--- <0> Box or item to add items 

waitUntil { !(isNil "objectVarsCompiled") };

[_this, 


//*** WEAPONS ***// 
//--- <1> Add Nato Common weapons 
[ 
    //["Binocular",							 5 ], 
    //["RangeFinder",						 5 ], 
    //["LaserDesignator",					 5 ], 
    //["MineDetector",						 5 ], 
    //["launch_B_Titan_F",					 5 ], 
    //["launch_B_Titan_short_F",			 5 ], 
    //["launch_NLAW_F",						 5 ] 
], 

//--- <2> Add Nato ACE Weapons 
[], 

//--- <3> Add Nato Arma 3 Weapons 
[ 
    //["arifle_MX_SW_F",					 5 ], 
    //["hgun_Pistol_heavy_01_F",			 5 ], 
    //["arifle_MXC_F",						 5 ], 
    //["arifle_MX_F",						 5 ], 
    //["arifle_MX_GL_F",					 5 ], 
    //["arifle_MXM_F",						 5 ], 
    //["srifle_LRR_LRPS_F",					 5 ] 
], 

//--- <4> Add Nato Massi Weapons 
[ 
    //["hgun_ACPC2_F",						 5 ], 
    //["arifle_Mk20_GL_F",					 5 ] 
], 

//--- <5> Add Nato RHS Weapons 
[ 
    ["rhs_weap_M136",                        4 ] 
], 


//*** AMMO ***// 
//--- <6> Add Nato Common Ammo 
[ 
    ["LaserBatteries",                		10 ], 
[TiAT, 5 ],
[TiAA, 5 ],
    ["RPG32_F",                         	 5 ], 
    //["NLAW_F",                            10 ], 
    ["9Rnd_45ACP_Mag",                  	10 ], 
    ["1Rnd_HE_Grenade_shell",				20 ], 
    ["1Rnd_Smoke_Grenade_shell",			10 ], 
    //["1Rnd_SmokeRed_Grenade_shell",        5 ], 
    //["1Rnd_SmokeGreen_Grenade_shell",      5 ], 
    //["1Rnd_SmokeYellow_Grenade_shell",     5 ], 
    ["UGL_FlareWhite_F",					 5 ], 
    //["UGL_FlareGreen_F",                  15 ], 
    //["SmokeShellRed",                     20 ], 
    //["SmokeShellGreen",                   20 ], 
    //["SmokeShellYellow",                  20 ], 
    //["SmokeShellPurple",                  20 ], 
    //["SmokeShellBlue",                    20 ], 
    //["SmokeShellOrange",                  20 ], 
    //["Chemlight_green",                   20 ], 
    //["Chemlight_red",                     20 ], 
    //["UGL_FlareRed_F",                    15 ], 
    //["UGL_FlareYellow_F",                 15 ], 
    ["UGL_FlareCIR_F",                  	 5 ], 
    [grenade,                    		20 ], 
    ["ACE_M84", 							20 ], 
    ["SmokeShell",                     		20 ], 
[chemY, 10 ],
    //["Chemlight_blue",                    20 ], 
    ["B_IR_Grenade",                    	 5 ], 
    //["ATMine_Range_Mag",                  10 ], 
    //["APERSMine_Range_Mag",                5 ], 
    //["APERSBoundingMine_Range_Mag",        5 ], 
    //["SLAMDirectionalMine_Wire_Mag",       5 ], 
    ["APERSTripMine_Wire_Mag",           	 5 ], 
    //["SatchelCharge_Remote_Mag",           5 ], 
    ["DemoCharge_Remote_Mag",             	 5 ] 
    //["Titan_AP",                          10 ] 
], 

//--- <7> Add Nato ACE Ammo 
[], 

//--- <8> Add Nato Arma 3 Ammo 
[ 
    ["16Rnd_9x21_Mag",                		20 ], 
    ["100Rnd_65x39_caseless_mag_Tracer",	20 ], 
[mx556_30, 30 ],
    ["5Rnd_127x108_Mag",                 	20 ], 
    ["7Rnd_408_Mag",						20 ], 
    //["11Rnd_45ACP_Mag",                   20 ], 
    ["30Rnd_65x39_caseless_mag",			30 ], 
[mx65_30trace, 30 ]
], 

//--- <9> Add Nato Massi Ammo 
[ 
    ["mas_MAAWS",							 5 ], 
    ["13Rnd_mas_9x21_Mag",					10 ], 
    ["100Rnd_mas_762x51_Stanag",			10 ], 
    ["100Rnd_mas_762x51_T_Stanag",			10 ], 
    ["200Rnd_mas_556x45_T_Stanag",  		10 ], 
    ["30Rnd_mas_9x21_Stanag",  				20 ], 
    ["30Rnd_mas_556x45_Stanag",          	20 ], 
    ["30Rnd_mas_556x45_T_Stanag",   		20 ], 
[mx556_30, 20 ],
    ["30Rnd_556x45_Stanag_Tracer_Yellow",	20 ], 
    ["5Rnd_mas_127x99_Stanag",				10 ], 
    ["5Rnd_mas_127x99_dem_Stanag",			10 ], 
    ["5Rnd_mas_127x99_T_Stanag",			10 ] 
], 

//--- <10> Add Nato RHS Ammo 
[], 


//*** ITEMS ***// 
//--- <11> Add Nato Common Items 
[ 
    //["optic_SOS",          				10 ], 
    //["optic_Hamr",         				10 ], 
    //["acc_flashlight",     				10 ], 
//[irPoint, 10 ],
    ["FirstAidKit",       					10 ], 
    //["NVGoggles",          				10 ], 
    //["MediKit",							 2 ], 
    //["ToolKit",							 2 ], 
    //["ItemRadio",							 5 ], 
    ["B_UavTerminal",						 5 ] 
], 

//--- <12> Add Nato ACE Items 
[], 

//--- <13> Add Nato Arma 3 Items 
[ 
    //["muzzle_snds_acp",					10 ], 
    //["muzzle_snds_M",						10 ] 
], 

//--- <14> Add Nato Massi Items 
[ 
    //["muzzle_mas_snds_Mc",				10 ], 
    //["muzzle_mas_snds_L",					10 ], 
    //["NVGoggles_mas_mask3",				 5 ] 
], 

//--- <15> Add Nato RHS Items 
[], 


//***BACKPACKS***// 
//--- <16> Add Nato Common Backpacks 
[ 
    ["B_UAV_01_backpack_F",					 2 ], 
    ["B_Carryall_mcamo",					 2 ], 
    ["tf_rt1523g",							 2 ] 
], 

//--- <17> Add Nato ACE Backpacks 
[], 

//--- <18> Add Nato Arma 3 Backpacks 
[], 

//--- <19> Add Nato Massi Backpacks 
[], 

//--- <20> Add Nato RHS Backpacks 
[], 

//*** RADIOS ***// 
//--- <21> How many backup short range radios? 
4 
] execVM "scripts\PR\loadouts\box\box_fill.sqf"; 
