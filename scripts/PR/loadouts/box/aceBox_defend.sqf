/* aceBox_defend.sqf
*  Add the following to the init field of an ammo box in the editor:  _nil = this execVM "scripts\PR\loadouts\box\aceBox_defend.sqf";
*  When adding items, make sure they are in the correct sections (weapons/ammo/items..), and make sure the last item in each group doesn't have a comma at the end of the line
*  If spawned box use - [[namebox], "fnc_aceDefend"] call BIS_fnc_MP;
*/

waitUntil { !(isNil "objectVarsCompiled") };

//--- <0> Box or item to add items
[_this,

//*** WEAPONS ***//
//--- <1> Add Nato Common weapons
[],

//--- <2> Add Nato ACE Weapons
[],

//--- <3> Add Nato Arma 3 Weapons
[],

//--- <4> Add Nato Massi Weapons
[],

//--- <5> Add Nato RHS Weapons
[],


//*** AMMO ***//
//--- <6> Add Nato Common Ammo
[
[grenade, 5 ],
[smoke, 5 ],
[chemY, 5 ]
],

//--- <7> Add Nato ACE Ammo

[
[aceHandFlare, 10 ],
[aceFlashBang, 5 ]
],

//--- <8> Add Nato Arma 3 Ammo
[],

//--- <9> Add Nato Massi Ammo
[],

//--- <10> Add RHS Ammo
[],

//*** ITEMS ***//
//--- <11> Add Nato Common Items
[],

//--- <12> Add Nato ACE Items
[
[aceSandBag, 30 ],
[aceWireCutter, 1 ]
],

//--- <13> Add Nato Arma 3 Items
[],

//--- <14> Add Nato Massi Items
[],

//--- <15> Add Nato RHS Items
[],

//***BACKPACKS***//
//--- <16> Add Nato Common Backpacks
[
[nqdySearchlight, 4 ]
],

//--- <17> Add Nato ACE Backpacks
[],

//--- <18> Add Nato Arma 3 Backpacks
[
["B_HMG_01_support_high_F", 2 ],
["B_HMG_01_high_weapon_F", 1 ],
["B_GMG_01_high_weapon_F", 1 ]
],

//--- <19> Add Nato Massi Backpacks
[
["B_HMG_01_support_high_F", 2 ],
["B_HMG_01_high_weapon_F", 1 ],
["B_GMG_01_high_weapon_F", 1 ]
],

//--- <20> Add Nato RHS Backpacks
[
[rhsM2TriBag, 1 ],
[rhsM2GunBag, 1 ],
[rhsMk19TriBag, 1 ],
[rhsMk19GunBag, 1 ]
],

//*** RADIOS ***//
//--- <21> How many backup short range radios?
0
] execVM "scripts\PR\loadouts\box\box_fill.sqf";
