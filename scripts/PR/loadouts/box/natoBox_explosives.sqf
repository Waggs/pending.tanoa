//--- natoBox_explosives.sqf
//--- Add the following to the init field of an ammo box in the editor
//--- _nil = this execVM "scripts\PR\loadouts\box\natoBox_explosives.sqf";
//--- When adding items, make sure they are in the correct sections (weapons/ammo/items..) 
//--- and make sure the last item in each group doesn't have a comma at the end of the line

[_this,
//--- <1> Add Nato Common weapons
[],
//--- <2> Add Nato Arma 3 Weapons
[], 
//--- <3> Add Nato Massi Weapons
[],
//--- <4> Add Nato Common Ammo
[
[grenade, 15 ],
[aceFlashBang, 15 ],
	["ATMine_Range_Mag",					15 ],
	["APERSMine_Range_Mag",					15 ],
	["APERSBoundingMine_Range_Mag",			15 ],
	["SLAMDirectionalMine_Wire_Mag",		15 ],
	["APERSTripMine_Wire_Mag",				15 ],
	["SatchelCharge_Remote_Mag",			10 ],
	["DemoCharge_Remote_Mag",				15 ]
],
//--- <5> Add Nato Arma 3 Ammo
[],
//--- <6> Add Nato Massi Ammo
[],
//--- <7> Add Nato Common Items
[],
//--- <8> Add Nato Arma 3 Items
[],
//--- <9> Add Nato Massi Items
[],
//--- <10> Add Nato Common Backpacks
[],
//--- <11> Add Nato Arma 3 Backpacks
[],
//--- <12> Add Nato Massi Backpacks
[],
//--- <13> How many backup short range radios?
0
]
execVM "scripts\PR\loadouts\box\box_fill.sqf";
