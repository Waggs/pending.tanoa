//--- box_fill.sqf
if (!isServer) exitWith{};
waitUntil { time > 0 };
private ["_box","_weaponsCommon","_weaponsACE","_weaponsA3","_weaponsMas","_weaponsRHS","_magazinesCommon","_magazinesACE","_magazinesA3","_magazinesMas","_magazinesRHS","_itemsCommon","_itemsACE","_itemsA3","_itemsMas","_itemsRHS","_rucksCommon","_rucksACE","_rucksA3","_rucksMas","_rucksRHS","_addRadios","_count"];

_box = _this select 0;  //--- <0> Box or item to add items

//*** WEAPONS ***//
_weaponsCommon = _this select 1;                                                   //--- <1> Add Nato Common weapons
if (aceOn) then { _weaponsACE = _this select 2; } else { _weaponsACE = []; };      //--- <2> Add Nato ACE Weapons
_weaponsA3 = _this select 3;                                                       //--- <3> Add Nato Arma 3 Weapons
if (masOn) then { _weaponsMas = _this select 4; } else { _weaponsMas = []; };      //--- <4> Add Nato Massi Weapons
if (rhsOn) then { _weaponsRHS = _this select 5; } else { _weaponsRHS = []; };      //--- <5> Add Nato RHS Weapons

//*** AMMO ***//
_magazinesCommon = _this select 6;                                                 //--- <6> Add Nato Common Ammo
if (aceOn) then { _magazinesACE = _this select 7; } else { _magazinesACE = []; };  //--- <7> Add Nato ACE Ammo
_magazinesA3 = _this select 8;                                                     //--- <8> Add Nato Arma 3 Ammo
if (masOn) then { _magazinesMas = _this select 9; } else { _magazinesMas = []; };  //--- <9> Add Nato Massi Ammo
if (rhsOn) then { _magazinesRHS = _this select 10; } else { _magazinesRHS = []; }; //--- <10> Add Nato RHS Ammo

//*** ITEMS ***//
_itemsCommon = _this select 11;                                                    //--- <11> Add Nato Common Items
if (aceOn) then { _itemsACE = _this select 12; } else { _itemsACE = []; };         //--- <12> Add Nato ACE Items
_itemsA3 = _this select 13;                                                        //--- <13> Add Nato Arma 3 Items
if (masOn) then { _itemsMas = _this select 14; } else { _itemsMas = []; };         //--- <14> Add Nato Massi Items
if (rhsOn) then { _itemsRHS = _this select 15; } else { _itemsRHS = []; };         //--- <15> Add Nato RHS Items

//*** BACKPACKS ***//
_rucksCommon = _this select 16;                                                    //--- <16> Add Nato Common Backpacks
if (aceOn) then { _rucksACE = _this select 17; } else { _rucksACE = []; };         //--- <17> Add Nato ACE Backpacks
_rucksA3 = _this select 18;                                                        //--- <18> Add Nato Arma 3 Backpacks
if (masOn) then { _rucksMas = _this select 19; } else { _rucksMas = []; };         //--- <19> Add Nato Massi Backpacks
if (rhsOn) then { _rucksRHS = _this select 20; } else { _rucksRHS = []; };         //--- <20> Add Nato RHS Backpacks

//*** RADIOS ***//
_addRadios = _this select 21;                                                      //--- <21> How many backup short range radios?

clearMagazineCargoGlobal _box;
clearWeaponCargoGlobal _box;
clearItemCargoGlobal _box;
clearBackpackCargoGlobal _box;

sleep 2;

//*** WEAPONS ***//
_count = 0; { _box addWeaponCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _weaponsCommon;               //--- <1> Add Nato Common weapons
_count = 0; { _box addWeaponCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _weaponsACE;                  //--- <2> Add Nato ACE Weapons
if ((defaultWeapon) || (tropicWeapon)) then {
    _count = 0; { _box addWeaponCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _weaponsA3;               //--- <3> Add Nato Arma 3 Weapons
} else {
    if (masWeapon) then {
        _count = 0; { _box addWeaponCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _weaponsMas;          //--- <4> Add Nato Massi Weapons
    } else {
        if (rhsWeapon) then {
            _count = 0; { _box addWeaponCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _weaponsRHS;      //--- <5> Add Nato RHS Weapons
        };
    };
};

//*** AMMO ***//
_count = 0; { _box addMagazineCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _magazinesCommon;           //--- <6> Add Nato Common Ammo
_count = 0; { _box addMagazineCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _magazinesACE;              //--- <7> Add Nato ACE Ammo
if ((defaultWeapon) || (tropicWeapon)) then {
    _count = 0; { _box addMagazineCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _magazinesA3;           //--- <8> Add Nato Arma 3 Ammo
} else {
    if (masWeapon) then {
        _count = 0; { _box addMagazineCargoGlobal [_x select 0, _x select 1]; _count = _count+ 1; } forEach _magazinesMas;       //--- <9> Add Nato Massi Ammo
    } else {
        if (rhsWeapon) then {
            _count = 0; { _box addMagazineCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _magazinesRHS;  //--- <10> Add Nato RHS Ammo
        };
    };
};

//*** ITEMS ***//
_count = 0; { _box addItemCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _itemsCommon;                   //--- <11> Add Nato Common Items
_count = 0; { _box addItemCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _itemsACE;                      //--- <12> Add Nato ACE Items
if ((defaultWeapon) || (tropicWeapon)) then {
    _count = 0; { _box addItemCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _itemsA3;                   //--- <13> Add Nato Arma 3 Items
} else {
    if (masWeapon) then {
        _count = 0; { _box addItemCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _itemsMas;              //--- <14> Add Nato Massi Items
    } else {
        if (rhsWeapon) then {
            _count = 0; { _box addItemCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _itemsRHS;          //--- <15> Add Nato RHS Items
        };
    };
};

//***BACKPACKS***//
_count = 0; { _box addBackpackCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _rucksCommon;              //--- <16> Add Nato Common Backpacks
_count = 0; { _box addBackpackCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _rucksACE;                 //--- <17> Add Nato ACE Backpacks
if ((defaultWeapon) || (tropicWeapon)) then {
    _count = 0; { _box addBackpackCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _rucksA3;              //--- <18> Add Nato Arma 3 Backpacks
} else {
    if (masWeapon) then {
        _count = 0; { _box addBackpackCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _rucksMas;         //--- <19> Add Nato Massi Backpacks
    } else {
        if (rhsWeapon) then {
            _count = 0; { _box addBackpackCargoGlobal [_x select 0, _x select 1]; _count = _count + 1; } forEach _rucksRHS;     //--- <20> Add Nato RHS Backpacks
        };
    };
};

//*** RADIOS ***//
//--- <21> How many backup short range radios?
if (_addRadios > 0) then {
    if (isNil "BOX_FILL_RADIO_ID") then { BOX_FILL_RADIO_ID = 999; publicVariable "BOX_FILL_RADIO_ID"; };
    for "_x" from 1 to _addRadios do {
        _box call compile format ["_this addItemCargo ['tf_anprc152_%1', 1]", BOX_FILL_RADIO_ID];
        BOX_FILL_RADIO_ID = BOX_FILL_RADIO_ID - 1; publicVariable "BOX_FILL_RADIO_ID";
    };
};
