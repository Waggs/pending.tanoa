/*  uniforms.sqf
    Add the following to the init field of an ammo box in the editor:  nil = this execVM "scripts\PR\loadouts\box\uniforms.sqf";
    When adding items, make sure they are in the correct sections (weapons/ammo/items..) and make sure the last item in each group doesn't have a comma at the end of the line
    If spawned box use:  [[namebox], "fnc_uniforms"] call BIS_fnc_MP; 
*/ 

//--- <0> Box or item to add items 
[_this, 

//*** WEAPONS ***// 
//--- <1> Add Common weapons 
[], 

//--- <2> Add ACE Weapons 
[], 

//--- <3> Add Arma 3 Weapons 
[], 

//--- <4> Add Massi Weapons 
[], 

//--- <5> Add RHS Weapons 
[], 


//*** AMMO ***// 
//--- <6> Add Common Ammo *** need russian 
[], 

//--- <7> Add ACE Ammo 
[], 

//--- <8> Add Arma 3 Ammo *** need russian 
[], 

//--- <9> Add Massi Ammo *** need russian 
[], 

//--- <10> Add RHS Ammo 
[], 

//*** ITEMS ***// 
//--- <11> Add Common Items 
[
["nqdy_leader_uniform",       10 ]
], 

//--- <12> Add ACE Items 
[], 

//--- <13> Add Arma 3 Items 
[], 

//--- <14> Add Massi Items 
[], 

//--- <15> Add RHS Items 
[], 

//***BACKPACKS***// 
//--- <16> Add Common Backpacks 
[], 

//--- <17> Add ACE Backpacks 
[], 

//--- <18> Add Arma 3 Backpacks 
[], 

//--- <19> Add Massi Backpacks 
[], 

//--- <20> Add RHS Backpacks 
[], 

//*** RADIOS ***// 
//--- <21> How many backup short range radios? 
0 
] execVM "scripts\PR\loadouts\box\box_fill.sqf"; 