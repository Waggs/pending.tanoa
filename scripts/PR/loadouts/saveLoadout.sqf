pr_fnc_hideMe = {
    _oldUnit = _this select 0;
    if (isServer) then {
        _oldUnit hideObjectGlobal true;
    };
};

while { true } do {
    waitUntil { !alive player };

    _oldUnit = player;

    _items = assignedItems player;
    _headgear = headgear player;

    _uniform = uniform player;
    _unitormContainer = uniformContainer player;
    _uniformItems = getItemCargo _unitormContainer;
    _magazinesUniform = getMagazineCargo _unitormContainer;

    _vest = vest player;
    _vestContainer = vestContainer player;
    _itemsVest = getItemCargo _vestContainer;
    _magazinesVest = getMagazineCargo _vestContainer;

    _backpack = backpack player;
    _backPackContainer = backpackcontainer player;
    _itemsBackpack = getItemCargo _backPackContainer;
    _magazinesBackpack = getMagazineCargo _backPackContainer;

    //--- get current weapons, loaded mags and accessories
    _primaryWeaponMagazine = primaryWeaponMagazine player;
    _primaryWeapon = primaryWeapon player;
    _primaryWeaponItems = primaryWeaponItems player;
    _handgunMagazine = handgunMagazine player;
    _handgunWeapon = handgunWeapon player;
    _handgunItems = handgunItems player;
    _secondaryWeaponMagazine = secondaryWeaponMagazine player;
    _secondaryWeapon = secondaryWeapon player;
    _secondaryWeaponItems = secondaryWeaponItems player;

    //--- get current ammo count
    _countPrimaryAmmo = player ammo (primaryweapon player);
    _countHandgunAmmo = player ammo (handgunWeapon player);

    [_oldUnit] call fn_remove;
    [[_oldUnit],"pr_fnc_hideMe",true,false] call BIS_fnc_MP;

    waitUntil { alive player };

    deleteVehicle _oldUnit;
    _unit = player;
    removeAllAssignedItems _unit;
    removeAllContainers _unit;
    removeAllWeapons _unit;
    removeBackpack _unit;
    removeHeadgear _unit;
    removeUniform _unit;
    removeGoggles _unit;
    removeVest _unit;

    _unit addHeadgear _headgear;
    _unit addUniform _uniform;
    _unit addVest _vest;
    _unit addBackpack _backpack;

    //--- add weapons back to unit
    if (count _primaryWeaponMagazine > 0) then {
        _count = count _primaryWeaponMagazine - 1;
        for "_i" from 0 to _count do {
            _unit addMagazine (_primaryWeaponMagazine select _i);
        };
    };
    _unit addWeapon _primaryWeapon;
    { _unit addPrimaryWeaponItem _x } forEach _primaryWeaponItems;

    if (count _handgunMagazine > 0) then {
        _count = count _handgunMagazine - 1;
        for "_i" from 0 to _count do {
            _unit addMagazine (_handgunMagazine select _i);
        };
    };
    _unit addWeapon _handgunWeapon;
    { _unit addHandgunItem _x } forEach _handgunItems;

    if (count _secondaryWeaponMagazine > 0) then {
        _count = count _secondaryWeaponMagazine - 1;
        for "_i" from 0 to _count do {
            _unit addMagazine (_secondaryWeaponMagazine select _i);
        };
    };
    _unit addWeapon _secondaryWeapon;
    { _unit addSecondaryWeaponItem _x } forEach _secondaryWeaponItems;

    _unitormContainer = uniformContainer player;
    _vestContainer = vestContainer player;
    _backPackContainer = backpackcontainer player;

    //--- add personal items back to unit
    if (count _items > 0) then {
        _count = count _items - 1;
        for "_i" from 0 to _count do {
            _unit addWeapon (_items select _i);
        };
    };

    //--- add items and magazines back to uniform
    _count = count (_uniformItems select 0) - 1;
    for "_i" from 0 to _count do {
        _unitormContainer addItemCargoGlobal [(_uniformItems select 0) select _i, (_uniformItems select 1) select _i];
    };
    _count = count (_magazinesUniform select 0) - 1;
    for "_i" from 0 to _count do {
        _unitormContainer addMagazineCargoGlobal [(_magazinesUniform select 0) select _i, (_magazinesUniform select 1) select _i];
    };

    //--- add items and magazines back to vest
    _count = count (_itemsVest select 0) - 1;
    for "_i" from 0 to _count do {
        _vestContainer addItemCargoGlobal [(_itemsVest select 0) select _i, (_itemsVest select 1) select _i];
    };
    _count = count (_magazinesVest select 0) - 1;
    for "_i" from 0 to _count do {
        _vestContainer addMagazineCargoGlobal [(_magazinesVest select 0) select _i, (_magazinesVest select 1) select _i];
    };

    reload _unit;

    //--- add items and magazines back to backpack
    _count = count (_itemsBackpack select 0) - 1;
    for "_i" from 0 to _count do {
        _backPackContainer addItemCargoGlobal [(_itemsBackpack select 0) select _i, (_itemsBackpack select 1) select _i];
    };
    _count = count (_magazinesBackpack select 0) - 1;
    for "_i" from 0 to _count do {
        _backPackContainer addMagazineCargoGlobal [(_magazinesBackpack select 0) select _i, (_magazinesBackpack select 1) select _i];
    };

    //--- set ammo count back to weapon
    _unit setAmmo [primaryWeapon player, _countPrimaryAmmo];
    _unit setAmmo [handgunWeapon player, _countHandgunAmmo];
};
