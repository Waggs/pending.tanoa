
defaultWeapon = false; tropicWeapon = false; masWeapon = false; masWeapon5 = false; masWeapon6 = false; masWeapon7 = false; masWeapon8 = false; rhsWeapon = false; closeQuarters = false; mediumAssault = false;
_rifleType = ["RifleType", 0] call BIS_fnc_getParamValue;
if (_rifleType == 1) then { mediumAssault = true; } else { if (_rifleType == 2) then { closeQuarters = true; }; };
    
    
if (
    (prWeapons <= 0)
    || (((prWeapons == 5) || (prWeapons == 6) || (prWeapons == 7) || (prWeapons == 8)) && !(masOn))
    || ((prWeapons == 10) && !(rhsOn))
    || (((prWeapons == 15) || (prWeapons == 16) || (prWeapons == 17) || (prWeapons == 18) || (prWeapons == 19)) && !(unsungOn))
) then {
    defaultWeapon = true;
} else {
    if (prWeapons == 1) then {
        tropicWeapon = true;
    } else {
        if (((prWeapons == 5) || (prWeapons == 6) || (prWeapons == 7) || (prWeapons == 8)) && (masOn)) then {
            masWeapon = true;
            if (prWeapons == 5) then {
                masWeapon5 = true;
            } else {
                if (prWeapons == 6) then {
                    masWeapon6 = true;
                } else {
                    if (prWeapons == 7) then {
                        masWeapon7 = true;
                    } else {
                        if (prWeapons == 8) then {
                            masWeapon8 = true;
                        };
                    };
                };
            };
        } else {
            if ((prWeapons == 10) && (rhsOn)) then {
                rhsWeapon = true;
            };
        };
    };
};

// temp fix until loadouts finished, remove once weapons added
if !(defaultWeapon) then {
    if ((prWeapons == 15) || (prWeapons == 16) || (prWeapons == 17) || (prWeapons == 18) || (prWeapons == 19)) then {
        if ((unsungOn) && (rhsOn)) then { 
            rhsWeapon = true;
        } else {
            defaultWeapon = true;
        };
    };
};

playWest = false; playEast = false; playRes  = false;
if (side player == west) then { playWest = true; } else { if (side player == east) then { playEast = true; } else { if (side player == resistance) then { playRes = true; }; }; };

//--- define objects

//--- DEFAULT ARMA
//--- items
irPoint = "acc_pointer_IR";
fak = "FirstAidKit";

//--- ammo
gl1HE = "1Rnd_HE_Grenade_shell";
gl1Smoke = "1Rnd_Smoke_Grenade_shell";
gl1SmokeG = "1Rnd_SmokeGreen_Grenade_shell";
glFlare = "UGL_FlareWhite_F";

mx65_30trace = "30Rnd_65x39_caseless_mag_Tracer";
mx65_100trace = "100Rnd_65x39_caseless_mag_Tracer";
sdar20 = "20Rnd_556x45_UW_mag";
m556_30 = "30Rnd_556x45_Stanag";
m556_30t_r = "30Rnd_556x45_Stanag_Tracer_Red";
sdar30G = "30Rnd_556x45_Stanag_green";
snip338_10r = "10Rnd_338_Mag";
lmg338_130r = "130Rnd_338_Mag";
mark762_20r = "20Rnd_762x51_Mag";
lmg_556_150r = objNull;
spar556_30t_r = objNull;
cq9m_30r = objNull;

if (closeQuarters) then {
    cq9m_30r = "30Rnd_9x21_Mag_SMG_02";
};

if (mediumAssault) then {
    spar556_30t_r = m556_30t_r;
    lmg_556_150r = "150Rnd_556x45_Drum_Mag_F";
    mx65_30trace = objNull;
    mx65_100trace = objNull;
};
//--- throw
chemY = "Chemlight_yellow";
grenade = "HandGrenade";
smoke = "SmokeShell";

//--- launcher / missles
nlaw = "NLAW_F";
rpg = "RPG32_F";
TiAA = "Titan_AA";
TiAT = "Titan_AT";


//--- ACE
aceBandage = objNull;
aceBlood1000 = objNull;
aceEpi = objNull;
aceFlashBang = objNull;
aceHandFlare = objNull;
aceMorphine = objNull;
aceSandBag = objNull;
aceWireCutter = objNull;
//--- RHS
rhsM2TriBag = objNull;
rhsM2GunBag = objNull;
rhsMk19TriBag = objNull;
rhsMk19GunBag = objNull;
nqdySearchlight = objNull;

if (aceOn) then {
    aceBandage = "ACE_fieldDressing";
    aceBlood1000 = "ACE_bloodIV";
    aceEpi = "ACE_epinephrine";
    aceFlashBang = "ACE_M84";
    aceHandFlare = "ACE_HandFlare_White";
    aceMorphine = "ACE_morphine";
    aceSandBag = "ACE_Sandbag_empty";
    aceWireCutter = "ACE_wirecutter";
};

if (rhsOn) then {
    rhsM2TriBag = "RHS_M2_Tripod_Bag";
    rhsM2GunBag = "RHS_M2_Gun_Bag";
    rhsMk19TriBag = "RHS_Mk19_Tripod_Bag";
    rhsMk19GunBag = "RHS_Mk19_Gun_Bag";
};

if (nqdyOn) then {
    nqdySearchlight = "B_Bag_Searchlight";
};

objectVarsCompiled = true;