Version 2.4 

2.4: 4/19/14
  Make Zeus compatible & put in default values for all getVariable calls to occasional fix errors

2.3: 4/10/14
  Added SpawnOnly option for use with FhMz and Upsmon
  
2.2: 3/31/14
  Updated so fillhouse can be called with civilians.  Updated militarize list of civilians.

2.1: 2/5/14
  Fix to HeliParadrop.sqf - bug where sometimes the unit's never jumpped. 
        fix was to delay slightly after creating unit before moving to under helo.
        
2.0: 2/4/14
  Fix in simple cache of vehicles in militarize where they never cached away if spwaned without any infantry.

1.1: 1/14/14:
  Issue was that unit would stop spawning near the end of the missions.

  Found that BIS function to create groups can only have 144 per side.  
  LV_fnc_simpleCache.sqf was deleting all the units from the groups, but didn't delete the groups.
  Fix was to add the following after the group was deleted:

       } forEach units _grp;
  >>>  deleteGroup _grp;                                                   <<<
       call compile format["LVgroup%1spawned = nil;", (_ids select _i)];

1.0; 12/27/13:
  Fix was applied to take care of the caching script spawning way too many enemies.

  The fix involved making 2 groups when militarize is called.  One group for men, and one for vehicles.

  The only part of the change that will effect users is that the militarize function will now use the LVgroup$ (where $ is the supplied id) is the group for men.  Vehicles will be in a group called LVgroupV$ (where $ is the last argument supplied to the militarize script as usual).  This variable is not often used so it may not effect your usage at all.
  
  Also included is a fix for manned UAV's entering into a cached area.  The UAV was not triggering the script to re-stock the area so a UAV would find the area empty, only to have it fill up when a real unit enters the area.  Manned UAVs are now seen the same as playable units.
