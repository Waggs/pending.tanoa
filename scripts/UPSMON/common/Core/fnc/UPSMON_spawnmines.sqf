/**************************************************************** 
File: UPSMON_spawnmines.sqf 
Author: Azroul13 
Description: 
Parameter(s): 
Returns: 
****************************************************************/

private ["_minesNbr","_minetype1","_minetype2","_positiontoambush","_minetype","_mineposition","_max","_min","_ang","_dir","_orgX","_orgY","_posX","_posY","_mineposition","_Mine"]; 

_minesNbr = _this select 0; // if mine number is array
_minetype1 = _this select 1; 
_minetype2 = _this select 2; 
_positiontoambush = _this select 3; 
_diramb = _this select 4; 
_side = _this select 5; 

// PapaReap - added mines array to set at and ap mines, also allow random mine placements 
_mineArray = false; 
_mineRandom = false; 
_mines = 0; 
_minesAT = 0; 
_minesAP = 0; 

if (count _this > 6) then { 
    _UCthis = _this select 6; 
    if ("MINE:" in _UCthis) then { 
        _mines = ["MINE:", 0, _UCthis] call UPSMON_getArg; pr_mines = _mines; 
        if (typeName _mines == "ARRAY") then { 
            _mineArray = true; 
            _minesAT = _mines select 0; 
            _minesAP = _mines select 1; 
            _minesNbr = _minesAT + _minesAP; 
            if (count _mines > 2) then { _mineRandom = _mines select 2; }; 
        }; 
    }; 
}; 

_mineTypeArray = []; 
_countAT = _minesAT; 

for [{ _i = 0 }, { _i < _minesNbr }, { _i = _i + 1 }] do { 
    if (_mineRandom) then { 
        _minetype1 = UPSMON_Minestype1 call BIS_fnc_selectRandom; 
        _minetype2 = UPSMON_Minestype2 call BIS_fnc_selectRandom; 
    }; 
    _minetype = _minetype1; 
    _mineposition = _positiontoambush; 

    if (_mineArray) then { 
        if (_minesAT > 1) then { 
            if ((_i > 0) && (_i < _minesAT)) then { 
                _minetype = _minetype1; // setting more at mines 
                _max = 0; _min = 0; 
                if (floor (random 4) < 2) then { 
                    _min = _diramb + 270; 
                    _max = _diramb + 335; 
                } else { 
                    _min = _diramb + 25; 
                    _max = _diramb + 90; 
                }; 
                _ang = _max - _min; 
                if (_ang < 0) then { _ang = _ang + 360 }; 
                if (_ang > 360) then { _ang = _ang - 360 }; 
                _dir = (_min + random _ang); 
                _orgX = _positiontoambush select 0; 
                _orgY = _positiontoambush select 1; 
                if ((_minesAT > 4) && (_countAT > 4)) then { 
                    if (_countAT > 8) then { 
                        _countAT = _countAT - 1; 
                        _posX = _orgX + (((random 14) + (random 14)) max 7 * sin _dir); 
                        _posY = _orgY + (((random 14) + (random 14)) max 7 * cos _dir); 
                    } else { 
                        if ((_countAT > 4) && (_countAT < 9)) then { 
                            _countAT = _countAT - 1; 
                            _posX = _orgX + (((random 7) + (random 7)) max 3.5 * sin _dir); 
                            _posY = _orgY + (((random 7) + (random 7)) max 3.5 * cos _dir); 
                        }; 
                    }; 
                } else { 
                    _posX = _orgX + (((random 3.5) + (random 3.5)) * sin _dir); 
                    _posY = _orgY + (((random 3.5) + (random 3.5)) * cos _dir); 
                }; 
                _mineposition = [_posX, _posY, 0]; 
            }; 
        }; 

        if (_minesAP > 0) then { 
            if (_i > _minesAT - 1) then { 
                _minetype = _minetype2; 
                _max = 0; _min = 0; 
                if (floor (random 4) < 2) then { 
                    _min = _diramb + 270; 
                    _max = _diramb + 335; 
                } else { 
                    _min = _diramb + 25; 
                    _max = _diramb + 90; 
                }; 
                _ang = _max - _min; 
                if (_ang < 0) then { _ang = _ang + 360 }; 
                if (_ang > 360) then { _ang = _ang - 360 }; 
                _dir = (_min + random _ang); 
                _orgX = _positiontoambush select 0; 
                _orgY = _positiontoambush select 1; 
                _posX = _orgX + (((random 10) + (random 30 +5)) * sin _dir); 
                _posY = _orgY + (((random 10) + (random 30 +5)) * cos _dir); 
                _mineposition = [_posX, _posY, 0]; 
            }; 
        }; 
    } else {  // end PapaReap script 
        if (_i > 0) then { 
            _minetype = _minetype2; 
            // Many thanks Shuko ... 
            _max = 0; _min = 0; 
            if (floor (random 4) < 2) then { 
                _min = _diramb + 270; 
                _max = _diramb + 335; 
            } else { 
                _min = _diramb + 25; 
                _max = _diramb + 90; 
            }; 
            _ang = _max - _min; 
            // Min bigger than max, can happen with directions around north 
            if (_ang < 0) then { _ang = _ang + 360 }; 
            if (_ang > 360) then { _ang = _ang - 360 }; 
            _dir = (_min + random _ang); 
            _orgX = _positiontoambush select 0; 
            _orgY = _positiontoambush select 1; 
            _posX = _orgX + (((random 10) + (random 30 +5)) * sin _dir); 
            _posY = _orgY + (((random 10) + (random 30 +5)) * cos _dir); 
            _mineposition = [_posX,_posY,0]; 
        }; 
    }; 

    _mineTypeArray = _mineTypeArray + [_minetype]; pr_mineTypeArray = _mineTypeArray; 

    _Mine = createMine [_minetype, _mineposition, [], 0]; 
    _side revealMine _Mine; 

    if (UPSMON_Debug > 0) then { 
        [_mineposition, "Sign_Arrow_Large_GREEN_F"] spawn UPSMON_createsign; 
        [_mineposition, "Icon", "Minefield", "Colorred"] spawn UPSMON_createmarker; 
    }; 
    sleep 0.01; 
}; 
