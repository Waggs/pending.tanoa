
_thisPlayer = _this select 0;

if !(aceOn) then {
    if (["PersistantTasks",0] call BIS_fnc_getParamValue == 1) then {
        if ((isDedicated) && (isClass (configfile >> "CfgPatches" >> "SRU_PDB"))) then {
            [_thisPlayer] execVM "\sru_pdb\functions\fn_Server_getUnit.sqf";
        };
    };
};
